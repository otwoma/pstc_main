<?php
include "../_functions.php";
include "../_variables.php";
//include "../packages/classes/PHPExcel.php";
require_once dirname(__FILE__) . '/../packages/Classes/PHPExcel.php';


$perm=mysql_real_escape_string($_REQUEST['p']);

$perm_label = read_registry_staff_permission_label($perm);

$defa_excel_expo_format='xlsx';
$expot_cateogo=$perm_label;
$exofile_name='Report'.' '.$perm_label.' Status - As at '.date("d-M-Y Hi").'Hrs';

$recruit_total_count = recruits_count();

$cleared_recruits = get_cleared_recruits($perm);

$doc_subject=$exofile_name;
$doc_description=$exofile_name;
$keywords=$exofile_name;
$doc_category=$expot_cateogo;
$doc_title=$exofile_name;
$last_mod_author=$software_label;
$doc_creator=$software_label.'>'.$school_name;

$objPHPExcel = new PHPExcel();

$objPHPExcel->getProperties()->setCreator($doc_creator)
    ->setLastModifiedBy($last_mod_author)
    ->setTitle($doc_title)
    ->setSubject($doc_subject)
    ->setDescription($doc_description)
    ->setKeywords($keywords)
    ->setCategory($doc_category);

$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getDefaultStyle()->getFont()
    ->setName('Segoe UI Symbol')
    ->setSize(11);

$bold= array(
    'font'  => array(
        'bold'  => true,
        'size'  => 11,
    ));

$white_text = array(
    'font'  => array(
        'color' => array('rgb' => 'FFFFFF'),
    ));

$report_title = $perm_label.' Status';
$as_at =  'As at '.date("d-M-Y H:iA");

$report_main_default_title = $excel_title;

$sub_title = 'Cleared Recruits';
$objPHPExcel->getActiveSheet()->mergeCells('A1:C1');
$objPHPExcel->getActiveSheet()->setCellValue('A1',$report_main_default_title);
$objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($bold);

$objPHPExcel->getActiveSheet()->getStyle('A1')
    ->getAlignment()->setWrapText(true);

$objPHPExcel->getActiveSheet()->mergeCells('A2:C2');
$objPHPExcel->getActiveSheet()->setCellValue('A2',$report_title);
$objPHPExcel->getActiveSheet()->getStyle('A2')->applyFromArray($bold);

$objPHPExcel->getActiveSheet()->getStyle('A2')
    ->getAlignment()->setWrapText(true);

$objPHPExcel->getActiveSheet()->mergeCells('A3:C3');
$objPHPExcel->getActiveSheet()->setCellValue('A3',$sub_title);
$objPHPExcel->getActiveSheet()->getStyle('A3')->applyFromArray($bold);

$objPHPExcel->getActiveSheet()->getStyle('A3')
    ->getAlignment()->setWrapText(true);



$objPHPExcel->getActiveSheet()->mergeCells('A4:C4');
$objPHPExcel->getActiveSheet()->setCellValue('A4',$as_at);
$objPHPExcel->getActiveSheet()->getStyle('A4')->applyFromArray($bold);

$objPHPExcel->getActiveSheet()->getStyle('A4')
    ->getAlignment()->setWrapText(true);



$objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->applyFromArray(
    array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,)
);
$objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->applyFromArray(
    array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,)
);
$objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->applyFromArray(
    array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,)
);
$objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->applyFromArray(
    array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,)
);
$objPHPExcel->getActiveSheet()->getStyle('A3')->getAlignment()->applyFromArray(
    array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,)
);
$objPHPExcel->getActiveSheet()->getStyle('A3')->getAlignment()->applyFromArray(
    array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,)
);
$objPHPExcel->getActiveSheet()->getStyle('A4')->getAlignment()->applyFromArray(
    array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,)
);
$objPHPExcel->getActiveSheet()->getStyle('A4')->getAlignment()->applyFromArray(
    array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,)
);

$pos_handler=6;
$title_po=$pos_handler-1;

$title_range = 'A'.($title_po).':'.'D'.($title_po);

cellColor($title_range, '356837');

$objPHPExcel->getActiveSheet()->setCellValue('A'.($title_po),'Name');
$objPHPExcel->getActiveSheet()->setCellValue('B'.($title_po),'Sto. Number');
$objPHPExcel->getActiveSheet()->setCellValue('C'.($title_po),'National ID Number');
//$objPHPExcel->getActiveSheet()->setCellValue('D'.($title_po),'Balance');

$objPHPExcel->getActiveSheet()->getStyle('A'.($title_po))->applyFromArray($bold);
$objPHPExcel->getActiveSheet()->getStyle('B'.($title_po))->applyFromArray($bold);
$objPHPExcel->getActiveSheet()->getStyle('C'.($title_po))->applyFromArray($bold);
//$objPHPExcel->getActiveSheet()->getStyle('D'.($title_po))->applyFromArray($bold);


$objPHPExcel->getActiveSheet()->getStyle('A'.($title_po))->applyFromArray($white_text);
$objPHPExcel->getActiveSheet()->getStyle('B'.($title_po))->applyFromArray($white_text);
$objPHPExcel->getActiveSheet()->getStyle('C'.($title_po))->applyFromArray($white_text);
//$objPHPExcel->getActiveSheet()->getStyle('D'.($title_po))->applyFromArray($white_text);

$objPHPExcel->getActiveSheet()->getStyle('B'.$title_po)->getAlignment()->applyFromArray(
    array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,)
);


$in = implode(",", $cleared_recruits);

if(count($cleared_recruits)==0){
    $query_recruits = m("SELECT * FROM recruits WHERE status =1885 ORDER BY surname,first_name");

}else{

    $query_recruits = m("SELECT * FROM recruits WHERE recruit_id IN ($in) AND status =1 ORDER BY surname,first_name");

}


$recruit_counter = 0;
while($recruit=msoc($query_recruits)){

    $full_name=$recruit['surname'].' '.$recruit['first_name'].' '.$recruit['other_name'];
    $sto_number = $recruit['sto_number'];
    $national_id = $recruit['national_id'];


    $objPHPExcel->getActiveSheet()->setCellValue('A'.$pos_handler,$full_name);
    $objPHPExcel->getActiveSheet()->setCellValue('B'.$pos_handler,$sto_number);
    $objPHPExcel->getActiveSheet()->setCellValue('C'.$pos_handler,$national_id);

    $objPHPExcel->getActiveSheet()->getStyle('C'.$pos_handler)->getAlignment()->applyFromArray(
        array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,)
    );


    $objPHPExcel->getActiveSheet()->getStyle('B'.$pos_handler)->getAlignment()->applyFromArray(
        array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,)
    );
    $recruit_counter++;
    $pos_handler=$pos_handler+1;
}

//
//$objPHPExcel->getActiveSheet()->setCellValue('A8',"Hello\nWorld");
//$objPHPExcel->getActiveSheet()->getRowDimension(8)->setRowHeight(-1);
//$objPHPExcel->getActiveSheet()->getStyle('A8')->getAlignment()->setWrapText(true);
$pos_handler++;

$objPHPExcel->getActiveSheet()->getStyle('B'.$pos_handler)->applyFromArray($bold);
$objPHPExcel->getActiveSheet()->getStyle('C'.$pos_handler)->applyFromArray($bold);

$percentage = number_format(($recruit_counter/$recruit_total_count)*100,2);

$objPHPExcel->getActiveSheet()->setCellValue('B'.$pos_handler,'Total Cleared Recruits: '.$recruit_counter.' ('.$percentage.'%)');
//$objPHPExcel->getActiveSheet()->setCellValue('C'.$pos_handler,$recruit_counter);


$objPHPExcel->getActiveSheet()->getHeaderFooter()->setEvenFooter($exofile_name);
$objPHPExcel->getActiveSheet()->getHeaderFooter()->setOddFooter('&L&B' . $objPHPExcel->getProperties()->getTitle() . '&RPage &P of &N');
$objPHPExcel->getActiveSheet()->getHeaderFooter()->setEvenFooter('&L&B' . $objPHPExcel->getProperties()->getTitle() . '&RPage &P of &N');

$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(22);

$objPHPExcel->getActiveSheet()
    ->getColumnDimension('B')
    ->setAutoSize(true);
$objPHPExcel->getActiveSheet()
    ->getColumnDimension('C')
    ->setAutoSize(true);

$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(17);

$objPHPExcel->getActiveSheet()->setTitle($sub_title);


if($percentage<100) {


    $sheet = 1;
    $objWorkSheet = $objPHPExcel->createSheet();

    $sheet = $objPHPExcel->setActiveSheetIndex($sheet);

    $report_title = $perm_label . ' Status';
    $as_at = 'As at ' . date("d-M-Y H:iA");

    $report_main_default_title = $excel_title;

    $sub_title = 'Not-Cleared Recruits';
    $objPHPExcel->getActiveSheet()->mergeCells('A1:C1');
    $objPHPExcel->getActiveSheet()->setCellValue('A1', $report_main_default_title);
    $objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($bold);

    $objPHPExcel->getActiveSheet()->getStyle('A1')
        ->getAlignment()->setWrapText(true);

    $objPHPExcel->getActiveSheet()->mergeCells('A2:C2');
    $objPHPExcel->getActiveSheet()->setCellValue('A2', $report_title);
    $objPHPExcel->getActiveSheet()->getStyle('A2')->applyFromArray($bold);

    $objPHPExcel->getActiveSheet()->getStyle('A2')
        ->getAlignment()->setWrapText(true);

    $objPHPExcel->getActiveSheet()->mergeCells('A3:C3');
    $objPHPExcel->getActiveSheet()->setCellValue('A3', $sub_title);
    $objPHPExcel->getActiveSheet()->getStyle('A3')->applyFromArray($bold);

    $objPHPExcel->getActiveSheet()->getStyle('A3')
        ->getAlignment()->setWrapText(true);


    $objPHPExcel->getActiveSheet()->mergeCells('A4:C4');
    $objPHPExcel->getActiveSheet()->setCellValue('A4', $as_at);
    $objPHPExcel->getActiveSheet()->getStyle('A4')->applyFromArray($bold);

    $objPHPExcel->getActiveSheet()->getStyle('A4')
        ->getAlignment()->setWrapText(true);


    $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->applyFromArray(
        array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,)
    );
    $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->applyFromArray(
        array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,)
    );
    $objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->applyFromArray(
        array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,)
    );
    $objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->applyFromArray(
        array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,)
    );
    $objPHPExcel->getActiveSheet()->getStyle('A3')->getAlignment()->applyFromArray(
        array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,)
    );
    $objPHPExcel->getActiveSheet()->getStyle('A3')->getAlignment()->applyFromArray(
        array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,)
    );
    $objPHPExcel->getActiveSheet()->getStyle('A4')->getAlignment()->applyFromArray(
        array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,)
    );
    $objPHPExcel->getActiveSheet()->getStyle('A4')->getAlignment()->applyFromArray(
        array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,)
    );

    $pos_handler = 6;
    $title_po = $pos_handler - 1;

    $title_range = 'A' . ($title_po) . ':' . 'D' . ($title_po);

    cellColor($title_range, '356837');

    $objPHPExcel->getActiveSheet()->setCellValue('A' . ($title_po), 'Name');
    $objPHPExcel->getActiveSheet()->setCellValue('B' . ($title_po), 'Sto. Number');
    $objPHPExcel->getActiveSheet()->setCellValue('C' . ($title_po), 'National ID Number');
//$objPHPExcel->getActiveSheet()->setCellValue('D'.($title_po),'Balance');

    $objPHPExcel->getActiveSheet()->getStyle('A' . ($title_po))->applyFromArray($bold);
    $objPHPExcel->getActiveSheet()->getStyle('B' . ($title_po))->applyFromArray($bold);
    $objPHPExcel->getActiveSheet()->getStyle('C' . ($title_po))->applyFromArray($bold);
//$objPHPExcel->getActiveSheet()->getStyle('D'.($title_po))->applyFromArray($bold);


    $objPHPExcel->getActiveSheet()->getStyle('A' . ($title_po))->applyFromArray($white_text);
    $objPHPExcel->getActiveSheet()->getStyle('B' . ($title_po))->applyFromArray($white_text);
    $objPHPExcel->getActiveSheet()->getStyle('C' . ($title_po))->applyFromArray($white_text);
//$objPHPExcel->getActiveSheet()->getStyle('D'.($title_po))->applyFromArray($white_text);

    $objPHPExcel->getActiveSheet()->getStyle('B' . $title_po)->getAlignment()->applyFromArray(
        array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,)
    );




    if(count($cleared_recruits)==0){
        $query_recruits = m("SELECT * FROM recruits WHERE status =1 ORDER BY surname,first_name");

    }else{

        $query_recruits = m("SELECT * FROM recruits WHERE recruit_id NOT IN ($in) AND status =1 ORDER BY surname,first_name");

    }



    $recruit_counter = 0;
    while ($recruit = msoc($query_recruits)) {

        $full_name = $recruit['surname'] . ' ' . $recruit['first_name'] . ' ' . $recruit['other_name'];
        $sto_number = $recruit['sto_number'];
        $national_id = $recruit['national_id'];


        $objPHPExcel->getActiveSheet()->setCellValue('A' . $pos_handler, $full_name);
        $objPHPExcel->getActiveSheet()->setCellValue('B' . $pos_handler, $sto_number);
        $objPHPExcel->getActiveSheet()->setCellValue('C' . $pos_handler, $national_id);

        $objPHPExcel->getActiveSheet()->getStyle('C' . $pos_handler)->getAlignment()->applyFromArray(
            array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,)
        );


        $objPHPExcel->getActiveSheet()->getStyle('B' . $pos_handler)->getAlignment()->applyFromArray(
            array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,)
        );
        $recruit_counter++;
        $pos_handler = $pos_handler + 1;
    }

//
//$objPHPExcel->getActiveSheet()->setCellValue('A8',"Hello\nWorld");
//$objPHPExcel->getActiveSheet()->getRowDimension(8)->setRowHeight(-1);
//$objPHPExcel->getActiveSheet()->getStyle('A8')->getAlignment()->setWrapText(true);
    $pos_handler++;

    $objPHPExcel->getActiveSheet()->getStyle('B' . $pos_handler)->applyFromArray($bold);
    $objPHPExcel->getActiveSheet()->getStyle('C' . $pos_handler)->applyFromArray($bold);

    //$percentage = ($recruit_counter / $recruit_total_count) * 100;
    $percentage = number_format(($recruit_counter/$recruit_total_count)*100,2);

    $objPHPExcel->getActiveSheet()->setCellValue('B' . $pos_handler, 'Total Non-Cleared Recruits: ' . $recruit_counter . ' (' . $percentage . '%)');
//$objPHPExcel->getActiveSheet()->setCellValue('C'.$pos_handler,$recruit_counter);


    $objPHPExcel->getActiveSheet()->getHeaderFooter()->setEvenFooter($exofile_name);
    $objPHPExcel->getActiveSheet()->getHeaderFooter()->setOddFooter('&L&B' . $objPHPExcel->getProperties()->getTitle() . '&RPage &P of &N');
    $objPHPExcel->getActiveSheet()->getHeaderFooter()->setEvenFooter('&L&B' . $objPHPExcel->getProperties()->getTitle() . '&RPage &P of &N');

    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(22);

    $objPHPExcel->getActiveSheet()
        ->getColumnDimension('B')
        ->setAutoSize(true);
    $objPHPExcel->getActiveSheet()
        ->getColumnDimension('C')
        ->setAutoSize(true);

    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(17);

    $objPHPExcel->getActiveSheet()->setTitle($sub_title);
}





$objPHPExcel->setActiveSheetIndex(0);


$exofile_name=$exofile_name.'.'.$defa_excel_expo_format;

if($defa_excel_expo_format=='xlsx'){
    header('Content-Type: application/vnd.ms-excel');
    header("Content-Disposition: attachment;filename=$exofile_name");
    header('Cache-Control: max-age=0');
    header('Cache-Control: max-age=1');
    header ('Expires: Mon, 18 Jul 2014 05:00:00 GMT');
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
    header ('Cache-Control: cache, must-revalidate');
    header ('Pragma: public');
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

}elseif($defa_excel_expo_format=='xls'){
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header("Content-Disposition: attachment;filename=$exofile_name");
    header('Cache-Control: max-age=0');
    header('Cache-Control: max-age=1');
    header ('Expires: Mon, 18 Jul 2014 05:00:00 GMT');
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
    header ('Cache-Control: cache, must-revalidate');
    header ('Pragma: public');
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
}

$objWriter->save('php://output');
exit;
