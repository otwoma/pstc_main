<?php
/**
 * Created by PhpStorm.
 * User: Otwoma
 * Date: 4/28/2017
 * Time: 3:49 PM
 */

include '_header_store.php';


$sno=sani($_POST['s_no']);
$name=sani($_POST['item_name']);
$unit=sani($_POST['unit']);
$category=sani($_POST['category']);
$quantity=sani($_POST['quantity']);
$quantity=intval($quantity);
$threshold=intval($_POST['threshold']);
$remark=sani($_POST['remark']);


if(($name!='' && $quantity!='')){

    m("INSERT INTO consumables SET s_no = '".$sno."', item_name = '".$name."', unit = '".$unit."', category ='".$category."' , threshold='".$threshold."', quantity='".$quantity."', remark = '".$remark."'");

    header("Location:store/inventory.php");
}else {
    header("Location:store/inventory.php?error=101");
}

$item_taker=cleaN($_POST['taker_t']);
$item_taken_by=cleaN($_POST['taker_id']);
$givetran_note = cleaN($_POST['gtran_note']);
$item_fo_gid=cleaN($_POST['give_item_lead']);
$give_n_items=cleaN($_POST['given_itms']);
if(isset($item_taker) && $item_taken_by !='' && isset($item_fo_gid)){
    $giid=store_get_item_id_give($item_fo_gid,$give_n_items);
    $mid_tranid = rand_letters(3).$stock_batch.time();
    foreach($giid as $item_d){
        m("INSERT INTO store_moves SET mid='".$mid_tranid."',item_id = '".$item_d."'");
        m("UPDATE store_items SET state = 0 WHERE id = '".$item_d."'");

    }
    m("INSERT INTO store_motion SET holder = '".$item_taken_by."',move_id = '".$mid_tranid."',holder_group  ='".$item_taker."',move_note='".$givetran_note."'");
}
?>

    <script type="text/javascript" charset="utf-8">
        $(function() {
            $('#consumableTable').dataTable();
            $('#non-consumableTable').dataTable();
        } );
    </script>


    <div id="main-container">

        <ul class="tab-bar grey-tab">
            <li class="protab">
                <a href="#consumables" data-toggle="tab">
                    <span class="block text-center">
                        <i class="fa fa-cart-plus fa-2x"></i>
                    </span>
                    Consumables
                </a>
            </li>

            <li class="protab">
                <a href="#non-consumables" data-toggle="tab">
                    <span class="block text-center">
                        <i class="fa fa-life-ring fa-2x"></i>
                    </span>
                    Non-Consumables
                </a>
            </li>
        </ul>

        <div class="padding-md">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                <div class="padding-sm font-16">
                            Store Items
                            <a href="#newStoreitem" role="button" data-toggle="modal" class="btn btn-info btn-small" style="margin-left: 900px"><span class="fa fa-plus"></span> Add Item</a>
                        </div>
                    <div class="tab-content">
                        <div class="tab-pane fade in active " id="consumables">
                            <div class="panel panel-default table-responsive">
                                <div class="seperator"></div><div class="seperator"></div>
                                <table class="table table-striped" id="consumableTable">
                                    <thead>
                                    <tr>
                                        <th width="" align="left"><span class=""></span>S/NO</th>
                                        <th width="" align="left"><span class=""></span>Category</th>
                                        <th width="" align="left"><span class=""></span>Item Name</th>
                                        <th width="" align="left"><span class=""></span>Initial Quantity</th>
                                        <th width="" align="left"><span class=""></span>Stock Qty</th>
                                        <th width="" align="left"><span class=""></span>Percentage Out</th>
                                        <th width="" align="center"></th>
                                        <th width="" align="center"></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $ask=m("SELECT id,s_no,item_name,quantity, category, threshold,unit FROM consumables WHERE status =1");
                                        while($data=mray($ask)){
                                            $id = $data['id'];
                                            $label = $data['item_name'];
                                            $sno = $data['s_no'];
                                            $cat = $data['category'];
                                            $quantity = $data['quantity'];
                                            $unit = $data['unit'];
                                            $threshold_qr = $data['threshold'];

                                            $remaining=count_remaining_consumable($id);
                                            $dif=$quantity-$remaining;

                                            $tage_figure=(($dif/$quantity)*100);
                                            $tage=number_format($tage_figure,2).'%';

                                            $threshold=consumable_threshold_level($id);
                                        ?>
                                        <tr>
                                            <td align="left"><?php echo $sno;?></td>
                                            <td><?php echo get_category_label($cat);?></td>
                                            <td align="left"><?php echo $label?></td>
                                            <td title="<?php echo 'Threshold Mark: '.$threshold_qr;?>"><?php echo $quantity;?></td>
                                            <td><?php echo $remaining;?></td>
                                            <td title="<?php echo number_format((100-$tage_figure),2).'%'.' Stock Remaining:'?>"><?php echo $tage;?></td>

                                            <?php
                                            if($remaining==0){
                                                //RESTOCK
                                            ?>

                                            <td><a class="btn btn-danger" data-name="<?php echo $label;?>" data-id="<?php echo $id;?>"><i class="fa fa-exclamation"></i> Out of Stock</a></td>
                                                <?php }elseif($remaining<=$threshold){?>
                                            <td title="Threshold Quantity: <?php echo $threshold;?>"><a  class="btn btn-warning " data-name="<?php echo $label;?>"  data-id="<?php echo $id;?>"><i class="fa fa-caret-right"></i> Give out</a></td>
                                                <?php }else{?>
                                            <td><a href="#storeGive" role="button" data-toggle="modal" class="btn btn-info " data-name="<?php echo $label;?>" data-id="<?php echo $id;?>"><i class="fa fa-caret-right"></i> Give out</a></td>
                                                <?php }?>
                                            <td><a href="purchase_request" class="btn btn-primary" data-name="<?php echo $label;?>" data-id="<?php echo $id;?>"><i class="fa fa-refresh"></i> Restock</a></td>

                                            </tr>
                                        <?php
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>


                        <div class="tab-pane fade" id="non-consumables">
                            <div class="panel panel-default table-responsive">
                                <div class="seperator"></div><div class="seperator"></div>
                                <table class="table table-striped" id="non-consumableTable">
                                    <thead>
                                    <tr>
                                        <th width="" align="left"><span class=""></span>S/NO</th>
                                        <th width="" align="left"><span class=""></span>Category</th>
                                        <th width="" align="left"><span class=""></span>Item Name</th>
                                        <th width="" align="left"><span class=""></span>Initial Quantity</th>
                                        <th width="" align="left"><span class=""></span>In-Stock</th>
                                        <th width="" align="left"><span class=""></span>Out</th>
                                        <th width="" align="center"></th>
                                        <th width="" align="center"></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $ask=m("SELECT id,s_no,item_name,quantity, category, threshold,unit FROM non-consumables WHERE status =1");
                                        while($data=mray($ask)){
                                            $id = $data['id'];
                                            $label = $data['item_name'];
                                            $sno = $data['s_no'];
                                            $cat = $data['category'];
                                            $quantity = $data['quantity'];
                                            $unit = $data['unit'];
                                            $threshold_qr = $data['threshold'];

                                            $remaining=count_remaining_consumable($id);
                                            $dif=$quantity-$remaining;

                                            $tage_figure=(($dif/$quantity)*100);
                                            $tage=number_format($tage_figure,2).'%';

                                            $threshold=consumable_threshold_level($id);

                                            $nout=count_items_out($label);
                                        ?>
                                        <tr>
                                            <td align="left"><?php echo $sno;?></td>
                                            <td><?php echo get_category_label($cat);?></td>
                                            <td align="left"><?php echo $label?></td>
                                            <td title="<?php echo 'Threshold Mark: '.$threshold_qr;?>"><?php echo $quantity;?></td>
                                            <td><?php echo $remaining;?></td>
                                            <td align="left"><?php echo $nout;?></td>

                                            <?php
                                            if($remaining==0){
                                                //RESTOCK
                                            ?>

                                            <td><a class="btn btn-danger" data-name="<?php echo $label;?>" data-id="<?php echo $id;?>"><i class="fa fa-exclamation"></i> Out of Stock</a></td>
                                                <?php }elseif($remaining<=$threshold){?>
                                            <td title="Threshold Quantity: <?php echo $threshold;?>"><a  class="btn btn-warning " data-name="<?php echo $label;?>"  data-id="<?php echo $id;?>"><i class="fa fa-caret-right"></i> Give out</a></td>
                                                <?php }else{?>
                                            <td align="center"><?php if($quantity!=0){echo "<a class='item_give' href='#storeGive' data-label='$utem' data-maxi='$nin' data-lead='$therd' role='button' data-toggle='modal'><button class='btn btn-success'><span class='fa fa-caret-right'></span> Give Out</button></a>";}else{echo "<button class='butn butn-red'> All Out</button>";}?></td>
                                                <?php }?>
                                            <td align="center"><?php if($nout!=0){echo "<a class='item_receive' href='#storeReceive' data-label='$utem' data-lead='$therd' role='button' data-toggle='modal'><button class='btn btn-primary'><span class='fa fa-chevron-down'></span> Receive</button></a>";}else{echo "<button class='btn btn-info'><span class='fa fa-check'></span> All In</button>";}?></td>

                                            </tr>
                                        <?php
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>


<div class="modal fade" id="newStoreitem" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add New Stock</h4>
            </div>
            <div class="modal-body">
                <div class="tabbable">
                        <ul class="nav nav-tabs" data-tabs="tabs">
                            <li id="tab1" class="active mytabs">
                                <a href="#One" data-toggle="tab"><i class="fa fa-cart-plus"> </i> Consumables</a>
                            </li>
                            <li id="tab2" class="mytabs">
                                <a href="#Two" data-toggle="tab"><i class="fa fa-life-ring"> </i> Non-Consumables</a>
                            </li>
                        </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="One"></br>
                            <form id="tab1-form" role="form" class="container-fluid" action="" method="post">
                                <div class="row">
                                    <div class="form-group">
                                        <label>S/NO</label></br>
                                        <input placeholder="S/No." type="" name="s_no" class="form-control" required>
                                    </div>

                                    <div class="form-group">
                                        <label>Category</label></br>
                                        <select name="category" id="" class="form-control">
                                            <?php echo consumable_item_category()?>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                       <label>Item Name</label></br>
                                       <input placeholder="Item Name" type="text" name="item_name" class="form-control" required>
                                    </div>

                                    <div class="form-group">
                                       <label>Quantity</label></br>
                                       <input placeholder="Quantity" type="text" name="quantity" class="form-control" required>
                                    </div>

                                    <div class="form-group">
                                       <label>Threshold Quantity</label></br>
                                       <input placeholder="Threshold Quantity (Optional)" type="number" name="threshold" class="form-control" required>
                                    </div>

                                    <div class="form-group">
                                       <label>Unit of Measure</label></br>
                                       <select name="unit" id="" class="form-control">
                                            <?php echo store_unit_options()?>
                                       </select>
                                    </div>

                                    <div class="form-group">
                                       <label>Remark</label></br>
                                       <textarea placeholder="Remark" type="text" name="remark" class="form-control" required></textarea>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="tab-pane" id="Two"></br>
                            <form id="tab2-form" role="form" class="container-fluid" action="" method="post">
                                <div class="row">
									<div class="form-group">
                                        <label>S/NO</label></br>
                                            <input placeholder="S/No." type="" name="s.no" class="form-control" required>
                                        </div>

                                        <div class="form-group">
                                            <label>Category</label></br>
                                            <select name="category" id="" class="form-control">
                                                <?php echo non_consumable_item_category()?>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label>Item Name</label></br>
                                            <input placeholder="Item Name" type="text" name="item_name" class="form-control" required>
                                        </div>

                                        <div class="form-group">
                                            <label>Quantity</label></br>
                                            <input placeholder="Quantity" type="text" name="quantity" class="form-control" required>
                                        </div>

                                        <div class="form-group">
                                            <label>Threshold Quantity</label></br>
                                            <input placeholder="Threshold Quantity (Optional)" type="number" name="threshold" class="form-control" required>
                                        </div>

                                        <div class="form-group">
                                            <label>Unit of Measure</label></br>
                                            <select name="unit" id="" class="form-control">
                                                <?php echo store_unit_options()?>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label>Remark</label></br>
                                            <textarea placeholder="Remark" type="text" name="remark" class="form-control" required></textarea>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button id="save-button" type="button" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="storeGive">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4>Giving Out</h4>
                    <span class="fa fa-chevron-right"> <span id="item_labelG" class="font-14"></span></span>

                </div>
                <div class="modal-body">
                    <form id="gve_outfom" action="" method="POST" enctype="multipart/form-data">
                        <span class="hidden"><input hidden="" id="item_leadG" name="give_item_lead"/>
                        <input hidden="" id="pmax" name="pimax"/>
                        </span>

                        <div class="form-group">
                            <label>Pieces</label>
                            <input id="pitem_c" onchange="handleChange(this);" style="width: 55px;" value="1" type="text" name="given_itms" class="form-control" required>
                            <script>
                                $('#pitem_c').on("keyup",function (mtc) {
                                    var max = $("#pmax").val();
                                    var mvax = $(this).val();
                                    if(mvax > max){
                                        alert('Currently the maximum number of pieces that can be given is '+max);
                                        $(this).val(max);
                                    }
                                });
                            </script>
                        </div>

                        <div class="form-group">
                            <label>Giving</label></br>
                            <select id="gvtaker_sel" class="form-control" style="width: 300px;" name="taker_t">
                                <?php echo store_give_out_options()?>
                            </select>
                        </div>


                        <div class="form-group">
                            <label id="taker_i">Student ID.</label> <label id="taker_"></label>
                            <input style="font-size: 16px;" id="taken_id" type="text" name="taker_id" class="form-control" required>

                        </div>


                        <div class="form-group">
                            <label>Note: </label>
                            <textarea class="form-control input-sm" name="gtran_note" style="height: 55px;"></textarea>
                        </div>

                        <span id="taker_nameclue"></span>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-success btn-sm" data-dismiss="modal" aria-hidden="true">Close</button>
                    <button type="submit" class="btn btn-danger btn-sm">Submit</button>
                </div>
            </div>
        </div>
        </form>
    </div>


    <div class="modal fade" id="storeReceive">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4>Receiving</h4>
                    <span class="fa fa-chevron-right"> <span id="item_labelR" class="font-14"></span></span>

                </div>
                <div class="modal-body">
                    <form id="rcv_inform" action="" method="POST" enctype="multipart/form-data">
                        <span class="hidden"><input hidden="" id="item_leadR" name="give_item_lead"/></span>

                        <div class="form-group">
                            <label>Pieces</label>
                            <input  style="width: 55px;" value="1" type="number" name="given_itms" class="form-control" required>

                        </div>

                        <div class="form-group">
                            <label>Giving</label></br>
                            <select id="gvtaker_sel" class="form-control" style="width: 300px;" name="taker_t">
                                <?php echo store_give_out_options()?>
                            </select>
                        </div>


                        <div class="form-group">
                            <label id="taker_i">Student ID.</label> <label id="taker_"></label>
                            <input style="font-size: 16px;" id="taken_id" type="text" name="taker_id" class="form-control" required>

                        </div>


                        <div class="form-group">
                            <label>Note: </label>
                            <textarea class="form-control input-sm" name="gtran_note" style="height: 55px;"></textarea>
                        </div>

                        <span id="taker_nameclue"></span>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-success btn-sm" data-dismiss="modal" aria-hidden="true">Close</button>
                    <button type="submit" class="btn btn-danger btn-sm">Submit</button>
                </div>
            </div>
        </div>
        </form>
    </div>




    <script> $(".inventory_mu").addClass('active');

        $(".gv_out").click(function() {
            var hd=$(this).data("id");
            var hd_name=$(this).data("name");

            $.post('.consumables_out.php',{hd:hd,hd_name:hd_name},
                function (cmovementm) {
                    $(".smartModal").html(cmovementm);
                    $("#modal_switch").trigger("click");
                });
        });

        $(".restock").click(function() {
            var hd=$(this).data("id");
            var hd_name=$(this).data("name");

            $.post('.consumable_restock.php',{hd:hd,hd_name:hd_name},
                function (cmovementm) {
                    $(".smartModal").html(cmovementm);
                    $("#modal_switch").trigger("click");
                });
        });

         $("#tab1").click(function() {
            $("#save-button").html("Submit Consumable");
        });

        $("#tab2").click(function() {
            $("#save-button").html("Submit Non-Consumable");
        });

        $("#save-button").click(function() {
            var id = $(".mytabs.active")[0].id;
            $("#" + id + "-form").submit();
        });

        $("body").on('click','.item_give',function(){
            var ilabel= $(this).data("label");
            var glead= $(this).data("lead");
            var mx= $(this).data("maxi");
            $("#item_labelG").html(ilabel);
            $("#item_leadG").val(glead);
            $("#pmax").val(mx);
        });
        function handleChange(input) {
            var qax=  $("#pmax").val();
            if (input.value < 0) input.value = 0;
            if (input.value > qax) input.value = qax;
        }

        $("body").on('click','.item_receive',function(){
            var ilabel= $(this).data("label");
            var glead= $(this).data("lead");
            $("#item_labelR").html(ilabel);
            $("#item_leadR").val(glead);

            $.ajax({
                url: '_ajax.php',
                type: 'POST',
                data: $('#rcv_inform').serialize(),
                success: function(cnamef){
                    var len = cnamef.length;
                    if(len!=2){
                        var nabel = '('+cnamef+')';
                        $("#taker_").html(nabel);
                    }
                }
            });




        });



        $("body").on('change','#gvtaker_sel',function(){
            $("#taker_").html('');
            $("#taken_id").val('');
            var tak = $(this).val();
            var label = $("#taker_i");
            if(tak==1){label.html("Recruit ID.")}
            if(tak==2){label.html("Instructor ID.")}
        });

        $('#taken_id').on("keyup",function (tnane) {
            $("#taker_").html('');
            $.ajax({
                url: '_ajax.php',
                type: 'POST',
                data: $('#gve_outfom').serialize(),
                success: function(cnamef){
                    var len = cnamef.length;
                    if(len!=2){
                        var nabel = '('+cnamef+')';
                        $("#taker_").html(nabel);
                    }
                }
            });
        });
    </script>

</div>

<?php include'../_footer.php';?>