<?php
/**
 * Created by PhpStorm.
 * User: Otwoma
 * Date: 5/9/2017
 * Time: 4:55 PM
 */

$consumable_id=sani($_POST['hd']);
$consumable_name=sani($_POST['hd_name']);


$ask=m("SELECT * FROM consumables WHERE id = '".$consumable_id."'");
while($data=msoc($ask)){
    $id=$data['id'];
    $label=$data['item_name'];
    $sno=$data['s_no'];
    $quantity=$data['quantity'];

    $remaining='';

}

$max=count_remaining_consumable($consumable_id);
?>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="color-line"></div>
        <div class="modal-header">
            <h4>Giving Out - <strong id="item_name_">
                    <?php echo $consumable_name ?></strong>
            </h4>
        </div>
        <div class="modal-body">

            <form method="post" id="check_in_form" enctype="multipart/form-data" class="form-horizontal">

                <input required id="io_id" type="hidden" name="main" value="<?php echo $consumable_id;?>" >



                <div class="form-group"><label for="person_select" class="col-sm-2 control-label">Giving To</label>

                    <div class="col-sm-9"><div class="input-group m-b"><span class="input-group-addon"><i class="fa fa-user"></i> </span>
                            <select id="person_select" class="form-control m-b smart_select" name="user">
                                <option value=""> Select</option>
                                <?php echo store_give_out_options();?>
                            </select>
                        </div>
                    </div>
                </div>


                <div title="Quantity Given Out" class="form-group"><label class="col-sm-2 control-label">Quantity</label>

                    <div class="col-sm-8">
                        <div class="input-group m-b"><span class="input-group-addon"></span> <input value="1" required id="quantity" min="1" max="<?php echo $max;?>" name="quantity" type="number" placeholder="Quantity Given Out" class="form-control"> </div>
                    </div>
                </div>


                <p class="col-lg-offset-1">Available Count: <?php echo $max;?></p>


                <div class="modal-footer">
                    <button id="close_modal" type="button" class="btn btn-default" data-dismiss="modal"> Close</button>
                    <button type="submit" class="btn btn-primary submit_form"><i class="fa fa-send"></i> Submit</button>
                </div>

            </form>
        </div>

    </div>
</div>
<script>$(".smart_select").select2();
    $("#note_container").hide();
    $.fn.datepicker.defaults.format = "yyyy-mm-dd";
    $.fn.datepicker.defaults.autoclose = "true";
    $('.form-group.date._date').datepicker({ });
    $('#indate').datepicker({ });

    $('#check_in_form').submit(function(t_in) {
        $("#key_button_pk").fadeOut();
        $.post("_handle_consumable_checkout.php",$(this).serialize(),function(ghost){
            if(ghost==77){
                $("#button_asset_view").fadeOut();
                var io= $("#io_id").val();

                $("#modal_switch").trigger("click");

                var item_name= $("#item_name_").text();
                var user_name= $("#person_select option:selected").text();

                var quantity=$("#quantity").val();

                toastr.success('('+quantity+') '+item_name+' Taken by '+user_name);


                window.setTimeout(function() {
                    window.location.href = 'consumables.php?target=' + quantity;
                },5600);

            }else{
                //alert(ghost);
                $("#key_button_pk").fadeIn();
                toastr.error(ghost);
            }

        });
        return false;
    });



</script>
