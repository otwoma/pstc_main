<?php
/**
 * Created by PhpStorm.
 * User: Otwoma
 * Date: 5/12/2017
 * Time: 11:21 AM
 */
include '_header_store.php';

?>

<script type="text/javascript" charset="utf-8">
    $(function() {
        $('#responsiveTable').dataTable();
        $('#responsiveTable2').dataTable();
    } );
</script>

<div id="main-container">
    <div class="small-header transition animated fadeIn">
        <div class="hpanel">
            <div class="panel-body">
                <h4 class="font-light m-b-xs">
                    <i class="fa fa-plus"></i> Add New Item Category
                </h4>
            </div>
        </div>
    </div>

    <div class="content animate-panel">

        <div>
            <div class="row">
                <div class="col-lg-9">
                    <div class="hpanel">
                        <div class="panel-heading">
                            Fill the following details to add a new category
                        </div>
                        <div class="panel-body">
                            <form method="post" enctype="multipart/form-data" action="../_actions.php" class="form-horizontal">
                                <div class="hr-line-dashed"></div>
                                <div class="form-group"><label class="col-sm-2 control-label">Category Name</label>

                                    <div class="col-sm-10">
                                        <div class="input-group m-b"><span class="input-group-addon"><i class="fa fa-tag"></i></span> <input name="name" type="text" placeholder="Category Name" class="form-control"></div>
                                    </div>
                                </div>

                                <div class="form-group"><label for="type_select" class="col-sm-2 control-label">Type</label>

                                    <div class="col-sm-10"><div class="input-group m-b"><span class="input-group-addon">T </span>
                                            <select id="type_select" class="form-control m-b" name="type">
                                                <?php echo store_item_type();?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12 col-sm-offset-6">
                                        <button class="btn btn-success" type="submit"><span class="fa fa-send"></span> Save</button>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="padding-md">
                            <div class="padding-md col-md-8 col-lg-push-3">
                                <div class="panel panel-default table-responsive">
                                    <div class="padding-sm font-16">
                                        Listing Item Categories
                                    </div>
                                    <table class="table table-striped" id="responsiveTable2">
                                        <thead>
                                        <tr>
                                            <th width="" align="left">#</th>
                                            <th width="" align="center"><span class=""></span>Label</th>
                                            <th width="" align="right"><span class=""></span>Type</th>
                                            <th width="" align="right"><span class=""></span>Date</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $list = "SELECT id, category_name, category_type, created_at FROM item_categories ORDER BY id ASC";
                                        $list_query = mysql_query($list);
                                        while($list_result = mysql_fetch_array($list_query)){
                                            ?>
                                            <tr>
                                                <td align="left"><?php echo $list_result['id']?></td>
                                                <td align="left"><?php echo ucwords($list_result['category_name']);?></td>
                                                <td align="left"><?php echo item_type_label($list_result['category_type'])?></td>
                                                <td align="left"><?php echo $list_result['created_at']?></td>
                                            </tr>
                                            <?php
                                        }
                                        ?></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>


<script>$('.category_mu').addClass('active')</script>
<?php include '../_footer.php'?>
