<?php
/**
 * Created by PhpStorm.
 * User: Otwoma
 * Date: 5/12/2017
 * Time: 1:11 PM
 */

include '../_functions.php';
require_once '../packages/Classes/PHPWord.php';

$phpWord = new PHPWord();

$title = 'Purchase Form '.date('d-M-Y');



$query = mysql_query("SELECT * FROM purchase_request WHERE status = 1");
$query_fetch = mysql_fetch_assoc($query);

    $section_head = $query_fetch ['section_head'];
    $section_head = trim(ucfirst($query_fetch['section_head']));
    $purpose = trim(ucfirst($query_fetch ['purpose']));
    $proc_officer = trim(ucfirst($query_fetch ['proc_officer']));
    $rqo = trim(ucfirst($query_fetch ['rqo']));
    $store_clerk = trim(ucfirst($query_fetch ['stores_clerk']));
    $vbb = trim(ucfirst($query_fetch ['book_balances']));
    $accountant = trim(ucfirst($query_fetch ['accountant']));

$qry = mysql_query("SELECT * FROM prform_data WHERE status = 1");

$document = $phpWord->loadTemplate('purchase request.docx');

$i = 1;
while($qry_ = mysql_fetch_array($qry)){
    $id = $qry_['id'];
    if($id >= 1){
    $sno = trim(ucfirst($qry_['code_number']));
    $desc = trim(ucfirst($qry_['description']));
    $unit_issue = trim(ucfirst($qry_['unit_issue']));
    $qty = trim(ucfirst($qry_['qty_received']));
    $unit_cost = trim(ucfirst($qry_['qty_cost']));
    $total = trim(ucfirst($qry_['value']));
    $balances = trim(ucfirst($qry_['balances']));
    $last_purchase = trim(ucfirst($qry_['date']));
    $remarks = trim(ucfirst($qry_['remarks']));

        $vara = 'Value'.$i.'A';
        $varb = 'Value'.$i.'B';
        $varc = 'Value'.$i.'C';
        $vard = 'Value'.$i.'D';
        $vare = 'Value'.$i.'E';
        $varf = 'Value'.$i.'F';
        $varg = 'Value'.$i.'G';
        $varh = 'Value'.$i.'H';
        $vari = 'Value'.$i.'I';

    $document->setValue($vara, $sno);
    $document->setValue($varb, $desc);
    $document->setValue($varc, $unit_issue);
    $document->setValue($vard, $qty);
    $document->setValue($vare, $unit_cost);
    $document->setValue($varf, $total);
    $document->setValue($varg, $balances);
    $document->setValue($varh, $last_purchase);
    $document->setValue($vari, $remarks);
    $i++;
    }

}


    $document->setValue('Value1', $section_head);
    $document->setValue('Value2', $purpose);
    $document->setValue('Value3', $rqo);
    $document->setValue('Value4', $store_clerk);
    $document->setValue('Value5', $proc_officer);
    $document->setValue('Value6', $vbb);
    $document->setValue('Value7', $accountant);


//    $document->setValue('weekday', date('dS-M-Y'));

$file_ = 'requests/'.$title .' .docx';
$document->save($file_);
