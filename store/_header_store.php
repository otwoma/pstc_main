<?php
/**
 * Created by PhpStorm.
 * User: Otwoma
 * Date: 4/27/2017
 * Time: 5:04 PM
 */

session_start();
error_reporting(0);
include('../_functions.php');
include('../_variables.php');

$mid = $_SESSION['_user_id'];
$mit = $_SESSION['_type'];


if ($mid == '' || $mit != 'Store') {
    header("location: ../logout.php");
    die();
} else {
    ob_start('compress_html');
}


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title><?php echo $school_name;?> | Store Portal</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../assets/images/kenya_prisons_logo.jpg"/>
    <script type="text/javascript" src="../assets/js/jquery-1.10.2.min.js"></script>
    <link rel="stylesheet" href="../assets/css/jquery.dataTables.css"/>

    <script type="text/javascript" language="javascript" src="../assets/js/jquery.dataTables.js"></script>
    <link href="../assets/css/bootstrap.css" rel="stylesheet">
    <link href="../assets/css/font-awesome.min.css" rel="stylesheet">
    <link href="../assets/css/pace.css" rel="stylesheet">
    <link rel="stylesheet" href="../assets/css/jquery.dataTables.css"/>

    <link href="../assets/css/index.css" rel="stylesheet">
    <link href="../assets/css/matrix.css" rel="stylesheet">
    <link href="../assets/css/matrix-skin.css" rel="stylesheet">
    <link href="../assets/css/select2.min.css" rel="stylesheet">

</head>

<body class="overflow-hidden">
<div id="overlay" class="transparent"></div>


<div id="wrapper" class="preload">
    <div id="top-nav" class="skin-6 fixed hidden-print">
        <div class="brand">
            <span>PSTC</span>
            <span class="text-toggle"> Store</span>
        </div>
        <button type="button" class="navbar-toggle pull-left" id="sidebarToggle">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <button type="button" class="navbar-toggle pull-left hide-menu" id="menuToggle">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>



        <ul class="nav-notification clearfix hidden-print">

            <li class="profile dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <strong><?php echo account_name();?> </strong>
                    <span><i class="fa fa-chevron-down"></i></span>
                </a>
                <ul class="dropdown-menu">
                    <li>
                        <a class="clearfix" href="#">
                            <img src="<?php echo avatar();?>">
                            <div class="detail">
                                <strong><?php echo account_name();?> </strong>
                            </div>
                        </a>
                    </li>
                    <li><a tabindex="-1" href="account" class="theme-setting"><i class="fa fa-cogs"></i> Account Settings</a></li>
                    <li class="divider"></li>
                    <li><a tabindex="-1" class="main-link logoutConfirm_open" href="#logoutConfirm"><i class="fa fa-lock fa-lg"></i> Log out</a></li>
                </ul>
            </li>
        </ul>
    </div>


    <aside class="fixed skin-6 hidden-print">
        <div class="sidebar-inner scrollable-sidebar">
            <div class="size-toggle">
                <a class="btn btn-sm" id="sizeToggle">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <a class="btn btn-sm pull-right logoutConfirm_open"  href="#logoutConfirm">
                    <i class="fa fa-power-off"></i>
                </a>
            </div>
            <div class="user-block clearfix">
                <img width="55px" src="<?php echo avatar();?>" alt="Avatar">

                <div class="detail">
                    <strong><?php echo account_name();?> </strong>

                    <ul class="list-inline">
                        <li><a href="account" class="no-margin">Account Settings</a></li>
                    </ul>
                </div>
            </div>

            <div class="main-menu hidden-print">
                <ul>
                    <li class="dashboard_mu">
                        <a href="dashboard">
								<span class="menu-icon">
									<i class="fa fa-desktop fa-lg"></i>
								</span>
								<span class="text">
									Dashboard
								</span>
                            <span class="menu-hover"></span>
                        </a>
                    </li>

                    <li class="inventory_mu">
                        <a href="inventory">
								<span class="menu-icon">
									<i class="fa fa-sliders fa-lg"></i>
								</span>
								<span class="text">
                                    Inventory
								</span>
                            <span class="menu-hover"></span>
                        </a>
                    </li>

                    <li class="category_mu">
                        <a href="add_category">
								<span class="menu-icon">
									<i class="fa fa-crosshairs fa-lg"></i>
								</span>
								<span class="text">
                                    Item Category
								</span>
                            <span class="menu-hover"></span>
                        </a>
                    </li>

                    <li class="openable open purchases_mu">
                        <a href="#">
								<span class="menu-icon">
									<i class="fa fa-cart-arrow-down fa-lg"></i>
								</span>
								<span class="text">
									New Purchase
								</span>
                            <span class="menu-hover"></span>
                        </a>
                        <ul class="submenu">
                            <li class="procure_mu"><a href="purchase_request"><span class="submenu-label"><i class="fa fa-shopping-basket fa-lg"></i> Purchase Request</span></a></li>
                            <li class="Purchase_records_mu"><a href="purchase_record"><span class="submenu-label"><i class="fa fa-list fa-lg"></i> Purchase Records</span></a></li>
                        </ul>
                    </li>

                    <li class="reports_mu">
                        <a href="#">
								<span class="menu-icon">
									<i class="fa fa-file-excel-o fa-lg"></i>
								</span>
								<span class="text">
                                Reports
								</span>
                            <span class="menu-hover"></span>
                        </a>
                    </li>

                    <li class="account_mu">
                        <a href="account">
								<span class="menu-icon">
									<i class="fa fa-cogs fa-lg"></i>
								</span>
								<span class="text">
                                    Account Settings
								</span>
                            <span class="menu-hover"></span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </aside>

