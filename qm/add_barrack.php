<?php
/**
 * Created by PhpStorm.
 * User: Otwoma
 * Date: 5/24/2017
 * Time: 12:58 PM
 */

include '_header_qm.php';
?>

    <script type="text/javascript" charset="utf-8">
        $(function() {
            $('#responsiveTable').dataTable();
            $('#responsiveTable2').dataTable();
        } );
    </script>

<div id="main-container">
    <div class="small-header transition animated fadeIn">
        <div class="hpanel">
            <div class="panel-body">
                <h4 class="font-light m-b-xs">
                    <i class="fa fa-plus"></i> Add Barrack
                </h4>
            </div>
        </div>
    </div>

    <div class="content animate-panel">
        <div class="row">
            <div class="col-lg-9">
                <div class="hpanel">
                    <div class="panel-heading">
                        Fill the following details to add a new barrack
                    </div>

                    <div class="panel-body">
                        <form action="../_actions.php" method="post" enctype="multipart/form-data" class="form-horizontal">
                            <div class="form-group"><label class="col-sm-2 control-label">Barrack Name</label>

                                <div class="col-sm-10">
                                    <div class="input-group m-b"><span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span> <input name="name" type="text" placeholder="Barrack Name" class="form-control"></div>
                                </div>
                            </div>
                            <div class="form-group"><label for="type_select" class="col-sm-2 control-label">Type</label>

                                <div class="col-sm-10"><div class="input-group m-b"><span class="input-group-addon">T </span>
                                        <select id="type_select" class="form-control m-b" name="category">
                                            <?php echo gender_options();?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group"><label class="col-sm-2 control-label">Capacity</label>

                                <div class="col-sm-10">
                                    <div class="input-group m-b"><span class="input-group-addon"><i class="fa fa-adjust"></i></span> <input name="capacity" type="text" placeholder="Capacity" class="form-control"></div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-12 col-sm-offset-6">
                                    <button class="btn btn-primary" type="submit"><span class="fa fa-send"></span> Save
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="padding-md">
                        <div class="padding-md col-md-8 col-lg-push-3">
                            <div class="panel panel-default table-responsive">
                                <div class="padding-sm font-16">
                                    Listing Barracks
                                </div>
                                <table class="table table-striped" id="responsiveTable2">
                                    <thead>
                                    <tr>
                                        <th width="" align="left">#</th>
                                        <th width="" align="center"><span class=""></span>Name</th>
                                        <th width="" align="right"><span class=""></span>Type</th>
                                        <th width="" align="right"><span class=""></span>Capacity</th>
                                        <th width="" align="right"><span class=""></span>Open Slots</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $list = mysql_query("SELECT id , name, category, capacity FROM barracks ORDER BY id ASC");
                                        while($list_result = mysql_fetch_array($list)){
                                            ?>
                                        <tr>
                                            <td align="left"><?php echo $list_result['id']?></td>
                                            <td align="left"><?php echo $list_result['name']?></td>
                                            <td align="left"><?php echo gender_label($list_result['category'])?></td>
                                            <td align="left"><?php echo $list_result['capacity']?></td>
                                            <td align="left"><?php ?></td>
                                        </tr>
                                        <?php
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

    <script>$('.barracks_mu').addClass('active')</script>

<?php include'../_footer.php';?>