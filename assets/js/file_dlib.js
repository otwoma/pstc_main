$(function(){

	var filemanager = $('.filemanager'),
		breadcrumbs = $('.breadcrumbs'),
		fileList = filemanager.find('.data');


	$.get('_files_dlib', function(data) {

		var response = [data],
			currentPath = '',
			breadcrumbsUrls = [];

		var folders = [],
			files = [];

		$(window).on('hashchange', function(){

			goto(window.location.hash);


		}).trigger('hashchange');



		filemanager.find('input').on('input', function(e){

			folders = [];
			files = [];

			var value = this.value.trim();

			if(value.length) {

				filemanager.addClass('searching');


				window.location.hash = 'search=' + value.trim();

			}

			else {

				filemanager.removeClass('searching');
				window.location.hash = encodeURIComponent(currentPath);

			}

		}).on('keyup', function(e){

			var search = $(this);

			if(e.keyCode == 27) {

				search.trigger('focusout');

			}


		}).focusout(function(e){


			var search = $(this);

			if(!search.val().trim().length) {

				window.location.hash = encodeURIComponent(currentPath);
				search.parent().find('span').show();



			}

		});



		fileList.on('click', 'li.folders', function(e){
			e.preventDefault();

			var nextDir = $(this).find('a.folders').attr('href');

			if(filemanager.hasClass('searching')) {

				breadcrumbsUrls = generateBreadcrumbs(nextDir);

				filemanager.removeClass('searching');
				filemanager.find('input[type=search]').val('');
				filemanager.find('span').show();
                filemanager.find('.new_fitems').show();
			}
			else {
				breadcrumbsUrls.push(nextDir);
			}

			window.location.hash = encodeURIComponent(nextDir);
			currentPath = nextDir;
		});




		breadcrumbs.on('click', 'a', function(e){
			e.preventDefault();

			var index = breadcrumbs.find('a').index($(this)),
				nextDir = breadcrumbsUrls[index];

			breadcrumbsUrls.length = Number(index);

			window.location.hash = encodeURIComponent(nextDir);

		});



		function goto(hash) {

			hash = decodeURIComponent(hash).slice(1).split('=');

			if (hash.length) {
				var rendered = '';


				if (hash[0] === 'search') {

					filemanager.addClass('searching');
					rendered = searchData(response, hash[1].toLowerCase());

					if (rendered.length) {
						currentPath = hash[0];
						render(rendered);
					}
					else {
						render(rendered);
					}

				}


				else if (hash[0].trim().length) {

					rendered = searchByPath(hash[0]);

					if (rendered.length) {

						currentPath = hash[0];
						breadcrumbsUrls = generateBreadcrumbs(hash[0]);
						render(rendered);

					}
					else {
						currentPath = hash[0];
						breadcrumbsUrls = generateBreadcrumbs(hash[0]);
						render(rendered);
					}

				}



				else {
					currentPath = data.path;
					breadcrumbsUrls.push(data.path);
					render(searchByPath(data.path));
				}
			}
		}

		function generateBreadcrumbs(nextDir){
			var path = nextDir.split('/').slice(0);
			for(var i=1;i<path.length;i++){
				path[i] = path[i-1]+ '/' +path[i];
			}
			return path;
		}


		function searchByPath(dir) {
			var path = dir.split('/'),
				demo = response,
				flag = 0;

			for(var i=0;i<path.length;i++){
				for(var j=0;j<demo.length;j++){
					if(demo[j].name === path[i]){
						flag = 1;
						demo = demo[j].items;
						break;
					}
				}
			}

			demo = flag ? demo : [];
			return demo;
		}


		function searchData(data, searchTerms) {

			data.forEach(function(d){
				if(d.type === 'folder') {

					searchData(d.items,searchTerms);

					if(d.name.toLowerCase().match(searchTerms)) {
						folders.push(d);
					}
				}
				else if(d.type === 'file') {
					if(d.name.toLowerCase().match(searchTerms)) {
						files.push(d);
					}
				}
			});
			return {folders: folders, files: files};
		}

                filemanager.on('click', '#clear_search_results' , function(clear_search){
                    filemanager.removeClass('searching');
                    window.location.hash = encodeURIComponent(currentPath);
                    filemanager.find('input[type=search]').val('');
                    filemanager.find('.new_fitems').show();
                });



                filemanager.on('click', '.tfile_ghost_action' ,function (filedownload) {
                    var gid=$(this).data(gid);
                    var kid = gid.gid;
                    $.post('_ajax.php', 'download_staff_file='+kid,
                        function (get_file) {
                            $("#create_in").append(get_file);
                        });
                });

		function render(data) {

			var scannedFolders = [],
				scannedFiles = [];

			if(Array.isArray(data)) {

				data.forEach(function (d) {

					if (d.type === 'folder') {
						scannedFolders.push(d);
					}
					else if (d.type === 'file') {
						scannedFiles.push(d);
					}

				});

			}
			else if(typeof data === 'object') {

				scannedFolders = data.folders;
				scannedFiles = data.files;

			}


			fileList.empty().hide();

			if(!scannedFolders.length && !scannedFiles.length) {
				filemanager.find('.nothingfound').show();
			}
			else {
				filemanager.find('.nothingfound').hide();
			}

			if(scannedFolders.length) {

				scannedFolders.forEach(function(f) {

					var itemsLength = f.items.length,
						name = escapeHTML(f.name),
                        fid = escapeHTML(f.fid),
                        fir = escapeHTML(f.r),
						icon = '<span class="icon folder"></span>';

					if(itemsLength) {
						icon = '<span class="icon folder full"></span>';
                        xclass= 'full';
					}

					if(itemsLength == 1) {
						itemsLength += ' item';
					}
					else if(itemsLength > 1) {
						itemsLength += ' items';
					}
					else {
						itemsLength = 'Empty';
					}

                    if(fid=='ghost_bin') {
                        icon = '<span class="icon recycle_bin fa fa-trash-o"></span>';
                        var folder = $('<li id="'+ fid +'" class="folders recycle_bin_holder context-menu-rbin box menu-1"><a href="'+ f.path +'" data-still="'+ fid +'" class="folders">'+icon+'<span class="name">' + name + '</span> <span class="details">' + itemsLength + '</span></a></li>');

                    }else if(fir=='bf'){
                        var folder = $('<li id="'+ fid +'" class="folders box menu-1 deleted-folder"><a data-still="'+ fid +'" class="folders">'+icon+'<span class="name">' + name + '</span> <span class="details">' + itemsLength + '</span></a></li>');

                    }else{
                        var folder = $('<li id="'+ fid +'" class="folders context-menu-one box menu-1"><a href="'+ f.path +'" data-still="'+ fid +'" class="folders">'+icon+'<span class="name">' + name + '</span> <span class="details">' + itemsLength + '</span></a></li>');

                    }


					folder.appendTo(fileList);
				});

			}

			if(scannedFiles.length) {

				scannedFiles.forEach(function(f) {

					var fileSize = bytesToSize(f.size),
                        gid= escapeHTML(f.name),
						name = escapeHTML(f.name),
                        bin = escapeHTML(f.r),
						fileType = name.split('.'),
						icon = '<span class="icon file"></span>';

					fileType = fileType[fileType.length-1].toLowerCase();


					icon = '<span class="icon file f-'+fileType+'">.'+fileType+'</span>';

                    if(bin=='bin'){
var file = $('<li id="'+ f.fid+'" data-file="'+ f.ext+'" class="files context-menu-dfile box menu-1 deleted-file"><a href="_actions?f_dlib='+ f.fid +'" class="tfile_ghost_action" data-loc="'+ f.path +'" data-gid="'+f.fid+'" class="files">'+icon+'<span class="name">'+ name +'</span> <span class="details">'+fileSize+'</span></a></li>');


                    }else{

                        var file = $('<li id="'+ f.fid+'" data-file="'+ f.ext+'" class="files context-menu-file box menu-1"><a href="_actions?f_dlib='+ f.fid +'" class="tfile_ghost_action" data-loc="'+ f.path +'" data-gid="'+f.fid+'" class="files">'+icon+'<span class="name">'+ name +'</span> <span class="details">'+fileSize+'</span></a></li>');
                    }

					file.appendTo(fileList);
				});

			}



			var url = '';

			if(filemanager.hasClass('searching')){

				url = '<span>Showing Search Results <a title="Return to previous location" id="clear_search_results" href="javascript:(void);">Clear results</a></span>';
				fileList.removeClass('animated');
                filemanager.find('.new_fitems').hide();

			}
			else {

				fileList.addClass('animated');

				breadcrumbsUrls.forEach(function (u, i) {

					var name = u.split('/');

					if (i !== breadcrumbsUrls.length - 1) {
						url += '<a class="breadc" href="'+u+'"><span class="folderName">' + name[name.length-1] + '</span></a> <span class="arrow">→</span> ';
					}
					else {


						url += '<span id="current_ghost_folder" data-tale="'+ u +'" class="folderName">' + name[name.length-1] + '</span>';
					}

				});

			}


			breadcrumbs.text('').append(url);
			fileList.animate({'display':'inline-block'});
		}

		function escapeHTML(text) {
			return text.replace(/\&/g,'&amp;').replace(/\</g,'&lt;').replace(/\>/g,'&gt;');
		}

		function bytesToSize(bytes) {
			var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
			if (bytes == 0) return '0 Bytes';
			var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
			return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
		}

	});
});
