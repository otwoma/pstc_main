<?php
/*Code by :: NEO*/

$tribes = array(
    array('id' => '1','tribe_name' => 'Akamba (Kamba)
'),
    array('id' => '2','tribe_name' => 'Boni
'),
    array('id' => '3','tribe_name' => 'Boran
'),
    array('id' => '4','tribe_name' => 'Elkony
'),
    array('id' => '5','tribe_name' => 'Elmolo
'),
    array('id' => '6','tribe_name' => 'Embu
'),
    array('id' => '7','tribe_name' => 'Gabbra
'),
    array('id' => '8','tribe_name' => 'Galla (Oromo)
'),
    array('id' => '9','tribe_name' => 'Gikuyu (Kikuyu)
'),
    array('id' => '10','tribe_name' => 'Giriama (Giryama)
'),
    array('id' => '11','tribe_name' => 'Giryama (Giriama)
'),
    array('id' => '12','tribe_name' => 'Gusii (Kisii)
'),
    array('id' => '13','tribe_name' => 'Issa (Somali)
'),
    array('id' => '14','tribe_name' => 'Iteso (Teso)
'),
    array('id' => '15','tribe_name' => 'Kalenjin
'),
    array('id' => '16','tribe_name' => 'Kamba (Akamba)
'),
    array('id' => '17','tribe_name' => 'Keiyo
'),
    array('id' => '18','tribe_name' => 'Kikuyu (Gikuyu)
'),
    array('id' => '19','tribe_name' => 'Kipsigis
'),
    array('id' => '20','tribe_name' => 'Kisii (Gusii)
'),
    array('id' => '21','tribe_name' => 'Kuria
'),
    array('id' => '22','tribe_name' => 'Luhya
'),
    array('id' => '23','tribe_name' => 'Luo
'),
    array('id' => '24','tribe_name' => 'Maasai (Masai)
'),
    array('id' => '25','tribe_name' => 'Marakwet
'),
    array('id' => '26','tribe_name' => 'Masai (Maasai)
'),
    array('id' => '27','tribe_name' => 'Mbere
'),
    array('id' => '28','tribe_name' => 'Meru
'),
    array('id' => '29','tribe_name' => 'Mikikenda
'),
    array('id' => '30','tribe_name' => 'Njemps
'),
    array('id' => '31','tribe_name' => 'Orma
'),
    array('id' => '32','tribe_name' => 'Oromo (Galla)
'),
    array('id' => '33','tribe_name' => 'Pakot
'),
    array('id' => '34','tribe_name' => 'Pokomo
'),
    array('id' => '35','tribe_name' => 'Pokot
'),
    array('id' => '36','tribe_name' => 'Rendille
'),
    array('id' => '37','tribe_name' => 'Sabaot
'),
    array('id' => '38','tribe_name' => 'Samburu
'),
    array('id' => '39','tribe_name' => 'Segeju
'),
    array('id' => '40','tribe_name' => 'Somali (Issa)
'),
    array('id' => '41','tribe_name' => 'Swahili
'),
    array('id' => '42','tribe_name' => 'Taita
'),
    array('id' => '43','tribe_name' => 'Taveta
'),
    array('id' => '44','tribe_name' => 'Terik
'),
    array('id' => '45','tribe_name' => 'Teso (Iteso)
'),
    array('id' => '46','tribe_name' => 'Tharaka
'),
    array('id' => '47','tribe_name' => 'Tugen
'),
    array('id' => '48','tribe_name' => 'Tuken
'),
    array('id' => '49','tribe_name' => 'Turgen
'),
    array('id' => '50','tribe_name' => 'Turkana')
);