<?php
session_start();
error_reporting(0);
include "_functions.php";
?>
<html>
        <head>
            <title>
                <?php  include '_variables.php'; echo $school_name." :: Portal Login";
                if ($_GET['loginuser'] == 'failed') {$message = 'Error Loging in, Try again.';}?>
            </title>
            <link rel="shortcut icon" href="assets/images/prisons.jpeg"/>
            <script type="text/javascript" src="assets/js/jquery-1.10.2.min.js"></script>
            <link href="assets/css/bootstrap.css" rel="stylesheet">
            <link href="assets/css/font-awesome.min.css" rel="stylesheet">
            <link href="assets/css/matrix.css?<?php echo time();?>" rel="stylesheet">
            <style>body {
                    display: table;
                    padding: 3em 15px;
                    background-color:#fff;


              /*      background-image: -webkit-radial-gradient(closest-corner, rgba(16, 47, 70, 0) 58%, rgba(16, 47, 70, 0.26)), -webkit-linear-gradient(108deg, #37BC9B, #4D407C 90%);
                    background-image: -moz-radial-gradient(closest-corner, rgba(16, 47, 70, 0) 58%, rgba(16, 47, 70, 0.26)), -moz-linear-gradient(108deg, #37BC9B, #4D407C 90%);
                    background-image: -ms-radial-gradient(closest-corner, rgba(16, 47, 70, 0) 58%, rgba(16, 47, 70, 0.26)), -ms-linear-gradient(108deg, #37BC9B, #4D407C 90%);
                */



                }

                .site-footer {
                    position: fixed;
                    bottom: -10px;
                    margin-left:-1%;

                    width: 100%;
                    background: #003300
                ;
                    border-top: 3px solid #6ec0ab;
                    opacity: 0.8;
                    cursor:auto ;
                    color:#F6F6F6;
                    font-size: 13px;
                }

                .site-footer:hover {
                    bottom: 0;
                    -webkit-transition-duration: 0.3s;
                    color:white;
                    font-size: 13px;
                    border-top:0;
                    background-color: transparent;
                }
                .site-footer__sticky {
                    padding: 6px 0;
                    margin-bottom: 1px;
                }
                .site-footer__detail{list-style:none; width: 100%;text-align: center;    }
                .site-footer__detail>li{display:inline-block;padding-right:10px;padding-left:10px}
                .site-footer__detail>li:first-child{padding-left:0;}

                #overlay {
                    position:fixed;
                    width:100%;
                    height:100%;
                    /*background:rgba(255,255,255,0.8);*/
                    background:rgba(0,0,0,0.8);
                    left:0;
                    top:0;
                    z-index:98;
                    display:none;
                    cursor:pointer;
                }

input{height:135px;!important;}
            </style>
        </head>

    <body>


    <div align="center" style="text-align: center; margin-top: 20px;  color: #FB1F1F;" id="message"><?php echo "$message";?></div>


    <div class="login-wrapper">
        <div class="text-center" align="center">
            <h2 class="fadeInUp animation-delay7" style="font-weight:bold">
                <img style="background-color: white;padding: 10px;border-radius: 3px;" src="assets/images/kenya_prison_service.png" alt=""><br>
                <span style="color:#336738;">Prison Staff Training College</span>
            </h2>
        </div>
        <div class="login-widget animation-delay1">
            <div class="panel panel-default">
                <div class="panel-heading clearfix" style="padding:10px;background-color:#356934;color:#fff;">
                    <div align="center" style="font-size: 20px;">
                      Welcome
                    </div>


                </div>
                <div class="panel-body" style="padding:20px; border-bottom:2px solid #346930;;">
                    <form action="_handle_login.php" method="post" class="form-login">
                        <div class="form-group">
                            <label><span class="fa fa-user"></span> Username </label>
                            <input maxlength="55" minlength="3" required="required" type="text" name="username" placeholder="Username" class="form-control inpt-sm bounceIn animation-delay2" >
                        </div>
                        <div class="form-group">
                            <label><span class="fa fa-asterisk"></span> Password</label>
                            <input required="required" type="password" name="password" placeholder="Password" class="form-control input-smx bounceIn animation-delay4">
                        </div>

                        <button type="submit" id="loginbtn" class="btn btn-primary btn-sm bounceIn animation-delay5 login-link pull-right" value="Login"><i class="fa fa-send"></i> Login</button>

                    </form>
                </div>
            </div>
        </div>


    </body>
        <script src="assets/js/bootstrap.js"></script>
        <script type="text/javascript">
            window.onload = function()
            {
                timedHide(document.getElementById('message'), 27);
            };

            function timedHide(element, seconds)
            {
                if (element) {
                    setTimeout(function() {
                        element.style.display = 'none';
                    }, seconds*200);
                }
            }
        </script>

</html>