-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 22, 2017 at 01:55 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pstc`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE `accounts` (
  `id` bigint(20) NOT NULL,
  `username` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `password_state` int(11) NOT NULL DEFAULT '1',
  `account_type` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `theme` varchar(30) NOT NULL DEFAULT 'skin-10',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `avatar` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`id`, `username`, `name`, `password`, `password_state`, `account_type`, `status`, `theme`, `created_at`, `avatar`) VALUES
(1, 'admin', 'Owen Otwoma', '3oG7melKrfXJhhvEt1BTKEJ3Tjsc4pIajOSy+IH0r7A=', 1, 'Admin', 1, 'default', '2017-04-15 09:45:56', ''),
(2, 'registry', 'Blade', '3oG7melKrfXJhhvEt1BTKEJ3Tjsc4pIajOSy+IH0r7A=', 1, 'registry', 1, 'default', '2017-04-11 10:39:38', ''),
(3, '10001', 'Makwata Jane Achilo--Class', 'vk5unTdCXASHqiW6cyv+TEi/h4kf7IRutWTG8mJsV2Q=', 1, 'Chief Instructor', 1, 'skin-10', '2017-05-03 10:15:27', ''),
(4, 'john', 'Musila John Mwene', 'veusKYyubnXcFg2dQ2NBdCA2zqLHUp/BC7Tdhjkymxk=', 1, 'Chief Instructor', 1, 'skin-10', '2017-04-20 13:17:13', ''),
(5, '10002', 'Oliver Mang''enyi Masai--Field', 'ruIcR4BuxlssQgEfdD1N1x5EAkB0VibL1uzN1K9d7O8=', 1, 'Store', 1, 'skin-10', '2017-05-04 09:05:57', ''),
(6, '12345678910', 'Philip', '3oG7melKrfXJhhvEt1BTKEJ3Tjsc4pIajOSy+IH0r7A=', 1, 'registry-staff', 1, 'default', '2017-04-11 10:39:38', ''),
(7, 'john', 'Musila John Mwene', '3oG7melKrfXJhhvEt1BTKEJ3Tjsc4pIajOSy+IH0r7A=', 1, 'Instructor', 1, 'skin-10', '2017-04-20 07:17:13', ''),
(8, 'class_c', 'Class Coordinator', '3oG7melKrfXJhhvEt1BTKEJ3Tjsc4pIajOSy+IH0r7A=', 1, 'cc', 1, 'skin-10', '2017-04-28 06:22:23', '');

-- --------------------------------------------------------

--
-- Table structure for table `assign_units`
--

CREATE TABLE `assign_units` (
  `id` int(11) NOT NULL,
  `unit_id` varchar(255) NOT NULL,
  `class_id` varchar(255) NOT NULL,
  `instructor_id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `assign_units`
--

INSERT INTO `assign_units` (`id`, `unit_id`, `class_id`, `instructor_id`) VALUES
(1, '2', '4', '1'),
(2, '2', '5', '1'),
(3, '2', '6', '1'),
(4, '7', '1', '2'),
(5, '7', '2', '2'),
(6, '7', '3', '2');

-- --------------------------------------------------------

--
-- Table structure for table `classes`
--

CREATE TABLE `classes` (
  `id` int(11) NOT NULL,
  `company_id` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `year` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `class_capacity` varchar(255) NOT NULL,
  `instructor_incharge` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `classes`
--

INSERT INTO `classes` (`id`, `company_id`, `name`, `year`, `created_at`, `status`, `class_capacity`, `instructor_incharge`) VALUES
(1, 'GROUP 1', 'CLASS 1A', '2017', '2017-04-24 07:35:50', 1, '45', ''),
(2, 'GROUP 1', 'CLASS  2A', '2017', '2017-04-24 07:36:57', 1, '45', ''),
(3, 'GROUP 1', 'CLASS 3A', '2017', '2017-04-24 07:37:23', 1, '55', ''),
(4, 'GROUP 2', 'CLASS  2B', '2017', '2017-04-24 07:38:39', 1, '45', ''),
(5, 'GROUP 2', 'CLASS 1B', '2017', '2017-04-24 07:39:30', 1, '45', ''),
(6, 'GROUP 2', 'CLASS 3B', '2017', '2017-04-24 07:41:42', 1, '55', ''),
(7, '2', 'CLASS 4B', '2017', '2017-04-24 12:49:47', 1, '55', ''),
(8, '2', 'CLASS 5B', '2017', '2017-04-24 12:50:32', 1, '55', '');

-- --------------------------------------------------------

--
-- Table structure for table `class_instructors`
--

CREATE TABLE `class_instructors` (
  `id` int(11) NOT NULL,
  `class` varchar(255) NOT NULL,
  `instructor_id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `class_instructors`
--

INSERT INTO `class_instructors` (`id`, `class`, `instructor_id`) VALUES
(1, 'CLASS 1B', '2'),
(2, 'CLASS 1A', '3'),
(3, 'CLASS 4B', '4');

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE `companies` (
  `company_id` int(11) NOT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`company_id`, `company_name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Company A', 1, '2017-04-16 09:57:09', '0000-00-00 00:00:00'),
(2, 'Company B', 1, '2017-04-16 10:03:00', '0000-00-00 00:00:00'),
(3, 'Company C', 1, '2017-04-18 14:36:29', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `consumables`
--

CREATE TABLE `consumables` (
  `id` int(12) NOT NULL,
  `s_no` varchar(255) NOT NULL,
  `item_name` varchar(255) NOT NULL,
  `category` int(11) NOT NULL,
  `quantity` bigint(20) NOT NULL,
  `unit` varchar(255) NOT NULL,
  `threshold` bigint(20) NOT NULL,
  `remark` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `consumables`
--

INSERT INTO `consumables` (`id`, `s_no`, `item_name`, `category`, `quantity`, `unit`, `threshold`, `remark`, `status`, `created_at`, `updated_at`) VALUES
(1, '0001', 'Bic Ball Point Pens', 2, 100, 'Packets', 35, 'n/a', 1, '2017-05-15 15:06:20', '2017-05-15 15:06:20');

-- --------------------------------------------------------

--
-- Table structure for table `consumable_categories`
--

CREATE TABLE `consumable_categories` (
  `id` int(4) NOT NULL,
  `category_name` varchar(255) NOT NULL,
  `category_type` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `consumable_categories`
--

INSERT INTO `consumable_categories` (`id`, `category_name`, `category_type`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Uniforms and Gears', 'Consumables', 1, '2017-05-12 09:21:11', '2017-05-12 09:21:11'),
(2, 'Stationery', 'Consumables', 1, '2017-05-15 14:54:46', '2017-05-15 14:54:46');

-- --------------------------------------------------------

--
-- Table structure for table `consumable_movement`
--

CREATE TABLE `consumable_movement` (
  `id` bigint(20) NOT NULL,
  `consumable_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `quantity_taken` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `counties`
--

CREATE TABLE `counties` (
  `id` int(11) NOT NULL,
  `county_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `counties`
--

INSERT INTO `counties` (`id`, `county_name`) VALUES
(1, 'NAIROBI'),
(2, 'MOMBASA'),
(3, 'KISUMU'),
(4, 'BOMET'),
(5, 'BUNGOMA'),
(6, 'BUSIA'),
(7, 'ELGEYO/MARAKWET'),
(8, 'EMBU'),
(9, 'GARISSA'),
(10, 'HOMA BAY'),
(11, 'ISIOLO'),
(12, 'KAJIADO'),
(13, 'KAKAMEGA'),
(14, 'KERICHO'),
(15, 'KIAMBU'),
(16, 'KILIFI'),
(17, 'KIRINYAGA'),
(18, 'KISII'),
(19, 'KITUI'),
(20, 'KWALE'),
(21, 'LAIKIPIA'),
(22, 'LAMU'),
(23, 'MAKUENI'),
(24, 'MANDERA'),
(25, 'MARSABIT'),
(26, 'MERU'),
(27, 'MIGORI'),
(28, 'MACHAKOS'),
(29, 'MURANGA'),
(30, 'NAKURU'),
(31, 'NANDI'),
(32, 'NAROK'),
(33, 'NYAMIRA'),
(34, 'NYANDARUA'),
(35, 'NYERI'),
(36, 'SAMBURU'),
(37, 'SIAYA'),
(38, 'TAITA TAVETA'),
(39, 'TANA RIVER'),
(40, 'THARAKA - NITHI'),
(41, 'TRANS NZOIA'),
(42, 'TURKANA'),
(43, 'UASIN GISHU'),
(44, 'VIHIGA'),
(45, 'WAJIR'),
(46, 'WEST POKOT'),
(47, 'BARINGO');

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `department_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`department_id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Psychology', '2017-04-15 09:47:44', '0000-00-00 00:00:00'),
(2, 'Amunition', '2017-04-15 09:48:31', '0000-00-00 00:00:00'),
(3, 'Linguistics', '2017-04-18 14:35:05', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `designations`
--

CREATE TABLE `designations` (
  `designation_id` int(11) NOT NULL,
  `name` varchar(22) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `designations`
--

INSERT INTO `designations` (`designation_id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Sergeant', '2017-04-15 09:48:54', '0000-00-00 00:00:00'),
(2, 'Corporal', '2017-04-15 09:49:04', '0000-00-00 00:00:00'),
(3, 'Constable', '2017-04-15 09:49:10', '0000-00-00 00:00:00'),
(4, 'Snr. Sergeant', '2017-04-20 07:04:46', '0000-00-00 00:00:00'),
(5, 'Mang''enyi Oliver Masai', '2017-04-27 14:44:00', '0000-00-00 00:00:00'),
(6, 'Mang''enyi Oliver Masai', '2017-04-27 15:14:29', '0000-00-00 00:00:00'),
(7, 'Makwata Jane Achilo--C', '2017-05-03 10:15:27', '0000-00-00 00:00:00'),
(8, 'Oliver Mang''enyi Masai', '2017-05-04 09:05:57', '0000-00-00 00:00:00'),
(9, 'Stationery', '2017-05-12 09:15:44', '0000-00-00 00:00:00'),
(10, 'Uniforms & Gears', '2017-05-12 09:18:53', '0000-00-00 00:00:00'),
(11, 'Uniforms and Gears', '2017-05-12 09:21:11', '0000-00-00 00:00:00'),
(12, 'Stationery', '2017-05-15 14:54:46', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `dis_recruits`
--

CREATE TABLE `dis_recruits` (
  `dis_recruit_id` bigint(20) NOT NULL,
  `old_recruit_id` bigint(20) NOT NULL,
  `replaced_id` bigint(20) NOT NULL,
  `date_replaced` timestamp NULL DEFAULT NULL,
  `created_by` int(11) NOT NULL DEFAULT '0',
  `notes` text NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dis_recruits`
--

INSERT INTO `dis_recruits` (`dis_recruit_id`, `old_recruit_id`, `replaced_id`, `date_replaced`, `created_by`, `notes`, `status`, `created_at`, `updated_at`) VALUES
(1, 3, 0, '2017-05-16 10:42:15', 2, '', 1, '2017-05-16 10:42:15', NULL),
(2, 3, 0, '2017-05-16 10:46:19', 2, 'aadad', 1, '2017-05-16 10:46:19', NULL),
(3, 3, 0, '2017-05-16 10:53:11', 2, 'aadad', 1, '2017-05-16 10:53:11', NULL),
(4, 5, 0, '2017-05-17 09:03:46', 2, 'aadad', 1, '2017-05-17 09:03:46', NULL),
(5, 5, 0, '2017-05-17 09:44:04', 2, 'aadad', 1, '2017-05-17 09:44:04', NULL),
(6, 5, 0, '2017-05-19 03:34:23', 2, 'aadad', 1, '2017-05-19 03:34:23', NULL),
(7, 5, 0, '2017-05-19 03:34:31', 2, 'aadad', 1, '2017-05-19 03:34:31', NULL),
(8, 5, 0, '2017-05-19 03:34:51', 2, 'aadad', 1, '2017-05-19 03:34:51', NULL),
(9, 5, 0, '2017-05-19 03:34:56', 2, 'aadad', 1, '2017-05-19 03:34:56', NULL),
(10, 5, 0, '2017-05-19 03:35:01', 2, 'aadad', 1, '2017-05-19 03:35:01', NULL),
(11, 5, 0, '2017-05-19 08:54:43', 2, 'aadad', 1, '2017-05-19 08:54:43', NULL),
(12, 3, 0, '2017-05-19 11:25:07', 2, 'aadad', 1, '2017-05-19 11:25:07', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `do_lists`
--

CREATE TABLE `do_lists` (
  `id` bigint(20) NOT NULL,
  `account` varchar(255) NOT NULL,
  `task` longtext NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `grades`
--

CREATE TABLE `grades` (
  `grade_id` int(11) NOT NULL,
  `recruit_id` int(11) NOT NULL,
  `instructor_id` int(11) NOT NULL,
  `cat_mark` int(11) NOT NULL,
  `exam_mark` int(11) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `instructors`
--

CREATE TABLE `instructors` (
  `instructor_id` bigint(20) NOT NULL,
  `surname` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `other_name` varchar(255) NOT NULL,
  `service_number` int(50) NOT NULL,
  `designation_id` int(11) NOT NULL,
  `assigned` tinyint(4) NOT NULL DEFAULT '1',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `phone` char(50) DEFAULT NULL,
  `address` varchar(255) NOT NULL,
  `email_address` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `instructors`
--

INSERT INTO `instructors` (`instructor_id`, `surname`, `first_name`, `other_name`, `service_number`, `designation_id`, `assigned`, `status`, `phone`, `address`, `email_address`, `created_at`, `updated_at`) VALUES
(1, 'Musila', 'John', 'Mwene', 20001, 3, 1, 1, '0718258963', '15486-0154', 'jm@gmaill.com', '2017-04-19 11:21:56', '0000-00-00 00:00:00'),
(2, 'Makwata', 'Jane', 'Nafula', 20002, 2, 0, 1, '0725897456', '5874-00658', 'mj@gmail.com', '2017-04-19 11:45:14', '0000-00-00 00:00:00'),
(3, 'Mang''enyi', 'Oliver', 'Masai', 2147483647, 2, 2, 1, '0769784512', '25485-01152', 'om@yahoo.com', '2017-04-27 12:13:53', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `instructor_department`
--

CREATE TABLE `instructor_department` (
  `instructor_department_id` int(11) NOT NULL,
  `instructor_id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `instructor_department`
--

INSERT INTO `instructor_department` (`instructor_department_id`, `instructor_id`, `department_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2017-04-19 14:21:56', '0000-00-00 00:00:00'),
(2, 2, 1, '2017-04-19 14:45:15', '0000-00-00 00:00:00'),
(3, 3, 2, '2017-04-27 14:38:06', '0000-00-00 00:00:00'),
(4, 3, 2, '2017-04-27 14:43:27', '0000-00-00 00:00:00'),
(5, 3, 2, '2017-04-27 15:13:53', '0000-00-00 00:00:00'),
(6, 4, 1, '2017-05-03 10:11:37', '0000-00-00 00:00:00'),
(7, 3, 2, '2017-05-04 08:59:36', '0000-00-00 00:00:00'),
(8, 4, 2, '2017-05-04 08:59:37', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE `logs` (
  `account_id` varchar(255) NOT NULL,
  `login_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ip_address` varchar(255) NOT NULL,
  `logout_time` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `logs`
--

INSERT INTO `logs` (`account_id`, `login_time`, `ip_address`, `logout_time`) VALUES
('1', '2017-04-15 09:46:55', '::1', '1492333728'),
('2', '2017-04-15 10:22:08', '::1', '1492251758'),
('1', '2017-04-15 10:22:46', '::1', '1492333728'),
('1', '2017-04-15 19:17:06', '::1', '1492333728'),
('1', '2017-04-16 08:04:59', '::1', '1492333728'),
('2', '2017-04-16 09:08:57', '::1', '1492334101'),
('1', '2017-04-16 09:15:08', '::1', '1492367793'),
('1', '2017-04-16 09:33:41', '::1', '1492367793'),
('1', '2017-04-16 14:19:48', '::1', '1492367793'),
('1', '2017-04-16 18:35:39', '::1', '1492367793'),
('1', '2017-04-16 18:36:43', '::1', '1492511359'),
('1', '2017-04-18 05:48:40', '::1', '1492511359'),
('3', '2017-04-18 10:28:36', '::1', '1492517682'),
('1', '2017-04-18 10:29:07', '::1', '1492511359'),
('3', '2017-04-18 10:32:16', '::1', '1492517682'),
('3', '2017-04-18 10:33:00', '::1', '1492517682'),
('3', '2017-04-18 10:33:10', '::1', '1492517682'),
('3', '2017-04-18 10:34:24', '::1', '1492517682'),
('3', '2017-04-18 10:41:25', '::1', '1492517682'),
('3', '2017-04-18 10:42:21', '::1', '1492517682'),
('3', '2017-04-18 10:53:02', '::1', '1492517682'),
('3', '2017-04-18 10:56:25', '::1', '1492517682'),
('1', '2017-04-18 11:29:23', '::1', '1492515337'),
('3', '2017-04-18 11:36:03', '::1', '1492517682'),
('1', '2017-04-18 11:52:09', '::1', '1492516336'),
('3', '2017-04-18 11:52:30', '::1', '1492517682'),
('1', '2017-04-18 12:15:15', '::1', '1492594289'),
('1', '2017-04-18 12:20:34', '::1', '1492594289'),
('1', '2017-04-18 12:46:54', '::1', '1492594289'),
('1', '2017-04-18 13:29:22', '::1', '1492594289'),
('1', '2017-04-19 09:22:50', '::1', '1492594289'),
('3', '2017-04-19 09:31:37', '::1', '1492594471'),
('1', '2017-04-19 09:34:48', '::1', '1492676003'),
('3', '2017-04-19 11:04:11', '::1', '1492677062'),
('3', '2017-04-19 11:44:57', '::1', '1492677062'),
('1', '2017-04-20 06:06:28', '::1', '1492676003'),
('3', '2017-04-20 08:13:31', '::1', '1492677062'),
('1', '2017-04-20 08:31:32', '::1', '1492678465'),
('3', '2017-04-20 08:54:33', '::1', '1492679336'),
('3', '2017-04-20 08:54:52', '::1', '1492679336'),
('1', '2017-04-20 09:09:12', '::1', '1492681334'),
('4', '2017-04-20 09:42:36', '::1', '1492681428'),
('1', '2017-04-20 09:43:55', '::1', '1492681732'),
('4', '2017-04-20 09:49:02', '::1', '1492681820'),
('1', '2017-04-20 09:50:28', '::1', '1492687852'),
('1', '2017-04-20 11:31:53', '::1', '1492687930'),
('1', '2017-04-20 11:32:42', '::1', '1492689947'),
('3', '2017-04-20 12:06:17', '::1', '1492691377'),
('1', '2017-04-20 12:29:45', '::1', '1492694001'),
('3', '2017-04-20 13:13:34', '::1', '1492694237'),
('3', '2017-04-20 13:15:04', '::1', '1492694237'),
('1', '2017-04-20 13:15:27', '::1', '1492695012'),
('3', '2017-04-20 13:16:48', '::1', '1492694237'),
('4', '2017-04-20 13:17:25', '::1', '1492694270'),
('1', '2017-04-20 13:17:58', '::1', '1492695012'),
('2', '2017-04-20 13:30:45', '::1', '1492695091'),
('1', '2017-04-20 13:31:43', '::1', '1492695494'),
('4', '2017-04-20 13:38:31', '::1', '1492697380'),
('1', '2017-04-20 14:09:46', '::1', '1492697579'),
('4', '2017-04-20 14:13:09', '::1', '1493037523'),
('1', '2017-04-21 13:26:58', '::1', '1493036383'),
('1', '2017-04-24 06:29:51', '::1', '1493036383'),
('4', '2017-04-24 12:19:50', '::1', '1493037523'),
('1', '2017-04-24 12:38:51', '::1', '1493043495'),
('3', '2017-04-24 14:18:31', '::1', '1493043528'),
('4', '2017-04-24 14:18:54', '::1', '1493043902'),
('3', '2017-04-24 14:25:09', '::1', '1493044302'),
('4', '2017-04-24 14:31:49', '::1', '1493112964'),
('4', '2017-04-25 06:56:21', '::1', '1493112964'),
('3', '2017-04-25 08:32:22', '::1', '1493112446'),
('1', '2017-04-25 09:27:34', '::1', '1493112773'),
('4', '2017-04-25 09:33:02', '::1', '1493112964'),
('3', '2017-04-25 09:36:10', '::1', '1493125721'),
('4', '2017-04-25 10:30:22', '::1', '1493118063'),
('1', '2017-04-25 11:01:13', '::1', '1493125679'),
('3', '2017-04-25 11:31:58', '::1', '1493125721'),
('3', '2017-04-25 11:36:35', '::1', '1493125721'),
('1', '2017-04-25 13:03:15', '::1', '1493125679'),
('4', '2017-04-25 13:06:44', '::1', '1493125663'),
('1', '2017-04-25 13:07:52', '::1', '1493125679'),
('3', '2017-04-25 13:08:08', '::1', '1493125721'),
('4', '2017-04-25 13:08:49', '::1', '1493211338'),
('4', '2017-04-25 14:03:30', '::1', '1493211338'),
('1', '2017-04-26 12:33:12', '::1', '1493211209'),
('4', '2017-04-26 12:53:36', '::1', '1493211338'),
('3', '2017-04-26 12:55:52', '::1', '1493300075'),
('4', '2017-04-27 12:19:23', '::1', '1493298840'),
('3', '2017-04-27 13:14:18', '::1', '1493300075'),
('4', '2017-04-27 13:15:48', '::1', '1493304214'),
('4', '2017-04-27 13:34:52', '::1', '1493304214'),
('4', '2017-04-27 14:36:42', '::1', '1493304214'),
('3', '2017-04-27 14:43:42', '::1', '1493304270'),
('5', '2017-04-27 14:44:36', '::1', '1493305048'),
('4', '2017-04-27 15:11:32', '::1', '1493306040'),
('3', '2017-04-27 15:14:14', '::1', '1493306081'),
('5', '2017-04-27 15:14:55', '::1', '1493368156'),
('5', '2017-04-27 15:27:17', '::1', '1493368156'),
('5', '2017-04-28 06:00:22', '::1', '1493368156'),
('4', '2017-04-28 08:29:23', '::1', '1493368953'),
('5', '2017-04-28 08:42:40', '::1', '1493379431'),
('4', '2017-04-28 11:37:31', '::1', '1493382773'),
('5', '2017-04-28 12:34:02', '::1', '1493713769'),
('5', '2017-04-30 14:48:11', '::1', '1493713769'),
('5', '2017-05-01 12:24:28', '::1', '1493713769'),
('5', '2017-05-01 12:50:04', '::1', '1493713769'),
('3', '2017-05-02 08:29:38', '::1', '1493713862'),
('3', '2017-05-02 08:31:32', '::1', '1493713916'),
('4', '2017-05-02 08:32:49', '::1', '1493714008'),
('3', '2017-05-02 08:33:36', '::1', '1493714145'),
('4', '2017-05-02 08:35:51', '::1', '1493714363'),
('4', '2017-05-02 08:48:56', '::1', '1493714958'),
('4', '2017-05-02 08:56:09', '::1', '1493715597'),
('4', '2017-05-02 09:00:56', '::1', '1493716044'),
('5', '2017-05-02 09:07:39', '::1', '1493716605'),
('5', '2017-05-02 09:16:56', '::1', '1493716668'),
('3', '2017-05-02 09:23:36', '::1', '1493717729'),
('4', '2017-05-02 12:53:54', '::1', '1493730451'),
('4', '2017-05-02 13:29:58', '::1', '1493796365'),
('4', '2017-05-03 07:26:39', '::1', '1493799532'),
('4', '2017-05-03 08:19:30', '::1', '1493805956'),
('4', '2017-05-03 08:49:12', '::1', '1493805956'),
('3', '2017-05-03 10:06:10', '::1', '1493806054'),
('4', '2017-05-03 10:07:55', '::1', '1493806474'),
('1', '2017-05-03 10:14:54', '::1', '1493806601'),
('3', '2017-05-03 10:17:11', '::1', '1493809559'),
('3', '2017-05-03 11:07:32', '::1', '1493811653'),
('3', '2017-05-03 11:27:29', '::1', '1493811653'),
('3', '2017-05-03 11:42:36', '::1', '1493812448'),
('3', '2017-05-03 11:55:02', '::1', '1493879107'),
('1', '2017-05-04 06:26:09', '::1', '1493888071'),
('1', '2017-05-04 08:54:59', '::1', '1493888234'),
('3', '2017-05-04 08:57:57', '::1', '1493888451'),
('1', '2017-05-04 09:05:38', '::1', '1493888760'),
('5', '2017-05-04 09:06:09', '::1', '1493888949'),
('5', '2017-05-04 09:09:29', '::1', '1493894600'),
('3', '2017-05-04 10:43:27', '::1', '1493895345'),
('5', '2017-05-04 10:55:56', '::1', '1493998462'),
('5', '2017-05-08 06:08:39', '::1', '1494227169'),
('3', '2017-05-08 07:06:31', '::1', '1494227769'),
('5', '2017-05-08 07:16:17', '::1', '1494228626'),
('3', '2017-05-08 07:30:42', '::1', '1494229496'),
('5', '2017-05-08 07:45:04', '::1', '1494234870'),
('3', '2017-05-08 09:14:39', '::1', '1494234925'),
('5', '2017-05-08 09:15:34', '::1', '1494421307'),
('5', '2017-05-08 14:41:04', '::1', '1494421307'),
('5', '2017-05-09 05:59:36', '::1', '1494421307'),
('5', '2017-05-10 06:12:27', '::1', '1494421307'),
('3', '2017-05-10 11:43:58', '::1', '1494419007'),
('3', '2017-05-10 12:24:01', '::1', '1494419827'),
('5', '2017-05-10 12:37:14', '::1', '1494421307'),
('5', '2017-05-11 11:32:25', '::1', '1494580987'),
('5', '2017-05-12 07:45:41', '::1', '1494580987'),
('3', '2017-05-12 09:23:33', '::1', '1494581874'),
('5', '2017-05-12 09:38:02', '::1', '1494835450'),
('5', '2017-05-13 15:54:48', '::1', '1494835450'),
('5', '2017-05-15 08:04:17', '::1', '1494837871'),
('5', '2017-05-15 08:44:37', '::1', '1495433830'),
('5', '2017-05-16 06:51:49', '::1', '1495433830'),
('5', '2017-05-17 08:05:45', '::1', '1495433830'),
('5', '2017-05-18 07:26:29', '::1', '1495433830'),
('5', '2017-05-18 09:18:43', '::1', '1495433830'),
('2', '2017-05-19 10:36:12', '::1', '1495196313'),
('2', '2017-05-19 10:39:09', '::1', '1495196313'),
('2', '2017-05-19 10:39:20', '::1', '1495196313'),
('2', '2017-05-19 10:42:58', '::1', '1495196313'),
('2', '2017-05-19 10:45:33', '::1', '1495196313'),
('2', '2017-05-19 10:46:00', '::1', '1495196313'),
('2', '2017-05-19 10:46:49', '::1', '1495196313'),
('2', '2017-05-19 10:48:12', '::1', '1495196313'),
('2', '2017-05-19 10:50:47', '::1', '1495196313'),
('2', '2017-05-19 12:19:15', '::1', '1495198725'),
('6', '2017-05-19 12:58:59', '::1', '1495201610'),
('2', '2017-05-19 13:47:04', '::1', '1495204365'),
('2', '2017-05-19 14:32:22', '::1', '1495204365'),
('6', '2017-05-19 14:32:58', '::1', '1495430067'),
('2', '2017-05-22 05:09:45', '::1', '1495429985'),
('6', '2017-05-22 05:13:55', '::1', '1495430067'),
('2', '2017-05-22 05:14:38', '::1', '1495430686'),
('6', '2017-05-22 05:24:55', '::1', '1495430780'),
('2', '2017-05-22 05:26:31', '::1', '1495430835'),
('2', '2017-05-22 05:28:41', '::1', '1495430975'),
('6', '2017-05-22 05:29:48', '::1', '1495432682'),
('5', '2017-05-22 05:58:15', '::1', '1495433830'),
('7', '2017-05-22 07:09:21', '::1', '1495448668'),
('8', '2017-05-22 07:09:53', '::1', '1495447536'),
('8', '2017-05-22 09:58:01', '::1', '1495447536'),
('7', '2017-05-22 10:06:12', '::1', '1495448668'),
('8', '2017-05-22 10:25:12', '::1', ''),
('2', '2017-05-22 11:53:09', '::1', '');

-- --------------------------------------------------------

--
-- Table structure for table `non-consumables`
--

CREATE TABLE `non-consumables` (
  `id` int(12) NOT NULL,
  `s_no` varchar(255) NOT NULL,
  `item_name` varchar(255) NOT NULL,
  `category` int(11) NOT NULL,
  `quantity` bigint(20) NOT NULL,
  `unit` varchar(255) NOT NULL,
  `threshold` bigint(20) NOT NULL,
  `remark` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `non-consumables`
--

INSERT INTO `non-consumables` (`id`, `s_no`, `item_name`, `category`, `quantity`, `unit`, `threshold`, `remark`, `status`, `created_at`, `updated_at`) VALUES
(1, '0001', 'Bic Ball Point Pens', 2, 100, 'Packets', 35, 'n/a', 1, '2017-05-15 15:06:20', '2017-05-15 15:06:20');

-- --------------------------------------------------------

--
-- Table structure for table `non-consumable_categories`
--

CREATE TABLE `non-consumable_categories` (
  `id` int(4) NOT NULL,
  `category_name` varchar(255) NOT NULL,
  `category_type` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `non-consumable_categories`
--

INSERT INTO `non-consumable_categories` (`id`, `category_name`, `category_type`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Uniforms and Gears', 'Consumables', 1, '2017-05-12 09:21:11', '2017-05-12 09:21:11'),
(2, 'Stationery', 'Consumables', 1, '2017-05-15 14:54:46', '2017-05-15 14:54:46');

-- --------------------------------------------------------

--
-- Table structure for table `platoon`
--

CREATE TABLE `platoon` (
  `platoon_id` int(11) NOT NULL,
  `platoon_name` varchar(255) DEFAULT NULL,
  `company_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `platoon`
--

INSERT INTO `platoon` (`platoon_id`, `platoon_name`, `company_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Platoon 1', 1, 1, '2017-04-16 18:26:06', '0000-00-00 00:00:00'),
(2, 'Platoon 2', 1, 1, '2017-04-16 18:26:18', '0000-00-00 00:00:00'),
(3, 'Platoon 3', 2, 1, '2017-04-18 14:37:31', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `prform_data`
--

CREATE TABLE `prform_data` (
  `id` bigint(20) NOT NULL,
  `prform_id` bigint(20) NOT NULL,
  `code_number` varchar(10) NOT NULL,
  `description` text NOT NULL,
  `unit_issue` varchar(255) NOT NULL,
  `qty_received` varchar(255) NOT NULL,
  `qty_cost` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  `remarks` text NOT NULL,
  `balances` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prform_data`
--

INSERT INTO `prform_data` (`id`, `prform_id`, `code_number`, `description`, `unit_issue`, `qty_received`, `qty_cost`, `value`, `remarks`, `balances`, `date`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, '1', 'Bic Ball Point Pen', '123ad45', '80', '20', '1600', 'dnjdsnjfd', '40', '0000-00-00', 1, '2017-05-11 14:51:34', '2017-05-11 14:51:34'),
(2, 2, '2', 'Bic Ball Point Pen', 'sdv', '100', '20', '2000', 'n/a', '40', '0000-00-00', 1, '2017-05-11 15:07:41', '2017-05-11 15:07:41'),
(3, 3, '3', 'Bic Ball Point Pen', 'sdv', '100', '20', '2000', 'n/a', '40', '0000-00-00', 1, '2017-05-11 15:07:41', '2017-05-11 15:07:41'),
(4, 4, '4', 'Bic Ball Point Pen', 'sdv', '100', '20', '2000', 'n/a', '40', '0000-00-00', 1, '2017-05-11 15:07:41', '2017-05-11 15:07:41'),
(5, 5, '5', 'Bic Ball Point Pen', 'sdv', '100', '20', '2000', 'n/a', '40', '0000-00-00', 1, '2017-05-11 15:07:41', '2017-05-11 15:07:41'),
(6, 6, '6', 'Bic Ball Point Pen', 'sdv', '100', '20', '2000', 'n/a', '40', '0000-00-00', 1, '2017-05-11 15:07:41', '2017-05-11 15:07:41'),
(7, 7, '7', 'Bic Ball Point Pen', 'sdv', '100', '20', '2000', 'n/a', '40', '0000-00-00', 1, '2017-05-11 15:07:41', '2017-05-11 15:07:41');

-- --------------------------------------------------------

--
-- Table structure for table `prform_uploads`
--

CREATE TABLE `prform_uploads` (
  `id` bigint(20) NOT NULL,
  `prform_id` bigint(20) NOT NULL,
  `file` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `purchase_request`
--

CREATE TABLE `purchase_request` (
  `id` bigint(20) NOT NULL,
  `section_head` varchar(100) NOT NULL,
  `proc_officer` text NOT NULL,
  `purpose` text NOT NULL,
  `date` varchar(50) NOT NULL,
  `rqo` varchar(255) NOT NULL,
  `stores_clerk` varchar(255) NOT NULL,
  `book_balances` varchar(50) NOT NULL,
  `accountant` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchase_request`
--

INSERT INTO `purchase_request` (`id`, `section_head`, `proc_officer`, `purpose`, `date`, `rqo`, `stores_clerk`, `book_balances`, `accountant`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Mark Musau Mwendwa', 'McJohn Otieno Ajua', 'Purchase of new Pens', '11-05-2017', 'Max Wafula Wamunyinyi', 'Jane Lesuda Naisula', 'SDVS', 'Mohamed Sharif Abdulahi', 1, '2017-05-11 14:51:34', '2017-05-11 14:51:34'),
(2, 'Mark Musau Mwendwa', 'McJohn Otieno Ajua', 'Purchase of new Pens', '11-05-2017', 'Max Wafula Wamunyinyi', 'Jane Lesuda Naisula', 'SDVS', 'Mohamed Sharif Abdulahi', 1, '2017-05-11 15:07:41', '2017-05-11 15:07:41');

-- --------------------------------------------------------

--
-- Table structure for table `recruits`
--

CREATE TABLE `recruits` (
  `recruit_id` bigint(20) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `surname` varchar(255) NOT NULL,
  `sto_number` varchar(50) NOT NULL,
  `other_name` varchar(255) NOT NULL,
  `service_number` varchar(50) NOT NULL,
  `personal_number` varchar(50) NOT NULL,
  `doa` date DEFAULT NULL,
  `terms` tinyint(4) NOT NULL,
  `education_level` tinyint(4) NOT NULL,
  `professional_level` varchar(255) DEFAULT NULL,
  `religion_id` tinyint(4) NOT NULL,
  `height_ft` varchar(55) NOT NULL,
  `height_inches` varchar(55) NOT NULL,
  `physical_figure` varchar(255) NOT NULL,
  `identification_marks` text NOT NULL,
  `place_of_birth` varchar(255) NOT NULL,
  `dob` date DEFAULT NULL,
  `birth_certificate_number` varchar(255) NOT NULL,
  `tribe_id` tinyint(4) NOT NULL,
  `marital_status` tinyint(4) NOT NULL,
  `spouse_name` varchar(255) NOT NULL,
  `phone_1` char(50) NOT NULL,
  `phone_2` char(50) NOT NULL,
  `male_children` tinyint(4) NOT NULL,
  `female_children` tinyint(4) NOT NULL,
  `county_id` tinyint(4) NOT NULL,
  `sub_county` int(11) NOT NULL,
  `ward_id` int(11) DEFAULT NULL,
  `village` varchar(255) NOT NULL,
  `chief` varchar(255) NOT NULL,
  `assistant_chief` varchar(255) NOT NULL,
  `kin` varchar(255) NOT NULL,
  `relationship` varchar(255) DEFAULT NULL,
  `address` varchar(100) NOT NULL,
  `kin_phone` char(50) NOT NULL,
  `kra_pin` varchar(50) NOT NULL,
  `national_id` varchar(50) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `gender` varchar(10) NOT NULL,
  `eye_color` varchar(25) NOT NULL,
  `platoon_id` bigint(20) NOT NULL DEFAULT '0',
  `extra_details` varchar(255) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `original_id` tinyint(1) NOT NULL DEFAULT '1',
  `checklist_certificates` tinyint(1) NOT NULL DEFAULT '1',
  `checklist_fingerprints` tinyint(1) NOT NULL DEFAULT '1',
  `checklist_medical` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `recruits`
--

INSERT INTO `recruits` (`recruit_id`, `first_name`, `surname`, `sto_number`, `other_name`, `service_number`, `personal_number`, `doa`, `terms`, `education_level`, `professional_level`, `religion_id`, `height_ft`, `height_inches`, `physical_figure`, `identification_marks`, `place_of_birth`, `dob`, `birth_certificate_number`, `tribe_id`, `marital_status`, `spouse_name`, `phone_1`, `phone_2`, `male_children`, `female_children`, `county_id`, `sub_county`, `ward_id`, `village`, `chief`, `assistant_chief`, `kin`, `relationship`, `address`, `kin_phone`, `kra_pin`, `national_id`, `status`, `gender`, `eye_color`, `platoon_id`, `extra_details`, `photo`, `original_id`, `checklist_certificates`, `checklist_fingerprints`, `checklist_medical`, `created_at`, `updated_at`) VALUES
(3, 'Joseph', 'Omolo', '1', 'Omondi', '', '', '2017-04-18', 0, 1, 'N/a', 0, '4', '5', 'Tall, heavily built', 'Injury mark on left wrist', 'Kisii', '2017-04-18', '7324721', 20, 2, 'Spouse', '0708147771', '0737820455', 1, 2, 35, 97, 484, 'mukuyu', 'Paul Mwangi', 'Joseph Omondi', 'Mary Njoki', 'grandmother', '19037 Nairobi', '070814771', 'KRAPIN', '285262777', 1, 'Male', 'brown', 3, '', '3-1495429871.jpg', 1, 1, 1, 1, '2017-04-18 05:12:53', NULL),
(4, 'Beatrice', 'Wanjiku', '2', '', '', '', '2017-04-18', 1, 2, 'Prof', 0, '4/5', '', 'figure', 'Identification marks', 'Kisii', '2017-04-18', 'BC2', 21, 2, 'Spouse', 'Tel1', 'Tel2', 1, 2, 15, 4, 82, '', 'ChiefName', 'AssChiefName', 'Kin Name', 'grandmother', '19037Nairobi', '070814771', 'KRAPIN', '215261520', 1, 'Female', 'brown', 3, 'hhad', NULL, 1, 1, 1, 1, '2017-04-18 05:29:08', NULL),
(5, 'Godfrey', 'Makori', '3', '', '', '', '2017-04-18', 1, 2, 'Prof', 0, '4/5', '', 'figure', 'Identification marks', 'Kisii', '2017-04-18', 'BC2', 14, 2, 'Spouse', 'Tel1', 'Tel2', 1, 2, 15, 4, 82, '', 'ChiefName', 'AssChiefName', 'Kin Name', 'grandmother', '19037Nairobi', '070814771', 'KRAPIN', '2154588', 5, 'Male', 'brown', 0, 'hhad', NULL, 1, 1, 1, 1, '2017-04-18 05:30:34', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `recruit_clearance`
--

CREATE TABLE `recruit_clearance` (
  `recruit_clearance_id` bigint(20) NOT NULL,
  `recruit_id` bigint(20) NOT NULL,
  `clearance_type` tinyint(4) NOT NULL,
  `cleared_by` bigint(20) NOT NULL,
  `permission_id` bigint(20) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `recruit_clearance`
--

INSERT INTO `recruit_clearance` (`recruit_clearance_id`, `recruit_id`, `clearance_type`, `cleared_by`, `permission_id`, `created_at`, `updated_at`, `status`) VALUES
(3, 3, 1, 3, 21, '2017-05-10 04:41:06', NULL, 1),
(4, 4, 1, 3, 21, '2017-05-10 04:41:24', NULL, 1),
(5, 5, 2, 3, 22, '2017-05-10 04:41:59', NULL, 1),
(6, 3, 2, 3, 22, '2017-05-10 04:42:05', NULL, 1),
(9, 5, 3, 3, 31, '2017-05-10 11:44:42', NULL, 1),
(10, 4, 2, 3, 30, '2017-05-10 11:57:46', NULL, 1),
(11, 5, 1, 3, 29, '2017-05-15 12:17:44', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `registry_permission`
--

CREATE TABLE `registry_permission` (
  `reg_perm_id` bigint(20) NOT NULL,
  `account_id` bigint(20) NOT NULL,
  `permission` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `registry_permission`
--

INSERT INTO `registry_permission` (`reg_perm_id`, `account_id`, `permission`, `created_at`, `updated_at`, `status`) VALUES
(1, 6, 2, '2017-05-04 09:30:47', NULL, 0),
(2, 6, 2, '2017-05-04 09:30:56', NULL, 0),
(3, 6, 2, '2017-05-04 09:32:58', NULL, 0),
(4, 6, 3, '2017-05-04 09:33:12', NULL, 0),
(5, 6, 1, '2017-05-04 09:33:26', NULL, 0),
(6, 6, 2, '2017-05-04 09:33:26', NULL, 0),
(7, 6, 3, '2017-05-04 09:33:26', NULL, 0),
(8, 6, 1, '2017-05-04 09:34:23', NULL, 0),
(9, 6, 2, '2017-05-04 09:34:23', NULL, 0),
(10, 6, 3, '2017-05-04 09:34:23', NULL, 0),
(11, 6, 1, '2017-05-04 09:36:52', NULL, 0),
(12, 6, 2, '2017-05-04 09:36:52', NULL, 0),
(13, 6, 1, '2017-05-04 09:41:49', NULL, 0),
(14, 6, 3, '2017-05-04 09:41:49', NULL, 0),
(15, 6, 1, '2017-05-04 09:42:12', NULL, 0),
(16, 6, 2, '2017-05-04 09:42:12', NULL, 0),
(17, 6, 3, '2017-05-04 09:42:12', NULL, 0),
(18, 6, 3, '2017-05-04 09:42:20', NULL, 0),
(19, 6, 2, '2017-05-04 09:42:25', NULL, 0),
(20, 6, 3, '2017-05-04 09:42:25', NULL, 0),
(21, 6, 1, '2017-05-04 09:42:29', NULL, 0),
(22, 6, 2, '2017-05-04 09:42:29', NULL, 0),
(23, 6, 3, '2017-05-04 09:42:29', NULL, 0),
(24, 6, 1, '2017-05-10 10:46:23', NULL, 0),
(25, 6, 2, '2017-05-10 10:46:23', NULL, 0),
(26, 3, 3, '2017-05-10 10:46:23', NULL, 0),
(27, 3, 1, '2017-05-10 11:41:57', NULL, 0),
(28, 3, 3, '2017-05-10 11:41:57', NULL, 0),
(29, 3, 1, '2017-05-10 11:43:19', NULL, 1),
(30, 3, 2, '2017-05-10 11:43:19', NULL, 1),
(31, 3, 3, '2017-05-10 11:43:19', NULL, 1),
(32, 6, 1, '2017-05-19 14:32:37', NULL, 0),
(33, 6, 2, '2017-05-19 14:32:37', NULL, 0),
(34, 6, 3, '2017-05-19 14:32:37', NULL, 0),
(35, 6, 1, '2017-05-22 05:23:48', NULL, 0),
(36, 6, 3, '2017-05-22 05:23:48', NULL, 0),
(37, 6, 1, '2017-05-22 05:29:29', NULL, 1),
(38, 6, 2, '2017-05-22 05:29:29', NULL, 1),
(39, 6, 3, '2017-05-22 05:29:29', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `store_items`
--

CREATE TABLE `store_items` (
  `id` bigint(20) NOT NULL,
  `iden` varchar(255) NOT NULL,
  `batch` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `label` varchar(255) NOT NULL,
  `unit` varchar(255) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `state` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `store_items`
--

INSERT INTO `store_items` (`id`, `iden`, `batch`, `type`, `description`, `label`, `unit`, `date_added`, `state`) VALUES
(1, '8s1j58d28a0848fa23mkzqd_1', '8s1j58d28a0848fa23mkzqd', 'Sport Wear', '', 'SPORT SHOES', '', '2017-03-22 14:28:24', 1),
(2, '8s1j58d28a0848fa23mkzqd_2', '8s1j58d28a0848fa23mkzqd', 'Sport Wear', '', 'SPORT SHOES', '', '2017-03-22 14:28:24', 1),
(3, '8s1j58d28a0848fa23mkzqd_3', '8s1j58d28a0848fa23mkzqd', 'Sport Wear', '', 'SPORT SHOES', '', '2017-03-22 14:28:24', 1),
(4, 'swgx58dcfce3a7a9c4edmsj_1', 'swgx58dcfce3a7a9c4edmsj', 'Electronics', '', 'COMPUTER', '', '2017-03-30 12:41:07', 0),
(5, 'swgx58dcfce3a7a9c4edmsj_2', 'swgx58dcfce3a7a9c4edmsj', 'Electronics', '', 'COMPUTER', '', '2017-03-30 12:41:07', 1),
(6, 'swgx58dcfce3a7a9c4edmsj_3', 'swgx58dcfce3a7a9c4edmsj', 'Electronics', '', 'COMPUTER', '', '2017-03-30 12:41:07', 1),
(7, 'swgx58dcfce3a7a9c4edmsj_4', 'swgx58dcfce3a7a9c4edmsj', 'Electronics', '', 'COMPUTER', '', '2017-03-30 12:41:07', 1),
(8, 'n5aj58de7191e7eb82xjrfh_1', 'n5aj58de7191e7eb82xjrfh', 'Balls', '', 'FOOTBALL', '', '2017-03-31 15:11:13', 1),
(9, 'n5aj58de7191e7eb82xjrfh_2', 'n5aj58de7191e7eb82xjrfh', 'Balls', '', 'FOOTBALL', '', '2017-03-31 15:11:14', 1);

-- --------------------------------------------------------

--
-- Table structure for table `store_motion`
--

CREATE TABLE `store_motion` (
  `id` bigint(20) NOT NULL,
  `holder` varchar(255) NOT NULL,
  `holder_group` varchar(50) NOT NULL,
  `move_id` varchar(255) NOT NULL,
  `move_note` mediumtext NOT NULL,
  `move_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `move_back` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `store_motion`
--

INSERT INTO `store_motion` (`id`, `holder`, `holder_group`, `move_id`, `move_note`, `move_time`, `move_back`) VALUES
(1, '700001', '1', 'vjy1rhq58d28a2e29478fgtcs1490192942', 'For Trip', '2017-03-22 14:29:02', '2017-03-22 17:29:26'),
(2, '700001', '1', 'uxbrs6c58d28a6882f63ecswk1490193000', 'sport weekend', '2017-03-22 14:30:00', '2017-03-29 15:04:46'),
(3, '2582', '1', 'ltrxac858dba28b68beajsamz1490789003', 'for sports', '2017-03-29 12:03:23', '2017-03-29 15:03:55'),
(4, '2580', '1', 'qnssk7558de71d19f52cjkyrz1490973137', 'For Sports', '2017-03-31 15:12:17', '2017-03-31 18:14:22'),
(5, '1', '1', 'mzf7dns59030ed3b5134nhzjl1493372627', '', '2017-04-28 09:43:48', '');

-- --------------------------------------------------------

--
-- Table structure for table `store_moves`
--

CREATE TABLE `store_moves` (
  `id` bigint(20) NOT NULL,
  `item_id` varchar(255) NOT NULL,
  `mid` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `store_moves`
--

INSERT INTO `store_moves` (`id`, `item_id`, `mid`) VALUES
(1, '1', 'vjy1rhq58d28a2e29478fgtcs1490192942'),
(2, '1', 'uxbrs6c58d28a6882f63ecswk1490193000'),
(3, '2', 'ltrxac858dba28b68beajsamz1490789003'),
(4, '8', 'qnssk7558de71d19f52cjkyrz1490973137'),
(5, '4', 'mzf7dns59030ed3b5134nhzjl1493372627');

-- --------------------------------------------------------

--
-- Table structure for table `sub_counties`
--

CREATE TABLE `sub_counties` (
  `id` int(11) NOT NULL,
  `county_id` int(11) NOT NULL,
  `sub_county_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sub_counties`
--

INSERT INTO `sub_counties` (`id`, `county_id`, `sub_county_name`) VALUES
(1, 2, 'changamwe'),
(2, 2, 'jomvu'),
(3, 2, 'kisauni'),
(4, 2, 'nyali'),
(5, 2, 'likoni'),
(6, 2, 'mvita'),
(7, 20, 'msambweni'),
(8, 20, 'lungalunga'),
(9, 20, 'matuga'),
(10, 20, 'kinango'),
(11, 16, 'kilifi north'),
(12, 16, 'kilifi south'),
(13, 16, 'kaloleni'),
(14, 16, 'rabai'),
(15, 16, 'ganze'),
(16, 16, 'malindi'),
(17, 16, 'magarini'),
(18, 39, 'garsen'),
(19, 39, 'galole'),
(20, 39, 'bura'),
(21, 22, 'lamu east'),
(22, 22, 'lamu west'),
(23, 38, 'taveta'),
(24, 38, 'wundanyi'),
(25, 38, 'mwatate'),
(26, 38, 'voi'),
(27, 9, 'garissa township'),
(28, 9, 'balambala'),
(29, 9, 'lagdera'),
(30, 9, 'dadaab'),
(31, 9, 'fafi'),
(32, 9, 'ijara'),
(33, 45, 'wajir north'),
(34, 45, 'wajir east'),
(35, 45, 'tarbaj'),
(36, 45, 'wajir west'),
(37, 45, 'eldas'),
(38, 45, 'wajir south'),
(39, 24, 'mandera west'),
(40, 24, 'banissa'),
(41, 24, 'mandera north'),
(42, 24, 'mandera south'),
(43, 24, 'mandera east'),
(44, 24, 'lafey'),
(45, 25, 'moyale'),
(46, 25, 'north horr'),
(47, 25, 'saku'),
(48, 25, 'laisamis'),
(49, 11, 'isiolo north'),
(50, 11, 'isiolo south'),
(51, 26, 'igembe south'),
(52, 26, 'igembe central'),
(53, 26, 'igembe north'),
(54, 26, 'tigania west'),
(55, 26, 'tigania east'),
(56, 26, 'north imenti'),
(57, 26, 'buuri'),
(58, 26, 'central imenti'),
(59, 26, 'south imenti'),
(60, 40, 'maara'),
(61, 40, 'chuka/igambang''om'),
(62, 40, 'tharaka'),
(63, 8, 'manyatta'),
(64, 8, 'runyenjes'),
(65, 8, 'mbeere south'),
(66, 8, 'mbeere north'),
(67, 19, 'mwingi north'),
(68, 19, 'mwingi west'),
(69, 19, 'mwingi central'),
(70, 19, 'kitui west'),
(71, 19, 'kitui rural'),
(72, 19, 'kitui central'),
(73, 19, 'kitui east'),
(74, 19, 'kitui south'),
(75, 28, 'masinga'),
(76, 28, 'yatta'),
(77, 28, 'kangundo'),
(78, 28, 'matungulu'),
(79, 28, 'kathiani'),
(80, 28, 'mavoko'),
(81, 28, 'machakos town'),
(82, 28, 'mwala'),
(83, 23, 'mbooni'),
(84, 23, 'kilome'),
(85, 23, 'kaiti'),
(86, 23, 'makueni'),
(87, 23, 'kibwezi west'),
(88, 23, 'kibwezi east'),
(89, 34, 'kinangop'),
(90, 34, 'kipipiri'),
(91, 34, 'ol kalou'),
(92, 34, 'ol jorok'),
(93, 34, 'ndaragwa'),
(94, 35, 'tetu'),
(95, 35, 'kieni'),
(96, 35, 'mathira'),
(97, 35, 'othaya'),
(98, 35, 'mukurweini'),
(99, 35, 'nyeri town'),
(100, 17, 'mwea'),
(101, 17, 'gichugu'),
(102, 17, 'ndia'),
(103, 17, 'kirinyaga central'),
(104, 29, 'kangema'),
(105, 29, 'mathioya'),
(106, 29, 'kiharu'),
(107, 29, 'kigumo'),
(108, 29, 'maragwa'),
(109, 29, 'kandara'),
(110, 29, 'gatanga'),
(111, 15, 'gatundu south'),
(112, 15, 'gatundu north'),
(113, 15, 'juja'),
(114, 15, 'thika town'),
(115, 15, 'ruiru'),
(116, 15, 'githunguri'),
(117, 15, 'kiambu'),
(118, 15, 'kiambaa'),
(119, 15, 'kabete'),
(120, 15, 'kikuyu'),
(121, 15, 'limuru'),
(122, 15, 'lari'),
(123, 42, 'turkana north'),
(124, 42, 'turkana west'),
(125, 42, 'turkana central'),
(126, 42, 'loima'),
(127, 42, 'turkana south'),
(128, 42, 'turkana east'),
(129, 46, 'kapenguria'),
(130, 46, 'sigor'),
(131, 46, 'kacheliba'),
(132, 46, 'pokot south'),
(133, 36, 'samburu west'),
(134, 36, 'samburu north'),
(135, 36, 'samburu east'),
(136, 41, 'kwanza'),
(137, 41, 'endebess'),
(138, 41, 'saboti'),
(139, 41, 'kiminini'),
(140, 41, 'cherangany'),
(141, 43, 'soy'),
(142, 43, 'turbo'),
(143, 43, 'moiben'),
(144, 43, 'ainabkoi'),
(145, 43, 'kapseret'),
(146, 43, 'kesses'),
(147, 7, 'marakwet east'),
(148, 7, 'marakwet west'),
(149, 7, 'keiyo north'),
(150, 7, 'keiyo south'),
(151, 31, 'tinderet'),
(152, 31, 'aldai'),
(153, 31, 'nandi hills'),
(154, 31, 'chesumei'),
(155, 31, 'emgwen'),
(156, 31, 'mosop'),
(157, 47, 'tiaty'),
(158, 47, 'baringo  north'),
(159, 47, 'baringo central'),
(160, 47, 'baringo south'),
(161, 47, 'mogotio'),
(162, 47, 'eldama ravine'),
(163, 21, 'laikipia west'),
(164, 21, 'laikipia east'),
(165, 21, 'laikipia north'),
(166, 30, 'molo'),
(167, 30, 'njoro'),
(168, 30, 'naivasha'),
(169, 30, 'gilgil'),
(170, 30, 'kuresoi south'),
(171, 30, 'kuresoi north'),
(172, 30, 'subukia'),
(173, 30, 'rongai'),
(174, 30, 'bahati'),
(175, 30, 'nakuru town west'),
(176, 30, 'nakuru town east'),
(177, 32, 'kilgoris'),
(178, 32, 'emurua dikirr'),
(179, 32, 'narok north'),
(180, 32, 'narok east'),
(181, 32, 'narok south'),
(182, 32, 'narok west'),
(183, 12, 'kajiado north'),
(184, 12, 'kajiado central'),
(185, 12, 'kajiado east'),
(186, 12, 'kajiado west'),
(187, 12, 'kajiado south'),
(188, 14, 'kipkelion east'),
(189, 14, 'kipkelion west'),
(190, 14, 'ainamoi'),
(191, 14, 'bureti'),
(192, 14, 'belgut'),
(193, 14, 'sigowet/soin'),
(194, 4, 'sotik'),
(195, 4, 'chepalungu'),
(196, 4, 'bomet east'),
(197, 4, 'bomet central'),
(198, 4, 'konoin'),
(199, 13, 'lugari'),
(200, 13, 'likuyani'),
(201, 13, 'malava'),
(202, 13, 'lurambi'),
(203, 13, 'navakholo'),
(204, 13, 'mumias west'),
(205, 13, 'mumias east'),
(206, 13, 'matungu'),
(207, 13, 'butere'),
(208, 13, 'khwisero'),
(209, 13, 'shinyalu'),
(210, 13, 'ikolomani'),
(211, 44, 'vihiga'),
(212, 44, 'sabatia'),
(213, 44, 'hamisi'),
(214, 44, 'luanda'),
(215, 44, 'emuhaya'),
(216, 5, 'mt.elgon'),
(217, 5, 'sirisia'),
(218, 5, 'kabuchai'),
(219, 5, 'bumula'),
(220, 5, 'kanduyi'),
(221, 5, 'webuye east'),
(222, 5, 'webuye west'),
(223, 5, 'kimilili'),
(224, 5, 'tongaren'),
(225, 6, 'teso north'),
(226, 6, 'teso south'),
(227, 6, 'nambale'),
(228, 6, 'matayos'),
(229, 6, 'butula'),
(230, 6, 'funyula'),
(231, 6, 'budalangi'),
(232, 37, 'ugenya'),
(233, 37, 'ugunja'),
(234, 37, 'alego usonga'),
(235, 37, 'gem'),
(236, 37, 'bondo'),
(237, 37, 'rarieda'),
(238, 3, 'kisumu east'),
(239, 3, 'kisumu west'),
(240, 3, 'kisumu central'),
(241, 3, 'seme'),
(242, 3, 'nyando'),
(243, 3, 'muhoroni'),
(244, 3, 'nyakach'),
(245, 10, 'kasipul'),
(246, 10, 'kabondo kasipul'),
(247, 10, 'karachuonyo'),
(248, 10, 'rangwe'),
(249, 10, 'homa bay town'),
(250, 10, 'ndhiwa'),
(251, 10, 'mbita'),
(252, 10, 'suba'),
(253, 27, 'rongo'),
(254, 27, 'awendo'),
(255, 27, 'suna east'),
(256, 27, 'suna west'),
(257, 27, 'uriri'),
(258, 27, 'nyatike'),
(259, 27, 'kuria west'),
(260, 27, 'kuria east'),
(261, 18, 'bonchari'),
(262, 18, 'south mugirango'),
(263, 18, 'bomachoge borabu'),
(264, 18, 'bobasi'),
(265, 18, 'bomachoge chache'),
(266, 18, 'nyaribari masaba'),
(267, 18, 'nyaribari chache'),
(268, 18, 'kitutu chache north'),
(269, 18, 'kitutu chache south'),
(270, 33, 'kitutu masaba'),
(271, 33, 'west mugirango'),
(272, 33, 'north mugirango'),
(273, 33, 'borabu'),
(274, 1, 'westlands'),
(275, 1, 'dagoretti north'),
(276, 1, 'dagoretti south'),
(277, 1, 'langata'),
(278, 1, 'kibra'),
(279, 1, 'roysambu'),
(280, 1, 'kasarani'),
(281, 1, 'ruaraka'),
(282, 1, 'embakasi south'),
(283, 1, 'embakasi north'),
(284, 1, 'embakasi central'),
(285, 1, 'embakasi east'),
(286, 1, 'embakasi west'),
(287, 1, 'makadara'),
(288, 1, 'kamukunji'),
(289, 1, 'starehe'),
(290, 1, 'mathare');

-- --------------------------------------------------------

--
-- Table structure for table `tribes`
--

CREATE TABLE `tribes` (
  `id` int(11) NOT NULL,
  `tribe_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tribes`
--

INSERT INTO `tribes` (`id`, `tribe_name`) VALUES
(1, 'Akamba (Kamba)\r'),
(2, 'Boni\r'),
(3, 'Boran\r'),
(4, 'Elkony\r'),
(5, 'Elmolo\r'),
(6, 'Embu\r'),
(7, 'Gabbra\r'),
(8, 'Galla (Oromo)\r'),
(9, 'Gikuyu (Kikuyu)\r'),
(10, 'Giriama (Giryama)\r'),
(11, 'Giryama (Giriama)\r'),
(12, 'Gusii (Kisii)\r'),
(13, 'Issa (Somali)\r'),
(14, 'Iteso (Teso)\r'),
(15, 'Kalenjin\r'),
(16, 'Kamba (Akamba)\r'),
(17, 'Keiyo\r'),
(18, 'Kikuyu (Gikuyu)\r'),
(19, 'Kipsigis\r'),
(20, 'Kisii (Gusii)\r'),
(21, 'Kuria\r'),
(22, 'Luhya\r'),
(23, 'Luo\r'),
(24, 'Maasai (Masai)\r'),
(25, 'Marakwet\r'),
(26, 'Masai (Maasai)\r'),
(27, 'Mbere\r'),
(28, 'Meru\r'),
(29, 'Mikikenda\r'),
(30, 'Njemps\r'),
(31, 'Orma\r'),
(32, 'Oromo (Galla)\r'),
(33, 'Pakot\r'),
(34, 'Pokomo\r'),
(35, 'Pokot\r'),
(36, 'Rendille\r'),
(37, 'Sabaot\r'),
(38, 'Samburu\r'),
(39, 'Segeju\r'),
(40, 'Somali (Issa)\r'),
(41, 'Swahili\r'),
(42, 'Taita\r'),
(43, 'Taveta\r'),
(44, 'Terik\r'),
(45, 'Teso (Iteso)\r'),
(46, 'Tharaka\r'),
(47, 'Tugen\r'),
(48, 'Tuken\r'),
(49, 'Turgen\r'),
(50, 'Turkana');

-- --------------------------------------------------------

--
-- Table structure for table `units`
--

CREATE TABLE `units` (
  `unit_id` int(11) NOT NULL,
  `unit_name` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `units`
--

INSERT INTO `units` (`unit_id`, `unit_name`, `code`, `status`, `created_at`, `updated_at`) VALUES
(1, 'INTRODUCTION TO CRIMINOLOGY AND SOCIOLOGY       ', 'DSC 037', 1, '2017-05-22 07:30:38', '0000-00-00 00:00:00'),
(2, 'INTRODUCTION TO PSYCHOLOGY', 'DSC 041', 1, '2017-05-22 07:30:38', '0000-00-00 00:00:00'),
(3, 'INTRODUCTION TO DISASTER MANAGEMENT', 'DCS 042', 1, '2017-05-22 07:30:38', '0000-00-00 00:00:00'),
(4, 'DRILLS AND PHYSICAL TRAINING', 'DSC 021', 1, '2017-05-22 07:30:38', '0000-00-00 00:00:00'),
(5, 'WEAPONRY TRAINING AND MANAGEMENT ', 'DSC 022', 1, '2017-05-22 07:30:38', '0000-00-00 00:00:00'),
(6, 'PRISONS ADMINISTRATION, MANAGEMENT AND PRACTICES', 'DSC 035', 1, '2017-05-22 07:30:38', '0000-00-00 00:00:00'),
(7, 'REHABILITATION, TREATMENT AND RE-INTEGRATION OF OFFENDERS', 'DSC 036', 1, '2017-05-22 07:30:38', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `wards`
--

CREATE TABLE `wards` (
  `id` int(11) NOT NULL,
  `county_id` int(11) NOT NULL,
  `sub_county_id` int(11) NOT NULL,
  `ward_name` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wards`
--

INSERT INTO `wards` (`id`, `county_id`, `sub_county_id`, `ward_name`, `created_at`, `updated_at`) VALUES
(1, 2, 1, 'port reitz', '2017-03-02 14:48:40', '2017-03-02 14:48:40'),
(2, 2, 1, 'kipevu', '2017-03-02 14:48:40', '2017-03-02 14:48:40'),
(3, 2, 1, 'airport', '2017-03-02 14:48:40', '2017-03-02 14:48:40'),
(4, 2, 1, 'changamwe', '2017-03-02 14:48:40', '2017-03-02 14:48:40'),
(5, 2, 1, 'chaani', '2017-03-02 14:48:40', '2017-03-02 14:48:40'),
(6, 2, 2, 'jomvu kuu', '2017-03-02 14:48:40', '2017-03-02 14:48:40'),
(7, 2, 2, 'miritini', '2017-03-02 14:48:40', '2017-03-02 14:48:40'),
(8, 2, 2, 'mikindani', '2017-03-02 14:48:40', '2017-03-02 14:48:40'),
(9, 2, 3, 'mjambere', '2017-03-02 14:48:40', '2017-03-02 14:48:40'),
(10, 2, 3, 'junda', '2017-03-02 14:48:40', '2017-03-02 14:48:40'),
(11, 2, 3, 'bamburi', '2017-03-02 14:48:40', '2017-03-02 14:48:40'),
(12, 2, 3, 'mwakirunge', '2017-03-02 14:48:40', '2017-03-02 14:48:40'),
(13, 2, 3, 'mtopanga', '2017-03-02 14:48:40', '2017-03-02 14:48:40'),
(14, 2, 3, 'magogoni', '2017-03-02 14:48:41', '2017-03-02 14:48:41'),
(15, 2, 3, 'shanzu', '2017-03-02 14:48:41', '2017-03-02 14:48:41'),
(16, 2, 4, 'frere town', '2017-03-02 14:48:41', '2017-03-02 14:48:41'),
(17, 2, 4, 'ziwa la ng''ombe', '2017-03-02 14:48:41', '2017-03-02 14:48:41'),
(18, 2, 4, 'mkomani', '2017-03-02 14:48:41', '2017-03-02 14:48:41'),
(19, 2, 4, 'kongowea', '2017-03-02 14:48:41', '2017-03-02 14:48:41'),
(20, 2, 4, 'kadzandani', '2017-03-02 14:48:41', '2017-03-02 14:48:41'),
(21, 2, 5, 'mtongwe', '2017-03-02 14:48:41', '2017-03-02 14:48:41'),
(22, 2, 5, 'shika adabu', '2017-03-02 14:48:41', '2017-03-02 14:48:41'),
(23, 2, 5, 'bofu', '2017-03-02 14:48:41', '2017-03-02 14:48:41'),
(24, 2, 5, 'likoni', '2017-03-02 14:48:41', '2017-03-02 14:48:41'),
(25, 2, 5, 'timbwani', '2017-03-02 14:48:41', '2017-03-02 14:48:41'),
(26, 2, 6, 'mji wa kale/makadara', '2017-03-02 14:48:41', '2017-03-02 14:48:41'),
(27, 2, 6, 'tudor', '2017-03-02 14:48:41', '2017-03-02 14:48:41'),
(28, 2, 6, 'tononoka', '2017-03-02 14:48:41', '2017-03-02 14:48:41'),
(29, 2, 6, 'shimanzi/ganjoni', '2017-03-02 14:48:41', '2017-03-02 14:48:41'),
(30, 2, 6, 'majengo', '2017-03-02 14:48:41', '2017-03-02 14:48:41'),
(31, 20, 7, 'gombatobongwe', '2017-03-02 14:48:41', '2017-03-02 14:48:41'),
(32, 20, 7, 'ukunda', '2017-03-02 14:48:41', '2017-03-02 14:48:41'),
(33, 20, 7, 'kinondo', '2017-03-02 14:48:41', '2017-03-02 14:48:41'),
(34, 20, 7, 'ramisi', '2017-03-02 14:48:42', '2017-03-02 14:48:42'),
(35, 20, 8, 'pongwekikoneni', '2017-03-02 14:48:42', '2017-03-02 14:48:42'),
(36, 20, 8, 'dzombo', '2017-03-02 14:48:42', '2017-03-02 14:48:42'),
(37, 20, 8, 'mwereni', '2017-03-02 14:48:42', '2017-03-02 14:48:42'),
(38, 20, 8, 'vanga', '2017-03-02 14:48:42', '2017-03-02 14:48:42'),
(39, 20, 9, 'tsimba golini', '2017-03-02 14:48:42', '2017-03-02 14:48:42'),
(40, 20, 9, 'waa', '2017-03-02 14:48:42', '2017-03-02 14:48:42'),
(41, 20, 9, 'tiwi', '2017-03-02 14:48:42', '2017-03-02 14:48:42'),
(42, 20, 9, 'kubo south', '2017-03-02 14:48:42', '2017-03-02 14:48:42'),
(43, 20, 9, 'mkongani', '2017-03-02 14:48:42', '2017-03-02 14:48:42'),
(44, 20, 10, 'nadavaya', '2017-03-02 14:48:42', '2017-03-02 14:48:42'),
(45, 20, 10, 'puma', '2017-03-02 14:48:42', '2017-03-02 14:48:42'),
(46, 20, 10, 'kinango', '2017-03-02 14:48:42', '2017-03-02 14:48:42'),
(47, 20, 10, 'mackinnon-road', '2017-03-02 14:48:42', '2017-03-02 14:48:42'),
(48, 20, 10, 'chengoni/samburu', '2017-03-02 14:48:42', '2017-03-02 14:48:42'),
(49, 20, 10, 'mwavumbo', '2017-03-02 14:48:42', '2017-03-02 14:48:42'),
(50, 20, 10, 'kasemeni', '2017-03-02 14:48:42', '2017-03-02 14:48:42'),
(51, 16, 11, 'tezo', '2017-03-02 14:48:42', '2017-03-02 14:48:42'),
(52, 16, 11, 'sokoni', '2017-03-02 14:48:42', '2017-03-02 14:48:42'),
(53, 16, 11, 'kibarani', '2017-03-02 14:48:42', '2017-03-02 14:48:42'),
(54, 16, 11, 'dabaso', '2017-03-02 14:48:42', '2017-03-02 14:48:42'),
(55, 16, 11, 'matsangoni', '2017-03-02 14:48:42', '2017-03-02 14:48:42'),
(56, 16, 11, 'watamu', '2017-03-02 14:48:42', '2017-03-02 14:48:42'),
(57, 16, 11, 'mnarani', '2017-03-02 14:48:42', '2017-03-02 14:48:42'),
(58, 16, 12, 'junju', '2017-03-02 14:48:42', '2017-03-02 14:48:42'),
(59, 16, 12, 'mwarakaya', '2017-03-02 14:48:42', '2017-03-02 14:48:42'),
(60, 16, 12, 'shimo la tewa', '2017-03-02 14:48:42', '2017-03-02 14:48:42'),
(61, 16, 12, 'chasimba', '2017-03-02 14:48:42', '2017-03-02 14:48:42'),
(62, 16, 12, 'mtepeni', '2017-03-02 14:48:42', '2017-03-02 14:48:42'),
(63, 16, 13, 'mariakani', '2017-03-02 14:48:43', '2017-03-02 14:48:43'),
(64, 16, 13, 'kayafungo', '2017-03-02 14:48:43', '2017-03-02 14:48:43'),
(65, 16, 13, 'kaloleni', '2017-03-02 14:48:43', '2017-03-02 14:48:43'),
(66, 16, 13, 'mwanamwinga', '2017-03-02 14:48:43', '2017-03-02 14:48:43'),
(67, 16, 14, 'mwawesa', '2017-03-02 14:48:43', '2017-03-02 14:48:43'),
(68, 16, 14, 'ruruma', '2017-03-02 14:48:43', '2017-03-02 14:48:43'),
(69, 16, 14, 'kambe/ribe', '2017-03-02 14:48:43', '2017-03-02 14:48:43'),
(70, 16, 14, 'rabai/kisurutini', '2017-03-02 14:48:43', '2017-03-02 14:48:43'),
(71, 16, 15, 'ganze', '2017-03-02 14:48:43', '2017-03-02 14:48:43'),
(72, 16, 15, 'bamba', '2017-03-02 14:48:43', '2017-03-02 14:48:43'),
(73, 16, 15, 'jaribuni', '2017-03-02 14:48:43', '2017-03-02 14:48:43'),
(74, 16, 15, 'sokoke', '2017-03-02 14:48:43', '2017-03-02 14:48:43'),
(75, 16, 16, 'jilore', '2017-03-02 14:48:43', '2017-03-02 14:48:43'),
(76, 16, 16, 'kakuyuni', '2017-03-02 14:48:43', '2017-03-02 14:48:43'),
(77, 16, 16, 'ganda', '2017-03-02 14:48:43', '2017-03-02 14:48:43'),
(78, 16, 16, 'malindi town', '2017-03-02 14:48:43', '2017-03-02 14:48:43'),
(79, 16, 16, 'shella', '2017-03-02 14:48:43', '2017-03-02 14:48:43'),
(80, 16, 17, 'marafa', '2017-03-02 14:48:43', '2017-03-02 14:48:43'),
(81, 16, 17, 'magarini', '2017-03-02 14:48:43', '2017-03-02 14:48:43'),
(82, 16, 17, 'gongoni', '2017-03-02 14:48:43', '2017-03-02 14:48:43'),
(83, 16, 17, 'adu', '2017-03-02 14:48:43', '2017-03-02 14:48:43'),
(84, 16, 17, 'garashi', '2017-03-02 14:48:43', '2017-03-02 14:48:43'),
(85, 16, 17, 'sabaki', '2017-03-02 14:48:43', '2017-03-02 14:48:43'),
(86, 39, 18, 'kipini east', '2017-03-02 14:48:43', '2017-03-02 14:48:43'),
(87, 39, 18, 'garsen south', '2017-03-02 14:48:44', '2017-03-02 14:48:44'),
(88, 39, 18, 'kipini west', '2017-03-02 14:48:44', '2017-03-02 14:48:44'),
(89, 39, 18, 'garsen central', '2017-03-02 14:48:44', '2017-03-02 14:48:44'),
(90, 39, 18, 'garsen west', '2017-03-02 14:48:44', '2017-03-02 14:48:44'),
(91, 39, 18, 'garsen north', '2017-03-02 14:48:44', '2017-03-02 14:48:44'),
(92, 39, 19, 'kinakomba', '2017-03-02 14:48:44', '2017-03-02 14:48:44'),
(93, 39, 19, 'mikinduni', '2017-03-02 14:48:44', '2017-03-02 14:48:44'),
(94, 39, 19, 'chewani', '2017-03-02 14:48:44', '2017-03-02 14:48:44'),
(95, 39, 19, 'wayu', '2017-03-02 14:48:44', '2017-03-02 14:48:44'),
(96, 39, 20, 'chewele', '2017-03-02 14:48:44', '2017-03-02 14:48:44'),
(97, 39, 20, 'bura', '2017-03-02 14:48:44', '2017-03-02 14:48:44'),
(98, 39, 20, 'bangale', '2017-03-02 14:48:44', '2017-03-02 14:48:44'),
(99, 39, 20, 'sala', '2017-03-02 14:48:44', '2017-03-02 14:48:44'),
(100, 39, 20, 'madogo', '2017-03-02 14:48:44', '2017-03-02 14:48:44'),
(101, 22, 21, 'faza', '2017-03-02 14:48:44', '2017-03-02 14:48:44'),
(102, 22, 21, 'kiunga', '2017-03-02 14:48:44', '2017-03-02 14:48:44'),
(103, 22, 21, 'basuba', '2017-03-02 14:48:44', '2017-03-02 14:48:44'),
(104, 22, 22, 'shella', '2017-03-02 14:48:44', '2017-03-02 14:48:44'),
(105, 22, 22, 'mkomani', '2017-03-02 14:48:44', '2017-03-02 14:48:44'),
(106, 22, 22, 'hindi', '2017-03-02 14:48:44', '2017-03-02 14:48:44'),
(107, 22, 22, 'mkunumbi', '2017-03-02 14:48:44', '2017-03-02 14:48:44'),
(108, 22, 22, 'hongwe', '2017-03-02 14:48:44', '2017-03-02 14:48:44'),
(109, 22, 22, 'witu', '2017-03-02 14:48:44', '2017-03-02 14:48:44'),
(110, 22, 22, 'bahari', '2017-03-02 14:48:44', '2017-03-02 14:48:44'),
(111, 38, 23, 'chala', '2017-03-02 14:48:44', '2017-03-02 14:48:44'),
(112, 38, 23, 'mahoo', '2017-03-02 14:48:45', '2017-03-02 14:48:45'),
(113, 38, 23, 'bomeni', '2017-03-02 14:48:45', '2017-03-02 14:48:45'),
(114, 38, 23, 'mboghoni', '2017-03-02 14:48:45', '2017-03-02 14:48:45'),
(115, 38, 23, 'mata', '2017-03-02 14:48:45', '2017-03-02 14:48:45'),
(116, 38, 24, 'wundanyi/mbale', '2017-03-02 14:48:45', '2017-03-02 14:48:45'),
(117, 38, 24, 'werugha', '2017-03-02 14:48:45', '2017-03-02 14:48:45'),
(118, 38, 24, 'wumingu/kishushe', '2017-03-02 14:48:45', '2017-03-02 14:48:45'),
(119, 38, 24, 'mwanda/mgange', '2017-03-02 14:48:45', '2017-03-02 14:48:45'),
(120, 38, 25, 'rong''e', '2017-03-02 14:48:45', '2017-03-02 14:48:45'),
(121, 38, 25, 'mwatate', '2017-03-02 14:48:45', '2017-03-02 14:48:45'),
(122, 38, 25, 'bura', '2017-03-02 14:48:45', '2017-03-02 14:48:45'),
(123, 38, 25, 'chawia', '2017-03-02 14:48:45', '2017-03-02 14:48:45'),
(124, 38, 25, 'wusi/kishamba', '2017-03-02 14:48:45', '2017-03-02 14:48:45'),
(125, 38, 26, 'mbololo', '2017-03-02 14:48:45', '2017-03-02 14:48:45'),
(126, 38, 26, 'sagalla', '2017-03-02 14:48:45', '2017-03-02 14:48:45'),
(127, 38, 26, 'kaloleni', '2017-03-02 14:48:45', '2017-03-02 14:48:45'),
(128, 38, 26, 'marungu', '2017-03-02 14:48:45', '2017-03-02 14:48:45'),
(129, 38, 26, 'kasigau', '2017-03-02 14:48:45', '2017-03-02 14:48:45'),
(130, 38, 26, 'ngolia', '2017-03-02 14:48:45', '2017-03-02 14:48:45'),
(131, 9, 27, 'waberi', '2017-03-02 14:48:45', '2017-03-02 14:48:45'),
(132, 9, 27, 'galbet', '2017-03-02 14:48:45', '2017-03-02 14:48:45'),
(133, 9, 27, 'township', '2017-03-02 14:48:45', '2017-03-02 14:48:45'),
(134, 9, 27, 'iftin', '2017-03-02 14:48:45', '2017-03-02 14:48:45'),
(135, 9, 28, 'balambala', '2017-03-02 14:48:45', '2017-03-02 14:48:45'),
(136, 9, 28, 'danyere', '2017-03-02 14:48:45', '2017-03-02 14:48:45'),
(137, 9, 28, 'jara jara', '2017-03-02 14:48:45', '2017-03-02 14:48:45'),
(138, 9, 28, 'saka', '2017-03-02 14:48:45', '2017-03-02 14:48:45'),
(139, 9, 28, 'sankuri', '2017-03-02 14:48:45', '2017-03-02 14:48:45'),
(140, 9, 29, 'modogashe', '2017-03-02 14:48:46', '2017-03-02 14:48:46'),
(141, 9, 29, 'benane', '2017-03-02 14:48:46', '2017-03-02 14:48:46'),
(142, 9, 29, 'goreale', '2017-03-02 14:48:46', '2017-03-02 14:48:46'),
(143, 9, 29, 'maalimin', '2017-03-02 14:48:46', '2017-03-02 14:48:46'),
(144, 9, 29, 'sabena', '2017-03-02 14:48:46', '2017-03-02 14:48:46'),
(145, 9, 29, 'baraki', '2017-03-02 14:48:46', '2017-03-02 14:48:46'),
(146, 9, 30, 'dertu', '2017-03-02 14:48:46', '2017-03-02 14:48:46'),
(147, 9, 30, 'dadaab', '2017-03-02 14:48:47', '2017-03-02 14:48:47'),
(148, 9, 30, 'labasigale', '2017-03-02 14:48:47', '2017-03-02 14:48:47'),
(149, 9, 30, 'damajale', '2017-03-02 14:48:47', '2017-03-02 14:48:47'),
(150, 9, 30, 'liboi', '2017-03-02 14:48:47', '2017-03-02 14:48:47'),
(151, 9, 30, 'abakaile', '2017-03-02 14:48:47', '2017-03-02 14:48:47'),
(152, 9, 31, 'bura', '2017-03-02 14:48:47', '2017-03-02 14:48:47'),
(153, 9, 31, 'dekaharia', '2017-03-02 14:48:47', '2017-03-02 14:48:47'),
(154, 9, 31, 'jarajila', '2017-03-02 14:48:47', '2017-03-02 14:48:47'),
(155, 9, 31, 'fafi', '2017-03-02 14:48:47', '2017-03-02 14:48:47'),
(156, 9, 31, 'nanighi', '2017-03-02 14:48:47', '2017-03-02 14:48:47'),
(157, 9, 32, 'hulugho', '2017-03-02 14:48:47', '2017-03-02 14:48:47'),
(158, 9, 32, 'sangailu', '2017-03-02 14:48:47', '2017-03-02 14:48:47'),
(159, 9, 32, 'ijara', '2017-03-02 14:48:47', '2017-03-02 14:48:47'),
(160, 9, 32, 'masalani', '2017-03-02 14:48:47', '2017-03-02 14:48:47'),
(161, 45, 33, 'gurar', '2017-03-02 14:48:47', '2017-03-02 14:48:47'),
(162, 45, 33, 'bute', '2017-03-02 14:48:47', '2017-03-02 14:48:47'),
(163, 45, 33, 'korondile', '2017-03-02 14:48:47', '2017-03-02 14:48:47'),
(164, 45, 33, 'malkagufu', '2017-03-02 14:48:47', '2017-03-02 14:48:47'),
(165, 45, 33, 'batalu', '2017-03-02 14:48:47', '2017-03-02 14:48:47'),
(166, 45, 33, 'danaba', '2017-03-02 14:48:47', '2017-03-02 14:48:47'),
(167, 45, 33, 'godoma', '2017-03-02 14:48:47', '2017-03-02 14:48:47'),
(168, 45, 34, 'wagberi', '2017-03-02 14:48:47', '2017-03-02 14:48:47'),
(169, 45, 34, 'township', '2017-03-02 14:48:48', '2017-03-02 14:48:48'),
(170, 45, 34, 'barwago', '2017-03-02 14:48:48', '2017-03-02 14:48:48'),
(171, 45, 34, 'khorof/harar', '2017-03-02 14:48:48', '2017-03-02 14:48:48'),
(172, 45, 35, 'elben', '2017-03-02 14:48:48', '2017-03-02 14:48:48'),
(173, 45, 35, 'sarman', '2017-03-02 14:48:48', '2017-03-02 14:48:48'),
(174, 45, 35, 'tarbaj', '2017-03-02 14:48:48', '2017-03-02 14:48:48'),
(175, 45, 35, 'wargadud', '2017-03-02 14:48:48', '2017-03-02 14:48:48'),
(176, 45, 36, 'arbajahan', '2017-03-02 14:48:48', '2017-03-02 14:48:48'),
(177, 45, 36, 'hadado/athibohol', '2017-03-02 14:48:48', '2017-03-02 14:48:48'),
(178, 45, 36, 'ademasajide', '2017-03-02 14:48:48', '2017-03-02 14:48:48'),
(179, 45, 36, 'wagalla/ganyure', '2017-03-02 14:48:48', '2017-03-02 14:48:48'),
(180, 45, 37, 'eldas', '2017-03-02 14:48:48', '2017-03-02 14:48:48'),
(181, 45, 37, 'della', '2017-03-02 14:48:48', '2017-03-02 14:48:48'),
(182, 45, 37, 'lakoley south/basir', '2017-03-02 14:48:48', '2017-03-02 14:48:48'),
(183, 45, 37, 'elnur/tula tula', '2017-03-02 14:48:48', '2017-03-02 14:48:48'),
(184, 45, 38, 'benane', '2017-03-02 14:48:48', '2017-03-02 14:48:48'),
(185, 45, 38, 'burder', '2017-03-02 14:48:48', '2017-03-02 14:48:48'),
(186, 45, 38, 'dadaja bulla', '2017-03-02 14:48:48', '2017-03-02 14:48:48'),
(187, 45, 38, 'habasswein', '2017-03-02 14:48:48', '2017-03-02 14:48:48'),
(188, 45, 38, 'lagboghol south', '2017-03-02 14:48:48', '2017-03-02 14:48:48'),
(189, 45, 38, 'ibrahim ure', '2017-03-02 14:48:48', '2017-03-02 14:48:48'),
(190, 45, 38, 'diif', '2017-03-02 14:48:48', '2017-03-02 14:48:48'),
(191, 24, 39, 'takaba south', '2017-03-02 14:48:48', '2017-03-02 14:48:48'),
(192, 24, 39, 'takaba', '2017-03-02 14:48:48', '2017-03-02 14:48:48'),
(193, 24, 39, 'lag sure', '2017-03-02 14:48:48', '2017-03-02 14:48:48'),
(194, 24, 39, 'dandu', '2017-03-02 14:48:48', '2017-03-02 14:48:48'),
(195, 24, 39, 'gither', '2017-03-02 14:48:48', '2017-03-02 14:48:48'),
(196, 24, 40, 'banissa', '2017-03-02 14:48:48', '2017-03-02 14:48:48'),
(197, 24, 40, 'derkhale', '2017-03-02 14:48:48', '2017-03-02 14:48:48'),
(198, 24, 40, 'guba', '2017-03-02 14:48:48', '2017-03-02 14:48:48'),
(199, 24, 40, 'malkamari', '2017-03-02 14:48:49', '2017-03-02 14:48:49'),
(200, 24, 40, 'kiliwehiri', '2017-03-02 14:48:49', '2017-03-02 14:48:49'),
(201, 24, 41, 'ashabito', '2017-03-02 14:48:49', '2017-03-02 14:48:49'),
(202, 24, 41, 'guticha', '2017-03-02 14:48:49', '2017-03-02 14:48:49'),
(203, 24, 41, 'morothile', '2017-03-02 14:48:49', '2017-03-02 14:48:49'),
(204, 24, 41, 'rhamu', '2017-03-02 14:48:49', '2017-03-02 14:48:49'),
(205, 24, 41, 'rhamu-dimtu', '2017-03-02 14:48:49', '2017-03-02 14:48:49'),
(206, 24, 42, 'wargudud', '2017-03-02 14:48:49', '2017-03-02 14:48:49'),
(207, 24, 42, 'kutulo', '2017-03-02 14:48:49', '2017-03-02 14:48:49'),
(208, 24, 42, 'elwak south', '2017-03-02 14:48:49', '2017-03-02 14:48:49'),
(209, 24, 42, 'elwak north', '2017-03-02 14:48:49', '2017-03-02 14:48:49'),
(210, 24, 42, 'shimbir fatuma', '2017-03-02 14:48:49', '2017-03-02 14:48:49'),
(211, 24, 43, 'arabia', '2017-03-02 14:48:49', '2017-03-02 14:48:49'),
(212, 24, 43, 'bulla mpya', '2017-03-02 14:48:49', '2017-03-02 14:48:49'),
(213, 24, 43, 'khalalio', '2017-03-02 14:48:49', '2017-03-02 14:48:49'),
(214, 24, 43, 'neboi', '2017-03-02 14:48:49', '2017-03-02 14:48:49'),
(215, 24, 43, 'township', '2017-03-02 14:48:49', '2017-03-02 14:48:49'),
(216, 24, 44, 'libehia', '2017-03-02 14:48:49', '2017-03-02 14:48:49'),
(217, 24, 44, 'fino', '2017-03-02 14:48:49', '2017-03-02 14:48:49'),
(218, 24, 44, 'lafey', '2017-03-02 14:48:49', '2017-03-02 14:48:49'),
(219, 24, 44, 'warankara', '2017-03-02 14:48:49', '2017-03-02 14:48:49'),
(220, 24, 44, 'alungo gof', '2017-03-02 14:48:49', '2017-03-02 14:48:49'),
(221, 25, 45, 'butiye', '2017-03-02 14:48:50', '2017-03-02 14:48:50'),
(222, 25, 45, 'sololo', '2017-03-02 14:48:50', '2017-03-02 14:48:50'),
(223, 25, 45, 'heilu-manyatta', '2017-03-02 14:48:50', '2017-03-02 14:48:50'),
(224, 25, 45, 'golbo', '2017-03-02 14:48:50', '2017-03-02 14:48:50'),
(225, 25, 45, 'moyale township', '2017-03-02 14:48:50', '2017-03-02 14:48:50'),
(226, 25, 45, 'uran', '2017-03-02 14:48:50', '2017-03-02 14:48:50'),
(227, 25, 45, 'obbu', '2017-03-02 14:48:50', '2017-03-02 14:48:50'),
(228, 25, 46, 'illeret', '2017-03-02 14:48:50', '2017-03-02 14:48:50'),
(229, 25, 46, 'north horr', '2017-03-02 14:48:50', '2017-03-02 14:48:50'),
(230, 25, 46, 'dukana', '2017-03-02 14:48:50', '2017-03-02 14:48:50'),
(231, 25, 46, 'maikona', '2017-03-02 14:48:50', '2017-03-02 14:48:50'),
(232, 25, 46, 'turbi', '2017-03-02 14:48:50', '2017-03-02 14:48:50'),
(233, 25, 47, 'sagante/jaldesa', '2017-03-02 14:48:50', '2017-03-02 14:48:50'),
(234, 25, 47, 'karare', '2017-03-02 14:48:50', '2017-03-02 14:48:50'),
(235, 25, 47, 'marsabit central', '2017-03-02 14:48:50', '2017-03-02 14:48:50'),
(236, 25, 48, 'loiyangalani', '2017-03-02 14:48:50', '2017-03-02 14:48:50'),
(237, 25, 48, 'kargi/south horr', '2017-03-02 14:48:51', '2017-03-02 14:48:51'),
(238, 25, 48, 'korr/ngurunit', '2017-03-02 14:48:51', '2017-03-02 14:48:51'),
(239, 25, 48, 'log logo', '2017-03-02 14:48:51', '2017-03-02 14:48:51'),
(240, 25, 48, 'laisamis', '2017-03-02 14:48:51', '2017-03-02 14:48:51'),
(241, 11, 49, 'wabera', '2017-03-02 14:48:51', '2017-03-02 14:48:51'),
(242, 11, 49, 'bulla pesa', '2017-03-02 14:48:51', '2017-03-02 14:48:51'),
(243, 11, 49, 'chari', '2017-03-02 14:48:51', '2017-03-02 14:48:51'),
(244, 11, 49, 'cherab', '2017-03-02 14:48:51', '2017-03-02 14:48:51'),
(245, 11, 49, 'ngare mara', '2017-03-02 14:48:51', '2017-03-02 14:48:51'),
(246, 11, 49, 'burat', '2017-03-02 14:48:51', '2017-03-02 14:48:51'),
(247, 11, 49, 'oldonyiro', '2017-03-02 14:48:51', '2017-03-02 14:48:51'),
(248, 11, 50, 'garbatulla', '2017-03-02 14:48:52', '2017-03-02 14:48:52'),
(249, 11, 50, 'kinna', '2017-03-02 14:48:52', '2017-03-02 14:48:52'),
(250, 11, 50, 'sericho', '2017-03-02 14:48:52', '2017-03-02 14:48:52'),
(251, 26, 51, 'maua', '2017-03-02 14:48:52', '2017-03-02 14:48:52'),
(252, 26, 51, 'kiegoi/antubochiu', '2017-03-02 14:48:52', '2017-03-02 14:48:52'),
(253, 26, 51, 'athiru gaiti', '2017-03-02 14:48:52', '2017-03-02 14:48:52'),
(254, 26, 51, 'akachiu', '2017-03-02 14:48:52', '2017-03-02 14:48:52'),
(255, 26, 51, 'kanuni', '2017-03-02 14:48:52', '2017-03-02 14:48:52'),
(256, 26, 52, 'akirang''ondu', '2017-03-02 14:48:52', '2017-03-02 14:48:52'),
(257, 26, 52, 'athiru ruujine', '2017-03-02 14:48:52', '2017-03-02 14:48:52'),
(258, 26, 52, 'igembe east', '2017-03-02 14:48:52', '2017-03-02 14:48:52'),
(259, 26, 52, 'njia', '2017-03-02 14:48:52', '2017-03-02 14:48:52'),
(260, 26, 52, 'kangeta', '2017-03-02 14:48:52', '2017-03-02 14:48:52'),
(261, 26, 53, 'antuambui', '2017-03-02 14:48:53', '2017-03-02 14:48:53'),
(262, 26, 53, 'ntunene', '2017-03-02 14:48:53', '2017-03-02 14:48:53'),
(263, 26, 53, 'antubetwe kiongo', '2017-03-02 14:48:53', '2017-03-02 14:48:53'),
(264, 26, 53, 'naathu', '2017-03-02 14:48:53', '2017-03-02 14:48:53'),
(265, 26, 53, 'amwathi', '2017-03-02 14:48:53', '2017-03-02 14:48:53'),
(266, 26, 54, 'athwana', '2017-03-02 14:48:53', '2017-03-02 14:48:53'),
(267, 26, 54, 'akithii', '2017-03-02 14:48:53', '2017-03-02 14:48:53'),
(268, 26, 54, 'kianjai', '2017-03-02 14:48:53', '2017-03-02 14:48:53'),
(269, 26, 54, 'nkomo', '2017-03-02 14:48:53', '2017-03-02 14:48:53'),
(270, 26, 54, 'mbeu', '2017-03-02 14:48:53', '2017-03-02 14:48:53'),
(271, 26, 55, 'thangatha', '2017-03-02 14:48:53', '2017-03-02 14:48:53'),
(272, 26, 55, 'mikinduri', '2017-03-02 14:48:53', '2017-03-02 14:48:53'),
(273, 26, 55, 'kiguchwa', '2017-03-02 14:48:54', '2017-03-02 14:48:54'),
(274, 26, 55, 'muthara', '2017-03-02 14:48:54', '2017-03-02 14:48:54'),
(275, 26, 55, 'karama', '2017-03-02 14:48:54', '2017-03-02 14:48:54'),
(276, 26, 56, 'municipality', '2017-03-02 14:48:54', '2017-03-02 14:48:54'),
(277, 26, 56, 'ntima east', '2017-03-02 14:48:54', '2017-03-02 14:48:54'),
(278, 26, 56, 'ntima west', '2017-03-02 14:48:54', '2017-03-02 14:48:54'),
(279, 26, 56, 'nyaki west', '2017-03-02 14:48:54', '2017-03-02 14:48:54'),
(280, 26, 56, 'nyaki east', '2017-03-02 14:48:54', '2017-03-02 14:48:54'),
(281, 26, 57, 'timau', '2017-03-02 14:48:54', '2017-03-02 14:48:54'),
(282, 26, 57, 'kisima', '2017-03-02 14:48:54', '2017-03-02 14:48:54'),
(283, 26, 57, 'kiirua/naari', '2017-03-02 14:48:54', '2017-03-02 14:48:54'),
(284, 26, 57, 'ruiri/rwarera', '2017-03-02 14:48:54', '2017-03-02 14:48:54'),
(285, 26, 57, 'kibirichia', '2017-03-02 14:48:54', '2017-03-02 14:48:54'),
(286, 26, 58, 'mwanganthia', '2017-03-02 14:48:55', '2017-03-02 14:48:55'),
(287, 26, 58, 'abothuguchi central', '2017-03-02 14:48:55', '2017-03-02 14:48:55'),
(288, 26, 58, 'abothuguchi west', '2017-03-02 14:48:55', '2017-03-02 14:48:55'),
(289, 26, 58, 'kiagu', '2017-03-02 14:48:55', '2017-03-02 14:48:55'),
(290, 26, 59, 'mitunguu', '2017-03-02 14:48:55', '2017-03-02 14:48:55'),
(291, 26, 59, 'igoji east', '2017-03-02 14:48:55', '2017-03-02 14:48:55'),
(292, 26, 59, 'igoji west', '2017-03-02 14:48:55', '2017-03-02 14:48:55'),
(293, 26, 59, 'abogeta east', '2017-03-02 14:48:55', '2017-03-02 14:48:55'),
(294, 26, 59, 'abogeta west', '2017-03-02 14:48:55', '2017-03-02 14:48:55'),
(295, 26, 59, 'nkuene', '2017-03-02 14:48:55', '2017-03-02 14:48:55'),
(296, 40, 60, 'mitheru', '2017-03-02 14:48:55', '2017-03-02 14:48:55'),
(297, 40, 60, 'muthambi', '2017-03-02 14:48:55', '2017-03-02 14:48:55'),
(298, 40, 60, 'mwimbi', '2017-03-02 14:48:55', '2017-03-02 14:48:55'),
(299, 40, 60, 'ganga', '2017-03-02 14:48:55', '2017-03-02 14:48:55'),
(300, 40, 60, 'chogoria', '2017-03-02 14:48:55', '2017-03-02 14:48:55'),
(301, 40, 61, 'mariani', '2017-03-02 14:48:55', '2017-03-02 14:48:55'),
(302, 40, 61, 'karingani', '2017-03-02 14:48:55', '2017-03-02 14:48:55'),
(303, 40, 61, 'magumoni', '2017-03-02 14:48:56', '2017-03-02 14:48:56'),
(304, 40, 61, 'mugwe', '2017-03-02 14:48:56', '2017-03-02 14:48:56'),
(305, 40, 61, 'igambang''ombe', '2017-03-02 14:48:56', '2017-03-02 14:48:56'),
(306, 40, 62, 'gatunga', '2017-03-02 14:48:56', '2017-03-02 14:48:56'),
(307, 40, 62, 'mukothima', '2017-03-02 14:48:56', '2017-03-02 14:48:56'),
(308, 40, 62, 'nkondi', '2017-03-02 14:48:56', '2017-03-02 14:48:56'),
(309, 40, 62, 'chiakariga', '2017-03-02 14:48:56', '2017-03-02 14:48:56'),
(310, 40, 62, 'marimanti', '2017-03-02 14:48:56', '2017-03-02 14:48:56'),
(311, 8, 63, 'ruguru/ngandori', '2017-03-02 14:48:56', '2017-03-02 14:48:56'),
(312, 8, 63, 'kithimu', '2017-03-02 14:48:56', '2017-03-02 14:48:56'),
(313, 8, 63, 'nginda', '2017-03-02 14:48:56', '2017-03-02 14:48:56'),
(314, 8, 63, 'mbeti north', '2017-03-02 14:48:56', '2017-03-02 14:48:56'),
(315, 8, 63, 'kirimari', '2017-03-02 14:48:56', '2017-03-02 14:48:56'),
(316, 8, 63, 'gaturi south', '2017-03-02 14:48:56', '2017-03-02 14:48:56'),
(317, 8, 64, 'gaturi north', '2017-03-02 14:48:56', '2017-03-02 14:48:56'),
(318, 8, 64, 'kagaari south', '2017-03-02 14:48:56', '2017-03-02 14:48:56'),
(319, 8, 64, 'central  ward', '2017-03-02 14:48:56', '2017-03-02 14:48:56'),
(320, 8, 64, 'kagaari north', '2017-03-02 14:48:56', '2017-03-02 14:48:56'),
(321, 8, 64, 'kyeni north', '2017-03-02 14:48:56', '2017-03-02 14:48:56'),
(322, 8, 64, 'kyeni south', '2017-03-02 14:48:56', '2017-03-02 14:48:56'),
(323, 8, 65, 'mwea', '2017-03-02 14:48:56', '2017-03-02 14:48:56'),
(324, 8, 65, 'makima', '2017-03-02 14:48:56', '2017-03-02 14:48:56'),
(325, 8, 65, 'mbeti south', '2017-03-02 14:48:56', '2017-03-02 14:48:56'),
(326, 8, 65, 'mavuria', '2017-03-02 14:48:56', '2017-03-02 14:48:56'),
(327, 8, 65, 'kiambere', '2017-03-02 14:48:56', '2017-03-02 14:48:56'),
(328, 8, 66, 'nthawa', '2017-03-02 14:48:56', '2017-03-02 14:48:56'),
(329, 8, 66, 'muminji', '2017-03-02 14:48:56', '2017-03-02 14:48:56'),
(330, 8, 66, 'evurore', '2017-03-02 14:48:57', '2017-03-02 14:48:57'),
(331, 19, 67, 'ngomeni', '2017-03-02 14:48:57', '2017-03-02 14:48:57'),
(332, 19, 67, 'kyuso', '2017-03-02 14:48:57', '2017-03-02 14:48:57'),
(333, 19, 67, 'mumoni', '2017-03-02 14:48:57', '2017-03-02 14:48:57'),
(334, 19, 67, 'tseikuru', '2017-03-02 14:48:57', '2017-03-02 14:48:57'),
(335, 19, 67, 'tharaka', '2017-03-02 14:48:57', '2017-03-02 14:48:57'),
(336, 19, 68, 'kyome/thaana', '2017-03-02 14:48:57', '2017-03-02 14:48:57'),
(337, 19, 68, 'nguutani', '2017-03-02 14:48:57', '2017-03-02 14:48:57'),
(338, 19, 68, 'migwani', '2017-03-02 14:48:57', '2017-03-02 14:48:57'),
(339, 19, 68, 'kiomo/kyethani', '2017-03-02 14:48:57', '2017-03-02 14:48:57'),
(340, 19, 69, 'central', '2017-03-02 14:48:57', '2017-03-02 14:48:57'),
(341, 19, 69, 'kivou', '2017-03-02 14:48:57', '2017-03-02 14:48:57'),
(342, 19, 69, 'nguni', '2017-03-02 14:48:57', '2017-03-02 14:48:57'),
(343, 19, 69, 'nuu', '2017-03-02 14:48:57', '2017-03-02 14:48:57'),
(344, 19, 69, 'mui', '2017-03-02 14:48:57', '2017-03-02 14:48:57'),
(345, 19, 69, 'waita', '2017-03-02 14:48:57', '2017-03-02 14:48:57'),
(346, 19, 70, 'mutonguni', '2017-03-02 14:48:57', '2017-03-02 14:48:57'),
(347, 19, 70, 'kauwi', '2017-03-02 14:48:57', '2017-03-02 14:48:57'),
(348, 19, 70, 'matinyani', '2017-03-02 14:48:57', '2017-03-02 14:48:57'),
(349, 19, 70, 'kwa mutonga/kithumula', '2017-03-02 14:48:57', '2017-03-02 14:48:57'),
(350, 19, 71, 'kisasi', '2017-03-02 14:48:57', '2017-03-02 14:48:57'),
(351, 19, 71, 'mbitini', '2017-03-02 14:48:57', '2017-03-02 14:48:57'),
(352, 19, 71, 'kwavonza/yatta', '2017-03-02 14:48:57', '2017-03-02 14:48:57'),
(353, 19, 71, 'kanyangi', '2017-03-02 14:48:58', '2017-03-02 14:48:58'),
(354, 19, 72, 'miambani', '2017-03-02 14:48:58', '2017-03-02 14:48:58'),
(355, 19, 72, 'township', '2017-03-02 14:48:58', '2017-03-02 14:48:58'),
(356, 19, 72, 'kyangwithya west', '2017-03-02 14:48:58', '2017-03-02 14:48:58'),
(357, 19, 72, 'mulango', '2017-03-02 14:48:58', '2017-03-02 14:48:58'),
(358, 19, 72, 'kyangwithya east', '2017-03-02 14:48:58', '2017-03-02 14:48:58'),
(359, 19, 73, 'zombe/mwitika', '2017-03-02 14:48:58', '2017-03-02 14:48:58'),
(360, 19, 73, 'chuluni', '2017-03-02 14:48:58', '2017-03-02 14:48:58'),
(361, 19, 73, 'nzambani', '2017-03-02 14:48:58', '2017-03-02 14:48:58'),
(362, 19, 73, 'voo/kyamatu', '2017-03-02 14:48:58', '2017-03-02 14:48:58'),
(363, 19, 73, 'endau/malalani', '2017-03-02 14:48:58', '2017-03-02 14:48:58'),
(364, 19, 73, 'mutito/kaliku', '2017-03-02 14:48:58', '2017-03-02 14:48:58'),
(365, 19, 74, 'ikanga/kyatune', '2017-03-02 14:48:58', '2017-03-02 14:48:58'),
(366, 19, 74, 'mutomo', '2017-03-02 14:48:58', '2017-03-02 14:48:58'),
(367, 19, 74, 'mutha', '2017-03-02 14:48:58', '2017-03-02 14:48:58'),
(368, 19, 74, 'ikutha', '2017-03-02 14:48:58', '2017-03-02 14:48:58'),
(369, 19, 74, 'kanziko', '2017-03-02 14:48:58', '2017-03-02 14:48:58'),
(370, 19, 74, 'athi', '2017-03-02 14:48:58', '2017-03-02 14:48:58'),
(371, 28, 75, 'kivaa', '2017-03-02 14:48:58', '2017-03-02 14:48:58'),
(372, 28, 75, 'masinga central', '2017-03-02 14:48:58', '2017-03-02 14:48:58'),
(373, 28, 75, 'ekalakala', '2017-03-02 14:48:58', '2017-03-02 14:48:58'),
(374, 28, 75, 'muthesya', '2017-03-02 14:48:58', '2017-03-02 14:48:58'),
(375, 28, 75, 'ndithini', '2017-03-02 14:48:58', '2017-03-02 14:48:58'),
(376, 28, 76, 'ndalani', '2017-03-02 14:48:59', '2017-03-02 14:48:59'),
(377, 28, 76, 'matuu', '2017-03-02 14:48:59', '2017-03-02 14:48:59'),
(378, 28, 76, 'kithimani', '2017-03-02 14:48:59', '2017-03-02 14:48:59'),
(379, 28, 76, 'ikombe', '2017-03-02 14:48:59', '2017-03-02 14:48:59'),
(380, 28, 76, 'katangi', '2017-03-02 14:48:59', '2017-03-02 14:48:59'),
(381, 28, 77, 'kangundo north', '2017-03-02 14:48:59', '2017-03-02 14:48:59'),
(382, 28, 77, 'kangundo central', '2017-03-02 14:48:59', '2017-03-02 14:48:59'),
(383, 28, 77, 'kangundo east', '2017-03-02 14:48:59', '2017-03-02 14:48:59'),
(384, 28, 77, 'kangundo west', '2017-03-02 14:48:59', '2017-03-02 14:48:59'),
(385, 28, 78, 'tala', '2017-03-02 14:48:59', '2017-03-02 14:48:59'),
(386, 28, 78, 'matungulu north', '2017-03-02 14:48:59', '2017-03-02 14:48:59'),
(387, 28, 78, 'matungulu east', '2017-03-02 14:48:59', '2017-03-02 14:48:59'),
(388, 28, 78, 'matungulu west', '2017-03-02 14:48:59', '2017-03-02 14:48:59'),
(389, 28, 78, 'kyeleni', '2017-03-02 14:48:59', '2017-03-02 14:48:59'),
(390, 28, 79, 'mitaboni', '2017-03-02 14:48:59', '2017-03-02 14:48:59'),
(391, 28, 79, 'kathiani central', '2017-03-02 14:48:59', '2017-03-02 14:48:59'),
(392, 28, 79, 'upper kaewa/iveti', '2017-03-02 14:48:59', '2017-03-02 14:48:59'),
(393, 28, 79, 'lower kaewa/kaani', '2017-03-02 14:48:59', '2017-03-02 14:48:59'),
(394, 28, 80, 'athi river', '2017-03-02 14:48:59', '2017-03-02 14:48:59'),
(395, 28, 80, 'kinanie', '2017-03-02 14:48:59', '2017-03-02 14:48:59'),
(396, 28, 80, 'muthwani', '2017-03-02 14:48:59', '2017-03-02 14:48:59'),
(397, 28, 80, 'syokimau/mulolongo', '2017-03-02 14:48:59', '2017-03-02 14:48:59'),
(398, 28, 81, 'kalama', '2017-03-02 14:48:59', '2017-03-02 14:48:59'),
(399, 28, 81, 'mua', '2017-03-02 14:48:59', '2017-03-02 14:48:59'),
(400, 28, 81, 'mutituni', '2017-03-02 14:48:59', '2017-03-02 14:48:59'),
(401, 28, 81, 'machakos central', '2017-03-02 14:48:59', '2017-03-02 14:48:59'),
(402, 28, 81, 'mumbuni north', '2017-03-02 14:48:59', '2017-03-02 14:48:59'),
(403, 28, 81, 'muvuti/kiima-kimwe', '2017-03-02 14:48:59', '2017-03-02 14:48:59'),
(404, 28, 81, 'kola', '2017-03-02 14:48:59', '2017-03-02 14:48:59'),
(405, 28, 82, 'mbiuni', '2017-03-02 14:49:00', '2017-03-02 14:49:00'),
(406, 28, 82, 'makutano/ mwala', '2017-03-02 14:49:00', '2017-03-02 14:49:00'),
(407, 28, 82, 'masii', '2017-03-02 14:49:00', '2017-03-02 14:49:00'),
(408, 28, 82, 'muthetheni', '2017-03-02 14:49:00', '2017-03-02 14:49:00'),
(409, 28, 82, 'wamunyu', '2017-03-02 14:49:00', '2017-03-02 14:49:00'),
(410, 28, 82, 'kibauni', '2017-03-02 14:49:00', '2017-03-02 14:49:00'),
(411, 23, 83, 'tulimani', '2017-03-02 14:49:00', '2017-03-02 14:49:00'),
(412, 23, 83, 'mbooni', '2017-03-02 14:49:00', '2017-03-02 14:49:00'),
(413, 23, 83, 'kithungo/kitundu', '2017-03-02 14:49:00', '2017-03-02 14:49:00'),
(414, 23, 83, 'kisau/kiteta', '2017-03-02 14:49:00', '2017-03-02 14:49:00'),
(415, 23, 83, 'waia/kako', '2017-03-02 14:49:00', '2017-03-02 14:49:00'),
(416, 23, 83, 'kalawa', '2017-03-02 14:49:00', '2017-03-02 14:49:00'),
(417, 23, 84, 'kasikeu', '2017-03-02 14:49:00', '2017-03-02 14:49:00'),
(418, 23, 84, 'mukaa', '2017-03-02 14:49:00', '2017-03-02 14:49:00'),
(419, 23, 84, 'kiima kiu/kalanzoni', '2017-03-02 14:49:00', '2017-03-02 14:49:00'),
(420, 23, 85, 'ukia', '2017-03-02 14:49:00', '2017-03-02 14:49:00'),
(421, 23, 85, 'kee', '2017-03-02 14:49:00', '2017-03-02 14:49:00'),
(422, 23, 85, 'kilungu', '2017-03-02 14:49:00', '2017-03-02 14:49:00'),
(423, 23, 85, 'ilima', '2017-03-02 14:49:00', '2017-03-02 14:49:00'),
(424, 23, 86, 'wote', '2017-03-02 14:49:00', '2017-03-02 14:49:00'),
(425, 23, 86, 'muvau/kikuumini', '2017-03-02 14:49:00', '2017-03-02 14:49:00'),
(426, 23, 86, 'mavindini', '2017-03-02 14:49:00', '2017-03-02 14:49:00'),
(427, 23, 86, 'kitise/kithuki', '2017-03-02 14:49:00', '2017-03-02 14:49:00'),
(428, 23, 86, 'kathonzweni', '2017-03-02 14:49:01', '2017-03-02 14:49:01'),
(429, 23, 86, 'nzaui/kilili/kalamba', '2017-03-02 14:49:01', '2017-03-02 14:49:01'),
(430, 23, 86, 'mbitini', '2017-03-02 14:49:01', '2017-03-02 14:49:01'),
(431, 23, 87, 'makindu', '2017-03-02 14:49:01', '2017-03-02 14:49:01'),
(432, 23, 87, 'nguumo', '2017-03-02 14:49:01', '2017-03-02 14:49:01'),
(433, 23, 87, 'kikumbulyu north', '2017-03-02 14:49:01', '2017-03-02 14:49:01'),
(434, 23, 87, 'kikumbulyu south', '2017-03-02 14:49:01', '2017-03-02 14:49:01'),
(435, 23, 87, 'nguu/masumba', '2017-03-02 14:49:01', '2017-03-02 14:49:01'),
(436, 23, 87, 'emali/mulala', '2017-03-02 14:49:01', '2017-03-02 14:49:01'),
(437, 23, 88, 'masongaleni', '2017-03-02 14:49:01', '2017-03-02 14:49:01'),
(438, 23, 88, 'mtito andei', '2017-03-02 14:49:01', '2017-03-02 14:49:01'),
(439, 23, 88, 'thange', '2017-03-02 14:49:02', '2017-03-02 14:49:02'),
(440, 23, 88, 'ivingoni/nzambani', '2017-03-02 14:49:02', '2017-03-02 14:49:02'),
(441, 34, 89, 'engineer', '2017-03-02 14:49:02', '2017-03-02 14:49:02'),
(442, 34, 89, 'gathara', '2017-03-02 14:49:02', '2017-03-02 14:49:02'),
(443, 34, 89, 'north kinangop', '2017-03-02 14:49:02', '2017-03-02 14:49:02'),
(444, 34, 89, 'murungaru', '2017-03-02 14:49:02', '2017-03-02 14:49:02'),
(445, 34, 89, 'njabini\\kiburu', '2017-03-02 14:49:02', '2017-03-02 14:49:02'),
(446, 34, 89, 'nyakio', '2017-03-02 14:49:02', '2017-03-02 14:49:02'),
(447, 34, 89, 'githabai', '2017-03-02 14:49:02', '2017-03-02 14:49:02'),
(448, 34, 89, 'magumu', '2017-03-02 14:49:02', '2017-03-02 14:49:02'),
(449, 34, 90, 'wanjohi', '2017-03-02 14:49:02', '2017-03-02 14:49:02'),
(450, 34, 90, 'kipipiri', '2017-03-02 14:49:02', '2017-03-02 14:49:02'),
(451, 34, 90, 'geta', '2017-03-02 14:49:02', '2017-03-02 14:49:02'),
(452, 34, 90, 'githioro', '2017-03-02 14:49:02', '2017-03-02 14:49:02'),
(453, 34, 91, 'karau', '2017-03-02 14:49:02', '2017-03-02 14:49:02'),
(454, 34, 91, 'kanjuiri ridge', '2017-03-02 14:49:02', '2017-03-02 14:49:02'),
(455, 34, 91, 'mirangine', '2017-03-02 14:49:02', '2017-03-02 14:49:02'),
(456, 34, 91, 'kaimbaga', '2017-03-02 14:49:02', '2017-03-02 14:49:02'),
(457, 34, 91, 'rurii', '2017-03-02 14:49:02', '2017-03-02 14:49:02'),
(458, 34, 92, 'gathanji', '2017-03-02 14:49:02', '2017-03-02 14:49:02'),
(459, 34, 92, 'gatimu', '2017-03-02 14:49:02', '2017-03-02 14:49:02'),
(460, 34, 92, 'weru', '2017-03-02 14:49:02', '2017-03-02 14:49:02'),
(461, 34, 92, 'charagita', '2017-03-02 14:49:02', '2017-03-02 14:49:02'),
(462, 34, 93, 'leshau pondo', '2017-03-02 14:49:02', '2017-03-02 14:49:02'),
(463, 34, 93, 'kiriita', '2017-03-02 14:49:02', '2017-03-02 14:49:02'),
(464, 34, 93, 'central', '2017-03-02 14:49:02', '2017-03-02 14:49:02'),
(465, 34, 93, 'shamata', '2017-03-02 14:49:02', '2017-03-02 14:49:02'),
(466, 35, 94, 'dedan kimanthi', '2017-03-02 14:49:02', '2017-03-02 14:49:02'),
(467, 35, 94, 'wamagana', '2017-03-02 14:49:03', '2017-03-02 14:49:03'),
(468, 35, 94, 'aguthi/gaaki', '2017-03-02 14:49:03', '2017-03-02 14:49:03'),
(469, 35, 95, 'mweiga', '2017-03-02 14:49:03', '2017-03-02 14:49:03'),
(470, 35, 95, 'naromoru kiamathaga', '2017-03-02 14:49:03', '2017-03-02 14:49:03'),
(471, 35, 95, 'mwiyogo/endarasha', '2017-03-02 14:49:03', '2017-03-02 14:49:03'),
(472, 35, 95, 'mugunda', '2017-03-02 14:49:03', '2017-03-02 14:49:03'),
(473, 35, 95, 'gatarakwa', '2017-03-02 14:49:03', '2017-03-02 14:49:03'),
(474, 35, 95, 'thegu river', '2017-03-02 14:49:03', '2017-03-02 14:49:03'),
(475, 35, 95, 'kabaru', '2017-03-02 14:49:03', '2017-03-02 14:49:03'),
(476, 35, 95, 'gakawa', '2017-03-02 14:49:03', '2017-03-02 14:49:03'),
(477, 35, 96, 'ruguru', '2017-03-02 14:49:03', '2017-03-02 14:49:03'),
(478, 35, 96, 'magutu', '2017-03-02 14:49:03', '2017-03-02 14:49:03'),
(479, 35, 96, 'iriaini', '2017-03-02 14:49:03', '2017-03-02 14:49:03'),
(480, 35, 96, 'konyu', '2017-03-02 14:49:03', '2017-03-02 14:49:03'),
(481, 35, 96, 'kirimukuyu', '2017-03-02 14:49:03', '2017-03-02 14:49:03'),
(482, 35, 96, 'karatina town', '2017-03-02 14:49:03', '2017-03-02 14:49:03'),
(483, 35, 97, 'mahiga', '2017-03-02 14:49:03', '2017-03-02 14:49:03'),
(484, 35, 97, 'iria-ini', '2017-03-02 14:49:03', '2017-03-02 14:49:03'),
(485, 35, 97, 'chinga', '2017-03-02 14:49:03', '2017-03-02 14:49:03'),
(486, 35, 97, 'karima', '2017-03-02 14:49:03', '2017-03-02 14:49:03'),
(487, 35, 98, 'gikondi', '2017-03-02 14:49:03', '2017-03-02 14:49:03'),
(488, 35, 98, 'rugi', '2017-03-02 14:49:03', '2017-03-02 14:49:03'),
(489, 35, 98, 'mukurwe-ini west', '2017-03-02 14:49:03', '2017-03-02 14:49:03'),
(490, 35, 98, 'mukurwe-ini central', '2017-03-02 14:49:03', '2017-03-02 14:49:03'),
(491, 35, 99, 'kiganjo/mathari', '2017-03-02 14:49:03', '2017-03-02 14:49:03'),
(492, 35, 99, 'rware', '2017-03-02 14:49:04', '2017-03-02 14:49:04'),
(493, 35, 99, 'gatitu/muruguru', '2017-03-02 14:49:04', '2017-03-02 14:49:04'),
(494, 35, 99, 'ruring''u', '2017-03-02 14:49:04', '2017-03-02 14:49:04'),
(495, 35, 99, 'kamakwa/mukaro', '2017-03-02 14:49:04', '2017-03-02 14:49:04'),
(496, 17, 100, 'mutithi', '2017-03-02 14:49:04', '2017-03-02 14:49:04'),
(497, 17, 100, 'kangai', '2017-03-02 14:49:04', '2017-03-02 14:49:04'),
(498, 17, 100, 'thiba', '2017-03-02 14:49:04', '2017-03-02 14:49:04'),
(499, 17, 100, 'wamumu', '2017-03-02 14:49:04', '2017-03-02 14:49:04'),
(500, 17, 100, 'nyangati', '2017-03-02 14:49:04', '2017-03-02 14:49:04'),
(501, 17, 100, 'murinduko', '2017-03-02 14:49:04', '2017-03-02 14:49:04'),
(502, 17, 100, 'gathigiriri', '2017-03-02 14:49:04', '2017-03-02 14:49:04'),
(503, 17, 100, 'tebere', '2017-03-02 14:49:04', '2017-03-02 14:49:04'),
(504, 17, 101, 'kabare', '2017-03-02 14:49:04', '2017-03-02 14:49:04'),
(505, 17, 101, 'baragwi', '2017-03-02 14:49:04', '2017-03-02 14:49:04'),
(506, 17, 101, 'njukiini', '2017-03-02 14:49:04', '2017-03-02 14:49:04'),
(507, 17, 101, 'ngariama', '2017-03-02 14:49:04', '2017-03-02 14:49:04'),
(508, 17, 101, 'karumandi', '2017-03-02 14:49:04', '2017-03-02 14:49:04'),
(509, 17, 102, 'mukure', '2017-03-02 14:49:04', '2017-03-02 14:49:04'),
(510, 17, 102, 'kiine', '2017-03-02 14:49:04', '2017-03-02 14:49:04'),
(511, 17, 102, 'kariti', '2017-03-02 14:49:04', '2017-03-02 14:49:04'),
(512, 17, 103, 'mutira', '2017-03-02 14:49:04', '2017-03-02 14:49:04'),
(513, 17, 103, 'kanyeki-ini', '2017-03-02 14:49:04', '2017-03-02 14:49:04'),
(514, 17, 103, 'kerugoya', '2017-03-02 14:49:04', '2017-03-02 14:49:04'),
(515, 17, 103, 'inoi', '2017-03-02 14:49:04', '2017-03-02 14:49:04'),
(516, 29, 104, 'kanyenyaini', '2017-03-02 14:49:05', '2017-03-02 14:49:05'),
(517, 29, 104, 'muguru', '2017-03-02 14:49:05', '2017-03-02 14:49:05'),
(518, 29, 104, 'rwathia', '2017-03-02 14:49:05', '2017-03-02 14:49:05'),
(519, 29, 105, 'gitugi', '2017-03-02 14:49:05', '2017-03-02 14:49:05'),
(520, 29, 105, 'kiru', '2017-03-02 14:49:05', '2017-03-02 14:49:05'),
(521, 29, 105, 'kamacharia', '2017-03-02 14:49:05', '2017-03-02 14:49:05'),
(522, 29, 106, 'wangu', '2017-03-02 14:49:05', '2017-03-02 14:49:05'),
(523, 29, 106, 'mugoiri', '2017-03-02 14:49:05', '2017-03-02 14:49:05'),
(524, 29, 106, 'mbiri', '2017-03-02 14:49:05', '2017-03-02 14:49:05'),
(525, 29, 106, 'township', '2017-03-02 14:49:05', '2017-03-02 14:49:05'),
(526, 29, 106, 'murarandia', '2017-03-02 14:49:05', '2017-03-02 14:49:05'),
(527, 29, 106, 'gaturi', '2017-03-02 14:49:05', '2017-03-02 14:49:05'),
(528, 29, 107, 'kahumbu', '2017-03-02 14:49:05', '2017-03-02 14:49:05'),
(529, 29, 107, 'muthithi', '2017-03-02 14:49:05', '2017-03-02 14:49:05'),
(530, 29, 107, 'kigumo', '2017-03-02 14:49:05', '2017-03-02 14:49:05'),
(531, 29, 107, 'kangari', '2017-03-02 14:49:05', '2017-03-02 14:49:05'),
(532, 29, 107, 'kinyona', '2017-03-02 14:49:05', '2017-03-02 14:49:05'),
(533, 29, 108, 'kimorori/wempa', '2017-03-02 14:49:05', '2017-03-02 14:49:05'),
(534, 29, 108, 'makuyu', '2017-03-02 14:49:05', '2017-03-02 14:49:05'),
(535, 29, 108, 'kambiti', '2017-03-02 14:49:06', '2017-03-02 14:49:06'),
(536, 29, 108, 'kamahuha', '2017-03-02 14:49:06', '2017-03-02 14:49:06'),
(537, 29, 108, 'ichagaki', '2017-03-02 14:49:06', '2017-03-02 14:49:06'),
(538, 29, 108, 'nginda', '2017-03-02 14:49:06', '2017-03-02 14:49:06'),
(539, 29, 109, 'ng''araria', '2017-03-02 14:49:06', '2017-03-02 14:49:06'),
(540, 29, 109, 'muruka', '2017-03-02 14:49:06', '2017-03-02 14:49:06'),
(541, 29, 109, 'kagundu-ini', '2017-03-02 14:49:06', '2017-03-02 14:49:06'),
(542, 29, 109, 'gaichanjiru', '2017-03-02 14:49:06', '2017-03-02 14:49:06'),
(543, 29, 109, 'ithiru', '2017-03-02 14:49:06', '2017-03-02 14:49:06'),
(544, 29, 109, 'ruchu', '2017-03-02 14:49:06', '2017-03-02 14:49:06'),
(545, 29, 110, 'ithanga', '2017-03-02 14:49:06', '2017-03-02 14:49:06'),
(546, 29, 110, 'kakuzi/mitubiri', '2017-03-02 14:49:06', '2017-03-02 14:49:06'),
(547, 29, 110, 'mugumo-ini', '2017-03-02 14:49:07', '2017-03-02 14:49:07'),
(548, 29, 110, 'kihumbu-ini', '2017-03-02 14:49:07', '2017-03-02 14:49:07'),
(549, 29, 110, 'gatanga', '2017-03-02 14:49:07', '2017-03-02 14:49:07'),
(550, 29, 110, 'kariara', '2017-03-02 14:49:07', '2017-03-02 14:49:07'),
(551, 15, 111, 'kiamwangi', '2017-03-02 14:49:07', '2017-03-02 14:49:07'),
(552, 15, 111, 'kiganjo', '2017-03-02 14:49:07', '2017-03-02 14:49:07'),
(553, 15, 111, 'ndarugu', '2017-03-02 14:49:07', '2017-03-02 14:49:07'),
(554, 15, 111, 'ngenda', '2017-03-02 14:49:07', '2017-03-02 14:49:07'),
(555, 15, 112, 'gituamba', '2017-03-02 14:49:07', '2017-03-02 14:49:07'),
(556, 15, 112, 'githobokoni', '2017-03-02 14:49:07', '2017-03-02 14:49:07'),
(557, 15, 112, 'chania', '2017-03-02 14:49:07', '2017-03-02 14:49:07'),
(558, 15, 112, 'mang''u', '2017-03-02 14:49:07', '2017-03-02 14:49:07'),
(559, 15, 113, 'murera', '2017-03-02 14:49:07', '2017-03-02 14:49:07'),
(560, 15, 113, 'theta', '2017-03-02 14:49:08', '2017-03-02 14:49:08'),
(561, 15, 113, 'juja', '2017-03-02 14:49:08', '2017-03-02 14:49:08'),
(562, 15, 113, 'witeithie', '2017-03-02 14:49:08', '2017-03-02 14:49:08'),
(563, 15, 113, 'kalimoni', '2017-03-02 14:49:08', '2017-03-02 14:49:08'),
(564, 15, 114, 'township', '2017-03-02 14:49:08', '2017-03-02 14:49:08'),
(565, 15, 114, 'kamenu', '2017-03-02 14:49:08', '2017-03-02 14:49:08'),
(566, 15, 114, 'hospital', '2017-03-02 14:49:08', '2017-03-02 14:49:08'),
(567, 15, 114, 'gatuanyaga', '2017-03-02 14:49:08', '2017-03-02 14:49:08'),
(568, 15, 114, 'ngoliba', '2017-03-02 14:49:08', '2017-03-02 14:49:08'),
(569, 15, 115, 'gitothua', '2017-03-02 14:49:08', '2017-03-02 14:49:08'),
(570, 15, 115, 'biashara', '2017-03-02 14:49:08', '2017-03-02 14:49:08'),
(571, 15, 115, 'gatongora', '2017-03-02 14:49:08', '2017-03-02 14:49:08'),
(572, 15, 115, 'kahawa sukari', '2017-03-02 14:49:08', '2017-03-02 14:49:08'),
(573, 15, 115, 'kahawa wendani', '2017-03-02 14:49:08', '2017-03-02 14:49:08'),
(574, 15, 115, 'kiuu', '2017-03-02 14:49:09', '2017-03-02 14:49:09'),
(575, 15, 115, 'mwiki', '2017-03-02 14:49:09', '2017-03-02 14:49:09'),
(576, 15, 115, 'mwihoko', '2017-03-02 14:49:09', '2017-03-02 14:49:09'),
(577, 15, 116, 'githunguri', '2017-03-02 14:49:09', '2017-03-02 14:49:09'),
(578, 15, 116, 'githiga', '2017-03-02 14:49:09', '2017-03-02 14:49:09'),
(579, 15, 116, 'ikinu', '2017-03-02 14:49:09', '2017-03-02 14:49:09'),
(580, 15, 116, 'ngewa', '2017-03-02 14:49:09', '2017-03-02 14:49:09'),
(581, 15, 116, 'komothai', '2017-03-02 14:49:09', '2017-03-02 14:49:09'),
(582, 15, 117, 'ting''ang''a', '2017-03-02 14:49:09', '2017-03-02 14:49:09'),
(583, 15, 117, 'ndumberi', '2017-03-02 14:49:09', '2017-03-02 14:49:09'),
(584, 15, 117, 'riabai', '2017-03-02 14:49:09', '2017-03-02 14:49:09'),
(585, 15, 117, 'township', '2017-03-02 14:49:09', '2017-03-02 14:49:09'),
(586, 15, 118, 'cianda', '2017-03-02 14:49:09', '2017-03-02 14:49:09'),
(587, 15, 118, 'karuri', '2017-03-02 14:49:10', '2017-03-02 14:49:10'),
(588, 15, 118, 'ndenderu', '2017-03-02 14:49:10', '2017-03-02 14:49:10'),
(589, 15, 118, 'muchatha', '2017-03-02 14:49:10', '2017-03-02 14:49:10'),
(590, 15, 118, 'kihara', '2017-03-02 14:49:10', '2017-03-02 14:49:10'),
(591, 15, 119, 'gitaru', '2017-03-02 14:49:10', '2017-03-02 14:49:10'),
(592, 15, 119, 'muguga', '2017-03-02 14:49:10', '2017-03-02 14:49:10'),
(593, 15, 119, 'nyadhuna', '2017-03-02 14:49:10', '2017-03-02 14:49:10'),
(594, 15, 119, 'kabete', '2017-03-02 14:49:10', '2017-03-02 14:49:10'),
(595, 15, 119, 'uthiru', '2017-03-02 14:49:10', '2017-03-02 14:49:10'),
(596, 15, 120, 'karai', '2017-03-02 14:49:10', '2017-03-02 14:49:10'),
(597, 15, 120, 'nachu', '2017-03-02 14:49:10', '2017-03-02 14:49:10'),
(598, 15, 120, 'sigona', '2017-03-02 14:49:10', '2017-03-02 14:49:10'),
(599, 15, 120, 'kikuyu', '2017-03-02 14:49:10', '2017-03-02 14:49:10'),
(600, 15, 120, 'kinoo', '2017-03-02 14:49:10', '2017-03-02 14:49:10'),
(601, 15, 121, 'bibirioni', '2017-03-02 14:49:10', '2017-03-02 14:49:10'),
(602, 15, 121, 'limuru central', '2017-03-02 14:49:10', '2017-03-02 14:49:10'),
(603, 15, 121, 'ndeiya', '2017-03-02 14:49:10', '2017-03-02 14:49:10'),
(604, 15, 121, 'limuru east', '2017-03-02 14:49:10', '2017-03-02 14:49:10'),
(605, 15, 121, 'ngecha tigoni', '2017-03-02 14:49:10', '2017-03-02 14:49:10'),
(606, 15, 122, 'kinale', '2017-03-02 14:49:10', '2017-03-02 14:49:10'),
(607, 15, 122, 'kijabe', '2017-03-02 14:49:10', '2017-03-02 14:49:10'),
(608, 15, 122, 'nyanduma', '2017-03-02 14:49:10', '2017-03-02 14:49:10'),
(609, 15, 122, 'kamburu', '2017-03-02 14:49:10', '2017-03-02 14:49:10'),
(610, 15, 122, 'lari/kirenga', '2017-03-02 14:49:10', '2017-03-02 14:49:10'),
(611, 42, 123, 'kaeris', '2017-03-02 14:49:10', '2017-03-02 14:49:10'),
(612, 42, 123, 'lake zone', '2017-03-02 14:49:10', '2017-03-02 14:49:10'),
(613, 42, 123, 'lapur', '2017-03-02 14:49:10', '2017-03-02 14:49:10'),
(614, 42, 123, 'kaaleng/kaikor', '2017-03-02 14:49:10', '2017-03-02 14:49:10'),
(615, 42, 123, 'kibish', '2017-03-02 14:49:10', '2017-03-02 14:49:10'),
(616, 42, 123, 'nakalale', '2017-03-02 14:49:11', '2017-03-02 14:49:11'),
(617, 42, 124, 'kakuma', '2017-03-02 14:49:11', '2017-03-02 14:49:11'),
(618, 42, 124, 'lopur', '2017-03-02 14:49:11', '2017-03-02 14:49:11'),
(619, 42, 124, 'letea', '2017-03-02 14:49:11', '2017-03-02 14:49:11'),
(620, 42, 124, 'songot', '2017-03-02 14:49:11', '2017-03-02 14:49:11'),
(621, 42, 124, 'kalobeyei', '2017-03-02 14:49:11', '2017-03-02 14:49:11'),
(622, 42, 124, 'lokichoggio', '2017-03-02 14:49:11', '2017-03-02 14:49:11'),
(623, 42, 124, 'nanaam', '2017-03-02 14:49:11', '2017-03-02 14:49:11'),
(624, 42, 125, 'kerio delta', '2017-03-02 14:49:11', '2017-03-02 14:49:11'),
(625, 42, 125, 'kang''atotha', '2017-03-02 14:49:11', '2017-03-02 14:49:11'),
(626, 42, 125, 'kalokol', '2017-03-02 14:49:11', '2017-03-02 14:49:11'),
(627, 42, 125, 'lodwar township', '2017-03-02 14:49:11', '2017-03-02 14:49:11'),
(628, 42, 125, 'kanamkemer', '2017-03-02 14:49:11', '2017-03-02 14:49:11'),
(629, 42, 126, 'kotaruk/lobei', '2017-03-02 14:49:11', '2017-03-02 14:49:11'),
(630, 42, 126, 'turkwel', '2017-03-02 14:49:11', '2017-03-02 14:49:11'),
(631, 42, 126, 'loima', '2017-03-02 14:49:11', '2017-03-02 14:49:11'),
(632, 42, 126, 'lokiriama/lorengippi', '2017-03-02 14:49:11', '2017-03-02 14:49:11'),
(633, 42, 127, 'kaputir', '2017-03-02 14:49:11', '2017-03-02 14:49:11'),
(634, 42, 127, 'katilu', '2017-03-02 14:49:11', '2017-03-02 14:49:11'),
(635, 42, 127, 'lobokat', '2017-03-02 14:49:11', '2017-03-02 14:49:11'),
(636, 42, 127, 'kalapata', '2017-03-02 14:49:11', '2017-03-02 14:49:11'),
(637, 42, 127, 'lokichar', '2017-03-02 14:49:12', '2017-03-02 14:49:12'),
(638, 42, 128, 'kapedo/napeitom', '2017-03-02 14:49:12', '2017-03-02 14:49:12'),
(639, 42, 128, 'katilia', '2017-03-02 14:49:12', '2017-03-02 14:49:12'),
(640, 42, 128, 'lokori/kochodin', '2017-03-02 14:49:12', '2017-03-02 14:49:12'),
(641, 46, 129, 'riwo', '2017-03-02 14:49:12', '2017-03-02 14:49:12'),
(642, 46, 129, 'kapenguria', '2017-03-02 14:49:12', '2017-03-02 14:49:12'),
(643, 46, 129, 'mnagei', '2017-03-02 14:49:12', '2017-03-02 14:49:12'),
(644, 46, 129, 'siyoi', '2017-03-02 14:49:12', '2017-03-02 14:49:12'),
(645, 46, 129, 'endugh', '2017-03-02 14:49:12', '2017-03-02 14:49:12'),
(646, 46, 129, 'sook', '2017-03-02 14:49:12', '2017-03-02 14:49:12'),
(647, 46, 130, 'sekerr', '2017-03-02 14:49:12', '2017-03-02 14:49:12'),
(648, 46, 130, 'masool', '2017-03-02 14:49:12', '2017-03-02 14:49:12'),
(649, 46, 130, 'lomut', '2017-03-02 14:49:12', '2017-03-02 14:49:12'),
(650, 46, 130, 'weiwei', '2017-03-02 14:49:12', '2017-03-02 14:49:12'),
(651, 46, 131, 'suam', '2017-03-02 14:49:12', '2017-03-02 14:49:12'),
(652, 46, 131, 'kodich', '2017-03-02 14:49:12', '2017-03-02 14:49:12'),
(653, 46, 131, 'kapckok', '2017-03-02 14:49:12', '2017-03-02 14:49:12'),
(654, 46, 131, 'kasei', '2017-03-02 14:49:12', '2017-03-02 14:49:12'),
(655, 46, 131, 'kiwawa', '2017-03-02 14:49:12', '2017-03-02 14:49:12'),
(656, 46, 131, 'alale', '2017-03-02 14:49:12', '2017-03-02 14:49:12'),
(657, 46, 132, 'chepareria', '2017-03-02 14:49:12', '2017-03-02 14:49:12'),
(658, 46, 132, 'batei', '2017-03-02 14:49:12', '2017-03-02 14:49:12'),
(659, 46, 132, 'lelan', '2017-03-02 14:49:12', '2017-03-02 14:49:12'),
(660, 46, 132, 'tapach', '2017-03-02 14:49:13', '2017-03-02 14:49:13'),
(661, 36, 133, 'lodokejek', '2017-03-02 14:49:13', '2017-03-02 14:49:13'),
(662, 36, 133, 'suguta marmar', '2017-03-02 14:49:13', '2017-03-02 14:49:13'),
(663, 36, 133, 'maralal', '2017-03-02 14:49:13', '2017-03-02 14:49:13'),
(664, 36, 133, 'loosuk', '2017-03-02 14:49:13', '2017-03-02 14:49:13'),
(665, 36, 133, 'poro', '2017-03-02 14:49:13', '2017-03-02 14:49:13'),
(666, 36, 134, 'el-barta', '2017-03-02 14:49:13', '2017-03-02 14:49:13'),
(667, 36, 134, 'nachola', '2017-03-02 14:49:13', '2017-03-02 14:49:13'),
(668, 36, 134, 'ndoto', '2017-03-02 14:49:13', '2017-03-02 14:49:13'),
(669, 36, 134, 'nyiro', '2017-03-02 14:49:13', '2017-03-02 14:49:13'),
(670, 36, 134, 'angata nanyokie', '2017-03-02 14:49:13', '2017-03-02 14:49:13'),
(671, 36, 134, 'baawa', '2017-03-02 14:49:13', '2017-03-02 14:49:13'),
(672, 36, 135, 'waso', '2017-03-02 14:49:13', '2017-03-02 14:49:13'),
(673, 36, 135, 'wamba west', '2017-03-02 14:49:13', '2017-03-02 14:49:13'),
(674, 36, 135, 'wamba east', '2017-03-02 14:49:13', '2017-03-02 14:49:13'),
(675, 36, 135, 'wamba north', '2017-03-02 14:49:13', '2017-03-02 14:49:13'),
(676, 41, 136, 'kapomboi', '2017-03-02 14:49:13', '2017-03-02 14:49:13'),
(677, 41, 136, 'kwanza', '2017-03-02 14:49:13', '2017-03-02 14:49:13'),
(678, 41, 136, 'keiyo', '2017-03-02 14:49:13', '2017-03-02 14:49:13'),
(679, 41, 136, 'bidii', '2017-03-02 14:49:13', '2017-03-02 14:49:13'),
(680, 41, 137, 'chepchoina', '2017-03-02 14:49:13', '2017-03-02 14:49:13'),
(681, 41, 137, 'endebess', '2017-03-02 14:49:13', '2017-03-02 14:49:13'),
(682, 41, 137, 'matumbei', '2017-03-02 14:49:13', '2017-03-02 14:49:13'),
(683, 41, 138, 'kinyoro', '2017-03-02 14:49:13', '2017-03-02 14:49:13'),
(684, 41, 138, 'matisi', '2017-03-02 14:49:13', '2017-03-02 14:49:13'),
(685, 41, 138, 'tuwani', '2017-03-02 14:49:13', '2017-03-02 14:49:13'),
(686, 41, 138, 'saboti', '2017-03-02 14:49:13', '2017-03-02 14:49:13'),
(687, 41, 138, 'machewa', '2017-03-02 14:49:14', '2017-03-02 14:49:14'),
(688, 41, 139, 'kiminini', '2017-03-02 14:49:14', '2017-03-02 14:49:14'),
(689, 41, 139, 'waitaluk', '2017-03-02 14:49:14', '2017-03-02 14:49:14'),
(690, 41, 139, 'sirende', '2017-03-02 14:49:14', '2017-03-02 14:49:14'),
(691, 41, 139, 'hospital', '2017-03-02 14:49:14', '2017-03-02 14:49:14'),
(692, 41, 139, 'sikhendu', '2017-03-02 14:49:14', '2017-03-02 14:49:14'),
(693, 41, 139, 'nabiswa', '2017-03-02 14:49:14', '2017-03-02 14:49:14'),
(694, 41, 140, 'sinyerere', '2017-03-02 14:49:14', '2017-03-02 14:49:14'),
(695, 41, 140, 'makutano', '2017-03-02 14:49:14', '2017-03-02 14:49:14'),
(696, 41, 140, 'kaplamai', '2017-03-02 14:49:14', '2017-03-02 14:49:14'),
(697, 41, 140, 'motosiet', '2017-03-02 14:49:14', '2017-03-02 14:49:14'),
(698, 41, 140, 'cherangany/suwerwa', '2017-03-02 14:49:14', '2017-03-02 14:49:14'),
(699, 41, 140, 'chepsiro/kiptoror', '2017-03-02 14:49:14', '2017-03-02 14:49:14');
INSERT INTO `wards` (`id`, `county_id`, `sub_county_id`, `ward_name`, `created_at`, `updated_at`) VALUES
(700, 41, 140, 'sitatunga', '2017-03-02 14:49:14', '2017-03-02 14:49:14'),
(701, 43, 141, 'moi''s bridge', '2017-03-02 14:49:14', '2017-03-02 14:49:14'),
(702, 43, 141, 'kapkures', '2017-03-02 14:49:14', '2017-03-02 14:49:14'),
(703, 43, 141, 'ziwa', '2017-03-02 14:49:14', '2017-03-02 14:49:14'),
(704, 43, 141, 'segero/barsombe', '2017-03-02 14:49:14', '2017-03-02 14:49:14'),
(705, 43, 141, 'kipsomba', '2017-03-02 14:49:14', '2017-03-02 14:49:14'),
(706, 43, 141, 'soy', '2017-03-02 14:49:14', '2017-03-02 14:49:14'),
(707, 43, 141, 'kuinet/kapsuswa', '2017-03-02 14:49:14', '2017-03-02 14:49:14'),
(708, 43, 142, 'ngenyilel', '2017-03-02 14:49:14', '2017-03-02 14:49:14'),
(709, 43, 142, 'tapsagoi', '2017-03-02 14:49:14', '2017-03-02 14:49:14'),
(710, 43, 142, 'kamagut', '2017-03-02 14:49:15', '2017-03-02 14:49:15'),
(711, 43, 142, 'kiplombe', '2017-03-02 14:49:15', '2017-03-02 14:49:15'),
(712, 43, 142, 'kapsaos', '2017-03-02 14:49:15', '2017-03-02 14:49:15'),
(713, 43, 142, 'huruma', '2017-03-02 14:49:15', '2017-03-02 14:49:15'),
(714, 43, 143, 'tembelio', '2017-03-02 14:49:15', '2017-03-02 14:49:15'),
(715, 43, 143, 'sergoit', '2017-03-02 14:49:15', '2017-03-02 14:49:15'),
(716, 43, 143, 'karuna/meibeki', '2017-03-02 14:49:15', '2017-03-02 14:49:15'),
(717, 43, 143, 'moiben', '2017-03-02 14:49:15', '2017-03-02 14:49:15'),
(718, 43, 143, 'kimumu', '2017-03-02 14:49:15', '2017-03-02 14:49:15'),
(719, 43, 144, 'kapsoya', '2017-03-02 14:49:15', '2017-03-02 14:49:15'),
(720, 43, 144, 'kaptagat', '2017-03-02 14:49:15', '2017-03-02 14:49:15'),
(721, 43, 144, 'ainabkoi/olare', '2017-03-02 14:49:15', '2017-03-02 14:49:15'),
(722, 43, 145, 'simat/kapseret', '2017-03-02 14:49:15', '2017-03-02 14:49:15'),
(723, 43, 145, 'kipkenyo', '2017-03-02 14:49:15', '2017-03-02 14:49:15'),
(724, 43, 145, 'ngeria', '2017-03-02 14:49:15', '2017-03-02 14:49:15'),
(725, 43, 145, 'megun', '2017-03-02 14:49:15', '2017-03-02 14:49:15'),
(726, 43, 145, 'langas', '2017-03-02 14:49:15', '2017-03-02 14:49:15'),
(727, 43, 146, 'racecourse', '2017-03-02 14:49:15', '2017-03-02 14:49:15'),
(728, 43, 146, 'cheptiret/kipchamo', '2017-03-02 14:49:15', '2017-03-02 14:49:15'),
(729, 43, 146, 'tulwet/chuiyat', '2017-03-02 14:49:15', '2017-03-02 14:49:15'),
(730, 43, 146, 'tarakwa', '2017-03-02 14:49:15', '2017-03-02 14:49:15'),
(731, 7, 147, 'kapyego', '2017-03-02 14:49:15', '2017-03-02 14:49:15'),
(732, 7, 147, 'sambirir', '2017-03-02 14:49:15', '2017-03-02 14:49:15'),
(733, 7, 147, 'endo', '2017-03-02 14:49:15', '2017-03-02 14:49:15'),
(734, 7, 147, 'embobut / embulot', '2017-03-02 14:49:16', '2017-03-02 14:49:16'),
(735, 7, 148, 'lelan', '2017-03-02 14:49:16', '2017-03-02 14:49:16'),
(736, 7, 148, 'sengwer', '2017-03-02 14:49:16', '2017-03-02 14:49:16'),
(737, 7, 148, 'cherang''any/chebororwa', '2017-03-02 14:49:16', '2017-03-02 14:49:16'),
(738, 7, 148, 'moiben/kuserwo', '2017-03-02 14:49:16', '2017-03-02 14:49:16'),
(739, 7, 148, 'kapsowar', '2017-03-02 14:49:16', '2017-03-02 14:49:16'),
(740, 7, 148, 'arror', '2017-03-02 14:49:16', '2017-03-02 14:49:16'),
(741, 7, 149, 'emsoo', '2017-03-02 14:49:16', '2017-03-02 14:49:16'),
(742, 7, 149, 'kamariny', '2017-03-02 14:49:16', '2017-03-02 14:49:16'),
(743, 7, 149, 'kapchemutwa', '2017-03-02 14:49:16', '2017-03-02 14:49:16'),
(744, 7, 149, 'tambach', '2017-03-02 14:49:16', '2017-03-02 14:49:16'),
(745, 7, 150, 'kaptarakwa', '2017-03-02 14:49:16', '2017-03-02 14:49:16'),
(746, 7, 150, 'chepkorio', '2017-03-02 14:49:16', '2017-03-02 14:49:16'),
(747, 7, 150, 'soy north', '2017-03-02 14:49:16', '2017-03-02 14:49:16'),
(748, 7, 150, 'soy south', '2017-03-02 14:49:16', '2017-03-02 14:49:16'),
(749, 7, 150, 'kabiemit', '2017-03-02 14:49:16', '2017-03-02 14:49:16'),
(750, 7, 150, 'metkei', '2017-03-02 14:49:16', '2017-03-02 14:49:16'),
(751, 31, 151, 'songhor/soba', '2017-03-02 14:49:16', '2017-03-02 14:49:16'),
(752, 31, 151, 'tindiret', '2017-03-02 14:49:16', '2017-03-02 14:49:16'),
(753, 31, 151, 'chemelil/chemase', '2017-03-02 14:49:16', '2017-03-02 14:49:16'),
(754, 31, 151, 'kapsimotwo', '2017-03-02 14:49:16', '2017-03-02 14:49:16'),
(755, 31, 152, 'kabwareng', '2017-03-02 14:49:16', '2017-03-02 14:49:16'),
(756, 31, 152, 'terik', '2017-03-02 14:49:16', '2017-03-02 14:49:16'),
(757, 31, 152, 'kemeloi-maraba', '2017-03-02 14:49:16', '2017-03-02 14:49:16'),
(758, 31, 152, 'kobujoi', '2017-03-02 14:49:17', '2017-03-02 14:49:17'),
(759, 31, 152, 'kaptumo-kaboi', '2017-03-02 14:49:17', '2017-03-02 14:49:17'),
(760, 31, 152, 'koyo-ndurio', '2017-03-02 14:49:17', '2017-03-02 14:49:17'),
(761, 31, 153, 'nandi hills', '2017-03-02 14:49:17', '2017-03-02 14:49:17'),
(762, 31, 153, 'chepkunyuk', '2017-03-02 14:49:17', '2017-03-02 14:49:17'),
(763, 31, 153, 'ol''lessos', '2017-03-02 14:49:17', '2017-03-02 14:49:17'),
(764, 31, 153, 'kapchorua', '2017-03-02 14:49:17', '2017-03-02 14:49:17'),
(765, 31, 154, 'chemundu/kapng''etuny', '2017-03-02 14:49:17', '2017-03-02 14:49:17'),
(766, 31, 154, 'kosirai', '2017-03-02 14:49:17', '2017-03-02 14:49:17'),
(767, 31, 154, 'lelmokwo/ngechek', '2017-03-02 14:49:17', '2017-03-02 14:49:17'),
(768, 31, 154, 'kaptel/kamoiywo', '2017-03-02 14:49:17', '2017-03-02 14:49:17'),
(769, 31, 154, 'kiptuya', '2017-03-02 14:49:17', '2017-03-02 14:49:17'),
(770, 31, 155, 'chepkumia', '2017-03-02 14:49:17', '2017-03-02 14:49:17'),
(771, 31, 155, 'kapkangani', '2017-03-02 14:49:17', '2017-03-02 14:49:17'),
(772, 31, 155, 'kapsabet', '2017-03-02 14:49:17', '2017-03-02 14:49:17'),
(773, 31, 155, 'kilibwoni', '2017-03-02 14:49:17', '2017-03-02 14:49:17'),
(774, 31, 156, 'chepterwai', '2017-03-02 14:49:17', '2017-03-02 14:49:17'),
(775, 31, 156, 'kipkaren', '2017-03-02 14:49:17', '2017-03-02 14:49:17'),
(776, 31, 156, 'kurgung/surungai', '2017-03-02 14:49:17', '2017-03-02 14:49:17'),
(777, 31, 156, 'kabiyet', '2017-03-02 14:49:17', '2017-03-02 14:49:17'),
(778, 31, 156, 'ndalat', '2017-03-02 14:49:17', '2017-03-02 14:49:17'),
(779, 31, 156, 'kabisaga', '2017-03-02 14:49:17', '2017-03-02 14:49:17'),
(780, 31, 156, 'sangalo/kebulonik', '2017-03-02 14:49:18', '2017-03-02 14:49:18'),
(781, 47, 157, 'tirioko', '2017-03-02 14:49:18', '2017-03-02 14:49:18'),
(782, 47, 157, 'kolowa', '2017-03-02 14:49:18', '2017-03-02 14:49:18'),
(783, 47, 157, 'ribkwo', '2017-03-02 14:49:18', '2017-03-02 14:49:18'),
(784, 47, 157, 'silale', '2017-03-02 14:49:18', '2017-03-02 14:49:18'),
(785, 47, 157, 'loiyamorock', '2017-03-02 14:49:18', '2017-03-02 14:49:18'),
(786, 47, 157, 'tangulbei/korossi', '2017-03-02 14:49:18', '2017-03-02 14:49:18'),
(787, 47, 157, 'churo/amaya', '2017-03-02 14:49:18', '2017-03-02 14:49:18'),
(788, 47, 158, 'barwessa', '2017-03-02 14:49:18', '2017-03-02 14:49:18'),
(789, 47, 158, 'kabartonjo', '2017-03-02 14:49:18', '2017-03-02 14:49:18'),
(790, 47, 158, 'saimo/kipsaraman', '2017-03-02 14:49:18', '2017-03-02 14:49:18'),
(791, 47, 158, 'saimo/soi', '2017-03-02 14:49:18', '2017-03-02 14:49:18'),
(792, 47, 158, 'bartabwa', '2017-03-02 14:49:18', '2017-03-02 14:49:18'),
(793, 47, 159, 'kabarnet', '2017-03-02 14:49:18', '2017-03-02 14:49:18'),
(794, 47, 159, 'sacho', '2017-03-02 14:49:18', '2017-03-02 14:49:18'),
(795, 47, 159, 'tenges', '2017-03-02 14:49:18', '2017-03-02 14:49:18'),
(796, 47, 159, 'ewalel chapchap', '2017-03-02 14:49:18', '2017-03-02 14:49:18'),
(797, 47, 159, 'kapropita', '2017-03-02 14:49:18', '2017-03-02 14:49:18'),
(798, 47, 160, 'marigat', '2017-03-02 14:49:18', '2017-03-02 14:49:18'),
(799, 47, 160, 'ilchamus', '2017-03-02 14:49:18', '2017-03-02 14:49:18'),
(800, 47, 160, 'mochongoi', '2017-03-02 14:49:18', '2017-03-02 14:49:18'),
(801, 47, 160, 'mukutani', '2017-03-02 14:49:18', '2017-03-02 14:49:18'),
(802, 47, 161, 'mogotio', '2017-03-02 14:49:18', '2017-03-02 14:49:18'),
(803, 47, 161, 'emining', '2017-03-02 14:49:18', '2017-03-02 14:49:18'),
(804, 47, 161, 'kisanana', '2017-03-02 14:49:18', '2017-03-02 14:49:18'),
(805, 47, 162, 'lembus', '2017-03-02 14:49:18', '2017-03-02 14:49:18'),
(806, 47, 162, 'lembus kwen', '2017-03-02 14:49:19', '2017-03-02 14:49:19'),
(807, 47, 162, 'ravine', '2017-03-02 14:49:19', '2017-03-02 14:49:19'),
(808, 47, 162, 'mumberes/maji mazuri', '2017-03-02 14:49:19', '2017-03-02 14:49:19'),
(809, 47, 162, 'lembus/perkerra', '2017-03-02 14:49:19', '2017-03-02 14:49:19'),
(810, 47, 162, 'koibatek', '2017-03-02 14:49:19', '2017-03-02 14:49:19'),
(811, 21, 163, 'olmoran', '2017-03-02 14:49:19', '2017-03-02 14:49:19'),
(812, 21, 163, 'rumuruti township', '2017-03-02 14:49:19', '2017-03-02 14:49:19'),
(813, 21, 163, 'kinamba', '2017-03-02 14:49:19', '2017-03-02 14:49:19'),
(814, 21, 163, 'marmanet', '2017-03-02 14:49:19', '2017-03-02 14:49:19'),
(815, 21, 163, 'igwamiti', '2017-03-02 14:49:19', '2017-03-02 14:49:19'),
(816, 21, 163, 'salama', '2017-03-02 14:49:19', '2017-03-02 14:49:19'),
(817, 21, 164, 'ngobit', '2017-03-02 14:49:19', '2017-03-02 14:49:19'),
(818, 21, 164, 'tigithi', '2017-03-02 14:49:19', '2017-03-02 14:49:19'),
(819, 21, 164, 'thingithu', '2017-03-02 14:49:19', '2017-03-02 14:49:19'),
(820, 21, 164, 'nanyuki', '2017-03-02 14:49:19', '2017-03-02 14:49:19'),
(821, 21, 164, 'umande', '2017-03-02 14:49:19', '2017-03-02 14:49:19'),
(822, 21, 165, 'sosian', '2017-03-02 14:49:19', '2017-03-02 14:49:19'),
(823, 21, 165, 'segera', '2017-03-02 14:49:19', '2017-03-02 14:49:19'),
(824, 21, 165, 'mukogondo west', '2017-03-02 14:49:19', '2017-03-02 14:49:19'),
(825, 21, 165, 'mukogondo east', '2017-03-02 14:49:19', '2017-03-02 14:49:19'),
(826, 30, 166, 'mariashoni', '2017-03-02 14:49:19', '2017-03-02 14:49:19'),
(827, 30, 166, 'elburgon', '2017-03-02 14:49:19', '2017-03-02 14:49:19'),
(828, 30, 166, 'turi', '2017-03-02 14:49:20', '2017-03-02 14:49:20'),
(829, 30, 166, 'molo', '2017-03-02 14:49:20', '2017-03-02 14:49:20'),
(830, 30, 167, 'maunarok', '2017-03-02 14:49:20', '2017-03-02 14:49:20'),
(831, 30, 167, 'mauche', '2017-03-02 14:49:20', '2017-03-02 14:49:20'),
(832, 30, 167, 'kihingo', '2017-03-02 14:49:20', '2017-03-02 14:49:20'),
(833, 30, 167, 'nessuit', '2017-03-02 14:49:20', '2017-03-02 14:49:20'),
(834, 30, 167, 'lare', '2017-03-02 14:49:20', '2017-03-02 14:49:20'),
(835, 30, 167, 'njoro', '2017-03-02 14:49:20', '2017-03-02 14:49:20'),
(836, 30, 168, 'biashara', '2017-03-02 14:49:20', '2017-03-02 14:49:20'),
(837, 30, 168, 'hells gate', '2017-03-02 14:49:20', '2017-03-02 14:49:20'),
(838, 30, 168, 'lakeview', '2017-03-02 14:49:21', '2017-03-02 14:49:21'),
(839, 30, 168, 'maai-mahiu', '2017-03-02 14:49:21', '2017-03-02 14:49:21'),
(840, 30, 168, 'maiella', '2017-03-02 14:49:21', '2017-03-02 14:49:21'),
(841, 30, 168, 'olkaria', '2017-03-02 14:49:21', '2017-03-02 14:49:21'),
(842, 30, 168, 'naivasha east', '2017-03-02 14:49:21', '2017-03-02 14:49:21'),
(843, 30, 168, 'viwandani', '2017-03-02 14:49:21', '2017-03-02 14:49:21'),
(844, 30, 169, 'gilgil', '2017-03-02 14:49:21', '2017-03-02 14:49:21'),
(845, 30, 169, 'elementaita', '2017-03-02 14:49:21', '2017-03-02 14:49:21'),
(846, 30, 169, 'mbaruk/eburu', '2017-03-02 14:49:21', '2017-03-02 14:49:21'),
(847, 30, 169, 'malewa west', '2017-03-02 14:49:21', '2017-03-02 14:49:21'),
(848, 30, 169, 'murindati', '2017-03-02 14:49:21', '2017-03-02 14:49:21'),
(849, 30, 170, 'amalo', '2017-03-02 14:49:21', '2017-03-02 14:49:21'),
(850, 30, 170, 'keringet', '2017-03-02 14:49:21', '2017-03-02 14:49:21'),
(851, 30, 170, 'kiptagich', '2017-03-02 14:49:22', '2017-03-02 14:49:22'),
(852, 30, 170, 'tinet', '2017-03-02 14:49:22', '2017-03-02 14:49:22'),
(853, 30, 171, 'kiptororo', '2017-03-02 14:49:22', '2017-03-02 14:49:22'),
(854, 30, 171, 'nyota', '2017-03-02 14:49:22', '2017-03-02 14:49:22'),
(855, 30, 171, 'sirikwa', '2017-03-02 14:49:22', '2017-03-02 14:49:22'),
(856, 30, 171, 'kamara', '2017-03-02 14:49:22', '2017-03-02 14:49:22'),
(857, 30, 172, 'subukia', '2017-03-02 14:49:22', '2017-03-02 14:49:22'),
(858, 30, 172, 'waseges', '2017-03-02 14:49:22', '2017-03-02 14:49:22'),
(859, 30, 172, 'kabazi', '2017-03-02 14:49:22', '2017-03-02 14:49:22'),
(860, 30, 173, 'menengai west', '2017-03-02 14:49:22', '2017-03-02 14:49:22'),
(861, 30, 173, 'soin', '2017-03-02 14:49:22', '2017-03-02 14:49:22'),
(862, 30, 173, 'visoi', '2017-03-02 14:49:22', '2017-03-02 14:49:22'),
(863, 30, 173, 'mosop', '2017-03-02 14:49:22', '2017-03-02 14:49:22'),
(864, 30, 173, 'solai', '2017-03-02 14:49:22', '2017-03-02 14:49:22'),
(865, 30, 174, 'dundori', '2017-03-02 14:49:23', '2017-03-02 14:49:23'),
(866, 30, 174, 'kabatini', '2017-03-02 14:49:23', '2017-03-02 14:49:23'),
(867, 30, 174, 'kiamaina', '2017-03-02 14:49:23', '2017-03-02 14:49:23'),
(868, 30, 174, 'lanet/umoja', '2017-03-02 14:49:23', '2017-03-02 14:49:23'),
(869, 30, 174, 'bahati', '2017-03-02 14:49:23', '2017-03-02 14:49:23'),
(870, 30, 175, 'barut', '2017-03-02 14:49:23', '2017-03-02 14:49:23'),
(871, 30, 175, 'london', '2017-03-02 14:49:23', '2017-03-02 14:49:23'),
(872, 30, 175, 'kaptembwo', '2017-03-02 14:49:23', '2017-03-02 14:49:23'),
(873, 30, 175, 'kapkures', '2017-03-02 14:49:23', '2017-03-02 14:49:23'),
(874, 30, 175, 'rhoda', '2017-03-02 14:49:23', '2017-03-02 14:49:23'),
(875, 30, 175, 'shaabab', '2017-03-02 14:49:23', '2017-03-02 14:49:23'),
(876, 30, 176, 'biashara', '2017-03-02 14:49:23', '2017-03-02 14:49:23'),
(877, 30, 176, 'kivumbini', '2017-03-02 14:49:24', '2017-03-02 14:49:24'),
(878, 30, 176, 'flamingo', '2017-03-02 14:49:24', '2017-03-02 14:49:24'),
(879, 30, 176, 'menengai', '2017-03-02 14:49:24', '2017-03-02 14:49:24'),
(880, 30, 176, 'nakuru east', '2017-03-02 14:49:24', '2017-03-02 14:49:24'),
(881, 32, 177, 'kilgoris central', '2017-03-02 14:49:24', '2017-03-02 14:49:24'),
(882, 32, 177, 'keyian', '2017-03-02 14:49:24', '2017-03-02 14:49:24'),
(883, 32, 177, 'angata barikoi', '2017-03-02 14:49:24', '2017-03-02 14:49:24'),
(884, 32, 177, 'shankoe', '2017-03-02 14:49:24', '2017-03-02 14:49:24'),
(885, 32, 177, 'kimintet', '2017-03-02 14:49:24', '2017-03-02 14:49:24'),
(886, 32, 177, 'lolgorian', '2017-03-02 14:49:24', '2017-03-02 14:49:24'),
(887, 32, 178, 'ilkerin', '2017-03-02 14:49:24', '2017-03-02 14:49:24'),
(888, 32, 178, 'ololmasani', '2017-03-02 14:49:24', '2017-03-02 14:49:24'),
(889, 32, 178, 'mogondo', '2017-03-02 14:49:24', '2017-03-02 14:49:24'),
(890, 32, 178, 'kapsasian', '2017-03-02 14:49:24', '2017-03-02 14:49:24'),
(891, 32, 179, 'olpusimoru', '2017-03-02 14:49:24', '2017-03-02 14:49:24'),
(892, 32, 179, 'olokurto', '2017-03-02 14:49:24', '2017-03-02 14:49:24'),
(893, 32, 179, 'narok town', '2017-03-02 14:49:24', '2017-03-02 14:49:24'),
(894, 32, 179, 'nkareta', '2017-03-02 14:49:24', '2017-03-02 14:49:24'),
(895, 32, 179, 'olorropil', '2017-03-02 14:49:24', '2017-03-02 14:49:24'),
(896, 32, 179, 'melili', '2017-03-02 14:49:24', '2017-03-02 14:49:24'),
(897, 32, 180, 'mosiro', '2017-03-02 14:49:24', '2017-03-02 14:49:24'),
(898, 32, 180, 'ildamat', '2017-03-02 14:49:24', '2017-03-02 14:49:24'),
(899, 32, 180, 'keekonyokie', '2017-03-02 14:49:24', '2017-03-02 14:49:24'),
(900, 32, 180, 'suswa', '2017-03-02 14:49:24', '2017-03-02 14:49:24'),
(901, 32, 181, 'majimoto/naroosura', '2017-03-02 14:49:25', '2017-03-02 14:49:25'),
(902, 32, 181, 'ololulung''a', '2017-03-02 14:49:25', '2017-03-02 14:49:25'),
(903, 32, 181, 'melelo', '2017-03-02 14:49:25', '2017-03-02 14:49:25'),
(904, 32, 181, 'loita', '2017-03-02 14:49:25', '2017-03-02 14:49:25'),
(905, 32, 181, 'sogoo', '2017-03-02 14:49:25', '2017-03-02 14:49:25'),
(906, 32, 181, 'sagamian', '2017-03-02 14:49:25', '2017-03-02 14:49:25'),
(907, 32, 182, 'ilmotiok', '2017-03-02 14:49:25', '2017-03-02 14:49:25'),
(908, 32, 182, 'mara', '2017-03-02 14:49:25', '2017-03-02 14:49:25'),
(909, 32, 182, 'siana', '2017-03-02 14:49:25', '2017-03-02 14:49:25'),
(910, 32, 182, 'naikarra', '2017-03-02 14:49:25', '2017-03-02 14:49:25'),
(911, 12, 183, 'olkeri', '2017-03-02 14:49:25', '2017-03-02 14:49:25'),
(912, 12, 183, 'ongata rongai', '2017-03-02 14:49:25', '2017-03-02 14:49:25'),
(913, 12, 183, 'nkaimurunya', '2017-03-02 14:49:25', '2017-03-02 14:49:25'),
(914, 12, 183, 'oloolua', '2017-03-02 14:49:25', '2017-03-02 14:49:25'),
(915, 12, 183, 'ngong', '2017-03-02 14:49:25', '2017-03-02 14:49:25'),
(916, 12, 184, 'purko', '2017-03-02 14:49:25', '2017-03-02 14:49:25'),
(917, 12, 184, 'ildamat', '2017-03-02 14:49:25', '2017-03-02 14:49:25'),
(918, 12, 184, 'dalalekutuk', '2017-03-02 14:49:25', '2017-03-02 14:49:25'),
(919, 12, 184, 'matapato north', '2017-03-02 14:49:25', '2017-03-02 14:49:25'),
(920, 12, 184, 'matapato south', '2017-03-02 14:49:25', '2017-03-02 14:49:25'),
(921, 12, 185, 'kaputiei north', '2017-03-02 14:49:25', '2017-03-02 14:49:25'),
(922, 12, 185, 'kitengela', '2017-03-02 14:49:25', '2017-03-02 14:49:25'),
(923, 12, 185, 'oloosirkon/sholinke', '2017-03-02 14:49:25', '2017-03-02 14:49:25'),
(924, 12, 185, 'kenyawa-poka', '2017-03-02 14:49:26', '2017-03-02 14:49:26'),
(925, 12, 185, 'imaroro', '2017-03-02 14:49:26', '2017-03-02 14:49:26'),
(926, 12, 186, 'keekonyokie', '2017-03-02 14:49:26', '2017-03-02 14:49:26'),
(927, 12, 186, 'iloodokilani', '2017-03-02 14:49:26', '2017-03-02 14:49:26'),
(928, 12, 186, 'magadi', '2017-03-02 14:49:26', '2017-03-02 14:49:26'),
(929, 12, 186, 'ewuaso oonkidong''i', '2017-03-02 14:49:26', '2017-03-02 14:49:26'),
(930, 12, 186, 'mosiro', '2017-03-02 14:49:26', '2017-03-02 14:49:26'),
(931, 12, 187, 'entonet/lenkisim', '2017-03-02 14:49:26', '2017-03-02 14:49:26'),
(932, 12, 187, 'mbirikani/eselenkei', '2017-03-02 14:49:26', '2017-03-02 14:49:26'),
(933, 12, 187, 'kuku', '2017-03-02 14:49:26', '2017-03-02 14:49:26'),
(934, 12, 187, 'rombo', '2017-03-02 14:49:26', '2017-03-02 14:49:26'),
(935, 12, 187, 'kimana', '2017-03-02 14:49:26', '2017-03-02 14:49:26'),
(936, 14, 188, 'londiani', '2017-03-02 14:49:26', '2017-03-02 14:49:26'),
(937, 14, 188, 'kedowa/kimugul', '2017-03-02 14:49:26', '2017-03-02 14:49:26'),
(938, 14, 188, 'chepseon', '2017-03-02 14:49:26', '2017-03-02 14:49:26'),
(939, 14, 188, 'tendeno/sorget', '2017-03-02 14:49:26', '2017-03-02 14:49:26'),
(940, 14, 189, 'kunyak', '2017-03-02 14:49:26', '2017-03-02 14:49:26'),
(941, 14, 189, 'kamasian', '2017-03-02 14:49:26', '2017-03-02 14:49:26'),
(942, 14, 189, 'kipkelion', '2017-03-02 14:49:26', '2017-03-02 14:49:26'),
(943, 14, 189, 'chilchila', '2017-03-02 14:49:26', '2017-03-02 14:49:26'),
(944, 14, 190, 'kapsoit', '2017-03-02 14:49:26', '2017-03-02 14:49:26'),
(945, 14, 190, 'ainamoi', '2017-03-02 14:49:26', '2017-03-02 14:49:26'),
(946, 14, 190, 'kapkugerwet', '2017-03-02 14:49:26', '2017-03-02 14:49:26'),
(947, 14, 190, 'kipchebor', '2017-03-02 14:49:26', '2017-03-02 14:49:26'),
(948, 14, 190, 'kipchimchim', '2017-03-02 14:49:26', '2017-03-02 14:49:26'),
(949, 14, 190, 'kapsaos', '2017-03-02 14:49:26', '2017-03-02 14:49:26'),
(950, 14, 191, 'kisiara', '2017-03-02 14:49:26', '2017-03-02 14:49:26'),
(951, 14, 191, 'tebesonik', '2017-03-02 14:49:26', '2017-03-02 14:49:26'),
(952, 14, 191, 'cheboin', '2017-03-02 14:49:27', '2017-03-02 14:49:27'),
(953, 14, 191, 'chemosot', '2017-03-02 14:49:27', '2017-03-02 14:49:27'),
(954, 14, 191, 'litein', '2017-03-02 14:49:27', '2017-03-02 14:49:27'),
(955, 14, 191, 'cheplanget', '2017-03-02 14:49:27', '2017-03-02 14:49:27'),
(956, 14, 191, 'kapkatet', '2017-03-02 14:49:27', '2017-03-02 14:49:27'),
(957, 14, 192, 'waldai', '2017-03-02 14:49:27', '2017-03-02 14:49:27'),
(958, 14, 192, 'kabianga', '2017-03-02 14:49:27', '2017-03-02 14:49:27'),
(959, 14, 192, 'cheptororiet/seretut', '2017-03-02 14:49:27', '2017-03-02 14:49:27'),
(960, 14, 192, 'chaik', '2017-03-02 14:49:27', '2017-03-02 14:49:27'),
(961, 14, 192, 'kapsuser', '2017-03-02 14:49:27', '2017-03-02 14:49:27'),
(962, 14, 193, 'sigowet', '2017-03-02 14:49:27', '2017-03-02 14:49:27'),
(963, 14, 193, 'kaplelartet', '2017-03-02 14:49:27', '2017-03-02 14:49:27'),
(964, 14, 193, 'soliat', '2017-03-02 14:49:27', '2017-03-02 14:49:27'),
(965, 14, 193, 'soin', '2017-03-02 14:49:27', '2017-03-02 14:49:27'),
(966, 4, 194, 'ndanai/abosi', '2017-03-02 14:49:27', '2017-03-02 14:49:27'),
(967, 4, 194, 'chemagel', '2017-03-02 14:49:27', '2017-03-02 14:49:27'),
(968, 4, 194, 'kipsonoi', '2017-03-02 14:49:27', '2017-03-02 14:49:27'),
(969, 4, 194, 'kapletundo', '2017-03-02 14:49:27', '2017-03-02 14:49:27'),
(970, 4, 194, 'rongena/manaret', '2017-03-02 14:49:27', '2017-03-02 14:49:27'),
(971, 4, 195, 'kong''asis', '2017-03-02 14:49:27', '2017-03-02 14:49:27'),
(972, 4, 195, 'nyangores', '2017-03-02 14:49:27', '2017-03-02 14:49:27'),
(973, 4, 195, 'sigor', '2017-03-02 14:49:27', '2017-03-02 14:49:27'),
(974, 4, 195, 'chebunyo', '2017-03-02 14:49:27', '2017-03-02 14:49:27'),
(975, 4, 195, 'siongiroi', '2017-03-02 14:49:27', '2017-03-02 14:49:27'),
(976, 4, 196, 'merigi', '2017-03-02 14:49:27', '2017-03-02 14:49:27'),
(977, 4, 196, 'kembu', '2017-03-02 14:49:27', '2017-03-02 14:49:27'),
(978, 4, 196, 'longisa', '2017-03-02 14:49:28', '2017-03-02 14:49:28'),
(979, 4, 196, 'kipreres', '2017-03-02 14:49:28', '2017-03-02 14:49:28'),
(980, 4, 196, 'chemaner', '2017-03-02 14:49:28', '2017-03-02 14:49:28'),
(981, 4, 197, 'silibwet township', '2017-03-02 14:49:28', '2017-03-02 14:49:28'),
(982, 4, 197, 'ndaraweta', '2017-03-02 14:49:28', '2017-03-02 14:49:28'),
(983, 4, 197, 'singorwet', '2017-03-02 14:49:28', '2017-03-02 14:49:28'),
(984, 4, 197, 'chesoen', '2017-03-02 14:49:28', '2017-03-02 14:49:28'),
(985, 4, 197, 'mutarakwa', '2017-03-02 14:49:28', '2017-03-02 14:49:28'),
(986, 4, 198, 'chepchabas', '2017-03-02 14:49:28', '2017-03-02 14:49:28'),
(987, 4, 198, 'kimulot', '2017-03-02 14:49:28', '2017-03-02 14:49:28'),
(988, 4, 198, 'mogogosiek', '2017-03-02 14:49:28', '2017-03-02 14:49:28'),
(989, 4, 198, 'boito', '2017-03-02 14:49:28', '2017-03-02 14:49:28'),
(990, 4, 198, 'embomos', '2017-03-02 14:49:28', '2017-03-02 14:49:28'),
(991, 13, 199, 'mautuma', '2017-03-02 14:49:28', '2017-03-02 14:49:28'),
(992, 13, 199, 'lugari', '2017-03-02 14:49:28', '2017-03-02 14:49:28'),
(993, 13, 199, 'lumakanda', '2017-03-02 14:49:28', '2017-03-02 14:49:28'),
(994, 13, 199, 'chekalini', '2017-03-02 14:49:28', '2017-03-02 14:49:28'),
(995, 13, 199, 'chevaywa', '2017-03-02 14:49:28', '2017-03-02 14:49:28'),
(996, 13, 199, 'lwandeti', '2017-03-02 14:49:28', '2017-03-02 14:49:28'),
(997, 13, 200, 'likuyani', '2017-03-02 14:49:28', '2017-03-02 14:49:28'),
(998, 13, 200, 'sango', '2017-03-02 14:49:28', '2017-03-02 14:49:28'),
(999, 13, 200, 'kongoni', '2017-03-02 14:49:28', '2017-03-02 14:49:28'),
(1000, 13, 200, 'nzoia', '2017-03-02 14:49:29', '2017-03-02 14:49:29'),
(1001, 13, 200, 'sinoko', '2017-03-02 14:49:29', '2017-03-02 14:49:29'),
(1002, 13, 201, 'west kabras', '2017-03-02 14:49:29', '2017-03-02 14:49:29'),
(1003, 13, 201, 'chemuche', '2017-03-02 14:49:29', '2017-03-02 14:49:29'),
(1004, 13, 201, 'east kabras', '2017-03-02 14:49:29', '2017-03-02 14:49:29'),
(1005, 13, 201, 'butali/chegulo', '2017-03-02 14:49:29', '2017-03-02 14:49:29'),
(1006, 13, 201, 'manda-shivanga', '2017-03-02 14:49:29', '2017-03-02 14:49:29'),
(1007, 13, 201, 'shirugu-mugai', '2017-03-02 14:49:29', '2017-03-02 14:49:29'),
(1008, 13, 201, 'south kabras', '2017-03-02 14:49:29', '2017-03-02 14:49:29'),
(1009, 13, 202, 'butsotso east', '2017-03-02 14:49:29', '2017-03-02 14:49:29'),
(1010, 13, 202, 'butsotso south', '2017-03-02 14:49:29', '2017-03-02 14:49:29'),
(1011, 13, 202, 'butsotso central', '2017-03-02 14:49:29', '2017-03-02 14:49:29'),
(1012, 13, 202, 'sheywe', '2017-03-02 14:49:29', '2017-03-02 14:49:29'),
(1013, 13, 202, 'mahiakalo', '2017-03-02 14:49:29', '2017-03-02 14:49:29'),
(1014, 13, 202, 'shirere', '2017-03-02 14:49:29', '2017-03-02 14:49:29'),
(1015, 13, 203, 'ingostse-mathia', '2017-03-02 14:49:29', '2017-03-02 14:49:29'),
(1016, 13, 203, 'shinoyi-shikomari-', '2017-03-02 14:49:29', '2017-03-02 14:49:29'),
(1017, 13, 203, 'bunyala west', '2017-03-02 14:49:29', '2017-03-02 14:49:29'),
(1018, 13, 203, 'bunyala east', '2017-03-02 14:49:29', '2017-03-02 14:49:29'),
(1019, 13, 203, 'bunyala central', '2017-03-02 14:49:29', '2017-03-02 14:49:29'),
(1020, 13, 204, 'mumias central', '2017-03-02 14:49:29', '2017-03-02 14:49:29'),
(1021, 13, 204, 'mumias north', '2017-03-02 14:49:29', '2017-03-02 14:49:29'),
(1022, 13, 204, 'etenje', '2017-03-02 14:49:29', '2017-03-02 14:49:29'),
(1023, 13, 204, 'musanda', '2017-03-02 14:49:29', '2017-03-02 14:49:29'),
(1024, 13, 205, 'lubinu/lusheya', '2017-03-02 14:49:29', '2017-03-02 14:49:29'),
(1025, 13, 205, 'isongo/makunga/malaha', '2017-03-02 14:49:29', '2017-03-02 14:49:29'),
(1026, 13, 205, 'east wanga', '2017-03-02 14:49:29', '2017-03-02 14:49:29'),
(1027, 13, 206, 'koyonzo', '2017-03-02 14:49:29', '2017-03-02 14:49:29'),
(1028, 13, 206, 'kholera', '2017-03-02 14:49:29', '2017-03-02 14:49:29'),
(1029, 13, 206, 'khalaba', '2017-03-02 14:49:29', '2017-03-02 14:49:29'),
(1030, 13, 206, 'mayoni', '2017-03-02 14:49:30', '2017-03-02 14:49:30'),
(1031, 13, 206, 'namamali', '2017-03-02 14:49:30', '2017-03-02 14:49:30'),
(1032, 13, 207, 'marama west', '2017-03-02 14:49:30', '2017-03-02 14:49:30'),
(1033, 13, 207, 'marama central', '2017-03-02 14:49:30', '2017-03-02 14:49:30'),
(1034, 13, 207, 'marenyo - shianda', '2017-03-02 14:49:30', '2017-03-02 14:49:30'),
(1035, 13, 207, 'marama north', '2017-03-02 14:49:30', '2017-03-02 14:49:30'),
(1036, 13, 207, 'marama south', '2017-03-02 14:49:30', '2017-03-02 14:49:30'),
(1037, 13, 208, 'kisa north', '2017-03-02 14:49:30', '2017-03-02 14:49:30'),
(1038, 13, 208, 'kisa east', '2017-03-02 14:49:30', '2017-03-02 14:49:30'),
(1039, 13, 208, 'kisa west', '2017-03-02 14:49:30', '2017-03-02 14:49:30'),
(1040, 13, 208, 'kisa central', '2017-03-02 14:49:30', '2017-03-02 14:49:30'),
(1041, 13, 209, 'isukha north', '2017-03-02 14:49:30', '2017-03-02 14:49:30'),
(1042, 13, 209, 'murhanda', '2017-03-02 14:49:30', '2017-03-02 14:49:30'),
(1043, 13, 209, 'isukha central', '2017-03-02 14:49:30', '2017-03-02 14:49:30'),
(1044, 13, 209, 'isukha south', '2017-03-02 14:49:30', '2017-03-02 14:49:30'),
(1045, 13, 209, 'isukha east', '2017-03-02 14:49:30', '2017-03-02 14:49:30'),
(1046, 13, 209, 'isukha west', '2017-03-02 14:49:30', '2017-03-02 14:49:30'),
(1047, 13, 210, 'idakho south', '2017-03-02 14:49:30', '2017-03-02 14:49:30'),
(1048, 13, 210, 'idakho east', '2017-03-02 14:49:30', '2017-03-02 14:49:30'),
(1049, 13, 210, 'idakho north', '2017-03-02 14:49:30', '2017-03-02 14:49:30'),
(1050, 13, 210, 'idakho central', '2017-03-02 14:49:30', '2017-03-02 14:49:30'),
(1051, 44, 211, 'lugaga-wamuluma', '2017-03-02 14:49:30', '2017-03-02 14:49:30'),
(1052, 44, 211, 'south maragoli', '2017-03-02 14:49:31', '2017-03-02 14:49:31'),
(1053, 44, 211, 'central maragoli', '2017-03-02 14:49:31', '2017-03-02 14:49:31'),
(1054, 44, 211, 'mungoma', '2017-03-02 14:49:31', '2017-03-02 14:49:31'),
(1055, 44, 212, 'lyaduywa/izava', '2017-03-02 14:49:31', '2017-03-02 14:49:31'),
(1056, 44, 212, 'west sabatia', '2017-03-02 14:49:31', '2017-03-02 14:49:31'),
(1057, 44, 212, 'chavakali', '2017-03-02 14:49:31', '2017-03-02 14:49:31'),
(1058, 44, 212, 'north maragoli', '2017-03-02 14:49:31', '2017-03-02 14:49:31'),
(1059, 44, 212, 'wodanga', '2017-03-02 14:49:31', '2017-03-02 14:49:31'),
(1060, 44, 212, 'busali', '2017-03-02 14:49:31', '2017-03-02 14:49:31'),
(1061, 44, 213, 'shiru', '2017-03-02 14:49:31', '2017-03-02 14:49:31'),
(1062, 44, 213, 'muhudu', '2017-03-02 14:49:31', '2017-03-02 14:49:31'),
(1063, 44, 213, 'shamakhokho', '2017-03-02 14:49:31', '2017-03-02 14:49:31'),
(1064, 44, 213, 'gisambai', '2017-03-02 14:49:31', '2017-03-02 14:49:31'),
(1065, 44, 213, 'banja', '2017-03-02 14:49:31', '2017-03-02 14:49:31'),
(1066, 44, 213, 'tambua', '2017-03-02 14:49:31', '2017-03-02 14:49:31'),
(1067, 44, 213, 'jepkoyai', '2017-03-02 14:49:31', '2017-03-02 14:49:31'),
(1068, 44, 214, 'luanda township', '2017-03-02 14:49:31', '2017-03-02 14:49:31'),
(1069, 44, 214, 'wemilabi', '2017-03-02 14:49:31', '2017-03-02 14:49:31'),
(1070, 44, 214, 'mwibona', '2017-03-02 14:49:31', '2017-03-02 14:49:31'),
(1071, 44, 214, 'luanda south', '2017-03-02 14:49:31', '2017-03-02 14:49:31'),
(1072, 44, 214, 'emabungo', '2017-03-02 14:49:31', '2017-03-02 14:49:31'),
(1073, 44, 215, 'north east bunyore', '2017-03-02 14:49:31', '2017-03-02 14:49:31'),
(1074, 44, 215, 'central bunyore', '2017-03-02 14:49:31', '2017-03-02 14:49:31'),
(1075, 44, 215, 'west bunyore', '2017-03-02 14:49:31', '2017-03-02 14:49:31'),
(1076, 5, 216, 'cheptais', '2017-03-02 14:49:32', '2017-03-02 14:49:32'),
(1077, 5, 216, 'chesikaki', '2017-03-02 14:49:32', '2017-03-02 14:49:32'),
(1078, 5, 216, 'chepyuk', '2017-03-02 14:49:32', '2017-03-02 14:49:32'),
(1079, 5, 216, 'kapkateny', '2017-03-02 14:49:32', '2017-03-02 14:49:32'),
(1080, 5, 216, 'kaptama', '2017-03-02 14:49:32', '2017-03-02 14:49:32'),
(1081, 5, 216, 'elgon', '2017-03-02 14:49:32', '2017-03-02 14:49:32'),
(1082, 5, 217, 'namwela', '2017-03-02 14:49:32', '2017-03-02 14:49:32'),
(1083, 5, 217, 'malakisi/south kulisiru', '2017-03-02 14:49:32', '2017-03-02 14:49:32'),
(1084, 5, 217, 'lwandanyi', '2017-03-02 14:49:32', '2017-03-02 14:49:32'),
(1085, 5, 218, 'kabuchai/chwele', '2017-03-02 14:49:32', '2017-03-02 14:49:32'),
(1086, 5, 218, 'west nalondo', '2017-03-02 14:49:32', '2017-03-02 14:49:32'),
(1087, 5, 218, 'bwake/luuya', '2017-03-02 14:49:32', '2017-03-02 14:49:32'),
(1088, 5, 218, 'mukuyuni', '2017-03-02 14:49:32', '2017-03-02 14:49:32'),
(1089, 5, 219, 'south bukusu', '2017-03-02 14:49:32', '2017-03-02 14:49:32'),
(1090, 5, 219, 'bumula', '2017-03-02 14:49:32', '2017-03-02 14:49:32'),
(1091, 5, 219, 'khasoko', '2017-03-02 14:49:32', '2017-03-02 14:49:32'),
(1092, 5, 219, 'kabula', '2017-03-02 14:49:32', '2017-03-02 14:49:32'),
(1093, 5, 219, 'kimaeti', '2017-03-02 14:49:32', '2017-03-02 14:49:32'),
(1094, 5, 219, 'west bukusu', '2017-03-02 14:49:32', '2017-03-02 14:49:32'),
(1095, 5, 219, 'siboti', '2017-03-02 14:49:32', '2017-03-02 14:49:32'),
(1096, 5, 220, 'bukembe west', '2017-03-02 14:49:32', '2017-03-02 14:49:32'),
(1097, 5, 220, 'bukembe east', '2017-03-02 14:49:32', '2017-03-02 14:49:32'),
(1098, 5, 220, 'township', '2017-03-02 14:49:32', '2017-03-02 14:49:32'),
(1099, 5, 220, 'khalaba', '2017-03-02 14:49:32', '2017-03-02 14:49:32'),
(1100, 5, 220, 'musikoma', '2017-03-02 14:49:32', '2017-03-02 14:49:32'),
(1101, 5, 220, 'east sang''alo', '2017-03-02 14:49:33', '2017-03-02 14:49:33'),
(1102, 5, 220, 'marakaru/tuuti', '2017-03-02 14:49:33', '2017-03-02 14:49:33'),
(1103, 5, 220, 'sang''alo west', '2017-03-02 14:49:33', '2017-03-02 14:49:33'),
(1104, 5, 221, 'mihuu', '2017-03-02 14:49:33', '2017-03-02 14:49:33'),
(1105, 5, 221, 'ndivisi', '2017-03-02 14:49:33', '2017-03-02 14:49:33'),
(1106, 5, 221, 'maraka', '2017-03-02 14:49:33', '2017-03-02 14:49:33'),
(1107, 5, 222, 'misikhu', '2017-03-02 14:49:33', '2017-03-02 14:49:33'),
(1108, 5, 222, 'sitikho', '2017-03-02 14:49:33', '2017-03-02 14:49:33'),
(1109, 5, 222, 'matulo', '2017-03-02 14:49:33', '2017-03-02 14:49:33'),
(1110, 5, 222, 'bokoli', '2017-03-02 14:49:33', '2017-03-02 14:49:33'),
(1111, 5, 223, 'kimilili', '2017-03-02 14:49:33', '2017-03-02 14:49:33'),
(1112, 5, 223, 'kibingei', '2017-03-02 14:49:33', '2017-03-02 14:49:33'),
(1113, 5, 223, 'maeni', '2017-03-02 14:49:33', '2017-03-02 14:49:33'),
(1114, 5, 223, 'kamukuywa', '2017-03-02 14:49:33', '2017-03-02 14:49:33'),
(1115, 5, 224, 'mbakalo', '2017-03-02 14:49:33', '2017-03-02 14:49:33'),
(1116, 5, 224, 'naitiri/kabuyefwe', '2017-03-02 14:49:33', '2017-03-02 14:49:33'),
(1117, 5, 224, 'milima', '2017-03-02 14:49:33', '2017-03-02 14:49:33'),
(1118, 5, 224, 'ndalu/ tabani', '2017-03-02 14:49:33', '2017-03-02 14:49:33'),
(1119, 5, 224, 'tongaren', '2017-03-02 14:49:33', '2017-03-02 14:49:33'),
(1120, 5, 224, 'soysambu/ mitua', '2017-03-02 14:49:33', '2017-03-02 14:49:33'),
(1121, 6, 225, 'malaba central', '2017-03-02 14:49:33', '2017-03-02 14:49:33'),
(1122, 6, 225, 'malaba north', '2017-03-02 14:49:33', '2017-03-02 14:49:33'),
(1123, 6, 225, 'ang''urai south', '2017-03-02 14:49:33', '2017-03-02 14:49:33'),
(1124, 6, 225, 'ang''urai north', '2017-03-02 14:49:34', '2017-03-02 14:49:34'),
(1125, 6, 225, 'ang''urai east', '2017-03-02 14:49:34', '2017-03-02 14:49:34'),
(1126, 6, 225, 'malaba south', '2017-03-02 14:49:34', '2017-03-02 14:49:34'),
(1127, 6, 226, 'ang''orom', '2017-03-02 14:49:34', '2017-03-02 14:49:34'),
(1128, 6, 226, 'chakol south', '2017-03-02 14:49:34', '2017-03-02 14:49:34'),
(1129, 6, 226, 'chakol north', '2017-03-02 14:49:34', '2017-03-02 14:49:34'),
(1130, 6, 226, 'amukura west', '2017-03-02 14:49:34', '2017-03-02 14:49:34'),
(1131, 6, 226, 'amukura east', '2017-03-02 14:49:34', '2017-03-02 14:49:34'),
(1132, 6, 226, 'amukura central', '2017-03-02 14:49:34', '2017-03-02 14:49:34'),
(1133, 6, 227, 'nambale township', '2017-03-02 14:49:34', '2017-03-02 14:49:34'),
(1134, 6, 227, 'bukhayo north/waltsi', '2017-03-02 14:49:34', '2017-03-02 14:49:34'),
(1135, 6, 227, 'bukhayo east', '2017-03-02 14:49:34', '2017-03-02 14:49:34'),
(1136, 6, 227, 'bukhayo central', '2017-03-02 14:49:34', '2017-03-02 14:49:34'),
(1137, 6, 228, 'bukhayo west', '2017-03-02 14:49:34', '2017-03-02 14:49:34'),
(1138, 6, 228, 'mayenje', '2017-03-02 14:49:35', '2017-03-02 14:49:35'),
(1139, 6, 228, 'matayos south', '2017-03-02 14:49:35', '2017-03-02 14:49:35'),
(1140, 6, 228, 'busibwabo', '2017-03-02 14:49:35', '2017-03-02 14:49:35'),
(1141, 6, 228, 'burumba', '2017-03-02 14:49:35', '2017-03-02 14:49:35'),
(1142, 6, 229, 'marachi west', '2017-03-02 14:49:35', '2017-03-02 14:49:35'),
(1143, 6, 229, 'kingandole', '2017-03-02 14:49:35', '2017-03-02 14:49:35'),
(1144, 6, 229, 'marachi central', '2017-03-02 14:49:35', '2017-03-02 14:49:35'),
(1145, 6, 229, 'marachi east', '2017-03-02 14:49:35', '2017-03-02 14:49:35'),
(1146, 6, 229, 'marachi north', '2017-03-02 14:49:35', '2017-03-02 14:49:35'),
(1147, 6, 229, 'elugulu', '2017-03-02 14:49:35', '2017-03-02 14:49:35'),
(1148, 6, 230, 'namboboto nambuku', '2017-03-02 14:49:35', '2017-03-02 14:49:35'),
(1149, 6, 230, 'nangina', '2017-03-02 14:49:35', '2017-03-02 14:49:35'),
(1150, 6, 230, 'ageng''a nanguba', '2017-03-02 14:49:35', '2017-03-02 14:49:35'),
(1151, 6, 230, 'bwiri', '2017-03-02 14:49:36', '2017-03-02 14:49:36'),
(1152, 6, 231, 'bunyala central', '2017-03-02 14:49:36', '2017-03-02 14:49:36'),
(1153, 6, 231, 'bunyala north', '2017-03-02 14:49:36', '2017-03-02 14:49:36'),
(1154, 6, 231, 'bunyala west', '2017-03-02 14:49:36', '2017-03-02 14:49:36'),
(1155, 6, 231, 'bunyala south', '2017-03-02 14:49:36', '2017-03-02 14:49:36'),
(1156, 37, 232, 'west ugenya', '2017-03-02 14:49:36', '2017-03-02 14:49:36'),
(1157, 37, 232, 'ukwala', '2017-03-02 14:49:36', '2017-03-02 14:49:36'),
(1158, 37, 232, 'north ugenya', '2017-03-02 14:49:37', '2017-03-02 14:49:37'),
(1159, 37, 232, 'east ugenya', '2017-03-02 14:49:37', '2017-03-02 14:49:37'),
(1160, 37, 233, 'sidindi', '2017-03-02 14:49:37', '2017-03-02 14:49:37'),
(1161, 37, 233, 'sigomere', '2017-03-02 14:49:37', '2017-03-02 14:49:37'),
(1162, 37, 233, 'ugunja', '2017-03-02 14:49:37', '2017-03-02 14:49:37'),
(1163, 37, 234, 'usonga', '2017-03-02 14:49:37', '2017-03-02 14:49:37'),
(1164, 37, 234, 'west alego', '2017-03-02 14:49:37', '2017-03-02 14:49:37'),
(1165, 37, 234, 'central alego', '2017-03-02 14:49:37', '2017-03-02 14:49:37'),
(1166, 37, 234, 'siaya township', '2017-03-02 14:49:37', '2017-03-02 14:49:37'),
(1167, 37, 234, 'north alego', '2017-03-02 14:49:37', '2017-03-02 14:49:37'),
(1168, 37, 234, 'south east alego', '2017-03-02 14:49:37', '2017-03-02 14:49:37'),
(1169, 37, 235, 'north gem', '2017-03-02 14:49:37', '2017-03-02 14:49:37'),
(1170, 37, 235, 'west gem', '2017-03-02 14:49:37', '2017-03-02 14:49:37'),
(1171, 37, 235, 'central gem', '2017-03-02 14:49:38', '2017-03-02 14:49:38'),
(1172, 37, 235, 'yala township', '2017-03-02 14:49:38', '2017-03-02 14:49:38'),
(1173, 37, 235, 'east gem', '2017-03-02 14:49:38', '2017-03-02 14:49:38'),
(1174, 37, 235, 'south gem', '2017-03-02 14:49:38', '2017-03-02 14:49:38'),
(1175, 37, 236, 'west yimbo', '2017-03-02 14:49:38', '2017-03-02 14:49:38'),
(1176, 37, 236, 'central sakwa', '2017-03-02 14:49:38', '2017-03-02 14:49:38'),
(1177, 37, 236, 'south sakwa', '2017-03-02 14:49:38', '2017-03-02 14:49:38'),
(1178, 37, 236, 'yimbo east', '2017-03-02 14:49:38', '2017-03-02 14:49:38'),
(1179, 37, 236, 'west sakwa', '2017-03-02 14:49:38', '2017-03-02 14:49:38'),
(1180, 37, 236, 'north sakwa', '2017-03-02 14:49:38', '2017-03-02 14:49:38'),
(1181, 37, 237, 'east asembo', '2017-03-02 14:49:38', '2017-03-02 14:49:38'),
(1182, 37, 237, 'west asembo', '2017-03-02 14:49:38', '2017-03-02 14:49:38'),
(1183, 37, 237, 'north uyoma', '2017-03-02 14:49:38', '2017-03-02 14:49:38'),
(1184, 37, 237, 'south uyoma', '2017-03-02 14:49:38', '2017-03-02 14:49:38'),
(1185, 37, 237, 'west uyoma', '2017-03-02 14:49:39', '2017-03-02 14:49:39'),
(1186, 3, 238, 'kajulu', '2017-03-02 14:49:39', '2017-03-02 14:49:39'),
(1187, 3, 238, 'kolwa east', '2017-03-02 14:49:39', '2017-03-02 14:49:39'),
(1188, 3, 238, 'manyatta ''b''', '2017-03-02 14:49:39', '2017-03-02 14:49:39'),
(1189, 3, 238, 'nyalenda ''a''', '2017-03-02 14:49:39', '2017-03-02 14:49:39'),
(1190, 3, 238, 'kolwa central', '2017-03-02 14:49:39', '2017-03-02 14:49:39'),
(1191, 3, 239, 'south west kisumu', '2017-03-02 14:49:39', '2017-03-02 14:49:39'),
(1192, 3, 239, 'central kisumu', '2017-03-02 14:49:39', '2017-03-02 14:49:39'),
(1193, 3, 239, 'kisumu north', '2017-03-02 14:49:39', '2017-03-02 14:49:39'),
(1194, 3, 239, 'west kisumu', '2017-03-02 14:49:39', '2017-03-02 14:49:39'),
(1195, 3, 239, 'north west kisumu', '2017-03-02 14:49:39', '2017-03-02 14:49:39'),
(1196, 3, 240, 'railways', '2017-03-02 14:49:39', '2017-03-02 14:49:39'),
(1197, 3, 240, 'migosi', '2017-03-02 14:49:39', '2017-03-02 14:49:39'),
(1198, 3, 240, 'shaurimoyo kaloleni', '2017-03-02 14:49:39', '2017-03-02 14:49:39'),
(1199, 3, 240, 'market milimani', '2017-03-02 14:49:39', '2017-03-02 14:49:39'),
(1200, 3, 240, 'kondele', '2017-03-02 14:49:39', '2017-03-02 14:49:39'),
(1201, 3, 240, 'nyalenda b', '2017-03-02 14:49:39', '2017-03-02 14:49:39'),
(1202, 3, 241, 'west seme', '2017-03-02 14:49:39', '2017-03-02 14:49:39'),
(1203, 3, 241, 'central seme', '2017-03-02 14:49:39', '2017-03-02 14:49:39'),
(1204, 3, 241, 'east seme', '2017-03-02 14:49:40', '2017-03-02 14:49:40'),
(1205, 3, 241, 'north seme', '2017-03-02 14:49:40', '2017-03-02 14:49:40'),
(1206, 3, 242, 'east kano/wawidhi', '2017-03-02 14:49:40', '2017-03-02 14:49:40'),
(1207, 3, 242, 'awasi/onjiko', '2017-03-02 14:49:40', '2017-03-02 14:49:40'),
(1208, 3, 242, 'ahero', '2017-03-02 14:49:40', '2017-03-02 14:49:40'),
(1209, 3, 242, 'kabonyo/kanyagwal', '2017-03-02 14:49:40', '2017-03-02 14:49:40'),
(1210, 3, 242, 'kobura', '2017-03-02 14:49:40', '2017-03-02 14:49:40'),
(1211, 3, 243, 'miwani', '2017-03-02 14:49:40', '2017-03-02 14:49:40'),
(1212, 3, 243, 'ombeyi', '2017-03-02 14:49:40', '2017-03-02 14:49:40'),
(1213, 3, 243, 'masogo/nyang''oma', '2017-03-02 14:49:40', '2017-03-02 14:49:40'),
(1214, 3, 243, 'chemelil', '2017-03-02 14:49:40', '2017-03-02 14:49:40'),
(1215, 3, 243, 'muhoroni/koru', '2017-03-02 14:49:40', '2017-03-02 14:49:40'),
(1216, 3, 244, 'south west nyakach', '2017-03-02 14:49:40', '2017-03-02 14:49:40'),
(1217, 3, 244, 'north nyakach', '2017-03-02 14:49:40', '2017-03-02 14:49:40'),
(1218, 3, 244, 'central nyakach', '2017-03-02 14:49:40', '2017-03-02 14:49:40'),
(1219, 3, 244, 'west nyakach', '2017-03-02 14:49:40', '2017-03-02 14:49:40'),
(1220, 3, 244, 'south east nyakach', '2017-03-02 14:49:40', '2017-03-02 14:49:40'),
(1221, 10, 245, 'west kasipul', '2017-03-02 14:49:40', '2017-03-02 14:49:40'),
(1222, 10, 245, 'south kasipul', '2017-03-02 14:49:40', '2017-03-02 14:49:40'),
(1223, 10, 245, 'central kasipul', '2017-03-02 14:49:40', '2017-03-02 14:49:40'),
(1224, 10, 245, 'east kamagak', '2017-03-02 14:49:40', '2017-03-02 14:49:40'),
(1225, 10, 245, 'west kamagak', '2017-03-02 14:49:40', '2017-03-02 14:49:40'),
(1226, 10, 246, 'kabondo east', '2017-03-02 14:49:40', '2017-03-02 14:49:40'),
(1227, 10, 246, 'kabondo west', '2017-03-02 14:49:40', '2017-03-02 14:49:40'),
(1228, 10, 246, 'kokwanyo/kakelo', '2017-03-02 14:49:40', '2017-03-02 14:49:40'),
(1229, 10, 246, 'kojwach', '2017-03-02 14:49:40', '2017-03-02 14:49:40'),
(1230, 10, 247, 'west karachuonyo', '2017-03-02 14:49:40', '2017-03-02 14:49:40'),
(1231, 10, 247, 'north karachuonyo', '2017-03-02 14:49:41', '2017-03-02 14:49:41'),
(1232, 10, 247, 'central', '2017-03-02 14:49:41', '2017-03-02 14:49:41'),
(1233, 10, 247, 'kanyaluo', '2017-03-02 14:49:41', '2017-03-02 14:49:41'),
(1234, 10, 247, 'kibiri', '2017-03-02 14:49:41', '2017-03-02 14:49:41'),
(1235, 10, 247, 'wangchieng', '2017-03-02 14:49:41', '2017-03-02 14:49:41'),
(1236, 10, 247, 'kendu bay town', '2017-03-02 14:49:41', '2017-03-02 14:49:41'),
(1237, 10, 248, 'west gem', '2017-03-02 14:49:41', '2017-03-02 14:49:41'),
(1238, 10, 248, 'east gem', '2017-03-02 14:49:41', '2017-03-02 14:49:41'),
(1239, 10, 248, 'kagan', '2017-03-02 14:49:41', '2017-03-02 14:49:41'),
(1240, 10, 248, 'kochia', '2017-03-02 14:49:41', '2017-03-02 14:49:41'),
(1241, 10, 249, 'homa bay central', '2017-03-02 14:49:41', '2017-03-02 14:49:41'),
(1242, 10, 249, 'homa bay arujo', '2017-03-02 14:49:41', '2017-03-02 14:49:41'),
(1243, 10, 249, 'homa bay west', '2017-03-02 14:49:41', '2017-03-02 14:49:41'),
(1244, 10, 249, 'homa bay east', '2017-03-02 14:49:41', '2017-03-02 14:49:41'),
(1245, 10, 250, 'kwabwai', '2017-03-02 14:49:41', '2017-03-02 14:49:41'),
(1246, 10, 250, 'kanyadoto', '2017-03-02 14:49:41', '2017-03-02 14:49:41'),
(1247, 10, 250, 'kanyikela', '2017-03-02 14:49:41', '2017-03-02 14:49:41'),
(1248, 10, 250, 'north kabuoch', '2017-03-02 14:49:41', '2017-03-02 14:49:41'),
(1249, 10, 250, 'kabuoch south/pala', '2017-03-02 14:49:41', '2017-03-02 14:49:41'),
(1250, 10, 250, 'kanyamwa kologi', '2017-03-02 14:49:41', '2017-03-02 14:49:41'),
(1251, 10, 250, 'kanyamwa kosewe', '2017-03-02 14:49:41', '2017-03-02 14:49:41'),
(1252, 10, 251, 'mfangano island', '2017-03-02 14:49:41', '2017-03-02 14:49:41'),
(1253, 10, 251, 'rusinga island', '2017-03-02 14:49:41', '2017-03-02 14:49:41'),
(1254, 10, 251, 'kasgunga', '2017-03-02 14:49:41', '2017-03-02 14:49:41'),
(1255, 10, 251, 'gembe', '2017-03-02 14:49:41', '2017-03-02 14:49:41'),
(1256, 10, 251, 'lambwe', '2017-03-02 14:49:42', '2017-03-02 14:49:42'),
(1257, 10, 252, 'gwassi south', '2017-03-02 14:49:42', '2017-03-02 14:49:42'),
(1258, 10, 252, 'gwassi north', '2017-03-02 14:49:42', '2017-03-02 14:49:42'),
(1259, 10, 252, 'kaksingri west', '2017-03-02 14:49:42', '2017-03-02 14:49:42'),
(1260, 10, 252, 'ruma kaksingri east', '2017-03-02 14:49:42', '2017-03-02 14:49:42'),
(1261, 27, 253, 'north kamagambo', '2017-03-02 14:49:42', '2017-03-02 14:49:42'),
(1262, 27, 253, 'central kamagambo', '2017-03-02 14:49:42', '2017-03-02 14:49:42'),
(1263, 27, 253, 'east kamagambo', '2017-03-02 14:49:42', '2017-03-02 14:49:42'),
(1264, 27, 253, 'south kamagambo', '2017-03-02 14:49:42', '2017-03-02 14:49:42'),
(1265, 27, 254, 'north sakwa', '2017-03-02 14:49:42', '2017-03-02 14:49:42'),
(1266, 27, 254, 'south sakwa', '2017-03-02 14:49:42', '2017-03-02 14:49:42'),
(1267, 27, 254, 'west sakwa', '2017-03-02 14:49:42', '2017-03-02 14:49:42'),
(1268, 27, 254, 'central sakwa', '2017-03-02 14:49:42', '2017-03-02 14:49:42'),
(1269, 27, 255, 'god jope', '2017-03-02 14:49:42', '2017-03-02 14:49:42'),
(1270, 27, 255, 'suna central', '2017-03-02 14:49:42', '2017-03-02 14:49:42'),
(1271, 27, 255, 'kakrao', '2017-03-02 14:49:42', '2017-03-02 14:49:42'),
(1272, 27, 255, 'kwa', '2017-03-02 14:49:42', '2017-03-02 14:49:42'),
(1273, 27, 256, 'wiga', '2017-03-02 14:49:42', '2017-03-02 14:49:42'),
(1274, 27, 256, 'wasweta ii', '2017-03-02 14:49:42', '2017-03-02 14:49:42'),
(1275, 27, 256, 'ragana-oruba', '2017-03-02 14:49:42', '2017-03-02 14:49:42'),
(1276, 27, 256, 'wasimbete', '2017-03-02 14:49:42', '2017-03-02 14:49:42'),
(1277, 27, 257, 'west kanyamkago', '2017-03-02 14:49:42', '2017-03-02 14:49:42'),
(1278, 27, 257, 'north kanyamkago', '2017-03-02 14:49:42', '2017-03-02 14:49:42'),
(1279, 27, 257, 'central kanyamkago', '2017-03-02 14:49:43', '2017-03-02 14:49:43'),
(1280, 27, 257, 'south kanyamkago', '2017-03-02 14:49:43', '2017-03-02 14:49:43'),
(1281, 27, 257, 'east kanyamkago', '2017-03-02 14:49:43', '2017-03-02 14:49:43'),
(1282, 27, 258, 'kachien''g', '2017-03-02 14:49:43', '2017-03-02 14:49:43'),
(1283, 27, 258, 'kanyasa', '2017-03-02 14:49:43', '2017-03-02 14:49:43'),
(1284, 27, 258, 'north kadem', '2017-03-02 14:49:43', '2017-03-02 14:49:43'),
(1285, 27, 258, 'macalder/kanyarwanda', '2017-03-02 14:49:43', '2017-03-02 14:49:43'),
(1286, 27, 258, 'kaler', '2017-03-02 14:49:43', '2017-03-02 14:49:43'),
(1287, 27, 258, 'got kachola', '2017-03-02 14:49:43', '2017-03-02 14:49:43'),
(1288, 27, 258, 'muhuru', '2017-03-02 14:49:43', '2017-03-02 14:49:43'),
(1289, 27, 259, 'bukira east', '2017-03-02 14:49:43', '2017-03-02 14:49:43'),
(1290, 27, 259, 'bukira centrl/ikerege', '2017-03-02 14:49:43', '2017-03-02 14:49:43'),
(1291, 27, 259, 'isibania', '2017-03-02 14:49:43', '2017-03-02 14:49:43'),
(1292, 27, 259, 'makerero', '2017-03-02 14:49:43', '2017-03-02 14:49:43'),
(1293, 27, 259, 'masaba', '2017-03-02 14:49:43', '2017-03-02 14:49:43'),
(1294, 27, 259, 'tagare', '2017-03-02 14:49:43', '2017-03-02 14:49:43'),
(1295, 27, 259, 'nyamosense/komosoko', '2017-03-02 14:49:43', '2017-03-02 14:49:43'),
(1296, 27, 260, 'gokeharaka/getambwega', '2017-03-02 14:49:43', '2017-03-02 14:49:43'),
(1297, 27, 260, 'ntimaru west', '2017-03-02 14:49:43', '2017-03-02 14:49:43'),
(1298, 27, 260, 'ntimaru east', '2017-03-02 14:49:43', '2017-03-02 14:49:43'),
(1299, 27, 260, 'nyabasi east', '2017-03-02 14:49:43', '2017-03-02 14:49:43'),
(1300, 27, 260, 'nyabasi west', '2017-03-02 14:49:43', '2017-03-02 14:49:43'),
(1301, 18, 261, 'bomariba', '2017-03-02 14:49:43', '2017-03-02 14:49:43'),
(1302, 18, 261, 'bogiakumu', '2017-03-02 14:49:43', '2017-03-02 14:49:43'),
(1303, 18, 261, 'bomorenda', '2017-03-02 14:49:43', '2017-03-02 14:49:43'),
(1304, 18, 261, 'riana', '2017-03-02 14:49:43', '2017-03-02 14:49:43'),
(1305, 18, 262, 'tabaka', '2017-03-02 14:49:43', '2017-03-02 14:49:43'),
(1306, 18, 262, 'boikang''a', '2017-03-02 14:49:43', '2017-03-02 14:49:43'),
(1307, 18, 262, 'bogetenga', '2017-03-02 14:49:43', '2017-03-02 14:49:43'),
(1308, 18, 262, 'borabu / chitago', '2017-03-02 14:49:43', '2017-03-02 14:49:43'),
(1309, 18, 262, 'moticho', '2017-03-02 14:49:44', '2017-03-02 14:49:44'),
(1310, 18, 262, 'getenga', '2017-03-02 14:49:44', '2017-03-02 14:49:44'),
(1311, 18, 263, 'bombaba borabu', '2017-03-02 14:49:44', '2017-03-02 14:49:44'),
(1312, 18, 263, 'boochi borabu', '2017-03-02 14:49:44', '2017-03-02 14:49:44'),
(1313, 18, 263, 'bokimonge', '2017-03-02 14:49:44', '2017-03-02 14:49:44'),
(1314, 18, 263, 'magenche', '2017-03-02 14:49:44', '2017-03-02 14:49:44'),
(1315, 18, 264, 'masige west', '2017-03-02 14:49:44', '2017-03-02 14:49:44'),
(1316, 18, 264, 'masige east', '2017-03-02 14:49:44', '2017-03-02 14:49:44'),
(1317, 18, 264, 'bobasi central', '2017-03-02 14:49:44', '2017-03-02 14:49:44'),
(1318, 18, 264, 'nyacheki', '2017-03-02 14:49:44', '2017-03-02 14:49:44'),
(1319, 18, 264, 'bobasi bogetaorio', '2017-03-02 14:49:44', '2017-03-02 14:49:44'),
(1320, 18, 264, 'bobasi chache', '2017-03-02 14:49:44', '2017-03-02 14:49:44'),
(1321, 18, 264, 'sameta/mokwerero', '2017-03-02 14:49:44', '2017-03-02 14:49:44'),
(1322, 18, 264, 'bobasi boitangare', '2017-03-02 14:49:44', '2017-03-02 14:49:44'),
(1323, 18, 265, 'majoge', '2017-03-02 14:49:44', '2017-03-02 14:49:44'),
(1324, 18, 265, 'boochi/tendere', '2017-03-02 14:49:44', '2017-03-02 14:49:44'),
(1325, 18, 265, 'bosoti/sengera', '2017-03-02 14:49:44', '2017-03-02 14:49:44'),
(1326, 18, 266, 'ichuni', '2017-03-02 14:49:44', '2017-03-02 14:49:44'),
(1327, 18, 266, 'nyamasibi', '2017-03-02 14:49:44', '2017-03-02 14:49:44'),
(1328, 18, 266, 'masimba', '2017-03-02 14:49:44', '2017-03-02 14:49:44'),
(1329, 18, 266, 'gesusu', '2017-03-02 14:49:44', '2017-03-02 14:49:44'),
(1330, 18, 266, 'kiamokama', '2017-03-02 14:49:44', '2017-03-02 14:49:44'),
(1331, 18, 267, 'bobaracho', '2017-03-02 14:49:44', '2017-03-02 14:49:44'),
(1332, 18, 267, 'kisii central', '2017-03-02 14:49:44', '2017-03-02 14:49:44'),
(1333, 18, 267, 'keumbu', '2017-03-02 14:49:45', '2017-03-02 14:49:45'),
(1334, 18, 267, 'kiogoro', '2017-03-02 14:49:45', '2017-03-02 14:49:45'),
(1335, 18, 267, 'birongo', '2017-03-02 14:49:45', '2017-03-02 14:49:45'),
(1336, 18, 267, 'ibeno', '2017-03-02 14:49:45', '2017-03-02 14:49:45'),
(1337, 18, 268, 'monyerero', '2017-03-02 14:49:45', '2017-03-02 14:49:45'),
(1338, 18, 268, 'sensi', '2017-03-02 14:49:45', '2017-03-02 14:49:45'),
(1339, 18, 268, 'marani', '2017-03-02 14:49:45', '2017-03-02 14:49:45'),
(1340, 18, 268, 'kegogi', '2017-03-02 14:49:45', '2017-03-02 14:49:45'),
(1341, 18, 269, 'bogusero', '2017-03-02 14:49:45', '2017-03-02 14:49:45'),
(1342, 18, 269, 'bogeka', '2017-03-02 14:49:45', '2017-03-02 14:49:45'),
(1343, 18, 269, 'nyakoe', '2017-03-02 14:49:45', '2017-03-02 14:49:45'),
(1344, 18, 269, 'kitutu   central', '2017-03-02 14:49:45', '2017-03-02 14:49:45'),
(1345, 18, 269, 'nyatieko', '2017-03-02 14:49:45', '2017-03-02 14:49:45'),
(1346, 33, 270, 'rigoma', '2017-03-02 14:49:45', '2017-03-02 14:49:45'),
(1347, 33, 270, 'gachuba', '2017-03-02 14:49:45', '2017-03-02 14:49:45'),
(1348, 33, 270, 'kemera', '2017-03-02 14:49:45', '2017-03-02 14:49:45'),
(1349, 33, 270, 'magombo', '2017-03-02 14:49:45', '2017-03-02 14:49:45'),
(1350, 33, 270, 'manga', '2017-03-02 14:49:45', '2017-03-02 14:49:45'),
(1351, 33, 270, 'gesima', '2017-03-02 14:49:45', '2017-03-02 14:49:45'),
(1352, 33, 271, 'nyamaiya', '2017-03-02 14:49:45', '2017-03-02 14:49:45'),
(1353, 33, 271, 'bogichora', '2017-03-02 14:49:45', '2017-03-02 14:49:45'),
(1354, 33, 271, 'bosamaro', '2017-03-02 14:49:45', '2017-03-02 14:49:45'),
(1355, 33, 271, 'bonyamatuta', '2017-03-02 14:49:45', '2017-03-02 14:49:45'),
(1356, 33, 271, 'township', '2017-03-02 14:49:45', '2017-03-02 14:49:45'),
(1357, 33, 272, 'itibo', '2017-03-02 14:49:45', '2017-03-02 14:49:45'),
(1358, 33, 272, 'bomwagamo', '2017-03-02 14:49:46', '2017-03-02 14:49:46'),
(1359, 33, 272, 'bokeira', '2017-03-02 14:49:46', '2017-03-02 14:49:46'),
(1360, 33, 272, 'magwagwa', '2017-03-02 14:49:46', '2017-03-02 14:49:46'),
(1361, 33, 272, 'ekerenyo', '2017-03-02 14:49:46', '2017-03-02 14:49:46'),
(1362, 33, 273, 'mekenene', '2017-03-02 14:49:46', '2017-03-02 14:49:46'),
(1363, 33, 273, 'kiabonyoru', '2017-03-02 14:49:46', '2017-03-02 14:49:46'),
(1364, 33, 273, 'nyansiongo', '2017-03-02 14:49:46', '2017-03-02 14:49:46'),
(1365, 33, 273, 'esise', '2017-03-02 14:49:46', '2017-03-02 14:49:46'),
(1366, 1, 274, 'kitisuru', '2017-03-02 14:49:46', '2017-03-02 14:49:46'),
(1367, 1, 274, 'parklands/highridge', '2017-03-02 14:49:46', '2017-03-02 14:49:46'),
(1368, 1, 274, 'karura', '2017-03-02 14:49:46', '2017-03-02 14:49:46'),
(1369, 1, 274, 'kangemi', '2017-03-02 14:49:46', '2017-03-02 14:49:46'),
(1370, 1, 274, 'mountain view', '2017-03-02 14:49:46', '2017-03-02 14:49:46'),
(1371, 1, 275, 'kilimani', '2017-03-02 14:49:46', '2017-03-02 14:49:46');
INSERT INTO `wards` (`id`, `county_id`, `sub_county_id`, `ward_name`, `created_at`, `updated_at`) VALUES
(1372, 1, 275, 'kawangware', '2017-03-02 14:49:46', '2017-03-02 14:49:46'),
(1373, 1, 275, 'gatina', '2017-03-02 14:49:46', '2017-03-02 14:49:46'),
(1374, 1, 275, 'kileleshwa', '2017-03-02 14:49:46', '2017-03-02 14:49:46'),
(1375, 1, 275, 'kabiro', '2017-03-02 14:49:46', '2017-03-02 14:49:46'),
(1376, 1, 276, 'mutuini', '2017-03-02 14:49:46', '2017-03-02 14:49:46'),
(1377, 1, 276, 'ngando', '2017-03-02 14:49:46', '2017-03-02 14:49:46'),
(1378, 1, 276, 'riruta', '2017-03-02 14:49:46', '2017-03-02 14:49:46'),
(1379, 1, 276, 'uthiru/ruthimitu', '2017-03-02 14:49:46', '2017-03-02 14:49:46'),
(1380, 1, 276, 'waithaka', '2017-03-02 14:49:46', '2017-03-02 14:49:46'),
(1381, 1, 277, 'karen', '2017-03-02 14:49:46', '2017-03-02 14:49:46'),
(1382, 1, 277, 'nairobi west', '2017-03-02 14:49:46', '2017-03-02 14:49:46'),
(1383, 1, 277, 'mugumo-ini', '2017-03-02 14:49:46', '2017-03-02 14:49:46'),
(1384, 1, 277, 'south-c', '2017-03-02 14:49:46', '2017-03-02 14:49:46'),
(1385, 1, 277, 'nyayo highrise', '2017-03-02 14:49:46', '2017-03-02 14:49:46'),
(1386, 1, 278, 'laini saba', '2017-03-02 14:49:47', '2017-03-02 14:49:47'),
(1387, 1, 278, 'lindi', '2017-03-02 14:49:47', '2017-03-02 14:49:47'),
(1388, 1, 278, 'makina', '2017-03-02 14:49:47', '2017-03-02 14:49:47'),
(1389, 1, 278, 'woodley/kenyatta golf', '2017-03-02 14:49:47', '2017-03-02 14:49:47'),
(1390, 1, 278, 'sarangombe', '2017-03-02 14:49:47', '2017-03-02 14:49:47'),
(1391, 1, 279, 'githurai', '2017-03-02 14:49:47', '2017-03-02 14:49:47'),
(1392, 1, 279, 'kahawa west', '2017-03-02 14:49:47', '2017-03-02 14:49:47'),
(1393, 1, 279, 'zimmerman', '2017-03-02 14:49:47', '2017-03-02 14:49:47'),
(1394, 1, 279, 'roysambu', '2017-03-02 14:49:47', '2017-03-02 14:49:47'),
(1395, 1, 279, 'kahawa', '2017-03-02 14:49:47', '2017-03-02 14:49:47'),
(1396, 1, 280, 'claycity', '2017-03-02 14:49:47', '2017-03-02 14:49:47'),
(1397, 1, 280, 'mwiki', '2017-03-02 14:49:47', '2017-03-02 14:49:47'),
(1398, 1, 280, 'kasarani', '2017-03-02 14:49:47', '2017-03-02 14:49:47'),
(1399, 1, 280, 'njiru', '2017-03-02 14:49:47', '2017-03-02 14:49:47'),
(1400, 1, 280, 'ruai', '2017-03-02 14:49:47', '2017-03-02 14:49:47'),
(1401, 1, 281, 'baba dogo', '2017-03-02 14:49:47', '2017-03-02 14:49:47'),
(1402, 1, 281, 'utalii', '2017-03-02 14:49:47', '2017-03-02 14:49:47'),
(1403, 1, 281, 'mathare north', '2017-03-02 14:49:47', '2017-03-02 14:49:47'),
(1404, 1, 281, 'lucky summer', '2017-03-02 14:49:47', '2017-03-02 14:49:47'),
(1405, 1, 281, 'korogocho', '2017-03-02 14:49:47', '2017-03-02 14:49:47'),
(1406, 1, 282, 'imara daima', '2017-03-02 14:49:47', '2017-03-02 14:49:47'),
(1407, 1, 282, 'kwa njenga', '2017-03-02 14:49:47', '2017-03-02 14:49:47'),
(1408, 1, 282, 'kwa reuben', '2017-03-02 14:49:48', '2017-03-02 14:49:48'),
(1409, 1, 282, 'pipeline', '2017-03-02 14:49:48', '2017-03-02 14:49:48'),
(1410, 1, 282, 'kware', '2017-03-02 14:49:48', '2017-03-02 14:49:48'),
(1411, 1, 283, 'kariobangi north', '2017-03-02 14:49:48', '2017-03-02 14:49:48'),
(1412, 1, 283, 'dandora area i', '2017-03-02 14:49:48', '2017-03-02 14:49:48'),
(1413, 1, 283, 'dandora area ii', '2017-03-02 14:49:48', '2017-03-02 14:49:48'),
(1414, 1, 283, 'dandora area iii', '2017-03-02 14:49:48', '2017-03-02 14:49:48'),
(1415, 1, 283, 'dandora area iv', '2017-03-02 14:49:48', '2017-03-02 14:49:48'),
(1416, 1, 284, 'kayole north', '2017-03-02 14:49:48', '2017-03-02 14:49:48'),
(1417, 1, 284, 'kayole central', '2017-03-02 14:49:48', '2017-03-02 14:49:48'),
(1418, 1, 284, 'kayole south', '2017-03-02 14:49:48', '2017-03-02 14:49:48'),
(1419, 1, 284, 'komarock', '2017-03-02 14:49:48', '2017-03-02 14:49:48'),
(1420, 1, 284, 'matopeni', '2017-03-02 14:49:48', '2017-03-02 14:49:48'),
(1421, 1, 285, 'upper savannah', '2017-03-02 14:49:48', '2017-03-02 14:49:48'),
(1422, 1, 285, 'lower savannah', '2017-03-02 14:49:48', '2017-03-02 14:49:48'),
(1423, 1, 285, 'embakasi', '2017-03-02 14:49:48', '2017-03-02 14:49:48'),
(1424, 1, 285, 'utawala', '2017-03-02 14:49:48', '2017-03-02 14:49:48'),
(1425, 1, 285, 'mihango', '2017-03-02 14:49:48', '2017-03-02 14:49:48'),
(1426, 1, 286, 'umoja i', '2017-03-02 14:49:48', '2017-03-02 14:49:48'),
(1427, 1, 286, 'umoja ii', '2017-03-02 14:49:48', '2017-03-02 14:49:48'),
(1428, 1, 286, 'mowlem', '2017-03-02 14:49:48', '2017-03-02 14:49:48'),
(1429, 1, 286, 'kariobangi south', '2017-03-02 14:49:48', '2017-03-02 14:49:48'),
(1430, 1, 287, 'makongeni', '2017-03-02 14:49:48', '2017-03-02 14:49:48'),
(1431, 1, 287, 'maringo/hamza', '2017-03-02 14:49:48', '2017-03-02 14:49:48'),
(1432, 1, 287, 'harambee', '2017-03-02 14:49:48', '2017-03-02 14:49:48'),
(1433, 1, 287, 'viwandani', '2017-03-02 14:49:49', '2017-03-02 14:49:49'),
(1434, 1, 288, 'pumwani', '2017-03-02 14:49:49', '2017-03-02 14:49:49'),
(1435, 1, 288, 'eastleigh north', '2017-03-02 14:49:49', '2017-03-02 14:49:49'),
(1436, 1, 288, 'eastleigh south', '2017-03-02 14:49:49', '2017-03-02 14:49:49'),
(1437, 1, 288, 'airbase', '2017-03-02 14:49:49', '2017-03-02 14:49:49'),
(1438, 1, 288, 'california', '2017-03-02 14:49:49', '2017-03-02 14:49:49'),
(1439, 1, 289, 'nairobi central', '2017-03-02 14:49:49', '2017-03-02 14:49:49'),
(1440, 1, 289, 'ngara', '2017-03-02 14:49:49', '2017-03-02 14:49:49'),
(1441, 1, 289, 'ziwani/kariokor', '2017-03-02 14:49:49', '2017-03-02 14:49:49'),
(1442, 1, 289, 'pangani', '2017-03-02 14:49:49', '2017-03-02 14:49:49'),
(1443, 1, 289, 'landimawe', '2017-03-02 14:49:49', '2017-03-02 14:49:49'),
(1444, 1, 289, 'nairobi south', '2017-03-02 14:49:49', '2017-03-02 14:49:49'),
(1445, 1, 290, 'hospital', '2017-03-02 14:49:49', '2017-03-02 14:49:49'),
(1446, 1, 290, 'mabatini', '2017-03-02 14:49:49', '2017-03-02 14:49:49'),
(1447, 1, 290, 'huruma', '2017-03-02 14:49:49', '2017-03-02 14:49:49'),
(1448, 1, 290, 'ngei', '2017-03-02 14:49:49', '2017-03-02 14:49:49'),
(1449, 1, 290, 'mlango kubwa', '2017-03-02 14:49:49', '2017-03-02 14:49:49'),
(1450, 1, 290, 'kiamaiko', '2017-03-02 14:49:50', '2017-03-02 14:49:50');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `assign_units`
--
ALTER TABLE `assign_units`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `classes`
--
ALTER TABLE `classes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `class_instructors`
--
ALTER TABLE `class_instructors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`company_id`);

--
-- Indexes for table `consumables`
--
ALTER TABLE `consumables`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `consumable_categories`
--
ALTER TABLE `consumable_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `consumable_movement`
--
ALTER TABLE `consumable_movement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `counties`
--
ALTER TABLE `counties`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`department_id`);

--
-- Indexes for table `designations`
--
ALTER TABLE `designations`
  ADD PRIMARY KEY (`designation_id`);

--
-- Indexes for table `dis_recruits`
--
ALTER TABLE `dis_recruits`
  ADD PRIMARY KEY (`dis_recruit_id`);

--
-- Indexes for table `do_lists`
--
ALTER TABLE `do_lists`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `grades`
--
ALTER TABLE `grades`
  ADD PRIMARY KEY (`grade_id`);

--
-- Indexes for table `instructors`
--
ALTER TABLE `instructors`
  ADD PRIMARY KEY (`instructor_id`);

--
-- Indexes for table `instructor_department`
--
ALTER TABLE `instructor_department`
  ADD PRIMARY KEY (`instructor_department_id`);

--
-- Indexes for table `non-consumables`
--
ALTER TABLE `non-consumables`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `non-consumable_categories`
--
ALTER TABLE `non-consumable_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `platoon`
--
ALTER TABLE `platoon`
  ADD PRIMARY KEY (`platoon_id`);

--
-- Indexes for table `prform_data`
--
ALTER TABLE `prform_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `prform_uploads`
--
ALTER TABLE `prform_uploads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchase_request`
--
ALTER TABLE `purchase_request`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `recruit_clearance`
--
ALTER TABLE `recruit_clearance`
  ADD PRIMARY KEY (`recruit_clearance_id`);

--
-- Indexes for table `registry_permission`
--
ALTER TABLE `registry_permission`
  ADD PRIMARY KEY (`reg_perm_id`);

--
-- Indexes for table `store_items`
--
ALTER TABLE `store_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `store_motion`
--
ALTER TABLE `store_motion`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `store_moves`
--
ALTER TABLE `store_moves`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_counties`
--
ALTER TABLE `sub_counties`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tribes`
--
ALTER TABLE `tribes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `units`
--
ALTER TABLE `units`
  ADD PRIMARY KEY (`unit_id`);

--
-- Indexes for table `wards`
--
ALTER TABLE `wards`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accounts`
--
ALTER TABLE `accounts`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `assign_units`
--
ALTER TABLE `assign_units`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `classes`
--
ALTER TABLE `classes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `class_instructors`
--
ALTER TABLE `class_instructors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `companies`
--
ALTER TABLE `companies`
  MODIFY `company_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `consumables`
--
ALTER TABLE `consumables`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `consumable_categories`
--
ALTER TABLE `consumable_categories`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `consumable_movement`
--
ALTER TABLE `consumable_movement`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `counties`
--
ALTER TABLE `counties`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `department_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `designations`
--
ALTER TABLE `designations`
  MODIFY `designation_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `dis_recruits`
--
ALTER TABLE `dis_recruits`
  MODIFY `dis_recruit_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `do_lists`
--
ALTER TABLE `do_lists`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `grades`
--
ALTER TABLE `grades`
  MODIFY `grade_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `instructors`
--
ALTER TABLE `instructors`
  MODIFY `instructor_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `instructor_department`
--
ALTER TABLE `instructor_department`
  MODIFY `instructor_department_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `non-consumables`
--
ALTER TABLE `non-consumables`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `non-consumable_categories`
--
ALTER TABLE `non-consumable_categories`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `platoon`
--
ALTER TABLE `platoon`
  MODIFY `platoon_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `prform_data`
--
ALTER TABLE `prform_data`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `prform_uploads`
--
ALTER TABLE `prform_uploads`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `purchase_request`
--
ALTER TABLE `purchase_request`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `recruit_clearance`
--
ALTER TABLE `recruit_clearance`
  MODIFY `recruit_clearance_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `registry_permission`
--
ALTER TABLE `registry_permission`
  MODIFY `reg_perm_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT for table `store_items`
--
ALTER TABLE `store_items`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `store_motion`
--
ALTER TABLE `store_motion`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `store_moves`
--
ALTER TABLE `store_moves`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `sub_counties`
--
ALTER TABLE `sub_counties`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=291;
--
-- AUTO_INCREMENT for table `tribes`
--
ALTER TABLE `tribes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT for table `units`
--
ALTER TABLE `units`
  MODIFY `unit_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `wards`
--
ALTER TABLE `wards`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1451;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
