<?php include '_header.instructor.php';

$clax=mysql_real_escape_string($_REQUEST['class_id']);
$subj=mysql_real_escape_string($_REQUEST['unit_id']);
$tgg_term = 1;
$tgg_year = 2017;
?>

<div id="main-container">
    <script type="text/javascript" charset="utf-8">
        $(function() {
            $('#responsiveTable').dataTable();
        } );
    </script>
    <div class="padding-md">
        <div align="center" class="panel-body">
            <form action="" method="get" class="form-inline no-margin">
                <div class="form-group">
                    <label class="sr-only">Classes</label>
                    <select id="class_id" class="form-control inline-block" name="class_id">
                        <?php
                        echo class_options();
                        ?>
                    </select>
                </div>
                <div class="form-group">
                    <label class="sr-only">Units</label>
                    <select id="myclasses_drop_stf" class="form-control inline-block" name="subject_id">
                        <?php echo unit_options();
                        ?>
                    </select>
                </div>

                <button type="submit" class="btn btn-sm btn-primary">Open Grade Form</button>
            </form>
        </div>

        <div class="panel panel-default table-responsive">
            <div class="padding-sm font-16 bg-grey" align="center">
                <?php
                $class_tag = make_class_tag($clax,$subj);

                if(isset($clax) && $clax!=''){
                    $vhcont='';
                    $rew_subject=ucwords(strtolower(unit_name($subj)));
                    echo 'Showing Grades for'.'<strong>'. $rew_classlab.' <small>'.$rew_subject.'</small></strong>';
                }else{ $vhcont='hidden';$rew_subject='';$rew_classlab='';echo "<div class='animate2 bounceIn font-normal'> <i class='fa fa-chevron-right'></i> Select a class and unit above</div>";}?>
            </div>

            <div class="seperator"></div><div class="seperator"></div>
            <form class="<?php echo $vhcont;?>" name="grader" id="grade_form" method="post"  action="handle_grade.php">
                <input class="hidden" hidden name="ccc" value="<?php echo $clax;?>"/>
                <input class="hidden" hidden name="sss" value="<?php echo $subj;?>"/>
                <table class="table table-striped" id="responsiveTable">
                    <thead>
                    <tr>
                        <th align="left"><span class=""></span>Student Name</th>
                        <th align="left"><span class=""></span>Sto No.</th>
                        <th width="12%" align="left"><span class=""></span> Cat  30%</th>
                        <th width="12%" align="left"><span class=""></span>Exams 70%</th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php
                    $list_query = mysql_query("SELECT sto_number FROM recruits WHERE status = 1");
                    $grade_inputs=0;
                    while($list_result = mysql_fetch_array($list_query)){
                        $sadminn = $list_result['sto_number'];
                        $sdname=recruit_name($sadminn);
                        $grade_check = check_grade_submission($class_tag,$sadminn,$subj);
                        if($grade_check==''){
                            $grade_inputs++
                            ?>
                            <tr>
                                <td align="left"><?php echo $sdname;?></td>
                                <td align="left"><?php echo $sadminn;?></td>
                                <td title="<?php echo $sdname.' - '.$rew_subject.'Marks';?>" align="left">
                                        <input type="text"   name="cat"  class="form-control font-15 score" />
                                </td>
                                <td title="<?php echo $sdname.' - '.$rew_subject.'Marks';?>" align="left">
                                    <input type="text"  name="exam"  class="form-control font-15 score" />

                                </td>
                            </tr>
                            <?php
                        }           else{
                            $remark = get_grade_remark($class_tag,$sadminn,$subj);
                            ?>

                            <tr>
                                <td align="left"><?php echo $sdname;?></td>
                                <td align="left"><?php echo $sadminn;?></td>
                                <td title="<?php echo $sdname.' - '.$rew_subject.' Marks';?>" align="left"><input style="width:67px;" disabled class="form-control font-14 score" value="<?php echo grade($grade_check);?>" type="text"/></td>
                                <td align="left"><input <?php if(($grade_check==1) || strlen($remark)==0){echo 'style="display:none;"';}?> disabled class="form-control" value="<?php echo $remark;?>" type="text"/></td>
                            </tr>

                            <?php
                        }
                    }
                    ?></tbody>
                </table>
                <input hidden="hidden" value="<?php echo $class_tag;?>" name="gr_classid"/>
                <input hidden="hidden" value="<?php echo $subj;?>" name="gr_subjectid"/>
                <input hidden="hidden" value="<?php echo $tgg_term;?>" name="gr_term"/>
                <input hidden="hidden" value="<?php echo $tgg_year;?>" name="gr_year"/>
                <input hidden="hidden" value="<?php echo $tid;?>" name="stf_idf"/>
                <div align="center">
                    <?php if($grade_inputs!=0){?><button id="grade_class" type="submit" style="margin-top: 38px; margin-bottom: 10px" class="btn btn-success"><span class="fa fa-check"></span> Submit Grade<?php echo plu  ($grade_inputs);?></button><?php }else{}?>
                </div>

            </form>
        </div>
    </div>
</div>


<script> $(".grade_mu").addClass('active');


    $(document).on("change", ".score", function () {
        var el = $(this);
        var target_id = el.data("id");
        var grade = el.val();

        var remark_el = $("#remark_"+target_id);
        remark_el.prop('required',false);

        if(grade==1){
            remark_el.hide();
            remark_el.val('');
        }

        if(grade==0){

            remark_el.show();
            remark_el.val('');

        }
        if(grade==3){

            remark_el.show();
            remark_el.val('');
            remark_el.prop('required',true);
        }




    });

    $(function onchange_subject() {
        $('#myclasses_drop_stf').change(function() {
            this.form.submit();
        });
    });

    function sp_subjects(){
        var class_id = $("#class_id").val();
        var placer= $('#myclasses_drop_stf');
        var staff_id = '<?php echo $_SESSION['_user_id'];?>';


        $.post('_ajax.php',{class_id:class_id,staff_id_subject_options:staff_id},function(subject_options_cl){
            console.log(subject_options_cl);
            placer.html(subject_options_cl);

        });

    }

    $(function onchange_class() {
        $('#class_id').change(function() {
            sp_subjects();
        });
    });

    sp_subjects();


</script>
<?php include'../_footer.php';?>
