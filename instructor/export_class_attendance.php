<?php
include "../_functions.php";
include "../_variables.php";
$defa_excel_expo_format='pdf';
//$defa_excel_expo_format='xlsx';
require_once dirname(__FILE__) . '/hummus/Classes/PHPExcel.php';

$class_id=mysql_real_escape_string($_REQUEST['clid']);

$expot_cateogo='Attendance Sheet';
$exofile_name=make_class_tag($class_id,'').' '.$expot_cateogo;


$student_admns=get_class_students($class_id);

$doc_subject=$exofile_name;
$doc_description=$exofile_name;
$keywords=$exofile_name;
$doc_category=$expot_cateogo;
$doc_title=$exofile_name;
$last_mod_author=$software_label;
$doc_creator=$software_label.'>'.$school_name;

$objPHPExcel = new PHPExcel();

$objPHPExcel->getProperties()->setCreator($doc_creator)
							 ->setLastModifiedBy($last_mod_author)
							 ->setTitle($doc_title)
							 ->setSubject($doc_subject)
							 ->setDescription($doc_description)
							 ->setKeywords($keywords)
							 ->setCategory($doc_category);

$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getDefaultStyle()->getFont()
    ->setName('Segoe UI Symbol')
    ->setSize(11);

$class_listex_titl= array(
    'font'  => array(
        'bold'  => true,
        'size'  => 11,
    ));

cellColor(1, 'C7EFD9');
$objPHPExcel->getActiveSheet()->mergeCells('A1:C1');
$objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($class_listex_titl);
$objPHPExcel->getActiveSheet()->setCellValue('A1',strtoupper($exofile_name));


$objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->applyFromArray(
    array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,)
);
$objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->applyFromArray(
    array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,)
);


$pos_handler=3;
$title_po=$pos_handler-1;
$ncmm='B'.($title_po);
$ncmmc='C'.($title_po);
$objPHPExcel->getActiveSheet()->setCellValue($ncmm,'Name');
$objPHPExcel->getActiveSheet()->setCellValue($ncmmc,$student_number_term);
//$objPHPExcel->getActiveSheet()->getStyle('A'.$title_po.':D'.$title_po)->applyFromArray($class_listex_titl);
$student_counter=0;
foreach($student_admns as $student_id){
$student_name_cllr=recruit_name($student_id);
$student_counter++;
$student_counter_a=$student_counter.'.';
$objPHPExcel->getActiveSheet()->setCellValue('A'.$pos_handler,$student_counter_a);
$objPHPExcel->getActiveSheet()->setCellValue('B'.$pos_handler,$student_name_cllr);
$objPHPExcel->getActiveSheet()->setCellValue('C'.$pos_handler,$student_id);


$objPHPExcel->getActiveSheet()->getStyle('A'.$pos_handler)->getAlignment()->applyFromArray(
    array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER_CONTINUOUS,)
);

$pos_handler=$pos_handler+1;
}

/*$week_id='Week 1';
$wdays='Mon | Tu | We | Th | Fr | Sa | Su ';
$objPHPExcel->getActiveSheet()->setCellValue('D2',$week_id);

$objPHPExcel->getActiveSheet()->setCellValue('D3',$wdays);*/

function attendance_days_short(){
    return array('Mo','Tu','We','Th','Fr');
    //return array('Mo','Tu');
}


$weeks_to_gen=array('1','2','3','4','5','6','7','8','9','10','11','12');

$weeks_to_gen=array('1','2','3','4','5','6','7','8','9','10','11','12');


$weeks_to_gen=array('1','2','3','4','5','6','7');



$number_of_weeks=count($weeks_to_gen);
//$number_of_weeks=8;
$attend=attendance_gen($number_of_weeks);

if($number_of_weeks>2){$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
}



$atten=explode(',',$attend);



$plus=0;
$pluser=3;
foreach($atten as $dax){
    $real_plus=($plus+$pluser);
    $kix=alphabet($real_plus);
    $objPHPExcel->getActiveSheet()->getColumnDimension($kix)->setWidth(4);
    $calc_ss=$kix.'2';
    $objPHPExcel->getActiveSheet()->setCellValue($calc_ss,$dax);
    $pluser++;
}

$pluser=3;
$number_of_days=count(attendance_days_short());
$wpos='';
$ni=0;
while($ni<$number_of_weeks){
    $ni=$ni+1;
    $int=$ni*$number_of_days;
    $end_k=$int+$pluser;
    $start_k=$int-$number_of_days;
    $start_k=$start_k+$pluser;
    $wpos.=alphabet($start_k).'1:'.alphabet($end_k-1).'1,';
}

$wpos=substr($wpos,0,-1);
$weeks_explode=explode(',',$wpos);


$week_fetch=0;
foreach($weeks_explode as $expo){
    $week_fetched=$weeks_to_gen[$week_fetch];
    $objPHPExcel->getActiveSheet()->mergeCells($expo);
    $wri_pos=explode(':',$expo);
    $wri_pos=$wri_pos[0];
    $objPHPExcel->getActiveSheet()->setCellValue($wri_pos,'Week '.$week_fetched);
    $objPHPExcel->getActiveSheet()->getStyle($wri_pos)->getAlignment()->applyFromArray(
        array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER_CONTINUOUS,)
    );
    $week_fetch++;
}


/*$last_line_exp=$pos_handler+4;
$updatex = 'B'.$last_line_exp;
cellColor($last_line_exp, 'C7EFD9');
$objPHPExcel->getActiveSheet()->setCellValue($updatex,'As at '.date("dS-M-Y H:i"));*/


//
//$objPHPExcel->getActiveSheet()->setCellValue('A8',"Hello\nWorld");
//$objPHPExcel->getActiveSheet()->getRowDimension(8)->setRowHeight(-1);
//$objPHPExcel->getActiveSheet()->getStyle('A8')->getAlignment()->setWrapText(true);

$objPHPExcel->getActiveSheet()->getHeaderFooter()->setEvenFooter($exofile_name);
$objPHPExcel->getActiveSheet()->getHeaderFooter()->setOddFooter('&L&B' . $objPHPExcel->getProperties()->getTitle() . '&RPage &P of &N');
$objPHPExcel->getActiveSheet()->getHeaderFooter()->setEvenFooter('&L&B' . $objPHPExcel->getProperties()->getTitle() . '&RPage &P of &N');

$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(7);

$objPHPExcel->getActiveSheet()
    ->getColumnDimension('B')
    ->setAutoSize(true);
$objPHPExcel->getActiveSheet()
    ->getColumnDimension('C')
    ->setAutoSize(true);



$objPHPExcel->getActiveSheet()->setPrintGridlines(TRUE);



$objPHPExcel->getActiveSheet()->setTitle($exofile_name);

$objPHPExcel->createSheet();

$objPHPExcel->setActiveSheetIndex(1);
$objPHPExcel->getActiveSheet()->setTitle('Test');
$objPHPExcel->getActiveSheet()->setCellValue('A4','Week');


$objPHPExcel->setActiveSheetIndex(1);




$exofile_name=$exofile_name.'.'.$defa_excel_expo_format;

if($defa_excel_expo_format=='xlsx'){
    header('Content-Type: application/vnd.ms-excel');
    header("Content-Disposition: attachment;filename=$exofile_name");
    header('Cache-Control: max-age=0');
    header('Cache-Control: max-age=1');
    header ('Expires: Mon, 18 Jul 2014 05:00:00 GMT');
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
    header ('Cache-Control: cache, must-revalidate');
    header ('Pragma: public');
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

}elseif($defa_excel_expo_format=='xls'){
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header("Content-Disposition: attachment;filename=$exofile_name");
    header('Cache-Control: max-age=0');
    header('Cache-Control: max-age=1');
    header ('Expires: Mon, 18 Jul 2014 05:00:00 GMT');
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
    header ('Cache-Control: cache, must-revalidate');
    header ('Pragma: public');
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
}elseif($defa_excel_expo_format=='pdf'){
    $rendererName = PHPExcel_Settings::PDF_RENDERER_MPDF;
    $rendererLibrary = 'mPDF5.7';
    $rendererLibraryPath = dirname(__FILE__) . '/hummus/Classes/pdf/';

    if (!PHPExcel_Settings::setPdfRenderer(
        $rendererName,
        $rendererLibraryPath
    )) {
        die(
            'NOTICE: Something ain`t right, kindly contact system developers' .
            '<br />' .
            'ERRORCODE: PDF_LIB404'
        );
    }

    header('Content-Type: application/pdf');
    header("Content-Disposition: attachment;filename=".$exofile_name."");
    header('Cache-Control: max-age=0');

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'PDF');

}


$objWriter->save('php://output');
exit;
