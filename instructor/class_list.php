<?php
include '_header.instructor.php';
$clax=mysql_real_escape_string($_REQUEST['class_id']);

?>
<div id="main-container">
    <script type="text/javascript" charset="utf-8">
        $(function() {
            $('#responsiveTable').dataTable();
        } );
    </script>
    <div class="padding-md">
        <div align="center" class="panel-body">
            <form action="class_list" method="post" class="form-inline no-margin">
                <div class="form-group">
                    <label class="sr-only">My Classes</label>
                    <select id="myclasses_drop_stf" class="form-control inline-block" name="class_id">
                        <option value="">My Classes</option>
                        <?php
                        echo myclasses($tid);
                        ?>
                    </select>
                </div>

                <button type="submit" class="btn btn-sm btn-primary">Get Class list</button>
            </form>
        </div>
        <?php if($clax!=''){ ?>
            <div class="panel panel-default table-responsive">
                <div class="padding-sm font-16 bold bg-grey">
                   Listing Students of<?php //echo room_label(get_room_id($clax));?>
                    <a href="export_class_students?clid=<?php echo $clax;?>" role="button" data-toggle="modal" class="btn btn-primary col-sm-2 pull-right"><span class="fa fa-download"></span> Download Class list</a>
                    <a href="export_class_attendance?clid=<?php echo $clax;?>" role="button" data-toggle="modal" class="btn btn-success col-sm-2 pull-right"><span class="fa fa-download"></span> Attendance List</a>

                </div>
                <div class="seperator"></div><div class="seperator"></div>
                <table class="table table-striped" id="responsiveTable">
                    <thead>
                    <tr>
                        <th align="left"><span class=""></span>Admission</th>
                        <th align="left"><span class=""></span>Student Name</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $list_query = m("SELECT sto_number FROM recruits WHERE  status = 1");
                    while($list_result = mysql_fetch_array($list_query)){
                        $sadminn = $list_result['sto_number'];
                        ?>
                        <tr>
                            <td align="left"><?php echo $sadminn?></td>
                            <td align="left"><?php echo recruit_name($sadminn)?></td>
                        </tr>
                        <?php
                    }
                    ?></tbody>
                </table>
            </div>

        <?php }else{echo "<div align='center' class='animate2 bounceIn font-normal bg-grey font-15 padding-sm'> <i class='fa fa-chevron-right'></i> Select a class from the drop down above</div>";} ?>

    </div>
</div>

<script> $(".classlist_mu").addClass('active');
    $(function() {
        $('#myclasses_drop_stf').change(function() {
            this.form.submit();
        });
    });
</script>


<?php include'../_footer.php';?>
