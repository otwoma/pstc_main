<?php include '_header.instructor.php';
?>
<link href="../assets/css/files.css" rel="stylesheet"/>
<script src="../assets/js/jquery.ui.position.js" type="text/javascript"></script>
<script src="../assets/js/jquery.contextMenu.js" type="text/javascript"></script>
<script src="../assets/js/screen.js" type="text/javascript"></script>
<link href="../assets/css/jquery.contextMenu.css" rel="stylesheet" type="text/css"/>

<script src="../assets/js/mediaelement-and-player.min.js"></script>
<link rel="stylesheet" href="../assets/css/style.css" media="screen">


<div id="main-container">
    <div class="padding-md">

        <div class="filemanager">

            <div class="input-group search col-md-3 pull-right">
                <input type="search" class="form-control input-sm" placeholder="Search...">
									<span class="input-group-btn">
									<button class="btn btn-default btn-sm" type="button"><i class="fa fa-search"></i>
                                    </button>
								</span>

            </div>


            <?php $file_fun = '<a id="new_folder_here" href="#formModalNewFolder" role="button" data-toggle="modal" class="btn btn-small btn-default"><k class="fa fa-plus"></k> Create New Folder</a>

                <a id="file_upl" href="#formModalNewFile" role="button" data-toggle="modal" class="btn btn-default"><k class="fa fa-file-o"></k> Add a file</a>
'; ?>

            <div class="breadcrumbs"></div>

            <div class="col-md-8 col-lg-push-4 new_fitems">
                <?php echo $file_fun; ?>
            </div>
            <ul class="data"></ul>
            <div class="nothingfound col-md-6 col-lg-push-3">
                <div class="nofiles"></div>
                <span> Hey <em><?php echo getname(); ?></em>, this folder is currently empty.</span>
                <?php echo $file_fun; ?>
            </div>

        </div>

        <script src="../assets/js/file.js"></script>

    </div>


</div>


<a id="open_view" href="#fileView" role="button" class="hidden" data-toggle="modal""></a>
<a id="open_share" href="#fileShare" role="button" class="hidden" data-toggle="modal""></a>


<div class="modal fade" id="formModalNewFile">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4>New File</h4>
            </div>
            <div class="modal-body" id="newfilealert">
                <form id="new_file_frm" action="../_actions.php" method="POST" enctype="multipart/form-data">

                    <div class="form-group" id="newfileho">
                        <label>File</label>

                        <input type="file" name="the_new_file[]" id="file" multiple="multiple" class="form-control"
                               required=""/>

                    </div>

                    <div class="form-group hidden">
                        <input id="file_target_folder" type="text" name="file_cont_folder" class="form-control hidden"
                               hidden required>
                    </div>


                    <div class="form-group" id="newfileho">
                        <label>File Description <!--<small><sub>(Optional)</sub></small>--></label>

                        <input type="text" placeholder="Description (Optional)" name="the_file_description[]"
                               id="file_desc" class="form-control"/>

                    </div>

                    <div class="form-group">
                        <span id="new_file_location"></span>
                    </div>

            </div>
            <div class="modal-footer">
                <button class="btn btn-success btn-sm" data-dismiss="modal" aria-hidden="true">Close</button>
                <button id="newfile_btsub" type="submit" class="btn btn-danger btn-sm">Submit</button>
            </div>
        </div>
    </div>
    </form>
</div>


<div class="modal fade" id="formModalNewFolder">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4>Creating New Folder</h4>
            </div>
            <div class="modal-body" id="newfolderalert">
                <form id="new_folder_frm" action="../_actions.php" method="POST" enctype="multipart/form-data">

                    <div class="form-group" id="newfolho">
                        <label>Folder Name</label>
                        <input type="text" id="folder_name" name="new_folder_name_staff" class="form-control font-14"
                               required>

                    </div>

                    <div class="form-group hidden">
                        <input id="create_in" type="text" name="cont_folder" class="form-control" hidden="" required>
                    </div>


                    <div class="form-group">
                        <span id="new_folder_location"></span>
                    </div>

            </div>
            <div class="modal-footer">
                <button class="btn btn-success btn-sm" data-dismiss="modal" aria-hidden="true">Close</button>
                <button id="newfold_btsub" type="submit" class="btn btn-danger btn-sm">Submit</button>
            </div>
        </div>
    </div>
    </form>
</div>

<input hidden class="hidden" id="rclick_file"/>
<input hidden class="hidden" id="rclick_folder"/>


<div class="modal fade" id="fileView">
    <div class="modal-dialog">
        <div class="modal-content" id="view_file">
        </div>
    </div>

</div>


<div class="modal fade" id="fileShare">
    <div class="modal-dialog">
        <div class="modal-content" id="share_folder">
        </div>
    </div>

</div>


<script type="text/javascript" class="showcase">
    $(function () {
        $(".modal").on("click", "#close_md", function () {
            $("#view_file").html('');
        });
        $(".modal").on("click", "#vplay", function () {
            $(".mejs-play").trigger('click');
            $(".mejs-pause").trigger('click');
        });

        $(".modal").on("click", "#vscreen", function () {
            $(".mejs-fullscreen-button").trigger('click');

        });
        $(".modal").on("click", "#vownload", function () {
            var dtfile = $(this).data("file");
            window.location.href = '_actions?f=' + dtfile;
        });


        $('body').on('playing', '.modal', function (yor) {
            yor.preventDefault();
            console.log('3322222ghjkljhgsjakldkfgeioprus');
        });


        $.contextMenu({
            selector: '.context-menu-one',
            trigger: 'right',
            delay: 190,
            autoHide: true,
            callback: function (key, options) {

                var m = "ghost_folder_function: " + key + ' >> triggered by ';
                console.log(m);
            },

            items: {
                "rename": {
                    name: " Rename", icon: "edit", callback: function (key, options) {
                        var rfolder = $("#rclick_folder").val();
                        $("#" + rfolder).addClass("context-menu-rename-folder");

                    }
                },
                "cut": {
                    name: " Cut/Move", icon: "cut", callback: function (key, options) {
                        var rfolder = $("#rclick_folder").val();
                        $("#" + rfolder).addClass("context-menu-cut-folder");

                    }
                },
                "delete": {
                    name: " Delete", icon: "trash", callback: function (key, options) {
                        var rfolder = $("#rclick_folder").val();
                        thisfolder = $("#" + rfolder);
                        $.post('_ajax.php', 'deal_folder=' + rfolder,
                            function (df) {
                                if (df == 's') {
                                    thisfolder.hide("slow");
                                } else {
                                    alert('Error in deleting file');
                                }
                            });

                    }
                },
                "sep1": "---------",

                "Share": {
                    name: " Share", icon: "user", callback: function (key, options) {
                        var frv = $("#rclick_folder").val();
                        $.post('_ajax.php', 'share_folder=' + frv,
                            function (sfol_) {
                                $("#share_folder").html(sfol_);
                                $("#open_share").trigger("click");
                            });

                    }
                },

                "download": {
                    name: " Download", icon: "download", callback: function (key, options) {
                        var rfolder = $("#rclick_folder").val();
                        thisfolder = $("#" + rfolder);
                        window.location.href = '_actions?dld_folder=' + rfolder;


                    }
                }
            }

        });


        $.contextMenu({
            selector: '.context-menu-file',
            trigger: 'right',
            delay: 150,
            autoHide: true,
            callback: function (key, options) {
                var m = "function: " + key;
            },
            items: {
                "View": {
                    name: " View", icon: "eye", callback: function (key, options) {
                        var rfile = $("#rclick_file").val();
                        $.post('_ajax.php', 'view_file=' + rfile,
                            function (vf) {
                                $("#view_file").html(vf);
                                $("#open_view").trigger("click");
                            });

                    }
                },
                "cut": {
                    name: " Cut/Move", icon: "cut", callback: function (key, options) {
                        var rfile = $("#rclick_file").val();
                        $("#" + rfile).addClass("context-menu-cut-folder");

                    }
                },

                "copy": {name: " Copy", icon: "copy"},
                "delete": {
                    name: " Delete", icon: "trash", callback: function (key, options) {
                        var rfile = $("#rclick_file").val();
                        var thisfile = $("#" + rfile);

                        $.post('_ajax.php', 'fycl=' + rfile,
                            function (df) {
                                if (df == 's') {
                                    thisfile.hide("slow");
                                } else {
                                    alert(df);
                                }
                            });

                        /*window.location.href = '_actions?fycl='+rfile;*/


                        /*thisfile.addClass("context-menu-delete-folder");*/


                    }
                },

                "sep1": "---------",
                "download": {
                    name: " Download", icon: "download", callback: function (key, options) {
                        var rfile = $("#rclick_file").val();
                        var thisfile = $("#" + rfile);

                        var dfile = thisfile.data("file");

                        console.log("clicked file is " + dfile);
                        window.location.href = '_actions?f=' + rfile;
                    }
                }
            }
        });


        $.contextMenu({
            selector: '.context-menu-dfile',
            trigger: 'right',
            delay: 150,
            autoHide: true,
            callback: function (key, options) {
                var m = "function: " + key;
            },
            items: {

                "restore": {
                    name: " Restore", icon: "reply", callback: function (key, options) {
                        var rfile = $("#rclick_file").val();
                        var thisfile = $("#" + rfile);
                        $.post('_ajax.php', 'rso=' + rfile,
                            function (df) {
                                if (df == 's') {
                                    thisfile.hide("slow");
                                } else {
                                }
                            });


                    }
                },
                "delete": {
                    name: " Delete", icon: "trash", callback: function (key, options) {
                        var rfile = $("#rclick_file").val();
                        var thisfile = $("#" + rfile);

                        $.post('_ajax.php', 'com_del=' + rfile,
                            function (df) {
                                if (df == 's') {
                                    thisfile.hide("slow");
                                } else {
                                }
                            });

                        /*window.location.href = '_actions?fycl='+rfile;*/

                        /*thisfile.addClass("context-menu-delete-folder");*/
                    }
                },

                "sep1": "---------",
                "download": {
                    name: " Download", icon: "download", callback: function (key, options) {
                        var rfile = $("#rclick_file").val();
                        var thisfile = $("#" + rfile);

                        var dfile = thisfile.data("file");

                        console.log("clicked file is " + dfile);

                        thisfile.addClass("context-menu-download-folder");
                        window.location.href = '_actions?f=' + rfile;

                        thisfile.removeClass("context-menu-download-folder");

                    }
                }
            }
        });


        $.contextMenu({
            selector: '.deleted-folder',
            trigger: 'hover',
            delay: 150,
            autoHide: true,
            callback: function (key, options) {
                var m = "function: " + key;
            },
            items: {
                "restore": {
                    name: " Restore", icon: "reply", callback: function (key, options) {
                        var rfolder = $("#rclick_folder").val();
                        var thisfolder = $("#" + rfolder);
                        $.post('_ajax.php', 'rsofol=' + rfolder,
                            function (df) {
                                if (df == 's') {
                                    thisfolder.hide("slow");
                                } else {
                                }
                            });
                    }
                },
                "delete": {
                    name: " Delete", icon: "trash", callback: function (key, options) {
                        var rfile = $("#rclick_file").val();
                        var thisfile = $("#" + rfile);

                        $.post('_ajax.php', 'com_del=' + rfile,
                            function (df) {
                                if (df == 's') {
                                    thisfile.hide("slow");
                                } else {
                                }
                            });

                    }
                },

                "sep1": "---------",
                "download": {
                    name: " Download", icon: "download", callback: function (key, options) {
                        var rfile = $("#rclick_file").val();
                        var thisfile = $("#" + rfile);

                        var dfile = thisfile.data("file");

                        console.log("clicked file is " + dfile);

                        thisfile.addClass("context-menu-download-folder");
                        window.location.href = '_actions?f=' + rfile;

                        thisfile.removeClass("context-menu-download-folder");

                    }
                }
            }
        });


        $.contextMenu({
            selector: '.context-menu-rbin',
            trigger: 'right',
            delay: 190,
            autoHide: true,
            callback: function (key, options) {

                var m = "ghost_folder_function: " + key + ' >> triggered by ';
                console.log(m);
            },
            items: {
                "restore all": {
                    name: " Restore all", icon: "reply", callback: function (key, options) {
                        var rfolder = $("#rclick_folder").val();
                        var rf = $("#" + rfolder);
                        var jx = 92345;
                        $.post('_ajax.php', 'restore_all=' + jx,
                            function (df) {
                                if (df == 's') {
                                    rf.hide("slow");
                                } else {
                                    alert(df);
                                }
                            });


                    }
                },
                "sep0": "---------",
                "delete all": {
                    name: " Empty Bin", icon: "circle-o", callback: function (key, options) {
                        var rfolder = $("#rclick_folder").val();
                        var rf = $("#" + rfolder);
                        var ax = 91345;
                        $.post('_ajax.php', 'x_all=' + ax,
                            function (df) {
                                if (df == 's') {
                                    rf.hide("slow");
                                } else {
                                    alert(df);
                                }
                            });
                    }
                },
                "sep1": "---------"
            }
        });


        $.contextMenu({
            selector: '#main-container',
            trigger: 'right',
            delay: 190,
            autoHide: true,
            callback: function (key, options) {


            },
            items: {
                "refresh": {
                    name: " Refresh", icon: "refresh", callback: function (key, options) {
                        var talefolder = $("#current_ghost_folder").data("tale");
                        $.post('_ajax.php', 'crfolder_loc=' + talefolder,
                            function (fox) {
                                console.log(fox);
                            });


                    }
                },
                "sep0": "---------",
                "paste": {
                    name: " Paste Here", icon: "paste", callback: function (key, options) {
                        var rfolder = $("#rclick_folder").val();
                        thisfolder = $("#" + rfolder);
                        thisfolder.addClass("context-menu-delete-folder");
                        thisfolder.hide("slow");
                    }
                },
                "sep1": "---------"
            }
        });

        $('body').on('contextmenu', '.context-menu-one', function (fol) {
            fol.preventDefault();
            var folder_id = this.id;
            $("#rclick_folder").val(folder_id);
        });

        $('body').on('contextmenu', '.context-menu-rbin', function (rbin) {
            rbin.preventDefault();
            var bin_id = this.id;
            $("#rclick_folder").val(bin_id);
        });

        $('body').on('contextmenu', '.context-menu-file', function (fil) {
            fil.preventDefault();
            var file_id = this.id;
            $("#rclick_file").val(file_id);
        });

        $('body').on('contextmenu', '.deleted-file', function (dfil) {
            dfil.preventDefault();
            var file_id = this.id;
            $("#rclick_file").val(file_id);
        });

        $('body').on('contextmenu', '.deleted-folder', function (dfol) {
            dfol.preventDefault();
            var foldi = this.id;
            $("#rclick_folder").val(foldi);
        });

        $('body').on('contextmenu', '#main-container', function (dfl) {
            var cop = $('#main-container');
            var pop = $('.context-menu-rbin');
            var talefolder = $("#current_ghost_folder").data("tale");
            var destfolder = talefolder.split('/').pop();
            if (destfolder == 'Recycle Bin') {
                cop.contextMenu(false);
            } else {
                cop.contextMenu(true);
            }

        });

    });
</script>


<script> $(".file_mu").addClass('active');
    $('body').on('click', '#file_upl', function (newfiles) {
        var talefolder = $("#current_ghost_folder").data("tale");
        var destfolder = talefolder.split('/').pop();

        if (destfolder == 'Recycle Bin') {
            var toast = "<span class='red bold font-14'>Not allowed to upload files directly to " + destfolder + " </span>";
            $("#newfileho").hide();
            $("#newfile_btsub").hide();
        } else {
            var toast = "File location: <strong class='font-14'>" + destfolder + "</strong> folder";
            $.post('_ajax.php', 'crfolder_loc=' + talefolder,
                function (fol) {
                    $("#file_target_folder").val(fol);
                    $("#newfileho").show();
                    $("#newfile_btsub").show();
                });
        }
        $("#new_file_location").html(toast);

    });

    $('body').on('click', '#new_folder_here', function (newfolder) {
        var talefolder = $("#current_ghost_folder").data("tale");
        var destfolder = talefolder.split('/').pop();

        if (destfolder == 'Recycle Bin') {
            var toast = "<span class='red bold font-14'>Not allowed to create a folder in " + destfolder + " </span>";
            $("#newfolho").hide();
            $("#newfold_btsub").hide();
        } else {
            var toast = "Creating a new folder in your <strong class='font-14'>" + destfolder + "</strong> folder";
            $.post('_ajax.php', 'crfolder_loc=' + talefolder,
                function (fin) {
                    $("#create_in").val(fin);
                    $("#newfolho").show();
                    $("#newfold_btsub").show();

                });
        }
        $("#new_folder_location").html(toast);
        7
    });


    $('body').on('click', '.modal-open', function (xxx) {


    });


</script>
<?php include '../_footer.php'; ?>

