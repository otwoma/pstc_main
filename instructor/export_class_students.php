<?php
include "_functions.php";
include "_variables.php";
$defa_excel_expo_format='pdf';
require_once dirname(__FILE__) . '/hummus/Classes/PHPExcel.php';

$class_id=mysql_real_escape_string($_REQUEST['clid']);

$expot_cateogo='Student Class List';
$exofile_name=make_class_tag($class_id,'').' '.$expot_cateogo;


$student_admns=get_class_students($class_id);

$doc_subject=$exofile_name;
$doc_description=$exofile_name;
$keywords=$exofile_name;
$doc_category=$expot_cateogo;
$doc_title=$exofile_name;
$last_mod_author=$software_label;
$doc_creator=$software_label.'>'.$school_name;

$objPHPExcel = new PHPExcel();

$objPHPExcel->getProperties()->setCreator($doc_creator)
							 ->setLastModifiedBy($last_mod_author)
							 ->setTitle($doc_title)
							 ->setSubject($doc_subject)
							 ->setDescription($doc_description)
							 ->setKeywords($keywords)
							 ->setCategory($doc_category);

$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getDefaultStyle()->getFont()
    ->setName('Segoe UI Symbol')
    ->setSize(11);

$class_listex_titl= array(
    'font'  => array(
        'bold'  => true,
        'size'  => 11,
    ));

cellColor(1, 'C7EFD9');
$objPHPExcel->getActiveSheet()->mergeCells('A1:D1');
$objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($class_listex_titl);
$objPHPExcel->getActiveSheet()->setCellValue('A1',strtoupper($exofile_name));


$objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->applyFromArray(
    array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,)
);
$objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->applyFromArray(
    array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,)
);


$pos_handler=3;
$title_po=$pos_handler-1;
$ncmm='B'.($title_po);
$ncmmc='C'.($title_po);
$objPHPExcel->getActiveSheet()->setCellValue($ncmm,'Name');
$objPHPExcel->getActiveSheet()->setCellValue($ncmmc,$student_number_term);
//$objPHPExcel->getActiveSheet()->getStyle('A'.$title_po.':D'.$title_po)->applyFromArray($class_listex_titl);
$student_counter=0;
foreach($student_admns as $student_id){
$student_name_cllr=student_name_full($student_id);
$student_counter++;
$student_counter_a=$student_counter.'.';
$objPHPExcel->getActiveSheet()->setCellValue('A'.$pos_handler,$student_counter_a);
$objPHPExcel->getActiveSheet()->setCellValue('B'.$pos_handler,$student_name_cllr);
$objPHPExcel->getActiveSheet()->setCellValue('C'.$pos_handler,$student_id);

$objPHPExcel->getActiveSheet()->getStyle('A'.$pos_handler)->getAlignment()->applyFromArray(
    array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER_CONTINUOUS,)
);

$pos_handler=$pos_handler+1;
}




/*$last_line_exp=$pos_handler+4;
$updatex = 'B'.$last_line_exp;
cellColor($last_line_exp, 'C7EFD9');
$objPHPExcel->getActiveSheet()->setCellValue($updatex,'As at '.date("dS-M-Y H:i"));*/


//
//$objPHPExcel->getActiveSheet()->setCellValue('A8',"Hello\nWorld");
//$objPHPExcel->getActiveSheet()->getRowDimension(8)->setRowHeight(-1);
//$objPHPExcel->getActiveSheet()->getStyle('A8')->getAlignment()->setWrapText(true);

$objPHPExcel->getActiveSheet()->getHeaderFooter()->setEvenFooter($exofile_name);
$objPHPExcel->getActiveSheet()->getHeaderFooter()->setOddFooter('&L&B' . $objPHPExcel->getProperties()->getTitle() . '&RPage &P of &N');
$objPHPExcel->getActiveSheet()->getHeaderFooter()->setEvenFooter('&L&B' . $objPHPExcel->getProperties()->getTitle() . '&RPage &P of &N');

$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(7);

$objPHPExcel->getActiveSheet()
    ->getColumnDimension('B')
    ->setAutoSize(true);
$objPHPExcel->getActiveSheet()
    ->getColumnDimension('C')
    ->setAutoSize(true);

$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(17);



$objPHPExcel->getActiveSheet()->setPrintGridlines(TRUE);




$objPHPExcel->getActiveSheet()->setTitle($exofile_name);


$objPHPExcel->setActiveSheetIndex(0);


$exofile_name=$exofile_name.'.'.$defa_excel_expo_format;

if($defa_excel_expo_format=='xlsx'){
    header('Content-Type: application/vnd.ms-excel');
    header("Content-Disposition: attachment;filename=$exofile_name");
    header('Cache-Control: max-age=0');
    header('Cache-Control: max-age=1');
    header ('Expires: Mon, 18 Jul 2014 05:00:00 GMT');
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
    header ('Cache-Control: cache, must-revalidate');
    header ('Pragma: public');
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

}elseif($defa_excel_expo_format=='xls'){
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header("Content-Disposition: attachment;filename=$exofile_name");
    header('Cache-Control: max-age=0');
    header('Cache-Control: max-age=1');
    header ('Expires: Mon, 18 Jul 2014 05:00:00 GMT');
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
    header ('Cache-Control: cache, must-revalidate');
    header ('Pragma: public');
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
}elseif($defa_excel_expo_format=='pdf'){
    $rendererName = PHPExcel_Settings::PDF_RENDERER_MPDF;
    $rendererLibrary = 'mPDF5.7';
    $rendererLibraryPath = dirname(__FILE__) . '/hummus/Classes/pdf/';

    if (!PHPExcel_Settings::setPdfRenderer(
        $rendererName,
        $rendererLibraryPath
    )) {
        die(
            'NOTICE: Something ain`t right, kindly contact system developers' .
            '<br />' .
            'ERRORCODE: PDF_LIB404'
        );
    }

    header('Content-Type: application/pdf');
    header("Content-Disposition: attachment;filename=".$exofile_name."");
    header('Cache-Control: max-age=0');

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'PDF');

}


$objWriter->save('php://output');
exit;
