<?php include '_header.reg.php';
?>
<div id="main-container" xmlns="http://www.w3.org/1999/html" xmlns="http://www.w3.org/1999/html">
    <script>
        jQuery.easing.jswing = jQuery.easing.swing;
        jQuery.extend(jQuery.easing, {
            def: "easeOutQuad", swing: function (e, f, a, h, g) {
                return jQuery.easing[jQuery.easing.def](e, f, a, h, g)
            }, easeInQuad: function (e, f, a, h, g) {
                return h * (f /= g) * f + a
            }, easeOutQuad: function (e, f, a, h, g) {
                return -h * (f /= g) * (f - 2) + a
            }, easeInOutQuad: function (e, f, a, h, g) {
                if ((f /= g / 2) < 1) {
                    return h / 2 * f * f + a
                }
                return -h / 2 * ((--f) * (f - 2) - 1) + a
            }, easeInCubic: function (e, f, a, h, g) {
                return h * (f /= g) * f * f + a
            }, easeOutCubic: function (e, f, a, h, g) {
                return h * ((f = f / g - 1) * f * f + 1) + a
            }, easeInOutCubic: function (e, f, a, h, g) {
                if ((f /= g / 2) < 1) {
                    return h / 2 * f * f * f + a
                }
                return h / 2 * ((f -= 2) * f * f + 2) + a
            }, easeInQuart: function (e, f, a, h, g) {
                return h * (f /= g) * f * f * f + a
            }, easeOutQuart: function (e, f, a, h, g) {
                return -h * ((f = f / g - 1) * f * f * f - 1) + a
            }, easeInOutQuart: function (e, f, a, h, g) {
                if ((f /= g / 2) < 1) {
                    return h / 2 * f * f * f * f + a
                }
                return -h / 2 * ((f -= 2) * f * f * f - 2) + a
            }, easeInQuint: function (e, f, a, h, g) {
                return h * (f /= g) * f * f * f * f + a
            }, easeOutQuint: function (e, f, a, h, g) {
                return h * ((f = f / g - 1) * f * f * f * f + 1) + a
            }, easeInOutQuint: function (e, f, a, h, g) {
                if ((f /= g / 2) < 1) {
                    return h / 2 * f * f * f * f * f + a
                }
                return h / 2 * ((f -= 2) * f * f * f * f + 2) + a
            }, easeInSine: function (e, f, a, h, g) {
                return -h * Math.cos(f / g * (Math.PI / 2)) + h + a
            }, easeOutSine: function (e, f, a, h, g) {
                return h * Math.sin(f / g * (Math.PI / 2)) + a
            }, easeInOutSine: function (e, f, a, h, g) {
                return -h / 2 * (Math.cos(Math.PI * f / g) - 1) + a
            }, easeInExpo: function (e, f, a, h, g) {
                return (f == 0) ? a : h * Math.pow(2, 10 * (f / g - 1)) + a
            }, easeOutExpo: function (e, f, a, h, g) {
                return (f == g) ? a + h : h * (-Math.pow(2, -10 * f / g) + 1) + a
            }, easeInOutExpo: function (e, f, a, h, g) {
                if (f == 0) {
                    return a
                }
                if (f == g) {
                    return a + h
                }
                if ((f /= g / 2) < 1) {
                    return h / 2 * Math.pow(2, 10 * (f - 1)) + a
                }
                return h / 2 * (-Math.pow(2, -10 * --f) + 2) + a
            }, easeInCirc: function (e, f, a, h, g) {
                return -h * (Math.sqrt(1 - (f /= g) * f) - 1) + a
            }, easeOutCirc: function (e, f, a, h, g) {
                return h * Math.sqrt(1 - (f = f / g - 1) * f) + a
            }, easeInOutCirc: function (e, f, a, h, g) {
                if ((f /= g / 2) < 1) {
                    return -h / 2 * (Math.sqrt(1 - f * f) - 1) + a
                }
                return h / 2 * (Math.sqrt(1 - (f -= 2) * f) + 1) + a
            }, easeInElastic: function (f, h, e, l, k) {
                var i = 1.70158;
                var j = 0;
                var g = l;
                if (h == 0) {
                    return e
                }
                if ((h /= k) == 1) {
                    return e + l
                }
                if (!j) {
                    j = k * 0.3
                }
                if (g < Math.abs(l)) {
                    g = l;
                    var i = j / 4
                } else {
                    var i = j / (2 * Math.PI) * Math.asin(l / g)
                }
                return -(g * Math.pow(2, 10 * (h -= 1)) * Math.sin((h * k - i) * (2 * Math.PI) / j)) + e
            }, easeOutElastic: function (f, h, e, l, k) {
                var i = 1.70158;
                var j = 0;
                var g = l;
                if (h == 0) {
                    return e
                }
                if ((h /= k) == 1) {
                    return e + l
                }
                if (!j) {
                    j = k * 0.3
                }
                if (g < Math.abs(l)) {
                    g = l;
                    var i = j / 4
                } else {
                    var i = j / (2 * Math.PI) * Math.asin(l / g)
                }
                return g * Math.pow(2, -10 * h) * Math.sin((h * k - i) * (2 * Math.PI) / j) + l + e
            }, easeInOutElastic: function (f, h, e, l, k) {
                var i = 1.70158;
                var j = 0;
                var g = l;
                if (h == 0) {
                    return e
                }
                if ((h /= k / 2) == 2) {
                    return e + l
                }
                if (!j) {
                    j = k * (0.3 * 1.5)
                }
                if (g < Math.abs(l)) {
                    g = l;
                    var i = j / 4
                } else {
                    var i = j / (2 * Math.PI) * Math.asin(l / g)
                }
                if (h < 1) {
                    return -0.5 * (g * Math.pow(2, 10 * (h -= 1)) * Math.sin((h * k - i) * (2 * Math.PI) / j)) + e
                }
                return g * Math.pow(2, -10 * (h -= 1)) * Math.sin((h * k - i) * (2 * Math.PI) / j) * 0.5 + l + e
            }, easeInBack: function (e, f, a, i, h, g) {
                if (g == undefined) {
                    g = 1.70158
                }
                return i * (f /= h) * f * ((g + 1) * f - g) + a
            }, easeOutBack: function (e, f, a, i, h, g) {
                if (g == undefined) {
                    g = 1.70158
                }
                return i * ((f = f / h - 1) * f * ((g + 1) * f + g) + 1) + a
            }, easeInOutBack: function (e, f, a, i, h, g) {
                if (g == undefined) {
                    g = 1.70158
                }
                if ((f /= h / 2) < 1) {
                    return i / 2 * (f * f * (((g *= (1.525)) + 1) * f - g)) + a
                }
                return i / 2 * ((f -= 2) * f * (((g *= (1.525)) + 1) * f + g) + 2) + a
            }, easeInBounce: function (e, f, a, h, g) {
                return h - jQuery.easing.easeOutBounce(e, g - f, 0, h, g) + a
            }, easeOutBounce: function (e, f, a, h, g) {
                if ((f /= g) < (1 / 2.75)) {
                    return h * (7.5625 * f * f) + a
                } else {
                    if (f < (2 / 2.75)) {
                        return h * (7.5625 * (f -= (1.5 / 2.75)) * f + 0.75) + a
                    } else {
                        if (f < (2.5 / 2.75)) {
                            return h * (7.5625 * (f -= (2.25 / 2.75)) * f + 0.9375) + a
                        } else {
                            return h * (7.5625 * (f -= (2.625 / 2.75)) * f + 0.984375) + a
                        }
                    }
                }
            }, easeInOutBounce: function (e, f, a, h, g) {
                if (f < g / 2) {
                    return jQuery.easing.easeInBounce(e, f * 2, 0, h, g) * 0.5 + a
                }
                return jQuery.easing.easeOutBounce(e, f * 2 - g, 0, h, g) * 0.5 + h * 0.5 + a
            }
        });
    </script>
    <style>

        /*form styles*/
        #msform {
            width: 100%;
            margin-top:25px;
            text-align: center;
        }
        #msform fieldset {
            background: white;
            border: 0 none;
            text-align: left;

            border-radius: 3px;
            box-shadow: 0 0 15px 1px rgba(0, 0, 0, 0.4);
            padding: 20px 30px;
            box-sizing: border-box;
            width: 90%;
            margin: 0 2%;

            position: relative;
        }
        #msform fieldset:not(:first-of-type) {
            display: none;
        }
        #msform input, #msform textarea {
            padding: 15px;
            border: 1px solid #ccc;
            border-radius: 3px;
            margin-bottom: 10px;
            width: 100%;
            box-sizing: border-box;
            font-family: montserrat;
            color: #2C3E50;
            font-size: 13px;
        }
        #msform .action-button {
            width: 100px;
            background: #10b0e4;
            font-weight: bold;
            color: white;
            border: 0 none;
            border-radius: 1px;
            cursor: pointer;
            padding: 10px 5px;
            margin: 10px 5px;
        }
        #msform .action-button:hover, #msform .action-button:focus {
            box-shadow: 0 0 0 2px white, 0 0 0 3px #10b0e4;
        }
        .fs-title {
            font-size: 15px;
            text-transform: uppercase;
            color: #2C3E50;
            margin-bottom: 10px;
        }
        .fs-subtitle {
            font-weight: normal;
            font-size: 13px;
            color: #666;
            margin-bottom: 20px;
        }
        #progressbar {
            margin-bottom: 30px;
            overflow: hidden;
            counter-reset: step;
            display: none;
        }
        #progressbar li {
            list-style-type: none;
            color: #373743;
            text-transform: uppercase;
            font-size: 12px;
            width: 33.33%;
            float: left;
            position: relative;
        }
        #progressbar li:before {
            content: counter(step);
            counter-increment: step;
            width: 20px;
            line-height: 20px;
            display: block;
            font-size: 10px;
            background: #323447;
            border-radius: 3px;
            color:#fff;
            margin: 0 auto 5px auto;
        }
        #progressbar li:after {
            content: '';
            width: 100%;
            height: 2px;
            background: white;
            position: absolute;
            left: -50%;
            top: 9px;
            z-index: -1;
        }
        #progressbar li:first-child:after {
            content: none;
        }

        #progressbar li.active:before,  #progressbar li.active:after{
            background: #27AE60;
            color: white;
        }
    </style>
    <form id="msform" action="_handle.new_recruit.php" method="POST" enctype="multipart/form-data">

        <input type="hidden" name="action" value="new_recruit">

        <!-- progressbar -->
        <ul id="progressbar">
            <li class="active">Step 1</li>
            <li>Step 2</li>
            <li>Step 3</li>
        </ul>
        <!-- fieldsets -->
        <fieldset>
            <h2 class="fs-title"> Admission Record</h2>
            <h3 class="fs-subtitle">Step 1</h3>



            <div class="row">
                <strong>Salutation</strong>
            <div class="form-group">

                <label class="col-md-4 control-label"></label>
                <?php echo salutation_options()?>
                <br>
            </div>
                <br>
            </div>


            <div class="form-group">
                <label>Surname</label>
                <input spellcheck="false" type="text" name="surname" class="form-control"
                       placeholder="Surname" required>

            </div>

            <div class="form-group">
                <label>First Name</label>
                <input spellcheck="false" type="text" name="first_name" class="form-control"
                       placeholder="First Name" required>

            </div>

            <div class="form-group">
                <label>Other Name</label>
                <input spellcheck="false" type="text" name="other_name" class="form-control"
                       placeholder="Other Name">
            </div>


            <div class="form-group">
                <label><span class="fa fa-calendar"></span> Date of Birth (d/m/y)</label>

                <input type="text" name="dob" id="dob" value="<?php echo date("d/m/Y"); ?>" class="form-control"/>
            </div>

            <div class="form-group">
                <label>Birth Certificate Number</label>
                <input type="text" name="birth_certificate_number" class="form-control"
                       placeholder="Birth Certificate Number">

            </div>

            <div class="form-group">
                <label>National ID</label>
                <input type="number" name="national_id" class="form-control"
                       placeholder="National ID">

            </div>

            <div class="form-group">
                <label>KRA Pin</label>
                <input type="text" name="kra_pin" class="form-control"
                       placeholder="KRA pin">

            </div>


            <div class="form-group">
                <label>Sto. Number.</label>
                <input type="text" name="sto_number" value="<?php echo recruit_id_generator(); ?>" id="admission"
                       class="form-control" style="width:100px;" required>
            </div>


            <div class="form-group">
                <label>Terms</label>
                <select class="form-control" style="margin-left:0px;" name="terms">
                    <?php echo recruit_terms() ?>
                </select>
            </div>




            <div class="form-group">
                <label>Highest Level of Education</label>
                <select class="form-control" style="margin-left:0px;" name="level_of_education">
                    <?php echo education_level() ?>
                </select>
            </div>


            <div class="form-group">
                <label>Highest Professional Level</label>
                <input type="text" class="form-control" style="margin-left:0px;" name="professional_level"/>
            </div>


            <div class="form-group">
                <label><span class="fa fa-calendar"></span> Date of Admission (d/m/y)</label>

                <input type="text" name="doa" id="doa" value="<?php echo date("d/m/Y"); ?>" class="form-control"/>
            </div>

            <div class="form-group">
                <div class="form-group">
                    <label>Gender</label>

                    <select class="form-control" style="margin-left:0px;" name="gender" id="gender">
                        <?php echo gender_options() ?>
                    </select>
                </div>

            </div>


            <input type="button" name="next" class="next action-button" id="step_1" value="Next" />


        </fieldset>
        <fieldset>
            <h2 class="fs-title">Physical Description</h2>
            <h3 class="fs-subtitle">Physical details of recruit</h3>


            <div class="form-group">

                <label>Color of the Eye</label>
                <input type="text" name="eye_color" class="form-control"
                       placeholder="Eye Color">

            </div>


            <div class="form-group">
                <label>Skin Color</label>
                <input type="text" name="skin_color" class="form-control"
                       placeholder="Skin Color">

            </div>


            <div class="form-group">
                <label>Height (Feet)</label>
                <input type="text" name="height_ft" class="form-control"
                       placeholder="">

            </div>



            <div class="form-group">
                <label>Height (Inches)</label>
                <input type="text" name="height_inches" class="form-control"
                       placeholder="">

            </div>


            <div class="form-group">
                <label>Physical Figure</label>
                <input type="text" name="physical" class="form-control"
                       placeholder="Physical Figure">

            </div>


            <div class="form-group">
                <label>Identification Marks</label>

                <textarea name="identification_marks" id="identification_marks" cols="30" rows="10"></textarea>
            </div>



            <div class="form-group">
                <label>Religion</label>
                <select class="form-control" style="margin-left:0px;" name="religion">
                    <?php echo religion_options() ?>
                </select>
            </div>



            <div class="form-group">
                <label>Place of Birth</label>
                <input name="pob" id="pob" type="text">
            </div>

         
        
            <div class="form-group">
                <label>Tribe Options</label>
                <select class="form-control" style="margin-left:0px;" name="tribe_id">
                    <?php echo tribe_options() ?>
                </select>
            </div>


            <div class="form-group">
                <label>Marital Status?</label>
                <select class="form-control" style="margin-left:0px;" name="marital_status" id="marital_status">
                    <option selected value="1">Single</option>
                    <option value="2">Married</option>
                </select>
            </div>

            <div class="form-group">
                <label>Name of Spouse</label>
                <input class="spouse_detail" name="spouse_name" id="spouse_name" type="text">
            </div>



            <div class="form-group">
                <label>Telephone 1</label>
                <input class="spouse_detail" name="tel_1" id="tel_1" type="text">
            </div>


            <div class="form-group">
                <label>Telephone 2</label>
                <input class="spouse_detail" name="tel_2" id="tel_2" type="text">
            </div>


            <div class="form-group">
                <label>Male Children</label>
                <input class="male_children" name="male_children" id="male_children" type="number" value="0">
            </div>


            <div class="form-group">
                <label>Female Children</label>
                <input class="female_children" name="female_children" id="female_children" type="number" value="0">
            </div>





            <div class="form-group">
                <label>County</label>
                <select class="form-control" style="margin-left:0px;" name="county">
                    <?php echo county_options() ?>
                </select>
            </div>




            <div class="form-group">
                <label>Sub-County</label>
                <select class="form-control" style="margin-left:0px;" name="sub_county">
                    <?php echo sub_county_options() ?>
                </select>
            </div>




            <div class="form-group">
                <label>Ward Options</label>
                <select class="form-control" style="margin-left:0px;" name="ward">
                    <?php echo ward_options() ?>
                </select>
            </div>



            <div class="form-group">
                <label>Village</label>
                <input class="village" name="village" id="village" type="text" placeholder="Village">
            </div>



            <div class="form-group">
                <label>Chief</label>
                <input class="chief" name="chief" id="chief" type="text" placeholder="Chief">
            </div>





            <div class="form-group">
                <label>Assistant-Chief</label>
                <input class="assistant_chief" name="assistant_chief" id="assistant_chief" type="text" placeholder="Assistant-Chief">
            </div>




          

            <input type="button" name="previous" class="previous action-button" value="Previous" />
            <input type="button" name="next" class="next action-button" value="Next" />
        </fieldset>



        <fieldset>
            <h2 class="fs-title">Next of Kin Details</h2>


            <div class="form-group">
                <label>Next of Kins Name</label>
                <input spellcheck="false" type="text" name="next_of_kin_name" class="form-control"
                       placeholder="Next of Kin's Full Name">
            </div>


            <div class="form-group">
                <label>Next of Kin Telephone No.</label>
                <input type="text" name="kin_phone" class="form-control"
                       placeholder="Next of Kin's Phone Number">
            </div>

            <div class="form-group">
                <label>Relationship</label>
                <input type="text" name="relationship" class="form-control"
                       placeholder="Next of Kin's Relationship">
            </div>


            <div class="form-group">
                <label>Address</label>
                <input type="text" spellcheck="false" name="address" class="form-control"
                       placeholder="Address"/>
            </div>

            <div class="form-group hide">
                <h4>Student Photo</h4>
                <hr style="border-top: dotted 1px; margin-top: -7px"/>
                <label>Upload Photo</label>
                <input type="file" name="photo" id="file" class="form-control"/>
            </div>

			
			  <div class="form-group" id="extra_details">
                <label>Other useful information (optional)</label>
                <br>
                    <textarea title="Other useful information" name="extra_details" id="extra_details_input" cols="90"
                              rows="7"></textarea>

            </div>

			
            <input type="button" name="previous" class="previous action-button" value="Previous" />
            <button type="submit" name="submit" class="submit action-button btn-success"> <i class="fa fa-send"></i> Submit</button>

        </fieldset>
    </form>























</div>

<script>$(".smart_select").select2();</script>

<script> $(".new-recruit_menu").addClass('active');



    $(document).on("click", "#step_1", function () {


        var kra_pin = $("input[name=kra_pin]").val();

        var kra_pin_len = kra_pin.length;



 var national_id = $("input[name=national_id]").val();

        var national_id_len = national_id.length;


        if(national_id_len<8){

            alert('Incorrect National ID');
        }






    });



    $(document).on("change", ".salutation_option", function () {
        var id = $(this).attr('id');
        var check = $(this).prop('checked');

        if((id=='mr' || id=='sir' || id=='mrs' || id=='ms') && check==true){
            $("#mr").prop('checked',false);
            $("#sir").prop('checked',false);
            $("#mrs").prop('checked',false);
            $("#ms").prop('checked',false);
            $("#"+id).prop('checked',true);
        }

    });


    function loadHousingOptions(){
        var gender = $("#gender").val();

        $.post('_ajax', {gender:gender,housing_options:'get'},
            function (options) {
                $("#housing").html(options);
            });

    }

    loadHousingOptions();

    $(document).on("change", "#gender", function () {
        loadHousingOptions();

    });

    $(document).on("change", "#phy_imp", function () {
        var option_value = $(this).val();

        if (option_value == 'yes') {
            $("#phy_imp_explain_div").show();
        } else {
            $("#phy_imp_explain_div").hide();
        }

    });


    var gur_1 = $("#phy_imp");

    if (gur_1 == 'yes') {
        $("#phy_imp_explain_div").show();
    } else {
        $("#phy_imp_explain_div").hide();
    }


    $(document).on("change", "#guardian_details", function () {
        var option_value = $(this).val();

        if (option_value == 'yes') {
            $("#guardian_details_div").show();
        } else {
            $("#guardian_details_div").hide();
        }

    });

    var gur_ = $("#guardian_details");

    if (gur_ == 'yes') {
        $("#guardian_details_div").show();
    } else {
        $("#guardian_details_div").hide();
    }





    var current_fs, next_fs, previous_fs;
    var left, opacity, scale;
    var animating;

    $(".next").click(function(){
        if(animating) return false;
        animating = true;

        current_fs = $(this).parent();
        next_fs = $(this).parent().next();

        $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

        next_fs.show();
        current_fs.animate({opacity: 0}, {
            step: function(now, mx) {
                scale = 1 - (1 - now) * 0.2;
                left = (now * 50)+"%";
                opacity = 1 - now;
                current_fs.css({
                    'transform': 'scale('+scale+')',
                    'position': 'absolute'
                });
                next_fs.css({'left': left, 'opacity': opacity});
            },
            duration: 800,
            complete: function(){
                current_fs.hide();
                animating = false;
            },
            easing: 'easeInOutBack'
        });
    });

    $(".previous").click(function(){
        if(animating) return false;
        animating = true;

        current_fs = $(this).parent();
        previous_fs = $(this).parent().prev();

        $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

        previous_fs.show();
        current_fs.animate({opacity: 0}, {
            step: function(now, mx) {
                scale = 0.8 + (1 - now) * 0.2;
                left = ((1-now) * 50)+"%";
                opacity = 1 - now;
                current_fs.css({'left': left});
                previous_fs.css({'transform': 'scale('+scale+')', 'opacity': opacity});
            },
            duration: 800,
            complete: function(){
                current_fs.hide();
                animating = false;
            },
            easing: 'easeInOutBack'
        });

    });





</script>
<div class="row">&nbsp;&nbsp;&nbsp;</div>
<div class="row">&nbsp;&nbsp;&nbsp;</div>
<div class="row">&nbsp;&nbsp;&nbsp;</div>
<div class="row">&nbsp;&nbsp;&nbsp;</div>
<?php include '../_footer.php'; ?>
