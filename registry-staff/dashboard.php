<?php
include "_header.reg-staff.php";
?>

    <div id="main-container">


        <div class="main-header clearfix">
            <div class="page-title">
                <h3 class="no-margin">Dashboard</h3>
            </div>

        </div>
        <div class="padding-md">


            <div class="row">
                <div class="col-sm-6 col-md-4">
                    <div class="panel-stat3 bg-green">

                        <h1 class="m-top-none" id="student_count"> <?php echo recruit_population();?> </h1>
                        <h5 class="bold">Recruit Population</h5>

                        <div class="stat-icon">
                            <i class="fa fa-user-circle fa-3x"></i>
                        </div>

                    </div>
                </div>
                <div class="col-sm-6 col-md-4">
                    <div class="panel-stat3 bg-blue">

                        <h1 class="m-top-none" id="staff_count"> <?php echo instructor_population();?></h1>
                        <h5 class="bold">Staff Population</h5>

                        <div class="stat-icon">
                            <i class="fa fa-briefcase fa-3x"></i>
                        </div>

                    </div>
                </div>
                <div class="col-sm-6 col-md-4">
                    <div class="panel-stat3 bg-orange">

                        <h1 class="m-top-none" id="class_count">33 </h1>
                        <h5 class="bold">Class Count</h5>

                        <div class="stat-icon">
                            <i class="fa fa-building-o fa-3x"></i>
                        </div>

                    </div>
                </div>


            </div>
        </div>


        <div class="shortcut-wrapper grey-container">
            <span class="font-16 padding-sm margin-sm grey">Quick Links <span
                    class="fa fa-angle-double-right"></span></span>


            <a href="see_class_schedule" class="shortcut-link">
					<span class="shortcut-icon">
						<i class="fa fa-calendar-o"></i>
					</span>
                <span class="text">Class Schedules</span>
            </a>
            <a href="see_staff_schedule" class="shortcut-link">
					<span class="shortcut-icon">
						<i class="fa fa-calendar-o"></i>
					</span>
                <span class="text">Staff Schedules</span>
            </a>


            <a href="account" class="shortcut-link">
					<span class="shortcut-icon">
						<i class="fa fa-cogs"></i></span>
                <span class="text">Account Settings</span>
            </a>
        </div>

        <div class="padding-md">

            <div class="row">

                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading clearfix">
										<span class="pull-left bold">
											<span class="text-success m-left-xs"><i class="fa fa-check"></i></span> To Do List
										</span>
                            <ul class="tool-bar">
                                <li><a href="#do_listModal" role="button" data-toggle="modal"
                                       class="bg-success btn-small"><i class="fa fa-plus"></i> New Task</a>
                                </li>
                                <li><a href="#toDoListWidget" data-toggle="collapse"><i class="fa fa-arrow-up"></i><i
                                            class="fa fa-arrow-down"></i></a></li>
                            </ul>
                        </div>
                        <div class="panel-body no-padding collapse in" id="toDoListWidget">
                            <ul class="list-group task-list no-margin collapse in">

                                <span id="new_tasks_holder"></span>



                            </ul>
                        </div>
                        <div class="loading-overlay">
                            <i class="loading-icon fa fa-refresh fa-spin fa-lg"></i>
                        </div>
                    </div>
                </div>


                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading panel-heading3 clearfix">
        <span class="pull-left bold">
           <i class="fa fa-graduation-cap"></i> Access Recruit Profile
        </span>

                        </div>
                        <div class="panel-body no-padding collapse in">
                            <ul class="list-group task-list no-margin collapse in">
                                <form action="_actions" method="get">
                                    <div class="form-group margin-sm">
                                        <label>Recruit ID:</label>
                                        <input type="text" name="quick_ac_std_pro" class="form-control"
                                               placeholder="Recruit Sto. Number." required></br>
                                        <button type="submit" class="butn butn-navy pull-right margin-sm"><span
                                                class="fa fa-angle-double-right"></span> See Profile
                                        </button>
                                    </div>
                                </form>
                            </ul>
                        </div>
                        <div class="loading-overlay">
                            <i class="loading-icon fa fa-refresh fa-spin fa-lg"></i>
                        </div>
                    </div>

                </div>


                <div class="col-lg-4">
                    <div class="panel panel-default">

                        <div class="loading-overlay">
                            <i class="loading-icon fa fa-refresh fa-spin fa-lg"></i>
                        </div>
                    </div>


                </div>


            </div>


        </div>

    </div>


    <div class="modal fade" id="do_listModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button id="close_modal" type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;</button>
                    <h4>New item</h4>
                </div>

                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <label>Task :</label>
                            <textarea spellcheck="false" required class="form-control input-sm sms_con" id="todo_item"
                                      name="list_item" style="height: 70px;"></textarea>
                        </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary btn-sm" data-dismiss="modal" aria-hidden="true">Close</button>
                    <button id="todo_add" type="button" class="btn butn-green btn-sm">Submit</button>
                </div>
            </div>
        </div>
        </form>
    </div>

    <script> $(".dashboard_mu").addClass('active');
        setInterval(function () {
            $.post('../_ajax', 'online_users=0',
                function (live_users) {
                    $("#holder_users_online").html(live_users);
                });
        }, 1000);
    </script>
<?php include '../_footer.php'; ?>