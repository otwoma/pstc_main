<?php include '_header.reg.php';

$recruit_id = sani($_REQUEST['td']);

if(intval($recruit_id)==0){
    header('dashboard.php');
}else{

    $recruit = m("SELECT * FROM recruits WHERE recruit_id = $recruit_id");
    $recruit_data = msoc($recruit);

    $sto_number = $recruit_data['sto_number'];
    $full_name = $recruit_data['first_name'].' '.$recruit_data['other_name'].' '.$recruit_data['surname'];
}


?>
    <style>
        .bg-pr-green{background-color:#1E6C42;}
        .pr-green,.green2{color:#1E6C42;}
    </style>

    <div id="main-container">


        <ul class="tab-bar grey-tab">
            <li class="protab">
                <a href="#overview" data-toggle="tab">
						<span class="block text-center">
							<i class="fa fa-user-circle fa-2x"></i>
						</span>
                    Overview
                </a>
            </li>

            <li>
                <a class="protab" href="#physical_description" data-toggle="tab">
						<span class="block text-center">
							<i class="fa fa-genderless fa-2x"></i>
						</span>
                  Physical Description
                </a>
            </li>


            <li>
                <a class="protab" href="#LocationDetails" data-toggle="tab">
						<span class="block text-center">
							<i class="fa fa-users fa-2x"></i>
						</span>
                    Family Info
                </a>
            </li>

            <li>
                <a class="protab" href="#Particulars" data-toggle="tab">
						<span class="block text-center">
							<i class="fa fa-check-square-o fa-2x"></i>
						</span>
                   Checklist
                </a>
            </li>

            <li class="hide">
                <a href="#academic" id="acadgraph" data-toggle="tab">
						<span class="block text-center">
							<i class="fa fa-area-chart fa-2x"></i>
						</span>
                    Transcript
                </a>
            </li>

            <li class="hide">
                <a class="protab" href="#ranking" id="acadgraph" data-toggle="tab">
						<span class="block text-center">
							<i class="fa fa-archive fa-2x"></i>
						</span>
                    Report Card Archive
                </a>
            </li>

        </ul>

        <div class="padding-md">


            <div class="row">
                <div class="col-md-3 col-sm-3">
                    <div class="row">
                        <div class="col-xs-9 col-sm-12 col-md-9">

                            <h4 class="headline bold font-16 bg-primary padding-sm br-2 white bg-pr-green">
                              <?php echo $full_name;?>
                                <span class="line" style="background-color:#fff;"></span>
                            </h4>


                            <img src="<?php //$images=$avad.student_avatar($td);
                            echo $avad . 'user.png';
                            ?>" alt="Student Photo" class="profile_photo">

                            <div class="seperator"></div>
                        </div>

                    </div>

                    <div class="row bold font-16 padding-md"> <?php echo 'Sto. Number: <span class="green2">' . $sto_number; ?></span>

                        <br>
                        <br>


                        <?php

                        $platoon_id = $recruit_data['platoon_id'];

                        $company_id = platoon_company_id($platoon_id);
                        $company_name = company_name($company_id);
                        ?>

                        <p class="font-14"> <?php echo 'Company: <span class="green2  ">' . $company_name; ?></span></p>
                        <p class="font-14"> <?php echo 'Platoon: <span class="green2  ">' . platoon_name($platoon_id); ?></span></p>

                        <p class="font-14"> <?php echo 'Barrack: <span class="green2  ">' . ''; ?></span></p>

                        <br>

                    </div>


                </div>


                <div class="col-md-9 col-sm-9">
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="overview">
                            <div class="row">
                                <div class="col-md-6">


                                    <div class="panel panel-default fadeInDown animation-delay2">
                                        <div class="padding-xs bg-pr-green white">
                                            <span class="padding-xs font-14"><span class="fa fa-user-circle"></span> Brief Info</span>
                                        </div>
                                        <div class="panel-body font-14 border-1">
                                            <ul class="list-group">
                                                <li class="list-group-item bg-light">

                                                    <p><strong class="font-normal">Gender: </strong><span
                                                            class="font-normal green2"><?php echo read_gender($recruit_data['gender']); ?></span>
                                                    </p>

                                                    <p>DOB: <span
                                                            class="green2"><?php $dob =$recruit_data['dob']; echo $recruit_data['dob'] . ' <small>(' . age($dob) . ' years old)</small>'; ?> </span>
                                                    </p>



                                                    <p>Tribe: <span
                                                            class="green2"><?php echo read_tribe($recruit_data['tribe_id']); ?></span>
                                                    </p>

                                                    <p>Religion: <span
                                                            class="green2"><?php $religion_id = $recruit_data['religion_id'];echo read_religion($religion_id); ?></span>
                                                    </p>


                                                    <br>

                                                    <p> National ID: <span
                                                            class="green2"><?php echo $recruit_data['national_id'];?></span>
                                                    </p>

                                                    <p>KRA Pin: <span
                                                            class="green2"><?php echo $recruit_data['kra_pin']; ?></span>
                                                    </p>
                                                    <p>Birth Certificate Number: <span
                                                            class="green2"><?php echo $recruit_data['birth_certificate_number']; ?></span>
                                                    </p>




                                                         </br>


                                                    <p>Date of Appointment: <span
                                                            class="green2"><?php $doa= $recruit_data['doa']; echo $doa; ?></span></p>


                                                    </br>





                                                    <p>Service Number: <span
                                                            class="green2"><?php $recruit_data['service_number'];?></span></p>


                                                    <p>Personal Number: <span
                                                            class="green2"><?php $recruit_data['service_number'];?></span></p>




                                                </li>
                                            </ul>
                                        </div>
                                    </div>


                                </div>


                                <div class="col-md-6">


                                    <div class="panel panel-default fadeInDown animation-delay2">
                                        <div class="padding-xs bg-pr-green white">
                                            <span class="padding-xs font-14"><span class="fa fa-graduation-cap"></span> Education Background</span>
                                        </div>
                                        <div class="panel-body font-14 border-1">
                                            <ul class="list-group">
                                                <li class="list-group-item bg-light">

                                                    <p><strong class="font-normal">Highest Level of Education: </strong><span
                                                            class="font-normal green2"><?php echo education_level($recruit_data['education_level']); ?></span>
                                                    </p>



                                                    <p><strong class="font-normal">Professional Level: </strong><span
                                                            class="font-normal green2"><?php echo ($recruit_data['professional_level']); ?></span>
                                                    </p>



                                                </li>
                                            </ul>
                                        </div>
                                    </div>



                                    <div class="panel panel-default fadeInDown animation-delay2">
                                        <div class="padding-xs bg-pr-green white">
                                            <span class="padding-xs font-14"><span class="fa fa-map-marker"></span> Location Details</span>
                                        </div>
                                        <div class="panel-body font-14 border-1">
                                            <ul class="list-group">
                                                <li class="list-group-item bg-light">

                                                    <p><strong class="font-normal">County: </strong><span
                                                            class="font-normal green2"><?php echo county_label($recruit_data['county_id']); ?></span>
                                                    </p>



                                                    <p><strong class="font-normal">Sub-County: </strong><span
                                                            class="font-normal green2"><?php echo sub_county_label($recruit_data['sub_county']); ?></span>
                                                    </p>



                                                    <p><strong class="font-normal">Ward: </strong><span
                                                            class="font-normal green2"><?php echo ward_label($recruit_data['ward_id']); ?></span>
                                                    </p>



                                                    <p><strong class="font-normal">Village: </strong><span
                                                            class="font-normal green2"><?php echo $recruit_data['village']; ?></span>
                                                    </p>


                                                    <br>




                                                    <p>Place of Birth: <span
                                                            class="green2"><?php echo $recruit_data['place_of_birth']; ?></span>
                                                    </p>


                                                    <br>



                                                    <p>Name of Chief: <span
                                                            class="green2"><?php echo $recruit_data['chief']; ?></span>
                                                    </p>

                                                    <p>Name of Assistant Chief: <span
                                                            class="green2"><?php echo $recruit_data['assistant_chief']; ?></span>
                                                    </p>


                                                </li>
                                            </ul>
                                        </div>
                                    </div>







                                </div>



                            </div>
                        </div>




                        <div class="tab-pane fade" id="Particulars">



                            <div class="row">
                                <div class="col-md-12">

                                    <div class="panel panel-default fadeInDown animation-delay2">
                                        <div class="padding-xs bg-pr-green white">
                                            <span class="padding-xs font-14"><span class="fa fa-check-square-o"></span> Recruit Checklist</span>
                                        </div>
                                        <div class="panel-body font-16 border-1">


                                            <table>

                                                <tr>
                                                    <td>
                                                        <div class="form-group">
                                                            <div class="col-lg-12 margin-sm">
                                                                <label class="label-checkbox">
                                                                    <input class="nn" name="sb[]" value="<?php echo $subjct_id;?>" type="checkbox">
                                                                    <span class="custom-checkbox"></span>
                                                                    Certificate Verification
                                                                </label>
                                                            </div>

                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>
                                                        <div class="form-group">
                                                            <div class="col-lg-10 margin-sm">
                                                                <label class="label-checkbox">
                                                                    <input class="nn" name="sb[]" value="<?php echo $subjct_id;?>" type="checkbox">
                                                                    <span class="custom-checkbox"></span>
                                                                    Fingerprints
                                                                </label>
                                                            </div>

                                                        </div>
                                                        </td>
                                                </tr>

                                                <tr>
                                                    <td>
                                                        <div class="form-group">
                                                            <div class="col-lg-10 margin-sm">
                                                                <label class="label-checkbox">
                                                                    <input class="nn" name="sb[]" value="<?php echo $subjct_id;?>" type="checkbox">
                                                                    <span class="custom-checkbox"></span>
                                                                    Medical
                                                                </label>
                                                            </div>

                                                        </div>
                                                        </td>
                                                </tr>



                                            </table>



                                            <br>
                                                <button class="btn btn-primary"> <i class="fa fa-send"></i>  Update</button>



                                        </div>
                                    </div>



                                </div>





                            </div>



                        </div>
















                        <div class="tab-pane fade" id="LocationDetails">



                            <div class="row">
                                <div class="col-md-6">

                                    <div class="panel panel-default fadeInDown animation-delay2">
                                        <div class="padding-xs bg-pr-green white">
                                            <span class="padding-xs font-14"><span class="fa fa-users"></span> Family</span>
                                        </div>
                                        <div class="panel-body font-14 border-1">
                                            <ul class="list-group">
                                                <li class="list-group-item bg-light">

                                                    <p><strong class="font-normal">	Marital Status: </strong><span
                                                            class="font-normal green2"><?php $marital_status_id = $recruit_data['marital_status']; echo read_marital_status($marital_status_id); ?></span>
                                                    </p>


                                                    <p>Spouse Name: <span
                                                            class="green2"><?php echo ($recruit_data['spouse_name']); ?></span>
                                                    </p>


                                                    <br>

                                                    <p>Phone Number 1: <span
                                                            class="green2"><?php echo ($recruit_data['phone_1']); ?></span>
                                                    </p>




                                                    <p>Phone Number 2: <span
                                                            class="green2"><?php echo ($recruit_data['phone_2']); ?></span>
                                                    </p>

                                                    <br>
                                                    <p>Male Children: <span
                                                            class="green2"><?php echo ($recruit_data['male_children']); ?></span>
                                                    </p>



                                                    <p>Female Children: <span
                                                            class="green2"><?php echo ($recruit_data['female_children']); ?></span>
                                                    </p>







                                                </li>
                                            </ul>
                                        </div>
                                    </div>



                                </div>


                                <div class="col-md-6">
                                    <div class="panel panel-default fadeInDown animation-delay3">
                                        <div class="padding-xs bg-pr-green white">
                                            <span class="padding-xs font-14 white font-reset bold"><span
                                                    class="fa fa-user"></span> Next of Kin Info</span>
                                        </div>
                                        <div class="panel-body font-14 border-1">
                                            <ul class="list-group">
                                                <li class="list-group-item bg-light">


                                                    <p><strong class="font-normal">Name: </strong><span
                                                            class="font-normal green2 bold"><?php echo $recruit_data['kin'];?></span></p>

                                                    <p>Relationship: <span
                                                            class="green2"><?php echo $recruit_data['relationship'];?></span></p>


                                                    <p>Address: <span
                                                            class="green2"><?php echo $recruit_data['address'];?></span></p>


                                                    <p>Kin Phone: <span
                                                            class="green2"><?php echo $recruit_data['kin_phone'];?></span></p>



                                                </li>
                                            </ul>
                                        </div>
                                    </div>


                                </div>



                            </div>



                        </div>


                    <style>

                    #physical_description p{
                    font-size: 15px;
                    }
</style>

                        <div class="tab-pane fade" id="physical_description">


                            <div class="panel panel-default fadeInDown animation-delay3">
                                <div class="padding-xs bg-pr-green white">
                                            <span class="padding-xs font-14 white font-reset bold"><span
                                                    class="fa fa-user"></span> Physical Details</span>
                                </div>
                                <div class="panel-body font-14 border-1">
                                    <ul class="list-group">
                                        <li class="list-group-item bg-light">


                                            <p>Physical Figure: <span
                                                    class="green2"><?php echo $recruit_data['physical_figure'];?></span></p>


                                            <p>Height: <span
                                                    class="green2"><?php echo $recruit_data['height_ft'];?> Ft <?php if($recruit_data['height_inches']==''){ }else{ echo $recruit_data['height_inches'].' Inches';};?> </span></p>

                                            <p>Identification Marks: <span
                                                    class="green2"><?php echo $recruit_data['identification_marks'];?></span></p>

                                            <p>Eye Color: <span
                                                    class="green2"><?php echo $recruit_data['eye_color'];?></span></p>



                                            <?php

                                            $additional_details = $recruit_data['extra_details'];
                                            if(strlen($additional_details)>3){
                                            ?>

                                                <p>Extra Details <span
                                                        class="green2"><?php echo $recruit_data['extra_details'];?></span></p>


                                            <?php }?>



                                        </li>
                                    </ul>
                                </div>
                            </div>















                        </div>







                        <div class="tab-pane fade" id="ranking">

                            <div class="panel panel-default">
                                <form id="gradeFormsp" class="form-horizontal form-border">
                                    <div class="panel-heading bold">
                                        Report Card Archives
                                    </div>
                                    <div class="panel-body"></div>
                                    <div class="panel-footer">
                                        <div class="text-right">
                                        </div>
                                    </div>
                                    <div id="grade_conh">


                                    </div>

                                </form>
                            </div>

                        </div>


                        <div class="tab-panel" id="academic">
                            <div class="panel panel-default">
                                <form class="form-horizontal form-border">


                                </form>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>


    </div>


    <script> $(".students_mu").addClass('active');

        $('body').on('click', '#showsing', function () {


            /*  $.ajax({type:'POST', url: '_ajax.php', data:$('#sn_stgr').serialize(), success: function(response) {
             alert(response);
             }});

             */
            var stid = $("#std_plain").val();
            var sgterm = $("#sing_std_gterm").val();
            var sgyear = $("#yearop_drop_stf").val();
            var exponame = stid + sgterm + sgyear;
            var classof
            $.post('_ajax', {fun: 'table_grade', student_id: stid, term: sgterm, year: sgyear}, function (npou) {
                $("#grade_conh").html(npou);
                /* window.location.href = '_actions?download='+npou+'&label='+exponame;*/

            });

        });

        var gplot = $("#magix");
        gplot.hide();
        $('body').on('click', '#acadgraph', function () {
            animate();
            gplot.show();
        })
        $('body').on('click', '.protab', function () {
            gplot.hide();
        })
    </script>

    <div class="modal fade" id="sendSMS">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4>New SMS to the student`s <span id="relat"></span></h4>
                </div>
                <div class="modal-body">
                    <form action="_handle_sms" method="POST">
                        <input value="direct" name="action" type="hidden" class="hidden">

                        <div class="form-group">
                            <label>Send to :</label>
                            <input id="phone_x" class="form-control" name="phone_num" type="text">
                        </div>

                        <div class="form-group">
                            <label>Message :</label>
                            <textarea required class="form-control input-sm sms_con" name="the_sms"
                                      style="height: 120px;"></textarea>
                            <span class="pull-right margin-sm bold font-14" id="sms_counter"></span>
                        </div>
                        <span>Sending SMS to <span class="bold" id="name_rec"></span> </span>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="butn butn-blue btn-sm"><i class="fa fa-send"></i> Send</button>
                    <button class="btn  btn-sm" data-dismiss="modal" aria-hidden="true"> Close</button>
                </div>

                </form>
            </div>
        </div>
    </div>


    <div class="modal fade" id="composeDIMS">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4>New Message</h4>
                </div>
                <div class="modal-body">
                    <form action="_handle_ims" method="POST">

                        <div class="form-group hidden">
                            <input hidden="hidden" value="G" required class="form-control" name="message_target"/>
                            <input hidden="hidden" value="<?php echo $parent_email; ?>" required class="form-control"
                                   name="gamail"/>
                            <input hidden="hidden" value="<?php echo $phon1; ?>" required class="form-control"
                                   name="gauser"/>
                            <input hidden="hidden" value="<?php echo $ppid; ?>" required class="form-control"
                                   name="tdp"/>
                        </div>

                        <div class="form-group">
                            <label>To: </label>
                            <input disabled value="<?php echo $guname . ' (' . $sname_posss . ')'; ?>" type="text"
                                   name="message_title" class="form-control" required>

                        </div>

                        <div class="form-group">
                            <label>Message Title :</label>
                            <input placeholder="eg. PTA Meeting" type="text" name="message_title" class="form-control"
                                   required/>

                        </div>

                        <div class="form-group">
                            <label>Message :</label>
                            <textarea required class="form-control input-sm" name="the_message"
                                      style="height: 120px;"></textarea>
                        </div>


                        <span>Message will be forwarded to <?php echo $parent_email; ?> </span>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-success btn-sm" data-dismiss="modal" aria-hidden="true">Close</button>
                    <button type="submit" class="btn btn-danger btn-sm">Submit</button>
                </div>
            </div>
        </div>
        </form>
    </div>


    <script>


        $(document).on("click", ".sms_home", function () {
            var number = $(this).data('phone');
            var name = $(this).data('name');
            var relation = $(this).data('rel');

            $("#phone_x").val(number);
            $("#name_rec").html(name);
            $("#relat").html(relation);


        });

    </script>
<?php include '../_footer.php'; ?>