<a href="#" id="scroll-to-top" class="hidden-print"><i class="fa fa-chevron-up"></i></a>

        <div align="center" class="col-sm-12 hidden-print">
					<span class="footer-brand">
						<strong class="text-danger">Prison Staff Training College System</strong>
					</span>
            <p class="no-margin">
                &copy; <?php echo date("Y");?>  All Rights Reserved. <strong>Entourage Business Solution</strong>.
            </p>

    </div>


<div class="custom-popup width-100 bg-grey hidden-print" id="logoutConfirm">
    <div class="padding-md">
        <h4 class="m-top-none"> Do you want to logout?</h4>
    </div>

    <div class="text-center hidden-print">
        <a class="btn btn-success m-right-sm" href="../logout.php">Logout</a>
        <a class="btn btn-danger logoutConfirm_close">Cancel</a>
    </div>
</div>
	<script src="../assets/js/jquery-1.10.2.min.js"></script>
    <script src="../assets/js/bootstrap.js"></script>
	<script src='../assets/js/modernizr.min.js'></script>
	<script src='../assets/js/pace.min.js'></script>
	<script src='../assets/js/jquery.popupoverlay.min.js'></script>
	<script src='../assets/js/jquery.slimscroll.min.js'></script>
	<script src='../assets/js/jquery.cookie.min.js'></script>
	<script src="../assets/js/matrix/main_matrix.js"></script>
<script>$("#blstudent_db_id").select2();

   /* $(document).on("keydown", "body", function (e) {

        if (e.altKey) {
            alert("shift+clickAlt");
        }
    });
*/
</script>
  </body>
</html>
<?php ob_end_flush();?>