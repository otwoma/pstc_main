<?php
/**
 * Created by PhpStorm.
 * User: Otwoma
 * Date: 4/24/2017
 * Time: 5:09 PM
 */

include '_header_admin.php';
?>
    <div id="main-container">


        <script type="text/javascript" charset="utf-8">
            $(function() {
                $('#responsiveTable').dataTable();
            } );
        </script>

        <div class="padding-md">

            <div class="panel panel-default table-responsive">
                <div class="padding-sm font-16">
                    Listing Recruits
                </div>

                <div class="seperator"></div><div class="seperator"></div>
                <table class="table table-striped" id="responsiveTable">
                    <thead>
                    <tr>
                        <th width="" align="left"><span class=""></span>#</th>
                        <th width="" align="left"><span class=""></span>Surname</th>
                        <th width="" align="left"><span class=""></span>First Name</th>
                        <th width="" align="left"><span class=""></span>Gender</th>
                        <th width="" align="left"><span class=""></span>Sto. Number</th>
                        <th width="" align="left"><span class=""></span>Service Number</th>
                        <th width="" align="left"><span class=""></span>Created At</th>
                        <th width="" align="left"><span class=""></span></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $list = "SELECT recruit_id, surname, first_name, sto_number,gender, service_number, created_at FROM recruits ORDER BY recruit_id ASC";
                    $list_query = mysql_query($list);
                    while($list_result = mysql_fetch_array($list_query)){
                        $recruit_id=$list_result['sto_number'];
                        $gender=$list_result['gender'];
                        $name=$list_result['surname'];

                        if($gender=='Male'){$bclas='butn-navy';}else{$bclas='butn-blue';}
                        $made_title=$name.' - '.$recruit_id;
                        ?>
                        <tr>
                            <td align="left"><?php echo $list_result['recruit_id']?></td>
                            <td align="left"><?php echo ucwords($name) ;?></td>
                            <td align="left"><?php echo ucwords($list_result['first_name']);?></td>
                            <td align="left"><?php echo ucwords($gender);?></td>
                            <td align="left"><?php echo ucwords($list_result['sto_number'])?></td>
                            <td align="left"><?php echo ucwords($list_result['service_nummber'])?></td>
                            <td align="left"><?php echo ucwords($list_result['created_at'])?></td>
                            <td align="left"> <a target="_blank" href="profile?td=<?php echo ($recruit_id)?>" class="std_pro_link" id="<?php echo $recruit_id;?>"> <button class="butn <?php echo $bclas;?>"><span class="fa fa-graduation-cap"></span> View Profile</button></a></td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
            </div><!-- /panel -->
            <!-- /panel -->
        </div><!-- /.padding-md -->
    </div><!-- /main-container -->


    <script> $(".recruits_mu").addClass('active');</script>

<?php include'../_footer.php';?>