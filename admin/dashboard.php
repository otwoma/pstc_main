<?php
/**
 * Created by PhpStorm.
 * User: Otwoma
 * Date: 4/12/2017
 * Time: 10:30 AM
 */

include '_header_admin.php';


?>

<div id="main-container">

    <div class="main-header clearfix">
        <div class="page-title">
            <h3 class="no-margin">Dashboard <small>Control Panel</small></h3>
        </div>
    </div>

    <div class="padding-md">
        <div class="row">
            <div class="col-sm-6 col-md-3">
                <div class="panel-stat3 bg-cyan">

                    <h1 class="m-top-none" id="student_count">
                        <?php echo count_recruits(); ?>
                    </h1>
                    <h5 class="bold">Recruit Population</h5>

                    <div class="stat-icon">
                        <i class="fa fa-graduation-cap fa-3x"></i>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 col-md-3">
                <div class="panel-stat3 bg-success">

                    <h1 class="m-top-none" id="staff_count">
                        <?php echo count_instructors(); ?>
                    </h1>
                    <h5 class="bold">Instructor Population</h5>

                    <div class="stat-icon">
                        <i class="fa fa-briefcase fa-3x"></i>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 col-md-3">
                <div class="panel-stat3 bg-blue">

                    <h1 class="m-top-none" id="class_count"><?php ?>25</h1>
                    <h5 class="bold">Class Count</h5>

                    <div class="stat-icon">
                        <i class="fa fa-building-o fa-3x"></i>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 col-md-3">
                <div class="panel-stat3 bg-blue2">
                    <span id="holder_users_online">
                        <h2 class="m-top-none" id="users_online">4</h2>
                        <h5 class="bold"><?php ?> Online</h5>
                    </span>
                    <div class="stat-icon">
                        <i class="fa fa-dashboard fa-3x"></i>
                    </div>
                    <div class="refresh-button">
                        <i class="fa fa-refresh"></i>
                    </div>
                    <div class="loading-overlay">
                        <i class="loading-icon fa fa-refresh fa-spin fa-lg"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="shortcut-wrapper grey-container">
        <span class="font-16 padding-sm margin-sm grey">Quick Links <span class="fa fa-angle-double-right"></span></span>

        <a href="#" class="shortcut-link">
					<span class="shortcut-icon">
						<i class="fa fa-bank"></i>
					</span>
            <span class="text">Class Schedules</span>
        </a>

        <a href="#" class="shortcut-link">
					<span class="shortcut-icon">
						<i class="fa fa-calendar-o"></i>
					</span>
            <span class="text">Instructor Schedules</span>
        </a>

        <a href="#formModal2" role="button" data-toggle="modal" class="shortcut-link">
					<span class="shortcut-icon">
						<i class="fa fa-user-plus"></i>
					</span>
            <span class="text">Create New Account</span>
        </a>

        <a href="account" class="shortcut-link">
					<span class="shortcut-icon">
						<i class="fa fa-cogs"></i></span>
            <span class="text">Account Settings</span>
        </a>
    </div>

    <div class="padding-md">

        <div class="row">




            <div class="col-lg-4">
                <div class="panel panel-default">
                    <div class="panel-heading panel-heading3 clearfix">
                        <span class="pull-left bold">
                           <i class="fa fa-graduation-cap"></i> Access Recruit Profile
                        </span>
                    </div>

                    <div class="panel-body no-padding collapse in">
                        <ul class="list-group task-list no-margin collapse in">
                            <form action="../_actions" method="get">
                                <div class="form-group margin-sm">
                                    <label>Recruit Store Number:</label>
                                    <input type="text" name="quick_ac_std_pro" class="form-control" placeholder="Store no." required></br>
                                    <button type="submit" class="butn butn-navy pull-right margin-sm"><span class="fa fa-angle-double-right"></span> See Profile</button>
                                </div>
                            </form>
                        </ul>
                    </div>

                    <div class="loading-overlay">
                        <i class="loading-icon fa fa-refresh fa-spin fa-lg"></i>
                    </div>
                </div>
            </div>



            <div class="col-lg-4">
                <div class="panel panel-default">
                    <div class="panel-heading2 panel-heading clearfix">
                        <span class="pull-left bold">
                          <span class="fa fa-calendar"></span> Class Timetable
                        </span>
                    </div>

                    <div class="panel-body no-padding collapse in">
                        <ul class="list-group task-list no-margin collapse in">
                            <form action="#" method="post">
                                <div class="form-group margin-sm">
                                    <label>Select Class:</label>
                                    <select class="form-control inline-block" name="class_id">
                                        <option value="">Class</option>
                                        <!--                                        --><?php
                                        //                                        echo class_options_2();
                                        //                                        ?>
                                    </select>
                                    <button type="submit" class="butn butn-blue pull-right margin-sm"><span class="fa fa-calendar"></span> Open Timetable</button>
                                </div>
                            </form>
                        </ul>
                    </div>
                    <div class="loading-overlay">
                        <i class="loading-icon fa fa-refresh fa-spin fa-lg"></i>
                    </div>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="panel panel-default">
                    <div class="panel-heading clearfix">
                        <span class="pull-left bold">
                            <span class="text-success m-left-xs"><i class="fa fa-check"></i></span> To Do List
                        </span>
                        <ul class="tool-bar">
                            <li><a href="#do_listModal" role="button" data-toggle="modal" class="bg-success btn-small"><i class="fa fa-plus"></i> New Task</a>
                            </li>
                            <li><a href="#toDoListWidget" data-toggle="collapse"><i class="fa fa-arrow-up"></i><i class="fa fa-arrow-down"></i></a></li>
                        </ul>
                    </div>
                    <div class="panel-body no-padding collapse in" id="toDoListWidget">
                        <ul class="list-group task-list no-margin collapse in">

                            <span id="new_tasks_holder"></span>

                            <?php echo tasks_display($tid);?>

                        </ul>
                    </div>
                    <div class="loading-overlay">
                        <i class="loading-icon fa fa-refresh fa-spin fa-lg"></i>
                    </div>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="panel panel-default">
                    <div class="loading-overlay">
                        <i class="loading-icon fa fa-refresh fa-spin fa-lg"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="do_listModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button id="close_modal" type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4>New item</h4>
            </div>

            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label>Task :</label>
                        <textarea spellcheck="false" required class="form-control input-sm sms_con" id="todo_item" name="list_item" style="height: 70px;"></textarea>
                    </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary btn-sm" data-dismiss="modal" aria-hidden="true">Close</button>
                <button id="todo_add" type="button" class="btn butn-green btn-sm">Submit</button>
            </div>
        </div>
    </div>
    </form>
</div>

<div class="modal fade" id="formModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4>Adding New Designation</h4>
            </div>
            <div class="modal-body">
                <form action="../_actions.php" method="POST" enctype="multipart/form-data">

                    <div class="form-group">
                        <label>Designation Title</label></br>
                        <input type="text" name="name" placeholder="eg. Sergeant " class="form-control" required>

                    </div>


            </div>
            <div class="modal-footer">
                <button class="btn btn-primary btn-sm" data-dismiss="modal" aria-hidden="true">Close</button>
                <button type="submit" class="btn btn-success btn-sm">Submit</button>
            </div>
        </div>
    </div>
    </form>
</div>

<div class="modal fade" id="formModal2">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4>Add New Staff</h4>
            </div>
            <div class="modal-body">
                <form action="../_actions.php" method="POST" enctype="multipart/form-data">

                    <div class="form-group">
                        <label>Username</label></br>
                        <input type="text" name="username" placeholder="John" class="form-control" required>
                    </div>

                    <div class="form-group">
                        <label>Names</label></br>
                        <select class="form-control" style="margin-left:0px;" name="name">
                            <?php echo instructors() ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Account Type</label>
                        <select class="form-control" style="margin-left:0px;" name="account_type">
                            <?php echo account_type() ?>
                        </select>
                    </div>
                </div>
            <div class="modal-footer">
                <button class="btn btn-primary btn-sm" data-dismiss="modal" aria-hidden="true">Close</button>
                <button type="submit" class="btn btn-success btn-sm">Submit</button>
            </div>
        </div>
    </div>
    </form>
</div>

<script>
    $(".dashboard_mu").addClass('active');
    setInterval(function(){
        $.post('_ajax', 'online_users=0',
            function (live_users) {
                $("#holder_users_online").html(live_users);
            });
    }, 1000);

</script>

<?php include '../_footer.php';?>
