<?php
include ("db_conn.php");



function cleaN($string){
    return mysql_real_escape_string($string);
}


class Cry {
    var $skey = "#7%+xQHa|oGs^hx61oxS_pR>VPSqz1X";

    public  function encode($value){
        if(!$value){return false;}
        $text = $value;
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $crypttext = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $this->skey, $text, MCRYPT_MODE_ECB, $iv);
        return trim($this->safe_b64encode($crypttext));
    }

    public  function safe_b64encode($string) {
        $data = base64_encode($string);
        $data = str_replace(array('+','/','='),array('-','_',''),$data);
        return $data;
    }

    public function decode($value){
        if(!$value){return false;}
        $crypttext = $this->safe_b64decode($value);
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $decrypttext = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $this->skey, $crypttext, MCRYPT_MODE_ECB, $iv);
        return trim($decrypttext);
    }

    public function safe_b64decode($string) {
        $data = str_replace(array('-','_'),array('+','/'),$string);
        $mod4 = strlen($data) % 4;
        if ($mod4) {
            $data .= substr('====', $mod4);
        }
        return base64_decode($data);
    }
}

function pass_encode($plain_password){
    include '_variables.php';
    $volt = md5(md5($password_key));
    $pps = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($password_key), $plain_password, MCRYPT_MODE_CBC, $volt);
    return base64_encode($pps);
}

function pass_decode($pro_password){
    include '_variables.php';
    $volt = md5(md5($password_key));
    $dps = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($password_key), base64_decode($pro_password), MCRYPT_MODE_CBC, $volt);
    return rtrim($dps, "");
}


function enc_id($id){
    include '_variables.php';
    $u_id=(double)$id * $url_prime_number;
    $id=base64_encode($u_id);
    $id = str_replace('==','__',$id);
    $id = str_replace('=','_',$id);
    return $id;
}

function dec_id($en_id){
    include '_variables.php';
    $idx=str_replace('_','=',$en_id);
    $idx=str_replace('__','==',$en_id);
    $u_id=base64_decode($idx);
    $dec=(double)$u_id / $url_prime_number;
    return $dec;
}

function mnr($res) {
    return mysql_num_rows($res);
}

function msoc($res) {
    return mysql_fetch_assoc($res);
}

function mray($res) {
    return mysql_fetch_array($res);
}

function md($res) {
    return mysql_result($res,0);
}

function m($res) {
    return mysql_query($res);
}

function sani($unclean_data){
    return htmlspecialchars(mysql_real_escape_string($unclean_data));
}

function get_ip(){
    if ( isset($_SERVER['HTTP_CLIENT_IP']) && ! empty($_SERVER['HTTP_CLIENT_IP'])) {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif ( isset($_SERVER['HTTP_X_FORWARDED_FOR']) && ! empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip = (isset($_SERVER['REMOTE_ADDR'])) ? $_SERVER['REMOTE_ADDR'] : '0.0.0.0';
    }
    $ip = filter_var($ip, FILTER_VALIDATE_IP);
    $ip = ($ip === false) ? '0.0.0.0' : $ip;
    return $ip;
}



function rand_string($len) {
    return substr(str_shuffle("abcde123456789fghjklmnpqrstuvwxyz"), 0, $len);
}

function rand_letters($len) {
    return substr(str_shuffle("abcdefghjklmnpqrstuvwxyz"), 0, $len);
}
function rand_number($len) {
    return substr(str_shuffle("1234567890"), 0, $len);
}


function account_name(){
    include '_variables.php';

    return md("SELECT name FROM accounts WHERE account_type = '".$act."' AND account_id = $tid");

}

//function avatar(){
//    include ('_variables.php');
//    if($act=='student'){
//        $avatar= student_avatar($tid);
//    }elseif($act=='staff'){
//        $avatar=staff_avatar($tid);
//    }elseif($act=='admin'){
//        $avatar='admin.png';
//    }elseif($act=='guardian'){
//        $avatar='guardian.png';
//    }elseif($act=='finance'){
//        $avatar='admin.png';
//    }elseif($act=='store'){
//        $avatar='admin.png';
//    }elseif($act=='registry-staff'){
//        $avatar='admin.png';
//    }elseif($act=='optional_payments'){
//        $avatar='admin.png';
//    }else{
//        $avatar='admin.png';
//
//    }
//    return $avad.$avatar;
//}


function avatar($account_id){
    $avad = '';
    $default_profile_pic = '';
    include '_variables.php';
    $image = md(m("SELECT avatar FROM accounts WHERE id = '" . $account_id . "'"));
    $image_ = $avad . $image;
    // return $image;
    if (file_exists($image_)) {
        return $image_;
    } else {
        return $avad . $default_profile_pic;
    }
}


function theme_box(){
    ?>
    <div class="theme-box">
        <a data-toggle="tooltip" title="Navy Blue" class="theme-color" style="background:#323447" id="default"></a>
        <a data-toggle="tooltip" title="Calm" class="theme-color" style="background:#9092C3" id="skin-1"></a>
        <a data-toggle="tooltip" title="Safe Grass" class="theme-color" style="background:#93C865" id="skin-2"></a>
        <a data-toggle="tooltip" title="Simple" class="theme-color" style="background:#3e6b96" id="skin-3"></a>
        <a data-toggle="tooltip" title="Choco" class="theme-color" style="background:#635247" id="skin-4"></a>
        <a data-toggle="tooltip" title="Pitch Black" class="theme-color" style="background:#3a3a3a" id="skin-5"></a>
        <a data-toggle="tooltip" title="Different" class="theme-color" style="background:#D6756F" id="skin-6"></a>
        <a data-toggle="tooltip" title="Lilac" class="theme-color" style="background:#A67ADC" id="skin-7"></a>
        <a data-toggle="tooltip" title="Pink Bit" class="theme-color" style="background:#F5859D" id="skin-8"></a>
        <a data-toggle="tooltip" title="Orange Beetroot" class="theme-color" style="background:#F9AF58" id="skin-9"></a>
        <a data-toggle="tooltip" title="Light Blue" class="theme-color" style="background:#3BAFDA" id="skin-10"></a>
    </div>
    <?php
}

function getname(){
    include '_variables.php';
    if($act=='registry-staff'){
        $got_name= student_name($tid);
    }elseif($act=='staff'){
        $got_name= staff_name($tid);
    }elseif($act=='admin'){
        $got_name='System Admin';
    }elseif($act=='finance'){
        $got_name='Finance Account';
    }elseif($act=='guardian'){
        $got_name= guardian_name($tid);
    }elseif($act=='store'){
        $got_name= 'Store';
    }elseif($act=='dlib'){
        $got_name= 'Digital Librarian';
    }elseif($act=='optional_payments'){
        $got_name= 'Optional Payments';
    }
    return $got_name;
}

function account_type(){
    $acc_types = array('Admin', 'Chief Instructor','Class Coordinator', 'Exam Coordinator', 'Instructor', 'Library', 'Registry', 'Store', 'QM');
    foreach($acc_types as $type){
        ?>
        <option value="<?php echo $type?>"><?php echo $type?></option>
        <?php
    }
}

function designation(){
    $desig=m("SELECT designation_id,name FROM designations");
    while($fet_sc=msoc($desig)){
        $sscid=$fet_sc['designation_id'];
        $ssctit=$fet_sc['name'];
        ?>
        <option value='<?php echo $sscid?>'><?php echo $ssctit?></option>
    <?php }
}

function designation_name($id){
    $desgin = md(m("SELECT name FROM designations WHERE designation_id = '".$id."'"));
    return $desgin;
}

function department(){
    $dept=m("SELECT department_id,name FROM departments");
    while($get_dept=msoc($dept)){
        $deptt=$get_dept['department_id'];
        $depttn=$get_dept['name'];
        ?>
        <option value='<?php echo $deptt?>'><?php echo $depttn?></option>
    <?php }
}

function status(){
    $stats = array('Active', 'Inactive');
    $val=0;
    foreach($stats as $stat){
        $val++
        ?>
        <option value="<?php echo $val;?>"><?php echo $stat;?></option>
        <?php
    }
}

function assigned_in(){
    $asgns = array('Class', 'Field');
    $val = 0;
    foreach($asgns as $asgn){
        $val ++;
        ?>
        <option value="<?php echo $val?>"><?php echo $asgn?></option>
   <?php }
}
function assign_label($assign_label)
{
    if ($assign_label == 1) {
        $label = ('Class');
    } else {
        $label = ('Field');
    }
    return $label;
}

function company_options(){
    $comps = m("SELECT company_id, company_name FROM companies");
    while($get_comps = msoc($comps)){
        $compid = $get_comps['company_id'];
        $compnm = $get_comps['company_name'];
        ?>
        <option value='<?php echo $compid?>'><?php echo $compnm?></option>
<?php    }
}

function companies($id){
    return $comp = md(m("SELECT company_name FROM companies WHERE company_id = '".$id."'"));
}

function instructors(){
    $inst = m("SELECT instructor_id, surname, first_name, other_name, assigned FROM instructors");
    while($get_details = msoc($inst)){
//        $inst_id = $get_details['instructor_id'];
        $inst_nm = $get_details['surname'].' '.$get_details['first_name'].' '.$get_details['other_name'].'--'.assign_label($get_details['assigned']);
        ?>
<option value="<?php echo $inst_nm?>"><?php echo $inst_nm?></option>
<?php    }
}

function task_add($user,$task){
    m("INSERT INTO do_lists SET account = '".$user."',task = '".$task."'");
    return mysql_insert_id();
}

function tasks_display($user){
    $r_task=m("SELECT id,task,status FROM do_lists WHERE account = '".$user."' ORDER BY id DESC");
    $ki='';
    while($tasks=msoc($r_task)){
        $task_status=$tasks['status'];
        $task_detail = $tasks['task'];
        $task_id = $tasks['id'];
        if($task_status=='7'){$selection='selected';$check_k='checked';}else{$selection='';$check_k='';}
        $ki.="<li id='$task_id' class='list-group-item $selection'>
                <label title='Mark as done' class='label-checkbox inline'>
                    <input data-did='$task_id' type='checkbox' $check_k class='task-finish'>
                    <span class='custom-checkbox'></span>
                </label>
                    $task_detail
                <span class='pull-right' title='delete task'>
                    <a data-id='$task_id' href='#' class='task-del'><i class='fa fa-trash-o fa-lg text-danger'></i></a>
                </span>
              </li>
    ";

    }
    return $ki;
}

function task_delete($user,$task_id){
    m("DELETE FROM do_lists WHERE id = '".$task_id."' AND account ='".$user."'");
}

function task_done($user,$task_id){
    m("UPDATE do_lists SET status = '7' WHERE account ='".$user."' AND id = '".$task_id."'");
}

function task_undo($user,$task_id){
    m("UPDATE do_lists SET status = '1' WHERE account ='".$user."' AND id = '".$task_id."'");
}

function get_platoons($id){
    return mnr(m("SELECT platoon_id FROM platoon WHERE company_id = '".$id."'"));
}

function count_instructors(){
    return mnr(m("SELECT instructor_id FROM instructors WHERE status = 1"));
}

function count_recruits(){
    return mnr(m("SELECT recruit_id FROM recruits WHERE status = 1"));
}

function count_platoons(){
    return mnr(m("SELECT platoon_id FROM platoon WHERE status = 1"));
}

/*NEO'S*/

function platoon_name($platoon_id)
{
    return md(m("SELECT platoon_name FROM platoon WHERE platoon_id = $platoon_id"));
}

function tribe_options($suffix_text = '')
{
    $tribes = [];
    include('_variables.php');

    $id = 0;
    foreach ($tribes as $tribe => $data) {
        $id++;
        ?>
        <option value="<?php echo $data['id'] ?>"><?php echo $data['tribe_name'].$suffix_text;?></option>
        <?php
    }
}

function read_tribe($tribe_id)
{
    return md(m("SELECT tribe_name FROM tribes WHERE id = $tribe_id"));

}

function platoon_options($selected_id='')
{
    $platoons = [];
    include('_variables.php');

    $comp = m("SELECT company_id,company_name FROM companies WHERE status = 1");
    $opt ='';
    while($company = mray($comp)){

        $company_id = $company['company_id'];

        $plat = m("SELECT platoon_name,platoon_id FROM platoon WHERE company_id = $company_id");

        while($platoon = mray($plat)){

            $platoon_id = $platoon['platoon_id'];
            $platoon_name = $platoon['platoon_name'];

            if($selected_id===$platoon_id){
                $opt.="<option selected='selected' value='$platoon_id'>$platoon_name</option>";

            }else{
                $opt.="<option value='$platoon_id'>$platoon_name</option>";

            }



        }

    }



    return $opt;
}

function json_PL_Platoon_unassigned()
{

    $recruits = m("SELECT * FROM recruits WHERE status =1 AND platoon_id = 0");
    $recruits_data = array();
    $string = '';
    while ($data = mray($recruits)) {

        $id = $data['recruit_id'];
        $tribe_id = $data['tribe_id'];

        $tribe_name = sani(read_tribe($tribe_id));

        $name = $data['surname'] . ' ' . $data['first_name'] . ' ' . $data['other_name'] . '---' . $tribe_name;
        $recruits_data[] = ['text' => $name, 'id' => $data['recruit_id']];


        $string .= " $id: {id: $id,text: '$name',tribe_id:'$tribe_id'},";

    }


    return rtrim($string, ',');
    $json = json_encode($recruits_data);

    $json = str_replace('[', '', $json);
    return str_replace(']', '', $json);

    // return str_replace(array('[', ']'), '', json_encode($recruits_data));
}


function json_PL_Platoon($platoon_id)
{

    $recruits = m("SELECT * FROM recruits WHERE status =1 AND platoon_id = $platoon_id");
    $recruits_data = array();
    $string = '';
    while ($data = mray($recruits)) {

        $id = $data['recruit_id'];
        $tribe_id = $data['tribe_id'];

        $tribe_name = sani(read_tribe($tribe_id));

        $name = $data['surname'] . ' ' . $data['first_name'] . ' ' . $data['other_name'] . '---' . $tribe_name;
        $recruits_data[] = ['text' => $name, 'id' => $data['recruit_id']];


        $string .= " $id: {id: $id,text: '$name',tribe_id:'$tribe_id'},";

    }


    return rtrim($string, ',');
    /*$json = json_encode($recruits_data);

    $json = str_replace('[', '', $json);
    return str_replace(']', '', $json);*/

    // return str_replace(array('[', ']'), '', json_encode($recruits_data));
}



function json_for_pick_list()
{

    $recruits = m("SELECT * FROM recruits WHERE status =1");
    $recruits_data = array();
    $string = '';
    while ($data = mray($recruits)) {

        $id = $data['recruit_id'];
        $tribe_id = $data['tribe_id'];

        $tribe_name = sani(read_tribe($tribe_id));

        $name = $data['surname'] . ' ' . $data['first_name'] . ' ' . $data['other_name'] . '---' . $tribe_name;
        $recruits_data[] = ['text' => $name, 'id' => $data['recruit_id']];


        $string .= " $id: {id: $id,text: '$name',tribe_id:'$tribe_id'},";

    }


    return rtrim($string, ',');
    $json = json_encode($recruits_data);

    $json = str_replace('[', '', $json);
    return str_replace(']', '', $json);

    // return str_replace(array('[', ']'), '', json_encode($recruits_data));
}
