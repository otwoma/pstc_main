<?php
include "_header.reg.php";
?>

    <div id="main-container">


        <div class="main-header clearfix">
            <div class="page-title">
                <h3 class="no-margin">Registry Reports</h3>
            </div>

        </div>


        <div class="padding-md">

            <div class="row">

                <div class="col-lg-4">





                    <div class="panel panel-default">
                        <div style="background-color: #356934; font-size:13.5px;" class="panel-heading panel-heading3 clearfix">
        <span class="pull-left bold">
           <i class="fa fa-check"></i> Recruit Clearance Report
        </span>

                        </div>
        <div class="panel-body no-padding collapse in">
            <ul class="list-group task-list no-margin collapse in">
                <form action="_actions.php" method="post">

    <input type="hidden" name="report" value="clearance_report"/>
    <input type="hidden" name="action" value="report"/>

                    <div class="form-group margin-sm">
                        <label>Clearance Report:</label>
                        <select class="form-control" name="clearance_type" id="clearance_type">
                            <option value="2">Medical Clearance</option>
                            <option value="1">Certificate Clearance</option>
                            <option value="3">Fingerprints Clearance</option>
                            <option value="4">National ID Clearance</option>
                            <option value="0">Clearance Summary</option>
                        </select>

                        <button type="submit" class="btn btn-primary pull-right margin-sm"><span
                                class="fa fa-send"></span> Get Report
                        </button>
                    </div>
                </form>
            </ul>
        </div>
                        <div class="loading-overlay">
                            <i class="loading-icon fa fa-refresh fa-spin fa-lg"></i>
                        </div>
                    </div>



                </div>


                <div class="col-lg-4">





                    <div class="panel panel-default">
                        <div style="background-color: #356934; font-size:13.5px;" class="panel-heading panel-heading3 clearfix">
        <span class="pull-left bold">
           <i class="fa fa-user"></i> Recruits Report
        </span>

                        </div>
                        <div class="panel-body no-padding collapse in">
                            <ul class="list-group task-list no-margin collapse in">
                                <form action="_actions" method="get">
                                    <div class="form-group margin-sm">
                                        <label>Company Filter:</label>
                                        <select class="form-control" name="clearance_type" id="clearance_type">


                                            <option value="all">Select All Recruits</option>
                                            
<?php $companu = m("SELECT * FROM companies WHERE status = 1");


while($companies = mray($companu)){
?>
                <option value="<?php echo $companies['company_id'];?>"> <?php echo $companies['company_name'];?></option>


                                            <?php }?>


                                        </select>
                                    </div>





                    <div class="form-group margin-sm">
                        <label>Platoon Filter:</label>
                        <select class="form-control" name="clearance_type" id="clearance_type">


                            <option value="all">Select All Recruits</option>

                            <?php $companu = m("SELECT * FROM platoon");


                            while($companies = mray($companu)){
                                ?>
                                <option value="<?php echo $companies['platoon_id'];?>"> <?php echo $companies['platoon_name'];?></option>


                            <?php }?>


                        </select>
                    </div>





                                    <button type="submit" class="btn btn-primary pull-right margin-sm"><span
                                                class="fa fa-send"></span> Get Report
                                        </button>

                                </form>
                            </ul>
                        </div>
                        <div class="loading-overlay">
                            <i class="loading-icon fa fa-refresh fa-spin fa-lg"></i>
                        </div>
                    </div>



                </div>


                <div class="col-lg-4">





                    <div class="panel panel-default">
                        <div style="background-color: #356934; font-size:13.5px;" class="panel-heading panel-heading3 clearfix">
        <span class="pull-left bold">
           <i class="fa fa-bed"></i> Recruit - Barrack Report
        </span>

                        </div>
                        <div class="panel-body no-padding collapse in">
                            <ul class="list-group task-list no-margin collapse in">
                                <form action="_actions" method="get">
                                    <div class="form-group margin-sm">
                                        <label>Select Barrack:</label>
                                        <select class="form-control" name="clearance_type" id="clearance_type">


                                            <option value="all">All Barracks</option>


                                        </select>
                                    </div>






                                    <button type="submit" class="btn btn-primary pull-right margin-sm"><span
                                            class="fa fa-send"></span> Get Report
                                    </button>

                                </form>
                            </ul>
                        </div>
                        <div class="loading-overlay">
                            <i class="loading-icon fa fa-refresh fa-spin fa-lg"></i>
                        </div>
                    </div>



                </div>


            </div>







            <div class="row">

                <div class="col-lg-4">





                    <div class="panel panel-default">
                        <div style="background-color: #356934; font-size:13.5px;" class="panel-heading panel-heading3 clearfix">
        <span class="pull-left bold">
           <i class="fa fa-archive"></i> Recruit - Tribe Report
        </span>

                        </div>
                        <div class="panel-body no-padding collapse in">
                            <ul class="list-group task-list no-margin collapse in">
                                <form action="_actions.php" method="post">


                                    <input type="hidden" name="report" value="tribe_report"/>
                                    <input type="hidden" name="action" value="report"/>


                                    <div class="form-group margin-sm">
                                        <label>Select Tribe Report:</label>

                                        <select class="form-control" name="tribe" id="tribe">


                                            <option value="all">Select All Tribes</option>

                                            <?php $companu = m("SELECT * FROM tribes");


                                            while($companies = mray($companu)){
                                                ?>
                                                <option value="<?php echo $companies['id'];?>"> <?php echo $companies['tribe_name'];?></option>


                                            <?php }?>


                                        </select>

                                        <button type="submit" class="btn btn-primary pull-right margin-sm"><span
                                                class="fa fa-send"></span> Get Report
                                        </button>
                                    </div>
                                </form>
                            </ul>
                        </div>
                        <div class="loading-overlay">
                            <i class="loading-icon fa fa-refresh fa-spin fa-lg"></i>
                        </div>
                    </div>



                </div>


                <div class="col-lg-4">





                    <div class="panel panel-default">
                        <div style="background-color: #356934; font-size:13.5px;" class="panel-heading panel-heading3 clearfix">
        <span class="pull-left bold">
           <i class="fa fa-map-marker"></i> Recruits - Location Report
        </span>

                        </div>
                        <div class="panel-body no-padding collapse in">
                            <ul class="list-group task-list no-margin collapse in">
                                <form action="_actions" method="get">
                                    <div class="form-group margin-sm">
                                        <label>County:</label>
                                        <select class="form-control" name="clearance_type" id="clearance_type">


                                            <option value="all">Select All Counties</option>

                                            <?php $companu = m("SELECT * FROM counties");


                                            while($companies = mray($companu)){
                                                ?>
                                                <option value="<?php echo $companies['id'];?>"> <?php echo $companies['county_name'];?></option>


                                            <?php }?>


                                        </select>
                                    </div>






                                    <button type="submit" class="btn btn-primary pull-right margin-sm"><span
                                            class="fa fa-send"></span> Get Report
                                    </button>

                                </form>
                            </ul>
                        </div>
                        <div class="loading-overlay">
                            <i class="loading-icon fa fa-refresh fa-spin fa-lg"></i>
                        </div>
                    </div>



                </div>


                <div class="col-lg-4">





                    <div class="panel panel-default">
                        <div style="background-color: #356934; font-size:13.5px;" class="panel-heading panel-heading3 clearfix">
        <span class="pull-left bold">
           <i class="fa fa-list"></i> Pass Out List
        </span>

                        </div>
                        <div class="panel-body no-padding collapse in">
                            <ul class="list-group task-list no-margin collapse in">
                                <form action="_actions" method="get">


                                    <br>
                                    <br>
                                    <br>
                                    <br>

                                    <button type="submit" class="btn btn-primary pull-right margin-sm"><span
                                            class="fa fa-send"></span> Get Report
                                    </button>

                                </form>
                            </ul>
                        </div>
                        <div class="loading-overlay">
                            <i class="loading-icon fa fa-refresh fa-spin fa-lg"></i>
                        </div>
                    </div>



                </div>


            </div>













        </div>

    </div>


    <div class="modal fade" id="do_listModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button id="close_modal" type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;</button>
                    <h4>New item</h4>
                </div>

                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <label>Task :</label>
                            <textarea spellcheck="false" required class="form-control input-sm sms_con" id="todo_item"
                                      name="list_item" style="height: 70px;"></textarea>
                        </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary btn-sm" data-dismiss="modal" aria-hidden="true">Close</button>
                    <button id="todo_add" type="button" class="btn butn-green btn-sm">Submit</button>
                </div>
            </div>
        </div>
        </form>
    </div>

    <script> $(".dashboard_mu").addClass('active');
        setInterval(function () {
            $.post('../_ajax', 'online_users=0',
                function (live_users) {
                    $("#holder_users_online").html(live_users);
                });
        }, 1000);
    </script>
<?php include '../_footer.php'; ?>