<?php include "_header.reg.php";
?>
    <div id="main-container">
    <div class="panel panel-default">
        <div class="panel-heading font-14 bold">
          <span class="fa fa-cogs"></span> Account Settings
        </div>
        <div class="panel-tab clearfix">
            <ul class="tab-bar left">
                <li class="active font-14 bold"><a href="#home2" data-toggle="tab"><i class="fa fa-2x fa-paint-brush"></i> Theme</a></li>
                <li class="font-14 bold"><a href="#profile2" data-toggle="tab"><i class="fa fa-2x fa-lock"></i> Password Change</a></li>
            </ul>
        </div>
        <div class="panel-body">
            <div class="tab-content">

                <div class="seperator"></div>
                <div class="tab-pane fade in active" id="home2">
                    <span class="bold font-14"> Pick a theme: </span>
                    <?php echo theme_box();?>
                    <div class="padding-sm bg-grey margin-sm">
                        <p>Select a theme by clicking the color boxes. Selected theme will be apply instantly.</p>
                    </div>
                </div>
                <div class="tab-pane fade" id="profile2">

                        <form id="dbl_pass" name="pass_changer" method="post">
                    <div class="panel-body col-md-6 margin-sm">
                    <div class="form-group">
                        <label class="control-label">Current Password</label>
                        <input type="password" id="password" name="password" placeholder="Your Current Password" class="form-control input-sm" data-required="true">
                    </div>

                            <div class="form-group">
                                <label class="control-label">New Password</label>
                                <input type="password" placeholder="New Password" class="form-control input-sm" name="new_password"  id="new_password" data-required="true" data-minlength="8">
                            </div>


                            <div class="form-group">
                                <label class="control-label">Confirm New Password</label>
                                <input type="password" id="new_password_s" name="new_password_s" placeholder="Confirm Password" class="form-control input-sm" data-equalto="#password" data-required="true">
                            </div>

<span id="pass_change_error"></span>
                    <div class="form-group" align="right">
                        <button id="push_pass_change" type="button" class="btn btn-primary"><span class="fa fa-check"></span> Submit</button>
                    </div>
                    </div>
                        </form>



                </div>

            </div>
        </div>









</div>






    <script> $(".account_mu").addClass('active');
$('body').on('click','#push_pass_change',function(){
        $.ajax({
            url: '../_ajax.php',
            type: 'POST',
            data: $('#dbl_pass').serialize(),
            success: function(dbl){
                if(dbl==77){
                    $("#dbl_pass").html('<div class="padding-sm font-14 bg-success margin-sm br-4 ">Password Change was a success - The system will log you out shortly to test your new password. <span id="d_counter"></span></div>');
                    var count = 11;
                    var counter = $('#d_counter');
                    setInterval(function(){
                        count--;
                        counter.html(count);
                        if (count === 0) {
                            window.location = 'logout';
                        }
                    }, 1000);
                }
                if(dbl==15){

                    $.post('../logout.php', 'x='+dbl,
                        function (iout) {

                        });

                    $("#dbl_pass").html('<div class="padding-sm font-14 bg-red margin-sm br-4 white"> You entered your current password wrongly, the sytem will require you to login <span id="d_counter"></span></div>');
                    var count = 21;
                    var counter = $('#d_counter');
                    setInterval(function(){
                        count--;
                        counter.html(count);
                        if (count === 0) {
                            window.location = 'logout';
                        }
                    }, 1000);
                }
                if(dbl==13){
/*new mism*/
                    $("#pass_change_error").html(dbl);
                }

                if(dbl==0){/*short or blank*/}

                if(dbl==44){/*short or blank*/}

            }
        });

});
    </script>
<?php include'../_footer.php';?>