<?php include '_header.reg.php';
?>
<div id="main-container">

    <script type="text/javascript" charset="utf-8">
        $(function() {
            $('#responsiveTable').dataTable();
        } );
    </script>
			<div class="padding-md">
				<div class="panel panel-default table-responsive">
					<div class="padding-sm font-16">
						<i class="fa fa-user-circle"></i> Recruits
                        <a href="new_recruit" role="button" class="btn btn-info btn-small pull-right"><span class="fa fa-plus"></span> New Admission</a>
					</div>

                    <div class="seperator"></div><div class="seperator"></div>
					<table class="table table-striped" id="responsiveTable">
						<thead>
							<tr>
                                <th width="" align="left"><span class=""></span>Recruit Name</th>
                                <th width="" align="left"><span class=""></span>Sto. Number</th>
                                <th width="" align="left"><span class=""></span>Gender</th>
                                <th width="" align="left"><span class=""></span>National ID</th>
                                <th width="" align="left"><span class=""></span>Platoon</th>
                                <th><span class=""></span></th>
							</tr>
						</thead>
						<tbody>
                        <?php
                        $list = "SELECT * FROM recruits WHERE status = 1";
                        $list_query= $d->q($list);
                        while($list_result = msoc($list_query)){
                            $sto_number=trailing_zeros($list_result['sto_number']);
                            $national_id=$list_result['national_id'];
                            $gender=$list_result['gender'];
                            $gender= read_gender($gender);
                            $recruit_id = $list_result['recruit_id'];
                            $platoon = platoon_name($list_result['platoon_id']);


                            $surname=$list_result['surname'];
                            $first_name=$list_result['first_name'];
                            $othername=$list_result['othername'];

                            $full_name=$surname.' '.$first_name.' '.$othername;
                            if($gender=='Male'){$bclas='butn-navy';}else{$bclas='butn-blue';}
                            $made_title=$full_name.' - '.$sto_number;
                            ?>
                            <tr title="<?php echo $made_title;?>">
                                <td><?php echo $full_name;?></span></td>
                                <td align="left"><?php echo $sto_number;?></td>
                                <td align="left"><?php echo $gender?></td>
                                <td align="left"><?php echo $national_id?></td>
                                <td align="left"><?php echo $platoon?></td>
                                <td align="left"> <a target="_blank" href="profile?td=<?php echo ($recruit_id)?>" class="std_pro_link" id="<?php echo $recruit_id;?>"> <button class="butn <?php echo $bclas;?>"><span class="fa fa-graduation-cap"></span> View Profile</button></a></td>
                            </tr>
                            <?php
                        }
                        ?>
                        </tbody>
					</table>
				</div>
			</div>
		</div>
<script> $(".recruits_menu").addClass('active');</script>

<?php include '../_footer.php'; ?>
