<?php

include ('../_functions.php');
include ('../_variables.php');
include ('_auth-registry.php');

$recruit_id = intval($_REQUEST['recruit_id']);
if($recruit_id==0){
	die();
}
$filename =  $recruit_id.'-'.time() . '.jpg';
$filepath = '../assets/avatars/';

$result = file_put_contents( $filepath.$filename, file_get_contents('php://input') );
if (!$result) {
	print "ERROR: Failed to write data to $filename, check permissions\n";
	exit();
}

m("UPDATE recruits SET photo = '".$filename."' WHERE recruit_id = $recruit_id");


echo $filepath.$filename;