<?php
include "_header.reg.php";
?>


    <script type="text/javascript" src="../assets/js/chart-js/Chart.bundle.min.js"></script>
    <script type="text/javascript" src="../assets/js/chart-js/utils.js"></script>


    <div id="main-container">


        <div class="main-header clearfix">
            <div class="page-title">
                <h3 class="no-margin">Dashboard</h3>
            </div>

        </div>
        <div class="padding-md">


            <div class="row">
                <div class="col-sm-6 col-md-4">
                    <div class="panel-stat3 bg-dark-green">

                        <h1 class="m-top-none" id="student_count"> <?php echo recruit_population();?> </h1>
                        <h5 class="bold">Recruit Population</h5>

                        <div class="stat-icon">
                            <i class="fa fa-user-circle fa-3x"></i>
                        </div>

                    </div>
                </div>



                <div class="col-sm-6 col-md-4">
                    <div class="panel-stat3 bg-dark-green">

                        <h1 class="m-top-none" id="staff_count"> <?php echo recruit_population_discontinued();?></h1>
                        <h5 class="bold">Discontinued Recruit</h5>

                        <div class="stat-icon">
                            <i class="fa fa-times fa-3x"></i>
                        </div>

                    </div>
                </div>



                <div class="col-sm-6 col-md-4">
                    <div class="panel-stat3 bg-dark-green">

                        <h1 class="m-top-none" id="platoon_count"> <?php echo platoon_count();?></h1>
                        <h5 class="bold">Platoon Count</h5>

                        <div class="stat-icon">
                            <i class="fa fa-hashtag fa-3x"></i>
                        </div>

                    </div>
                </div>



        <div class="row center col-lg-offset-1" align="center" >
                <div class="col-sm-5 col-md-5" style="margin-right:10px;background-color: #fff; border:3px solid #356934; border-radius:7px;">
                    <div class="panel-stat">
                        <canvas id="tribeChart"></canvas>
                    </div>
                </div>
                <div class="col-sm-5 col-md-5" style="margin-left:10px;background-color: #fff; border:3px solid #356934; border-radius:7px;">
                    <div class="panel-stat">
                        <canvas id="genderChart"></canvas>
                    </div>
                </div>

        </div>


            </div>
        </div>


    </div>






    <script> $(".dashboard_mu").addClass('active');
        setInterval(function () {
            $.post('../_ajax', 'online_users=0',
                function (live_users) {
                    $("#holder_users_online").html(live_users);
                });
        }, 1000);
    </script>
<?php include '../_footer.php'; ?>


    <script>
    var data = [12, 19, 3, 5, 2, 3];
    var labels = ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"];
    var bgColor = ["#179483","#A7CA74","#A3CBF0","#697480","#5FE28B","#F2CA84","#9d5f51","#EE422B","#1485BA","#1AC9B9","#674265","#FE6599","#26327F","#FC6E51","#9367C8","#000000","#FFAA31","#B61F24"];
    var default_colors = ['#3366CC','#DC3912','#FF9900','#109618','#990099','#3B3EAC','#0099C6','#DD4477','#66AA00','#B82E2E','#316395','#994499','#22AA99','#AAAA11','#6633CC','#E67300','#8B0707','#329262','#5574A6','#3B3EAC'];
    var ctx = document.getElementById("tribeChart");
    var myChart = new Chart(ctx, {
        type: 'doughnut',
        palette: "Soft Pastel",
        data: {
            labels: labels,
            datasets: [
                {
                    data: data,
                    backgroundColor: bgColor
                }]
        },
        options: {
            responsive: true,
            legend: {
                position: 'right',
                labels: { padding: 15,fontSize:14.5}
            },

            title: {
                display: true,fontSize:15.5,
                text: 'Recruit Tribe Distribution'
            },
            tooltips: {
                mode: 'single',
                titleFontSize:14.2,
                callbacks: {
                    label: function(tooltipItem, data) {
                        var label = data.labels[tooltipItem.index];
                        var datasetLabel = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                        return label + ': ' + datasetLabel + '';
                    }
                }
            }
        }


    });

</script>

<script>
    var data = [12, 19, 3, 5, 2, 3];
    var labels = ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"];
    var bgColor = ["#179483","#A7CA74","#A3CBF0","#697480","#5FE28B","#F2CA84","#9d5f51","#EE422B","#1485BA","#1AC9B9","#674265","#FE6599","#26327F","#FC6E51","#9367C8","#000000","#FFAA31","#B61F24"];
    var default_colors = ['#3366CC','#DC3912','#FF9900','#109618','#990099','#3B3EAC','#0099C6','#DD4477','#66AA00','#B82E2E','#316395','#994499','#22AA99','#AAAA11','#6633CC','#E67300','#8B0707','#329262','#5574A6','#3B3EAC'];
    var ctx = document.getElementById("genderChart");
    var myChart = new Chart(ctx, {
        type: 'pie',
        palette: "Soft Pastel",
        data: {
            labels: labels,
            datasets: [
                {
                    data: data,
                    backgroundColor: bgColor
                }]
        },
        options: {
            responsive: true,
            legend: {
                position: 'top',
                labels: { padding: 15,fontSize:14.5}
            },

            title: {
                display: true,fontSize:15.5,
                text: 'Recruit Tribe Distribution'
            },
            tooltips: {
                mode: 'single',
                titleFontSize:14.2,
                callbacks: {
                    label: function(tooltipItem, data) {
                        var label = data.labels[tooltipItem.index];
                        var datasetLabel = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                        return label + ': ' + datasetLabel + '';
                    }
                }
            }
        }


    });

</script>


