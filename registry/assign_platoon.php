<?php include '_header.reg.php';

$platoon_id = ($_POST['platoon_filter']);

$title = platoon_name($platoon_id).' Members';

?>
<div id="main-container">

    <style>
        .pickListButtons {
            padding: 10px;
            text-align: center;
        }

        .pickListButtons button {
            margin-bottom: 5px;
        }

        .pickListSelect {
            height: 200px !important;
        }

    </style>

    <br>

    <form action="" id="assignForm" method="post">
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Assigning to Platoons</h3>


                <!--TODO:: implement search by tribe, automate-->


            </div>
            <div class="panel-body">

                <div class="row" style="margin-bottom:20px; background-color: #EFEFEF; padding:15px;">

                    <div class="col-lg-4 pull-left">
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">

                                <select name="tribe_filter" id="tribe_filter" class="form-control">
                                    <option value="0"> Show All Tribes</option>
                                    <?php echo tribe_options('Only');?>
                                </select>
                            </div>
                        </div>

                    </div>



                    <div class="col-lg-4 pull-right">
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <select name="platoon_filter" id="platoon_filter" class="form-control">
                                    <option value="0"> Select Platoon</option>
                                    <?php echo platoon_options($platoon_id);?>
                                </select>
                            </div>
                        </div>

                    </div>



                    <br><br>

                </div>

                All Unassigned Recruits
                <div id="pickList"></div>
                <br>
                <button class="btn btn-primary hide hidden" id="getSelected">Get Selected</button>
            </div>

            <button type="button" id="submitForm" class="btn btn-primary pull-right"><i class="fa fa-send"></i>  Submit Changes</button>

        </div>
    </div>
    </form>
</div>


<?php include '../_footer.php'; ?>


<script>


    $(".new-assign-platoon").addClass('active');
    (function($) {

        $.fn.pickList = function(options,selectedPick) {

            var opts = $.extend({}, $.fn.pickList.defaults, options);
            var optselected = $.extend({}, $.fn.pickList.defaults, selectedPick);

            this.fill = function() {
                var option = '';

                $.each(opts.data, function(key, val) {
                    option += '<option class="viable" data-tribe_id='+val.tribe_id+''+' data-id=' + val.id + '>' + val.text + '</option>';
                });
                this.find('.pickData').append(option);
            };

            this.fillSelected = function() {
                var option = '';

                $.each(optselected.data, function(key, val) {
                    option += '<option class="viable" data-tribe_id='+val.tribe_id+''+' data-id=' + val.id + '>' + val.text + '</option>';
                });

                this.find('.pickListResult').append(option);
            };


            this.controll = function() {
                var pickThis = this;

                pickThis.find(".pAdd").on('click', function() {
                    var p = pickThis.find(".pickData option:selected");
                    p.clone().appendTo(pickThis.find(".pickListResult"));
                    p.remove();
                });

                pickThis.find(".pAddAll").on('click', function() {
                    var p = pickThis.find(".pickData option");
                    p.clone().appendTo(pickThis.find(".pickListResult"));
                    p.remove();
                });

                pickThis.find(".pRemove").on('click', function() {
                    var p = pickThis.find(".pickListResult option:selected");
                    p.clone().appendTo(pickThis.find(".pickData"));
                    p.remove();
                });

                pickThis.find(".pRemoveAll").on('click', function() {
                    var p = pickThis.find(".pickListResult option");
                    p.clone().appendTo(pickThis.find(".pickData"));
                    p.remove();
                });
            };

            this.getValues = function() {
                var objResult = [];
                this.find(".pickListResult option").each(function() {
                    objResult.push({
                        id: $(this).data('id'),
                        text: this.text
                    });
                });
                return objResult;
            };

            this.init = function() {
                var pickListHtml =
                    "<div class='row'>     <?php echo $title;?>" +
                    "  <div class='col-sm-5'>" +
                    "	 <select class='form-control pickListSelect pickData' multiple></select>" +
                    " </div>" +
                    " <div class='col-sm-2 pickListButtons'>" +
                    "	<button type='button'  class='pAdd btn btn-primary btn-sm'>" + opts.add + "</button>" +
                    "      <button  type='button' class='pAddAll btn btn-primary btn-sm'>" + opts.addAll + "</button>" +
                    "	<button   type='button' class='pRemove btn btn-primary btn-sm'>" + opts.remove + "</button>" +
                    "	<button   type='button' class='pRemoveAll btn btn-primary btn-sm'>" + opts.removeAll + "</button>" +
                    " </div>" +
                    " <div class='col-sm-5'>" +
                    "    <select class='form-control pickListSelect pickListResult' multiple></select>" +
                    " </div>" +
                    "</div>";

                this.append(pickListHtml);

                this.fill();
                this.fillSelected();
                this.controll();
            };

            this.init();
            return this;
        };

        $.fn.pickList.defaults = {
            add: 'Add',
            addAll: 'Add All',
            remove: 'Remove',
            removeAll: 'Remove All'
        };


    }(jQuery));


    var val = {
        <?php echo json_PL_Platoon_unassigned();?>
    };

    var val2 = {
        <?php echo json_PL_Platoon($platoon_id);?>
    };

    var pick = $("#pickList").pickList({
        data: val
    },{
        data: val2
    });




    $("#xmain-container").click(function() {
        console.log(pick.getValues());
    });



    $(document).on("click", ".viable", function () {
        var option_value = $(this).val();


    });

    $(document).on("change", "#platoon_filter", function () {


        $('#assignForm').submit();

    });



    $(document).on("click", "#submitForm", function () {
        var target_platoon = $("#platoon_filter").val();
        var data = pick.getValues();

        var target =$("#panel-title");

        $.post('_ajax.php', {
                data: data,target_platoon:target_platoon,intent:'assignPlatoon'
        },
            function (response) {
                alert(response);
                target.html(response);

            });



    });



    $(document).on("change", "#tribe_filter", function () {
        var option_value = $(this).val();
        var option_value = $.trim(option_value);

        var option_value = parseInt(option_value);



        $('.pickData .viable').each(function(){
            var tribe_id = $(this).data('tribe_id');
            var tribe_id = $.trim(tribe_id);


            var tribe_id = parseInt(tribe_id);


            if(option_value==0){

                $(this).removeClass('hide');
            }else {

                if (tribe_id === option_value) {
                    $(this).removeClass('hide');

                } else {
                    $(this).addClass('hide');

                }

            }



        });







    });



</script>