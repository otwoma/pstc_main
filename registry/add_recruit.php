<?php include '_header.reg.php';
?>
<link href="../assets/js/datepicker/bootstrap-datetimepicker.min.css" rel="stylesheet">
<link href="../assets/js/datepicker/calendar.min.css" rel="stylesheet">
<link href="../assets/js/datepicker/daterangepicker.css" rel="stylesheet">
<link href="../assets/js/datepicker/fullcalendar.min.css" rel="stylesheet">


<div id="main-container" xmlns="http://www.w3.org/1999/html" xmlns="http://www.w3.org/1999/html">


    <style>
        .checkboxes label {
            display: block;
            float: left;
            padding-right: 10px;
            padding-left: 22px;
            text-indent: 22px;
        }
        .checkboxes input {
            vertical-align: middle;
        }
        .checkboxes label span {
            vertical-align: middle;
        }
    </style>


    <div class="row container">
        <br>
        <br>
        <div class="col-lg-12">

    <ul class="nav nav-tabs" style="font-size:13px;">
        <li class="active"><a class="step1" data-toggle="tab" href="#step1">Step 1</a></li>
        <li><a class="step2" data-toggle="tab" href="#step2">Step 2</a></li>
        <li><a class="step3" data-toggle="tab" href="#step3">Step 3</a></li>
    </ul>
            <form id="msform" action="_handle.new_recruit.php" method="POST" enctype="multipart/form-data">

    <div class="tab-content container">
        <div id="step1" class="tab-pane fade in active">



            <div class="row">
                <br>
            </div>

            <div class="row">

                <div class="form-group col-lg-3">
                    <label>Sto. Number.</label>
                    <input placeholder="Recruit Sto. Number" type="text" name="sto_number" value="" id="admission"
                           class="form-control"/>
                </div>



                <div class="form-group col-lg-3">
                    <label>Terms</label>
                    <select class="form-control" style="margin-left:0px;" name="terms">
                        <?php echo recruit_terms() ?>
                    </select>
                </div>


                <div class="form-group col-lg-3">
                    <label>National ID</label>
                    <input type="number" name="national_id" class="form-control"
                           placeholder="National ID">

                </div>

            </div>


            <div class="row">

                <div class="form-group checkboxes col-lg-6">
                    <strong>Salutation</strong>
                    <label class="col-lg-12 control-label"></label>
                    <?php echo salutation_options()?>
                    <br>
                </div>



                    <div class="form-group col-lg-3">
                        <label>Gender</label>

                        <select class="form-control" style="margin-left:0px;" name="gender" id="gender">
                            <?php echo gender_options() ?>
                        </select>
                    </div>





                <br>
            </div>



            <div class="row">


                <div class="form-group col-lg-3">
                    <label>Surname</label>
                    <input spellcheck="false" type="text" name="surname" class="form-control"
                           placeholder="Surname" required>

                </div>

                <div class="form-group col-lg-3">
                    <label>First Name</label>
                    <input spellcheck="false" type="text" name="first_name" class="form-control"
                           placeholder="First Name" required>

                </div>

                <div class="form-group col-lg-3">
                    <label>Other Name</label>
                    <input spellcheck="false" type="text" name="other_name" class="form-control"
                           placeholder="Other Name">
                </div>

            </div>


            <div class="row">
            <div class="form-group col-lg-3">
                <label><span class="fa fa-calendar"></span> Date of Birth (d/m/y)</label>

                <input type="text" name="dob" id="dob" class="form-control date_picker"/>
            </div>



                <div class="form-group col-lg-3">
                    <label>Birth Certificate Number</label>
                    <input type="text" name="birth_certificate_number" class="form-control"
                           placeholder="Birth Certificate Number">

                </div>



                <div class="form-group col-lg-3">
                    <label>KRA Pin</label>
                    <input type="text" name="kra_pin" class="form-control"
                           placeholder="KRA pin">

                </div>

            </div>


            <div class="row">



                <div class="form-group col-lg-3">
                    <label>Highest Level of Education</label>
                    <select class="form-control" style="margin-left:0px;" name="level_of_education">
                        <?php echo education_level() ?>
                    </select>
                </div>


                <div class="form-group col-lg-3">
                    <label>Highest Professional Level</label>
                    <input placeholder="Highest Professional Level" type="text" class="form-control" style="margin-left:0px;" name="professional_level"/>
                </div>



            </div>




            <div class="row">
                <div class="form-group col-lg-3">
                    <label>Religion</label>
                    <select class="form-control" style="margin-left:0px;" name="religion">
                        <?php echo religion_options() ?>
                    </select>
                </div>


                <div class="form-group col-lg-3">
                    <label><span class="fa fa-calendar"></span> Date of Appointment (d/m/y)</label>
                    <input type="text" name="doa" id="doa" class="form-control date_picker"/>
                </div>


            </div>




            <div class="row">
                <div class="col-lg-4 col-lg-offset-3">
                    <button type="button" data-step="step2" title="Move to Step 2" class="btn btn-primary right step_trigger"> Next Step <i class="fa fa-chevron-right"></i></button>
                </div>
            </div>

        </div>
        <div id="step2" class="tab-pane fade">

            <div class="row">
                <br>


                <div class="form-group col-lg-3">
                    <label>Color of the Eye</label>
                    <input type="text" name="eye_color" class="form-control"
                           placeholder="Eye Color">
                </div>



                <div class="form-group col-lg-3">
                    <label>Skin Color</label>
                    <input type="text" name="skin_color" class="form-control"
                           placeholder="Skin Color">

                </div>


                <div class="form-group col-lg-3">
                    <label>Physical Figure</label>
                    <input type="text" name="physical" class="form-control"
                           placeholder="Physical Figure">

                </div>








            </div>










            <div class="row">


                <div class="form-group col-lg-1">
                    <label>Height (Feet)</label>
                    <input type="text" name="height_ft" class="form-control"
                           placeholder="">

                </div>

                <div class="form-group col-lg-1">
                    <label>Height (Inches)</label>
                    <input type="text" name="height_inches" class="form-control"
                           placeholder="">

                </div>


                <div class="form-group col-lg-7">
                    <label>Identification Marks</label>

                    <textarea name="identification_marks" class="form-control" id="identification_marks" cols="30" rows="5"></textarea>
                </div>



            </div>


            <div class="row">


                <div class="form-group col-lg-3">
                    <label>Place of Birth</label>
                    <input placeholder="Place of Birth" class="form-control" name="pob" id="pob" type="text">
                </div>

                <div class="form-group col-lg-3">
                    <label>Tribe Options</label>
                    <select class="form-control" style="margin-left:0px;" name="tribe_id">
                        <?php echo tribe_options() ?>
                    </select>
                </div>



            </div>




            <div class="row">


                <div class="form-group col-lg-3">
                    <label>Marital Status</label>
                    <select class="form-control" style="margin-left:0px;" name="marital_status" id="marital_status">
                        <option selected value="1">Single</option>
                        <option value="2">Married</option>
                    </select>
                </div>


                <div class="form-group col-lg-3">
                    <label>Name of Spouse</label>
                    <input placeholder="Name of Spouse" class="spouse_detail form-control" name="spouse_name" id="spouse_name" type="text">
                </div>



            </div>






            <div class="row">

                <div class="form-group col-lg-3">
                    <label>Telephone 1</label>
                    <input placeholder="Phone Number 1" class="form-control" name="tel_1" id="tel_1" type="text">
                </div>

                <div class="form-group col-lg-3">
                    <label>Telephone 2</label>
                    <input placeholder="Phone Number 2" class="form-control" name="tel_2" id="tel_2" type="text">
                </div>


            </div>







            <div class="row">

                <div class="form-group col-lg-3">
                    <label>Male Children</label>
                    <input class="male_children form-control" name="male_children" id="male_children" type="number" value="0">
                </div>


                <div class="form-group col-lg-3">
                    <label>Female Children</label>
                    <input class="female_children form-control" name="female_children" id="female_children" type="number" value="0">
                </div>

            </div>




            <div class="row">



                <div class="form-group col-lg-3">
                    <label>County</label>
                    <select class="form-control" style="margin-left:0px;" name="county" id="county">
                        <?php echo county_options() ?>
                    </select>
                </div>




                <div class="form-group col-lg-3">
                    <label>Sub-County</label>
                    <select id="sub_county" class="form-control" style="margin-left:0px;" name="sub_county">
                        <?php echo sub_county_options() ?>
                    </select>
                </div>




                <div class="form-group col-lg-3">
                    <label>Ward Options</label>
                    <select id="ward" class="form-control" style="margin-left:0px;" name="ward">
                        <?php echo ward_options() ?>
                    </select>
                </div>





            </div>






            <div class="row">

                <div class="form-group col-lg-3">
                    <label>Village</label>
                    <input class="village form-control" name="village" id="village" type="text" placeholder="Village">
                </div>



                <div class="form-group col-lg-3">
                    <label>Chief</label>
                    <input class="chief form-control" name="chief" id="chief" type="text" placeholder="Chief">
                </div>





                <div class="form-group col-lg-3">
                    <label>Assistant-Chief</label>
                    <input class="assistant_chief form-control" name="assistant_chief" id="assistant_chief" type="text" placeholder="Assistant-Chief">
                </div>




            </div>




            <div class="row">
                <div class="col-lg-4 col-lg-offset-1">
                    <button type="button" data-step="step1" title="Back to Step 1" class="btn right step_trigger"><i class="fa fa-chevron-left"></i> Previous Step </button>
                </div>
                <div class="col-lg-4">
                    <button type="button" data-step="step3" title="Move to Step 3" class="btn btn-primary right step_trigger"> Next Step <i class="fa fa-chevron-right"></i></button>
                </div>
            </div>




        </div>
        <div id="step3" class="tab-pane fade">



            <h5>Next of Kin Information</h5>
            <div class="row">


                <div class="form-group col-lg-3">
                    <label>Name</label>
                    <input spellcheck="false" type="text" name="next_of_kin_name" class="form-control"
                           placeholder="Full Name">
                </div>



                <div class="form-group col-lg-3">
                    <label>Relationship</label>
                    <input type="text" name="relationship" class="form-control"
                           placeholder="Relationship">
                </div>


                </div>



            <div class="row">

                <div class="form-group col-lg-3">
                    <label>Phone Number</label>
                    <input type="text" name="kin_phone" class="form-control"
                           placeholder="Phone Number">
                </div>


                <div class="form-group col-lg-3">
                    <label>Address</label>
                    <input type="text" spellcheck="false" name="address" class="form-control"
                           placeholder="Address"/>
                </div>

            </div>




            <div class="row">



                <div class="form-group col-lg-4" id="extra_details">
                    <label>Other useful information (optional)</label>
                    <br>
                    <textarea title="Other useful information" name="extra_details" id="extra_details_input" cols="90"
                              rows="7"></textarea>

                </div>



            </div>



            <div class="row">

                <div class="row">
                    <div class="col-lg-4">
                        <button type="button" data-step="step2" title="Back to Step 2" class="btn right step_trigger"><i class="fa fa-chevron-left"></i> Previous Step </button>
                    </div>
                    <div class="col-lg-4">
                        <button type="submit" title="Submit Recruit Details" class="btn btn-primary right"> Submit Form <i class="fa fa-send"></i></button>
                    </div>
                </div>

            </div>



        </div>
    </div>


                </form>




        </div>
    </div>
















</div>

<script>$(".smart_select").select2();</script>

<script> $(".new-recruit_menu").addClass('active');


    function load_level_3(){
        var sub_county = $("#sub_county").val();
        $.post('_ajax', {sub_county:sub_county,intent:'get_wards'},
            function (options) {
                $("#ward").html(options);
            });

    }


    function load_level_2(){
        var county = $("#county").val();
        $.post('_ajax', {county:county,intent:'get_sub_counties'},
            function (options) {
                $("#sub_county").html(options);
            });
        load_level_3();
    }


    load_level_2();

    load_level_3();


    $(document).on("change", "#county", function (e) {

        load_level_2();

    });

    $(document).on("change", "#sub_county", function (e) {

        load_level_3();

    });





    $(document).on("click", ".step_trigger", function (e) {

        e.preventDefault();
        var step_label = $(this).data('step');



        $("."+step_label).click();

    });



   var step_label = 'step2';
    $("."+step_label).click();


    $(document).on("click", "#step_1", function () {


        var kra_pin = $("input[name=kra_pin]").val();

        var kra_pin_len = kra_pin.length;



        var national_id = $("input[name=national_id]").val();

        var national_id_len = national_id.length;


        if(national_id_len<8){

            alert('Incorrect National ID');
        }






    });



    $(document).on("change", ".salutation_option", function () {
        var id = $(this).attr('id');
        var check = $(this).prop('checked');

        if((id=='mr' || id=='sir' || id=='mrs' || id=='ms') && check==true){
            $("#mr").prop('checked',false);
            $("#sir").prop('checked',false);
            $("#mrs").prop('checked',false);
            $("#ms").prop('checked',false);
            $("#"+id).prop('checked',true);
        }

    });


    function loadHousingOptions(){
        var gender = $("#gender").val();

        $.post('_ajax', {gender:gender,housing_options:'get'},
            function (options) {
                $("#housing").html(options);
            });

    }

    loadHousingOptions();

    $(document).on("change", "#gender", function () {
        loadHousingOptions();

    });

    $(document).on("change", "#phy_imp", function () {
        var option_value = $(this).val();

        if (option_value == 'yes') {
            $("#phy_imp_explain_div").show();
        } else {
            $("#phy_imp_explain_div").hide();
        }

    });


    var gur_1 = $("#phy_imp");

    if (gur_1 == 'yes') {
        $("#phy_imp_explain_div").show();
    } else {
        $("#phy_imp_explain_div").hide();
    }


    $(document).on("change", "#guardian_details", function () {
        var option_value = $(this).val();

        if (option_value == 'yes') {
            $("#guardian_details_div").show();
        } else {
            $("#guardian_details_div").hide();
        }

    });

    var gur_ = $("#guardian_details");

    if (gur_ == 'yes') {
        $("#guardian_details_div").show();
    } else {
        $("#guardian_details_div").hide();
    }





    var current_fs, next_fs, previous_fs;
    var left, opacity, scale;
    var animating;

    $(".next").click(function(){
        if(animating) return false;
        animating = true;

        current_fs = $(this).parent();
        next_fs = $(this).parent().next();

        $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

        next_fs.show();
        current_fs.animate({opacity: 0}, {
            step: function(now, mx) {
                scale = 1 - (1 - now) * 0.2;
                left = (now * 50)+"%";
                opacity = 1 - now;
                current_fs.css({
                    'transform': 'scale('+scale+')',
                    'position': 'absolute'
                });
                next_fs.css({'left': left, 'opacity': opacity});
            },
            duration: 800,
            complete: function(){
                current_fs.hide();
                animating = false;
            },
            easing: 'easeInOutBack'
        });
    });

    $(".previous").click(function(){
        if(animating) return false;
        animating = true;

        current_fs = $(this).parent();
        previous_fs = $(this).parent().prev();

        $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

        previous_fs.show();
        current_fs.animate({opacity: 0}, {
            step: function(now, mx) {
                scale = 0.8 + (1 - now) * 0.2;
                left = ((1-now) * 50)+"%";
                opacity = 1 - now;
                current_fs.css({'left': left});
                previous_fs.css({'transform': 'scale('+scale+')', 'opacity': opacity});
            },
            duration: 800,
            complete: function(){
                current_fs.hide();
                animating = false;
            },
            easing: 'easeInOutBack'
        });

    });





</script>
<div class="row">&nbsp;&nbsp;&nbsp;</div>
<div class="row">&nbsp;&nbsp;&nbsp;</div>
<div class="row">&nbsp;&nbsp;&nbsp;</div>
<div class="row">&nbsp;&nbsp;&nbsp;</div>
<?php include '../_footer.php'; ?>
<script src='../assets/js/datepicker/jquery-ui.min.js'></script>

<script src='../assets/js/datepicker/moment.min.js'></script>

<script src='../assets/js/datepicker/bootstrap-datetimepicker.min.js'></script>


<script>


    $('.date_picker').datetimepicker({
        widgetPositioning: {
            horizontal: 'right'
        },
        debug: false,
        viewMode: 'years',
        maxDate: new Date(),
        format: 'DD-MM-YYYY'
    });

</script>
