<?php
include ("db_conn.php");

function cleaN($string){
    return mysql_real_escape_string($string);
}


class Cry {
    var $skey = "#7%+xQHa|oGs^hx61oxS_pR>VPSqz1X";

    public  function encode($value){
        if(!$value){return false;}
        $text = $value;
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $crypttext = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $this->skey, $text, MCRYPT_MODE_ECB, $iv);
        return trim($this->safe_b64encode($crypttext));
    }

    public  function safe_b64encode($string) {
        $data = base64_encode($string);
        $data = str_replace(array('+','/','='),array('-','_',''),$data);
        return $data;
    }

    public function decode($value){
        if(!$value){return false;}
        $crypttext = $this->safe_b64decode($value);
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $decrypttext = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $this->skey, $crypttext, MCRYPT_MODE_ECB, $iv);
        return trim($decrypttext);
    }

    public function safe_b64decode($string) {
        $data = str_replace(array('-','_'),array('+','/'),$string);
        $mod4 = strlen($data) % 4;
        if ($mod4) {
            $data .= substr('====', $mod4);
        }
        return base64_decode($data);
    }
}

function pass_encode($plain_password){
    include '_variables.php';
    $volt = md5(md5($password_key));
    $pps = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($password_key), $plain_password, MCRYPT_MODE_CBC, $volt);
    return base64_encode($pps);
}

function pass_decode($pro_password){
    include '_variables.php';
    $volt = md5(md5($password_key));
    $dps = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($password_key), base64_decode($pro_password), MCRYPT_MODE_CBC, $volt);
    return rtrim($dps, "");
}


function enc_id($id){
    include '_variables.php';
    $u_id=(double)$id * $url_prime_number;
    $id=base64_encode($u_id);
    $id = str_replace('==','__',$id);
    $id = str_replace('=','_',$id);
    return $id;
}

function dec_id($en_id){
    include '_variables.php';
    $idx=str_replace('_','=',$en_id);
    $idx=str_replace('__','==',$en_id);
    $u_id=base64_decode($idx);
    $dec=(double)$u_id / $url_prime_number;
    return $dec;
}

function mnr($res) {
    return mysql_num_rows($res);
}

function msoc($res) {
    return mysql_fetch_assoc($res);
}

function mray($res) {
    return mysql_fetch_array($res);
}

function md($res) {
    return mysql_result($res,0);
}

function m($res) {
    return mysql_query($res);
}

function sani($unclean_data){
    return htmlspecialchars(mysql_real_escape_string($unclean_data));
}

function get_ip(){
    if ( isset($_SERVER['HTTP_CLIENT_IP']) && ! empty($_SERVER['HTTP_CLIENT_IP'])) {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif ( isset($_SERVER['HTTP_X_FORWARDED_FOR']) && ! empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip = (isset($_SERVER['REMOTE_ADDR'])) ? $_SERVER['REMOTE_ADDR'] : '0.0.0.0';
    }
    $ip = filter_var($ip, FILTER_VALIDATE_IP);
    $ip = ($ip === false) ? '0.0.0.0' : $ip;
    return $ip;
}



function rand_string($len) {
    return substr(str_shuffle("abcde123456789fghjklmnpqrstuvwxyz"), 0, $len);
}

function rand_letters($len) {
    return substr(str_shuffle("abcdefghjklmnpqrstuvwxyz"), 0, $len);
}
function rand_number($len) {
    return substr(str_shuffle("1234567890"), 0, $len);
}


function account_name(){
    include '_variables.php';

    return md("SELECT name FROM accounts WHERE account_type = '".$act."' AND account_id = $tid");

}

function avatar(){
    include ('_variables.php');
    if($act=='student'){
        $avatar= student_avatar($tid);
    }elseif($act=='staff'){
        $avatar=staff_avatar($tid);
    }elseif($act=='admin'){
        $avatar='admin.png';
    }elseif($act=='guardian'){
        $avatar='guardian.png';
    }elseif($act=='finance'){
        $avatar='admin.png';
    }elseif($act=='store'){
        $avatar='admin.png';
    }elseif($act=='registry-staff'){
        $avatar='admin.png';
    }elseif($act=='optional_payments'){
        $avatar='admin.png';
    }else{
        $avatar='admin.png';

    }
    return $avad.$avatar;
}


//function avatar($account_id){
//    $avad = '';
//    $default_profile_pic = '';
//    include '_variables.php';
//    $image = md(m("SELECT avatar FROM accounts WHERE id = '" . $account_id . "'"));
//    $image_ = $avad . $image;
//    // return $image;
//    if (file_exists($image_)) {
//        return $image_;
//    } else {
//        return $avad . $default_profile_pic;
//    }
//}

function theme_box(){
    ?>
    <div class="theme-box">
        <a data-toggle="tooltip" title="Navy Blue" class="theme-color" style="background:#323447" id="default"></a>
        <a data-toggle="tooltip" title="Calm" class="theme-color" style="background:#9092C3" id="skin-1"></a>
        <a data-toggle="tooltip" title="Safe Grass" class="theme-color" style="background:#93C865" id="skin-2"></a>
        <a data-toggle="tooltip" title="Simple" class="theme-color" style="background:#3e6b96" id="skin-3"></a>
        <a data-toggle="tooltip" title="Choco" class="theme-color" style="background:#635247" id="skin-4"></a>
        <a data-toggle="tooltip" title="Pitch Black" class="theme-color" style="background:#3a3a3a" id="skin-5"></a>
        <a data-toggle="tooltip" title="Different" class="theme-color" style="background:#D6756F" id="skin-6"></a>
        <a data-toggle="tooltip" title="Lilac" class="theme-color" style="background:#A67ADC" id="skin-7"></a>
        <a data-toggle="tooltip" title="Pink Bit" class="theme-color" style="background:#F5859D" id="skin-8"></a>
        <a data-toggle="tooltip" title="Orange Beetroot" class="theme-color" style="background:#F9AF58" id="skin-9"></a>
        <a data-toggle="tooltip" title="Light Blue" class="theme-color" style="background:#3BAFDA" id="skin-10"></a>
        <a data-toggle="tooltip" title="White & Green" class="theme-color" style="background:#356934" id="skin-11"></a>
    </div>
    <?php
}

function getname(){
    include '_variables.php';
    if($act=='registry-staff'){
        $got_name= student_name($tid);
    }elseif($act=='staff'){
        $got_name= staff_name($tid);
    }elseif($act=='admin'){
        $got_name='System Admin';
    }elseif($act=='finance'){
        $got_name='Finance Account';
    }elseif($act=='guardian'){
        $got_name= guardian_name($tid);
    }elseif($act=='store'){
        $got_name= 'Store';
    }elseif($act=='dlib'){
        $got_name= 'Digital Librarian';
    }elseif($act=='optional_payments'){
        $got_name= 'Optional Payments';
    }
    return $got_name;
}

function account_type(){
    $acc_types = array('Admin', 'Chief Instructor','Class Coordinator', 'Exam Coordinator', 'Instructor', 'Library', 'Registry', 'Store', 'QM', 'Registry Staff');
    foreach($acc_types as $type){
        ?>
        <option value="<?php echo $type?>"><?php echo $type?></option>
        <?php
    }
}

function designation(){
    $desig=m("SELECT designation_id,name FROM designations");
    while($fet_sc=msoc($desig)){
        $sscid=$fet_sc['designation_id'];
        $ssctit=$fet_sc['name'];
        ?>
        <option value='<?php echo $sscid?>'><?php echo $ssctit?></option>
    <?php }
}

function designation_name($id){
    $desgin = md(m("SELECT name FROM designations WHERE designation_id = '".$id."'"));
    return $desgin;
}

function department(){
    $dept=m("SELECT department_id,name FROM departments");
    while($get_dept=msoc($dept)){
        $deptt=$get_dept['department_id'];
        $depttn=$get_dept['name'];
        ?>
        <option value='<?php echo $deptt?>'><?php echo $depttn?></option>
    <?php }
}

function status(){
    $stats = array('Active', 'Inactive');
    $val=0;
    foreach($stats as $stat){
        $val++
        ?>
        <option value="<?php echo $val;?>"><?php echo $stat;?></option>
        <?php
    }
}

function status_label($assign_label)
{
    if ($assign_label == 1) {
        $label = ('Active');
    } else {
        $label = ('Inactive');
    }
    return $label;
}

function assigned_in(){
    $asgns = array('Class', 'Field');
    $val = 0;
    foreach($asgns as $asgn){
        $val ++;
        ?>
        <option value="<?php echo $val?>"><?php echo $asgn?></option>
   <?php }
}
function assign_label($assign_label)
{
    if ($assign_label == 1) {
        $label = ('Class');
    } else {
        $label = ('Field');
    }
    return $label;
}

function company_options(){
    $comps = m("SELECT company_id, company_name FROM companies");
    while($get_comps = msoc($comps)){
        $compid = $get_comps['company_id'];
        $compnm = $get_comps['company_name'];
        ?>
        <option value='<?php echo $compid?>'><?php echo $compnm?></option>
<?php    }
}

function companies($id){
    return $comp = md(m("SELECT company_name FROM companies WHERE company_id = '".$id."'"));
}

function instructors(){
    $inst = m("SELECT instructor_id, surname, first_name, other_name, assigned FROM instructors");
    while($get_details = msoc($inst)){
//        $inst_id = $get_details['instructor_id'];
        $inst_nm = $get_details['surname'].' '.$get_details['first_name'].' '.$get_details['other_name'].'--'.assign_label($get_details['assigned']);
        ?>
<option value="<?php echo $inst_nm?>"><?php echo $inst_nm?></option>
<?php    }
}

function task_add($user,$task){
    m("INSERT INTO do_lists SET account = '".$user."',task = '".$task."'");
    return mysql_insert_id();
}

function tasks_display($user){
    $r_task=m("SELECT id,task,status FROM do_lists WHERE account = '".$user."' ORDER BY id DESC");
    $ki='';
    while($tasks=msoc($r_task)){
        $task_status=$tasks['status'];
        $task_detail = $tasks['task'];
        $task_id = $tasks['id'];
        if($task_status=='7'){$selection='selected';$check_k='checked';}else{$selection='';$check_k='';}
        $ki.="<li id='$task_id' class='list-group-item $selection'>
                <label title='Mark as done' class='label-checkbox inline'>
                    <input data-did='$task_id' type='checkbox' $check_k class='task-finish'>
                    <span class='custom-checkbox'></span>
                </label>
                    $task_detail
                <span class='pull-right' title='delete task'>
                    <a data-id='$task_id' href='#' class='task-del'><i class='fa fa-trash-o fa-lg text-danger'></i></a>
                </span>
              </li>
    ";

    }
    return $ki;
}

function task_delete($user,$task_id){
    m("DELETE FROM do_lists WHERE id = '".$task_id."' AND account ='".$user."'");
}

function task_done($user,$task_id){
    m("UPDATE do_lists SET status = '7' WHERE account ='".$user."' AND id = '".$task_id."'");
}

function task_undo($user,$task_id){
    m("UPDATE do_lists SET status = '1' WHERE account ='".$user."' AND id = '".$task_id."'");
}

function get_platoons($id){
    return mnr(m("SELECT platoon_id FROM platoon WHERE company_id = '".$id."'"));
}

function count_instructors(){
    return mnr(m("SELECT instructor_id FROM instructors WHERE status = 1"));
}

function instructor_population()
{
    return mnr(m("SELECT instructor_id FROM instructors WHERE status=1"));
}

function count_recruits(){
    return mnr(m("SELECT recruit_id FROM recruits WHERE status = 1"));
}

function count_platoons(){
    return mnr(m("SELECT platoon_id FROM platoon WHERE status = 1"));
}

function company_name($company_id){
    return md(m("SELECT company_name FROM companies WHERE company_id = $company_id"));

}

function platoon_company_id($platoon_id){

    return md(m("SELECT company_id FROM platoon WHERE platoon_id = $platoon_id"));

}

function store_get_item_id_give($item_lead,$number_of_items){

    $item_label=md(m("SELECT label FROM store_items WHERE id = '".$item_lead."'"));

    $id_items=array();
    $mqi=m("SELECT id FROM store_items WHERE label = '".$item_label."' AND state = 1 LIMIT $number_of_items");

    while($rpg=msoc($mqi)){

        $id_items[]=$rpg['id'];

    }

    return $id_items;
}

function count_items_out($label){
    $m=m("SELECT id FROM non-consumables WHERE label = '".$label."' AND state = 0");
    return mnr($m);
}

function count_items_in($label){
    $m=m("SELECT id FROM store_items WHERE label = '".$label."' AND state = 1");
    return mnr($m);
}

function store_item_condition(){
    $conds=array('New','Old');
    foreach($conds as $cond){
        echo "<option value='$cond'>$cond</option>";
    }
}

function store_unit_options(){
    $suos = array('Pieces', 'Packets', 'Boxes', 'Kgs', 'Grams', 'Milligrams', 'Litres');
    foreach ($suos as $unit){
        echo "<option value='$unit'>$unit</option>";
    }
}

function store_item_type(){
    $itps=array('Consumables','Non-Consumables');
    foreach($itps as $item_type){
        echo "<option  value='$item_type'>$item_type</option>";
    }
}

function item_type_label($assign_label)
{
    if ($assign_label == 0) {
        $label = ('Consumables');
    } else {
        $label = ('Non-Consumables');
    }
    return $label;
}

function store_give_out_options(){
    $s_tar = array('Recruit', 'Instructor');
    $val=0;
    foreach($s_tar as $stg){
        $val++
        ?>
        <option value="<?php echo $val;?>"><?php echo strtoupper($stg);?></option>
        <?php
    }
}

function initial_consumable_count($id)
{
    return md(m("SELECT quantity FROM consumables WHERE id = '" . $id . "'"));
}

function taken_consumable_count($id)
{
    $k = m("SELECT SUM(quantity_taken) AS total FROM consumable_movement WHERE consumable_id = '" . $id . "'");
    $cc = mysql_fetch_array($k);
    return $cc['total'];

}

function consumable_threshold_level($consumable_id)
{
    return md(m("SELECT threshold FROM consumables WHERE id = '" . $consumable_id . "'"));
}


function count_remaining_consumable($id)
{
    $initial = intval(initial_consumable_count($id));

    $total = intval(taken_consumable_count($id));


    return $initial - $total;

}

function consumable_item_category(){
    $cats = m("SELECT id, category_name FROM consumable_categories WHERE status = 1");

    while($getcat = msoc($cats)){
        $catid = $getcat['id'];
        $catname = $getcat['category_name'];
        ?>
        <option value="<?php echo $catid?>"><?php echo $catname?></option>
        <?php
    }
}

function get_category_label($category_id)
{
    return md(m("SELECT category_name FROM item_categories WHERE id = '" . $category_id . "'"));
}

function non_consumable_item_category(){
    $cats = m("SELECT id, category_name FROM non_consumable_categories WHERE status = 1");

    while($getcat = msoc($cats)){
        $catid = $getcat['id'];
        $catname = $getcat['category_name'];
        ?>
        <option value="<?php echo $catid?>"><?php echo $catname?></option>
        <?php
    }
}

/*NEO'S*/

function platoon_name($platoon_id)
{
    return md(m("SELECT platoon_name FROM platoon WHERE platoon_id = $platoon_id"));
}

function tribe_options($suffix_text = '')
{
    $tribes = [];
    include('_variables.php');

    $id = 0;
    foreach ($tribes as $tribe => $data) {
        $id++;
        ?>
        <option value="<?php echo $data['id'] ?>"><?php echo $data['tribe_name'].$suffix_text;?></option>
        <?php
    }
}

function read_tribe($tribe_id)
{
    return md(m("SELECT tribe_name FROM tribes WHERE id = $tribe_id"));

}

function barrack_options($selected_id='')
{
    $platoons = [];
    include('_variables.php');

    $comp = m("SELECT * FROM barracks WHERE status = 1");
    $opt ='';
    while($company = mray($comp)){

        $barrack_id = $company['barrack_id'];
        $barrack_name = $company['barrack_name'];

            if($selected_id===$barrack_id){
                $opt.="<option selected='selected' value='$barrack_id'>$barrack_name</option>";

            }else{
                $opt.="<option value='$barrack_id'>$barrack_name</option>";

            }



    }



    return $opt;
}





function platoon_options($selected_id='')
{
    $platoons = [];
    include('_variables.php');

    $comp = m("SELECT company_id,company_name FROM companies WHERE status = 1");
    $opt ='';
    while($company = mray($comp)){

        $company_id = $company['company_id'];

        $plat = m("SELECT platoon_name,platoon_id FROM platoon WHERE company_id = $company_id");

        while($platoon = mray($plat)){

            $platoon_id = $platoon['platoon_id'];
            $platoon_name = $platoon['platoon_name'].' ('.company_name($company_id).')';

            if($selected_id===$platoon_id){
                $opt.="<option selected='selected' value='$platoon_id'>$platoon_name</option>";

            }else{
                $opt.="<option value='$platoon_id'>$platoon_name</option>";

            }



        }

    }



    return $opt;
}

function json_PL_Platoon_unassigned()
{

    $recruits = m("SELECT * FROM recruits WHERE status =1 AND platoon_id = 0");
    $recruits_data = array();
    $string = '';
    while ($data = mray($recruits)) {

        $id = $data['recruit_id'];
        $tribe_id = $data['tribe_id'];

        $tribe_name = sani(read_tribe($tribe_id));

        $name = $data['surname'] . ' ' . $data['first_name'] . ' ' . $data['other_name'] . '---' . $tribe_name;
        $recruits_data[] = ['text' => $name, 'id' => $data['recruit_id']];


        $string .= " $id: {id: $id,text: '$name',tribe_id:'$tribe_id'},";

    }


    return rtrim($string, ',');
    $json = json_encode($recruits_data);

    $json = str_replace('[', '', $json);
    return str_replace(']', '', $json);

    // return str_replace(array('[', ']'), '', json_encode($recruits_data));
}


function barrack_name($barrack_id)
{
    return md(m("SELECT barrack_name FROM barracks WHERE  barrack_id = $barrack_id"));

}






function json_PL_Platoon($platoon_id)
{

    $recruits = m("SELECT * FROM recruits WHERE status =1 AND platoon_id = $platoon_id");
    $recruits_data = array();
    $string = '';
    while ($data = mray($recruits)) {

        $id = $data['recruit_id'];
        $tribe_id = $data['tribe_id'];

        $tribe_name = sani(read_tribe($tribe_id));

        $name = $data['surname'] . ' ' . $data['first_name'] . ' ' . $data['other_name'] . '---' . $tribe_name;
        $recruits_data[] = ['text' => $name, 'id' => $data['recruit_id']];


        $string .= " $id: {id: $id,text: '$name',tribe_id:'$tribe_id'},";

    }


    return rtrim($string, ',');
    /*$json = json_encode($recruits_data);

    $json = str_replace('[', '', $json);
    return str_replace(']', '', $json);*/

    // return str_replace(array('[', ']'), '', json_encode($recruits_data));
}



function json_for_pick_list()
{

    $recruits = m("SELECT * FROM recruits WHERE status =1");
    $recruits_data = array();
    $string = '';
    while ($data = mray($recruits)) {

        $id = $data['recruit_id'];
        $tribe_id = $data['tribe_id'];

        $tribe_name = sani(read_tribe($tribe_id));

        $name = $data['surname'] . ' ' . $data['first_name'] . ' ' . $data['other_name'] . '---' . $tribe_name;
        $recruits_data[] = ['text' => $name, 'id' => $data['recruit_id']];


        $string .= " $id: {id: $id,text: '$name',tribe_id:'$tribe_id'},";

    }


    return rtrim($string, ',');
    $json = json_encode($recruits_data);

    $json = str_replace('[', '', $json);
    return str_replace(']', '', $json);

    // return str_replace(array('[', ']'), '', json_encode($recruits_data));
}

function read_gender($short_name)
{
    $short_name = strtolower($short_name);
    $female_code = array('f', 'female', 'fe', 'fem', 'girl');
    //$fm=in_array($short_name,$female_code;
    if (in_array($short_name, $female_code)) {
        $gender = 'Female';
    } else {
        $gender = 'Male';

    }

    return $gender;
}

function age($dob){

    $dob=trim($dob);
    $explode=explode('-',$dob);
    $d=$explode[2];
    $m=$explode[1];
    $y=$explode[0];

    $newd=$y.'-'.$m.'-'.$d;

    if(checkdate($m,$d,$y)){

    }else{
        return 'Wrong Date Format';
    }

    $date = new DateTime($newd);
    $now = new DateTime();
    $interval = $now->diff($date);
    return $interval->y;
}

function read_religion($religion_id){
    $rel = array('Christian', 'Islam', 'Hinduism', 'N/A');

    return $rel[$religion_id];
}

function read_education_level($education_level){
    $ed = array('','KCSE', 'KCPE', 'Under-Graduate', 'Post-Graduate');
    return $ed[$education_level];
}

function county_label($county_id)
{
    $name= md(m("SELECT county_name FROM counties WHERE id = $county_id"));

    return ucwords(strtolower($name));

}


function sub_county_label($sub_county_id)
{
    $name= md(m("SELECT sub_county_name FROM sub_counties WHERE id = $sub_county_id"));
    return ucwords(strtolower($name));

}


function ward_label($ward_id)
{
    $name =  md(m("SELECT ward_name FROM wards WHERE id = $ward_id"));
    return ucwords(strtolower($name));
}

function read_marital_status($marital_status_id){
    $values = array('','Married', 'Single');
    return $values[$marital_status_id];
}
/*after compilation NEO*/
function recruit_population()
{
    return mnr(m("SELECT recruit_id FROM recruits WHERE status=1"));
}

function recruit_population_discontinued()
{
    return mnr(m("SELECT recruit_id FROM recruits WHERE status=5"));
}

function platoon_count()
{
    return mnr(m("SELECT platoon_id FROM platoon"));
}

function recruit_id_generator()
{
    $getlast_id = md(m("SELECT sto_number FROM recruits ORDER BY recruit_id DESC LIMIT 1"));

    return trailing_zeros($getlast_id + 1);
}


function trailing_zeros($int)
{
    return sprintf('%04d', $int);
}

function remove_trailing_zeros($value)
{

    return intval($value);
}

function salutation_options()
{
    $sal = array('Mr', 'Ms', 'Mrs');
    $id = 0;
    foreach ($sal as $sal_) {
        $id++;
        ?>

            <label class="label-checkbox">
                <input class="nn" name="salutation[]" value="<?php echo $id ?>" type="radio">
                <span class="custom-radio"></span>
                <?php echo $sal_; ?>
            </label>


        <?php
    }
}

function recruit_terms()
{
    $values = array('Probation', 'Permanent');
    $id = 0;

    foreach ($values as $value) {
        $id++;
        ?>
        <option value="<?php echo $id ?>"><?php echo $value ?></option>
        <?php
    }
}

function education_level()
{
    $ed = array('KCSE', 'KCPE', 'Under-Graduate', 'Post-Graduate');
    $id = 0;
    foreach ($ed as $edu) {
        $id++;
        ?>
        <option value="<?php echo $id ?>"><?php echo $edu ?></option>
        <?php
    }
}

function gender_options()
{
    $gdr = array('Female', 'Male');
    $id = 0;
    foreach ($gdr as $gender) {
        $id++;
        ?>
        <option value="<?php echo $id; ?>"><?php echo $gender ?></option>
        <?php
    }
}

function religion_options()
{
    $rel = array('Christian', 'Islam', 'Hinduism', 'N/A');
    $id = 0;
    foreach ($rel as $relig) {
        $id++;
        ?>
        <option value="<?php echo $relig ?>"><?php echo $relig ?></option>
        <?php
    }
}

function county_options()
{
    $counties = [];
    include('_variables.php');

    $id = 0;
    foreach ($counties as $county => $data) {
        $id++;
        ?>
        <option value="<?php echo $data['id'] ?>"><?php echo ucwords($data['county_name']) ?></option>
        <?php
    }
}

function sub_county_options()
{
    $sub_counties = [];
    include('_variables.php');

    $id = 0;
    foreach ($sub_counties as $sub_county => $data) {
        $id++;

        ?>
        <option value="<?php echo $data['id'] ?>"><?php echo ucwords($data['sub_county_name']) ?></option>
        <?php
    }
}

function ward_options()
{
    $wards = [];
    include('_variables.php');

    $id = 0;
    foreach ($wards as $ward => $data) {
        $id++;
        ?>
        <option value="<?php echo $data['id'] ?>"><?php echo ucwords($data['ward_name']) ?></option>
        <?php
    }
}

function stamp_time($timestamp)
{
    $timed = strtotime($timestamp);
    return date('g:i A', $timed);
}

function stamp_date_time($timestamp)
{
    $timed = strtotime($timestamp);
    return date('M jS Y (l) g:iA', $timed);
}

function stamp_date($timestamp)
{
    $timed = strtotime($timestamp);
    return date('M jS Y (l)', $timed);
}

function stamp_date_clean($timestamp)
{
    $timed = strtotime($timestamp);
    return date('M jS Y', $timed);
}

function stamp_date_time_now()
{
    $timed = time();
    return date('M jS Y g:iA', $timed);
}

function stamp_date_time_now_full()
{
    $timed = time();
    return date('M jS Y (l) g:iA', $timed);
}

function stamp_date_now_full()
{
    $timed = time();
    return date('M jS Y (l)', $timed);
}

function stamp_date_time_simple($timestamp)
{
    $timed = strtotime($timestamp);
    return date('M jS y g:iA', $timed);
}

function read_registry_staff_permission_label($permission_id){
    $registry_permissions = '';
    include ('_variables.php');
    $permission_id= $permission_id-1;
    return $registry_permissions[$permission_id];

}

function read_registry_staff_permission_icon($permission_id){
    $registry_permissions_icons = '';
    include ('_variables.php');
    $permission_id= $permission_id-1;
    return $registry_permissions_icons[$permission_id];

}

function read_registry_staff_permissions($account_id){

    $q = m("SELECT permission FROM registry_permission WHERE account_id = $account_id AND status =1");

    $perms = [];
    while($ray = msoc($q)){

        $permission_id = $ray['permission'];
        $permission_label = read_registry_staff_permission_label($permission_id);
        $perms[] = $permission_label;

    }

    return implode(',</br>',$perms);
}


function read_registry_staff_permissions_array($account_id){

    $q = m("SELECT permission FROM registry_permission WHERE account_id = $account_id AND status =1");

    $perms = [];
    while($ray = msoc($q)){

        $permission_id = $ray['permission'];
        $perms[] = $permission_id;

    }

    return $perms;
}

function fetch_access_id($pf_number)
{
    return md(m("SELECT id FROM accounts WHERE username = $pf_number"));
}

function get_cleared_recruits($perm_type){

    $q = m("SELECT * FROM recruit_clearance WHERE clearance_type = $perm_type");

    $recruits = [];

    while($data = mray($q)){
        $recruits[] = $data['recruit_id'];
    }

    return $recruits;
}

function read_clearance_status($clearance_result){

    if(($clearance_result==TRUE) || ($clearance_result==1)){

        return "CLEARED";

    }else{
        return "NOT CLEARED";

    }

}

function read_clearance_status_color($clearance_result){

    if(($clearance_result==TRUE) || ($clearance_result==1)){

        return "CLEARED";

    }else{
        return "NOT CLEARED";

    }


}

function detect_clearance($recruit_id,$clearance_type){

    $q = m("SELECT cleared_by FROM recruit_clearance WHERE recruit_id = '".$recruit_id."' AND clearance_type = '".$clearance_type."' AND status = 1");

    $count = intval(mnr($q));

    if($count==1){
        return true;
    }else{
        return false;
    }

}

/*Meg Functions*/
function count_classes(){
    return mnr(m("SELECT id FROM classes WHERE status=1"));
}

function get_instructor_name($id){
    $got_instructor_name=msoc(m("SELECT surname,first_name FROM instructors WHERE instructor_id ='".$id."'"));
    return ucwords($got_instructor_name['surname'].' '.$got_instructor_name['first_name']);
}

function instructor_options(){
    $scs=m("SELECT instructor_id ,service_number,surname,first_name FROM instructors");
    while($fet_sc=msoc($scs)){
        $sscid=$fet_sc['instructor_id'];
        $ssctit=$fet_sc['surname'].' '.$fet_sc['first_name'];
        ?>
        <option value='<?php echo $sscid?>'><?php echo $ssctit?></option>
    <?php }
}

function class_options(){
    $clas=m("SELECT id,company_id,name FROM classes WHERE status = 1");
    while($class=msoc($clas)){
        $id=$class['id'];
        $comapany_id = $class['company_id'];
        $class_name = $class['name'];
        ?>
        <option value='<?php echo $id?>'><?php echo $class_name?></option>
        <?php
    }
}

function check_unit_options(){
    $sub=m("SELECT unit_id,unit_name,code FROM units WHERE status = 1");
    while($subj=msoc($sub)){
        $subjct_id = $subj['unit_id'];
        $subject_code = $subj['code'];
        $subject_title = $subj['unit_name'];
        ?>
        <div class="col-lg-10 margin-sm">
            <label class="label-checkbox">
                <input class="nn" name="unit_id[]" value="<?php echo $subjct_id;?>" type="checkbox">
                <span class="custom-checkbox"></span>
                <?php echo $subject_title;?>
            </label>
        </div>
        </br>
        <?php
    }
}

function unit_options(){
    $myunits=m("SELECT unit_id,unit_name,code FROM units WHERE status = 1");
    while($fet_sc=msoc($myunits)){
        $sscid=$fet_sc['unit_id'];
        $sscir=$fet_sc['unit_name'];
        ?>
        <option value='<?php echo $sscid?>'><?php echo $sscir;?></option>
    <?php }
}

function unit_name($unit_id){
    return md(m("SELECT unit_name FROM units WHERE id = '".$unit_id."'"));

}

function class_options_check(){
    $clas=m("SELECT id,company_id,name FROM classes WHERE status = 'on'");
     while($class=msoc($clas)){
        $id=$class['id'];
        $comapany_id = $class['company_id'];
        $class_name = $class['name'];
        ?>
        <div class="col-lg-10 margin-sm">
            <label class="label-checkbox">
                <input class="nn" name="class_id[]" value="<?php echo $id;?>" type="checkbox">
                <span class="custom-checkbox"></span>
                <?php echo $class_name;?>
            </label>
        </div>
        </br>
        <?php
     }
}

function class_year_options(){
   $clas=m("SELECT year FROM classes WHERE status = 1");
     while($class=msoc($clas)){
        $year = $class['year'];
        ?>
                <option value='<?php echo $year?>'><?php echo $year?></option>


        <?php
    }
}

function get_class_year($class_id){
    return md(m("SELECT year FROM classes WHERE id ='".$class_id."'"));
}

function class_label($class_id){

    $class_full_name=md(m("SELECT name FROM classes WHERE id = '".$class_id."'"));

    $refined_name=str_replace('CLASS','',$class_full_name);

    return trim($refined_name);
}

function make_class_tag($class_id,$class_year){
    $clabel=class_label($class_id);

        if($class_year==''){
            $class_year=get_class_year($class_id);
        }else{}

    return $clabel.'_'.$class_year;
}

function know_myclasses($instructor_id){
$q1=m("SELECT DISTINCT class_id FROM assign_units WHERE instructor_id = '".$instructor_id."'");
    while($result=msoc($q1)){
        $myc[]=$result['class_id'];
    }
    return $myc;
}

function myclasses($instructor_id){
    $data= know_myclasses($instructor_id);

   foreach($data as $class_id){
       $class_label=class_label($class_id);
        ?>
        <option value='<?php echo $class_id;?>'><?php echo $class_label;?></option>
    <?php }
}

function recruit_name($recruit_id){
    $feccn=m("SELECT surname,first_name,other_name FROM recruits WHERE sto_number= '".$recruit_id."'");
    $name=msoc($feccn);
    return $name['surname'].' '.$name['first_name'].' '.$name['other_name'];
}

function check_grade_submission($class_tag,$recruit_id,$unit_id){
    return md(m("SELECT exam_mark FROM grades WHERE unit_id = '".$unit_id."' AND class_tag = '".$class_tag."' AND recruit_id ='".$recruit_id."'"));
}

    function get_grade_remark($class_tag,$recruit_id,$unit_id){
    return md(m("SELECT remark FROM grades WHERE unit_id = '".$unit_id."' AND class_tag = '".$class_tag."' AND recruit_id ='".$recruit_id."'"));

}

function plu($item_count){
    if($item_count!=1){
        $pru='s';
    }else {$pru='';}
return $pru;
}

function grade($score) {
    if($score=='pending' || $score==''){return $score;}else{
        $score=number_format($score);
    }
    if($score>=80)
        return 'A';
    if($score>=75)
        return 'A-';
    if($score>=70)
        return 'B+';
    if($score>=65)
        return 'B';
    if($score>=60)
        return 'B-';
    if($score>=55)
        return 'C+';
    if($score>=45)
        return 'C';
    if($score>=40)
        return 'C-';
    if($score>=35)
        return 'D+';
    if($score>=30)
        return 'D';
    if($score>=25)
        return 'D-';
    if($score<25)
        return 'E';

}

function recruits_count(){
    return mnr(m("SELECT recruit_id FROM recruits WHERE status = 1"));
}


function cellColor($cells,$color){
    global $objPHPExcel;
    $objPHPExcel->getActiveSheet()->getStyle($cells)->getFill()
        ->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID,
            'startcolor' => array('rgb' => $color)
        ));
}


function cellAlignCenter($cell){
    global $objPHPExcel;
    $objPHPExcel->getActiveSheet()->getStyle($cell)->getAlignment()->applyFromArray(
        array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,)
    );

}

    function contains($needle, $haystack)
{
    return strpos($haystack, $needle) !== false;
}


    function sub_county_options_filtered($county_id)
{
    $sub_counties = [];
    include('_variables.php');

    $id = 0;
    foreach ($sub_counties as $sub_county => $data) {
        $id++;

        $data_county_id = $data['county_id'];
            if($data_county_id==$county_id){
        ?>
        <option value="<?php echo $data['id'] ?>"><?php echo ucwords($data['sub_county_name']) ?></option>
        <?php
        }
    }
}

function ward_options_filtered($sub_county_id)
{
    $wards = [];
    include('_variables.php');

    $id = 0;
    foreach ($wards as $ward => $data) {
        $id++;
          $data_sub_county_id = $data['sub_county_id'];
            if($data_sub_county_id==$sub_county_id){
        ?>
        <option value="<?php echo $data['id'] ?>"><?php echo ucwords($data['ward_name']) ?></option>
        <?php
    }
    }

}



    function json_barrack_unassigned()
{

    $recruits = m("SELECT * FROM recruits WHERE barrack_id = 0");
    $recruits_data = array();
    $string = '';
    while ($data = mray($recruits)) {

        $id = $data['recruit_id'];
        $tribe_id = $data['tribe_id'];

        $tribe_name = sani(read_tribe($tribe_id));

        $name = $data['surname'] . ' ' . $data['first_name'] . ' ' . $data['other_name'] . '---' . $tribe_name;
        $recruits_data[] = ['text' => $name, 'id' => $data['recruit_id']];
$gender = $data['gender'];

        $string .= " $id: {id: $id,text: '$name',tribe_id:'$tribe_id',gender:'$gender'},";

    }


    return rtrim($string, ',');
    $json = json_encode($recruits_data);

    $json = str_replace('[', '', $json);
    return str_replace(']', '', $json);

    // return str_replace(array('[', ']'), '', json_encode($recruits_data));
}


    function json_barrack_recruits($barrack_id)
{

    $recruits = m("SELECT * FROM recruits WHERE barrack_id = $barrack_id");
    $recruits_data = array();
    $string = '';
    while ($data = mray($recruits)) {

        $id = $data['recruit_id'];
        $tribe_id = $data['tribe_id'];

        $tribe_name = sani(read_tribe($tribe_id));

        $name = $data['surname'] . ' ' . $data['first_name'] . ' ' . $data['other_name'] . '---' . $tribe_name;
        $recruits_data[] = ['text' => $name, 'id' => $data['recruit_id']];


        $string .= " $id: {id: $id,text: '$name',tribe_id:'$tribe_id'},";

    }


    return rtrim($string, ',');
    $json = json_encode($recruits_data);

    $json = str_replace('[', '', $json);
    return str_replace(']', '', $json);

    // return str_replace(array('[', ']'), '', json_encode($recruits_data));
}
