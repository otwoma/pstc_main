<?php
/**
 * Created by PhpStorm.
 * User: Otwoma
 * Date: 4/18/2017
 * Time: 2:17 PM
 */

include '_header_ci.php';

?>
<div id="main-container">


    <script type="text/javascript" charset="utf-8">
        $(function() {
            $('#responsiveTable').dataTable();
        } );
    </script>

    <div class="padding-md">

        <div class="panel panel-default table-responsive">
            <div class="padding-sm font-16">
                Listing Instructors
                <a href="#formModal" role="button" data-toggle="modal" class="btn btn-info btn-small pull-right"><span class="fa fa-plus"></span> Add New Instructor</a>
            </div>

            <div class="seperator"></div><div class="seperator"></div>
            <table class="table table_stripped" id="responsiveTable">
                <thead>
                <tr>
                    <th width="" align="center"><span class="">Surname</span></th>
                    <th width="" align="center"><span class="">First Name</span></th>
                    <th width="" align="center"><span class="">Service Number</span></th>
                    <th width="" align="center"><span class="">Phone Number</span></th>
                    <th width="" align="center"><span class="">Designation</span></th>
                    <th width="" align="center"><span class="">Assignment</span></th>
                    <th width="" align="center"><span class=""></span></th>
                </tr>
                </thead>
                <tbody>
                <?php
                $list = "SELECT surname, first_name, service_number, phone, designation_id, assigned FROM instructors ORDER BY instructor_id";
                $listing = mysql_query($list);
                while($listing_result = mysql_fetch_array($listing)){
                    ?>
                    <tr>
                        <td align=""><?php echo $listing_result['surname']?></td>
                        <td align=""><?php echo $listing_result['first_name']?></td>
                        <td align=""><?php echo $instructor_id = $listing_result['service_number']?></td>
                        <td align=""><?php echo $listing_result['phone']?></td>
                        <td align=""><?php echo designation_name($listing_result['designation_id'])?></td>
                        <td align=""><?php echo assign_label($listing_result['assigned'])?></td>
                        <td align="left"> <a target="_blank" href="instructor_profile?td=<?php echo ($instructor_id)?>" class="std_pro_link" id="<?php echo $instructor_id;?>"> <button class="btn btn-primary "><span class="fa fa-user-circle-o"></span> View Profile</button></a></td>
                    </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>
        </div><!-- /panel -->
        <!-- /panel -->
    </div><!-- /.padding-md -->
</div><!-- /main-container -->

<div class="modal fade" id="formModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4>Adding New Instructor</h4>
            </div>
            <div class="modal-body">
                <form name="cla_gr" action="../_actions.php" method="POST" enctype="multipart/form-data">

                    <div class="modal-body">
                        <form action="" method="POST" enctype="multipart/form-data">

                            <div class="form-group">
                                <label>Surname</label></br>
                                <input type="text" name="surname" placeholder="Musila " class="form-control" required>
                            </div>

                            <div class="form-group">
                                <label>First Name</label></br>
                                <input type="text" name="first_name" placeholder="John " class="form-control" required>
                            </div>

                            <div class="form-group">
                                <label>Other Name</label></br>
                                <input type="text" name="other_name" placeholder="Mwene " class="form-control" required>
                            </div>

                            <div class="form-group">
                                <label>Service Number</label></br>
                                <input type="text" name="service_number" placeholder="gk18131318" class="form-control" maxlength="12" required>
                            </div>

                            <div class="form-group">
                                <label>Phone Number</label></br>
                                <input type="text" name="phone" placeholder="0721588965 " maxlength="10" class="form-control" required>
                            </div>

                            <div class="form-group">
                                <label>Address</label></br>
                                <input type="text" name="address" placeholder="77354-00654 " class="form-control" required>
                            </div>

                            <div class="form-group">
                                <label>Email Address</label></br>
                                <input type="text" name="email_address" placeholder="jm@gmail.com " class="form-control" required>
                            </div>

                            <div class="form-group">
                                <label>Designation</label>
                                <select class="form-control" style="margin-left:0px;" name="designation">
                                    <?php echo designation() ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Department</label>
                                <select class="form-control" style="margin-left:0px;" name="department">
                                    <?php echo department() ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="">Instructor Status</label>
                                <select style="margin-left:0px;" id="" class="form-control" name="status">
                                    <?php echo status() ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="">Instructor Assignment</label>
                                <select style="margin-left:0px;" id="" class="form-control" name="assigned">
                                    <?php echo assigned_in() ?>
                                </select>
                            </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary btn-sm" data-dismiss="modal" aria-hidden="true">Close</button>
                <button type="submit" class="btn btn-success btn-sm">Submit</button>
            </div>
        </div>
    </div>
    </form>
</div>

<script> $(".instructors_mu").addClass('active');</script>


<?php include '../_footer.php'?>

