<?php
/**
 * Created by PhpStorm.
 * User: Otwoma
 * Date: 4/18/2017
 * Time: 1:17 PM
 */

include "_header_ci.php";

?>

    <div id="main-container">

        <div class="main-header clearfix">
            <div class="page-title">
                <h3 class="no-margin">Dashboard <small>Control Panel</small></h3>
            </div>
        </div>

        <div class="padding-md">

            <div class="row">
                <div class="col-sm-6 col-md-3">
                    <div class="panel-stat3 bg-green">

                        <h1 class="m-top-none" id="student_count">
                            <?php echo count_recruits(); ?>
                        </h1>
                        <h5 class="bold">Recruit Population</h5>

                        <div class="stat-icon">
                            <i class="fa fa-graduation-cap fa-3x"></i>
                        </div>

                    </div>
                </div>
                <div class="col-sm-6 col-md-3">
                    <div class="panel-stat3 bg-blue">

                        <h1 class="m-top-none" id="staff_count">
                            <?php echo count_instructors(); ?>
                        </h1>
                        <h5 class="bold">Instructor Population</h5>

                        <div class="stat-icon">
                            <i class="fa fa-briefcase fa-3x"></i>
                        </div>

                    </div>
                </div>
                <div class="col-sm-6 col-md-3">
                    <div class="panel-stat3 bg-orange">

                        <h1 class="m-top-none" id="class_count">
<!--                            --><?php //echo count_classes(); ?>
                            40
                        </h1>
                        <h5 class="bold">Class Count</h5>

                        <div class="stat-icon">
                            <i class="fa fa-building-o fa-3x"></i>
                        </div>

                    </div>
                </div>

                <div class="col-sm-6 col-md-3">
                    <div class="panel-stat3 bg-cyan">

                        <h1 class="m-top-none" id="student_count">
                            <?php echo count_platoons(); ?>
                        </h1>
                        <h5 class="bold">Platoons</h5>

                        <div class="stat-icon">
                            <i class="fa fa-users fa-3x"></i>
                        </div>

                    </div>
                </div>
            </div>
        </div>


        <div class="shortcut-wrapper grey-container">
            <span class="font-16 padding-sm margin-sm grey">Quick Links <span
                    class="fa fa-angle-double-right"></span></span>


            <a href="#" class="shortcut-link">
					<span class="shortcut-icon">
						<i class="fa fa-calendar-o"></i>
					</span>
                <span class="text">Class Schedules</span>
            </a>
            <a href="#" class="shortcut-link">
					<span class="shortcut-icon">
						<i class="fa fa-calendar-o"></i>
					</span>
                <span class="text">Instructor Schedules</span>
            </a>

            <a href="#formModal" role="button" data-toggle="modal" class="shortcut-link">
                <span class="shortcut-icon">
                    <i class="fa fa-user-circle-o"></i>
                </span>
                <span class="text">Add Instructor</span>
            </a>

<!--            <a href="account" class="shortcut-link">-->
<!--                <span class="shortcut-icon">-->
<!--						<i class="fa fa-cogs"></i>-->
<!--                </span>-->
<!--                <span class="text">Account Settings</span>-->
<!--            </a>-->
        </div>

        <div class="padding-md">

            <div class="row">

                <div class="col-lg-4">
                    <div class="panel panel-default">
                        <div class="panel-heading panel-heading3 clearfix">
                            <span class="pull-left bold">
                               <i class="fa fa-user-o"></i> Access Instructor Profile
                            </span>
                        </div>

                        <div class="panel-body no-padding collapse in">
                            <ul class="list-group task-list no-margin collapse in">
                                <form action="../_actions" method="get">
                                    <div class="form-group margin-sm">
                                        <label>Instructor PF Number:</label>
                                        <input type="text" name="quick_ac_inst_pro" class="form-control"
                                               placeholder="PF Number" required></br>
                                        <button type="submit" class="butn butn-navy pull-right margin-sm"><span
                                                class="fa fa-angle-double-right"></span> See Profile
                                        </button>
                                    </div>
                                </form>
                            </ul>
                        </div>
                        <div class="loading-overlay">
                            <i class="loading-icon fa fa-refresh fa-spin fa-lg"></i>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="panel panel-default">
                        <div class="panel-heading panel-heading3 clearfix">
                            <span class="pull-left bold">
                               <i class="fa fa-graduation-cap"></i> Access Recruit Profile
                            </span>
                        </div>

                        <div class="panel-body no-padding collapse in">
                            <ul class="list-group task-list no-margin collapse in">
                                <form action="../_actions" method="get">
                                    <div class="form-group margin-sm">
                                        <label>Recruit Store Number:</label>
                                        <input type="text" name="quick_ac_std_pro" class="form-control"
                                               placeholder="Sto. Number" required></br>
                                        <button type="submit" class="butn butn-navy pull-right margin-sm"><span
                                                class="fa fa-angle-double-right"></span> See Profile
                                        </button>
                                    </div>
                                </form>
                            </ul>
                        </div>
                        <div class="loading-overlay">
                            <i class="loading-icon fa fa-refresh fa-spin fa-lg"></i>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="panel panel-default">
                        <div class="panel-heading clearfix">
                            <span class="pull-left bold">
                                <span class="text-success m-left-xs"><i class="fa fa-check"></i></span> To Do List
                            </span>
                            <ul class="tool-bar">
                                <li><a href="#do_listModal" role="button" data-toggle="modal"
                                       class="bg-success btn-small"><i class="fa fa-plus"></i> New Task</a>
                                </li>
                                <li><a href="#toDoListWidget" data-toggle="collapse"><i class="fa fa-arrow-up"></i><i
                                            class="fa fa-arrow-down"></i></a></li>
                            </ul>
                        </div>
                        <div class="panel-body no-padding collapse in" id="toDoListWidget">
                            <ul class="list-group task-list no-margin collapse in">

                                <span id="new_tasks_holder"></span>

                                <?php echo tasks_display($tid); ?>
                            </ul>
                        </div>
                        <div class="loading-overlay">
                            <i class="loading-icon fa fa-refresh fa-spin fa-lg"></i>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="panel panel-default">
                        <div class="loading-overlay">
                            <i class="loading-icon fa fa-refresh fa-spin fa-lg"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="do_listModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button id="close_modal" type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;</button>
                    <h4>New item</h4>
                </div>

                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <label>Task :</label>
                            <textarea spellcheck="false" required class="form-control input-sm sms_con" id="todo_item"
                                      name="list_item" style="height: 70px;"></textarea>
                        </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary btn-sm" data-dismiss="modal" aria-hidden="true">Close</button>
                    <button id="todo_add" type="button" class="btn butn-green btn-sm">Submit</button>
                </div>
            </div>
        </div>
        </form>
    </div>

    <div class="modal fade" id="formModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4>Adding New Instructor</h4>
                </div>
                <div class="modal-body">
                    <form name="cla_gr" action="../_actions.php" method="POST" enctype="multipart/form-data">

                        <div class="modal-body">
                            <form action="" method="POST" enctype="multipart/form-data">

                                <div class="form-group">
                                    <label>Surname</label></br>
                                    <input type="text" name="surname" placeholder="Musila " class="form-control" required>
                                </div>

                                <div class="form-group">
                                    <label>First Name</label></br>
                                    <input type="text" name="first_name" placeholder="John " class="form-control" required>
                                </div>

                                <div class="form-group">
                                    <label>Other Name</label></br>
                                    <input type="text" name="other_name" placeholder="Mwene " class="form-control" required>
                                </div>

                                <div class="form-group">
                                    <label>Service Number</label></br>
                                    <input type="text" name="service_number" placeholder="gk18131318" class="form-control" maxlength="12" required>
                                </div>

                                <div class="form-group">
                                    <label>Phone Number</label></br>
                                    <input type="text" name="phone" placeholder="0721588965 " maxlength="10" class="form-control" required>
                                </div>

                                <div class="form-group">
                                    <label>Address</label></br>
                                    <input type="text" name="address" placeholder="77354-00654 " class="form-control" required>
                                </div>

                                <div class="form-group">
                                    <label>Email Address</label></br>
                                    <input type="text" name="email_address" placeholder="jm@gmail.com " class="form-control" required>
                                </div>

                                <div class="form-group">
                                    <label>Designation</label>
                                    <select class="form-control" style="margin-left:0px;" name="designation">
                                        <?php echo designation() ?>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>Department</label>
                                    <select class="form-control" style="margin-left:0px;" name="department">
                                        <?php echo department() ?>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="">Instructor Status</label>
                                    <select style="margin-left:0px;" id="" class="form-control" name="status">
                                        <?php echo status() ?>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="">Instructor Assignment</label>
                                    <select style="margin-left:0px;" id="" class="form-control" name="assigned">
                                        <?php echo assigned_in() ?>
                                    </select>
                                </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary btn-sm" data-dismiss="modal" aria-hidden="true">Close</button>
                    <button type="submit" class="btn btn-success btn-sm">Submit</button>
                </div>
            </div>
        </div>
        </form>
    </div>


    <script>
        $(".dashboard_mu").addClass('active');

        setInterval(function () {
            $.post('_ajax', 'online_users=0',
                function (live_users) {
                    $("#holder_users_online").html(live_users);
                });
        }, 1000);
    </script>

<?php include '../_footer.php'; ?>