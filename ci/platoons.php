<?php
/**
 * Created by PhpStorm.
 * User: Otwoma
 * Date: 4/20/2017
 * Time: 6:23 PM
 */

include '_header_ci.php';

?>

<div id="main-container">


    <script type="text/javascript" charset="utf-8">
        $(function() {
            $('#responsiveTable').dataTable();
        } );
    </script>

    <div class="padding-md">

        <div class="panel panel-default table-responsive">
            <div class="padding-sm font-16">
                Listing Platoons
            </div>

            <div class="seperator"></div><div class="seperator"></div>
            <table class="table table-striped" id="responsiveTable2">
                <thead>
                <tr>
                    <th width="" align="left">#</th>
                    <th width="" align="center"><span class=""></span>Platoon Name</th>
                    <th width="" align="right"><span class=""></span>Company Name</th>
                    <th width="" align="right"><span class=""></span>Number of Recruits</th>
                    <th width="" align="right"><span class=""></span>Date Created</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $list = "SELECT platoon_id, platoon_name, company_id, created_at FROM platoon ORDER BY platoon_id ASC";
                $list_query = mysql_query($list);
                while($list_result = mysql_fetch_array($list_query)){
                    ?>
                    <tr>
                        <td align="left"><?php echo $list_result['platoon_id']?></td>
                        <td align="left"><?php echo ucwords($list_result['platoon_name']);?></td>
                        <td align="left"><?php echo companies($list_result['company_id'])?></td>
                        <td align="left">
                            <!--                                    --><?php //echo count_student_platoon($list_result['id'])?>
                        </td>
                        <td align="left"><?php echo $list_result['created_at']?></td>
                    </tr>
                    <?php
                }
                ?></tbody>
            </table>
        </div><!-- /panel -->
        <!-- /panel -->
    </div><!-- /.padding-md -->
</div><!-- /main-container -->

</div><!-- /main-container -->

<script> $(".platoons_mu").addClass('active');</script>


<?php include '../_footer.php'?>

