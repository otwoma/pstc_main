<?php include'_header.class_coordinator.php';
?>
<div id="main-container">

    <link rel="stylesheet" href="../assets/css/time_table_style.css" type="text/css" media="screen"/>
    <script type="text/javascript" src="../assets/js/redips-drag-min.js"></script>
    <script type="text/javascript" src="../assets/js/script.js"></script>

    <div class="padding-md">
        <div class="panel panel-default table-responsive">
            <div class="padding-sm font-16">
                <h1>Master Timetable</h1>
                <div class="button_container">
                    <button type="button" class="btn btn-primary" onclick="save()" title="Save timetable"><span class="fa fa-save"></span> Save Changes</button>
                </div>
                <div id="main_container">
                    <div id="drag">

                        <div id="left">
                            <table id="table1">
                                <colgroup>
                                    <col width="190"/>
                                </colgroup>
                                <tbody>
                                <tr><td class="trash" title="Drag Here to delete"><span class="fa fa-trash"></span> Trash Bin</td></tr>
                                <tr><td class="bla"></td></tr>
                                <?php units() ?>
                                <tr><td class="trash" title="Drag Here to delete"><span class="fa fa-trash"></span> Trash Bin</td></tr>
                                </tbody>
                            </table>
                        </div>

                        <div id="right">

                            <table id="table2">
                                <colgroup>
                                    <col width="50"/>
                                    <col width="100"/>
                                    <col width="100"/>
                                    <col width="100"/>
                                    <col width="100"/>
                                    <col width="100"/>
                                </colgroup>
                                <tbody>

                                <td class="mark blank">
                                    <input id="week" type="checkbox" title="Apply school subjects to the week"/>
                                    <input id="report" type="checkbox" title="Show subject report"/>
                                </td>

                                <?php

                                $get_days=school_days();
                                foreach($get_days as $day){
                                    echo " <td class='mark dark2'>$day</td>";

                                }
                                ?>


                                </tr>



                                <?php
                                $time_sp=array('7:45-9:45',);
                                $ix=0;

                                $classes=list_classes();

                                foreach($time_sp as $time){
                                    $jkp=0;
                                    foreach($classes as $class){
                                        $ix++;

                                        if($jkp==1){$time='';}
                                        $class_name=company_label($class);

                                        $hc=$time.' ('.$class_name.')';

                                        timetable($hc,$ix);
                                        $jkp=$jkp+1;
                                    }


                                }

                                echo '<tr>
                            <td class="mark break_time">9:45-10:30</td>
                            <td class="mark break" colspan="5">BREAK</td>
                        </tr>';

                                $ix=$ix+1;

                                $time_sp=array('10.30-12.30',);
                                foreach($time_sp as $time){
                                    $jkp=0;

                                    foreach($classes as $class){
                                        $ix++;


                                        if($jkp==1){$time='';}
                                        $class_name=company_label($class);

                                        $hc=$time.' ('.$class_name.')';

                                        timetable($hc,$ix);
                                        $jkp=$jkp+1;
                                    }

                                }

                                echo '<tr>
                            <td class="mark break_time">12:30-1:30</td>
                            <td class="mark break" colspan="5">LUNCH</td>
                        </tr>';

                                $ix=$ix+1;

                                $time_sp=array('1:30-3:30',);
                                foreach($time_sp as $time){
                                    $jkp=0;

                                    foreach($classes as $class){
                                        $ix++;
                                        if($jkp==1){$time='';}
                                        $class_name=company_label($class);

                                        $hc=$time.' ('.$class_name.')';

                                        timetable($hc,$ix);
                                        $jkp=$jkp+1;
                                    }

                                }
                                echo '<tr>
                                    <td class="mark break_time">3:30-4:00</td>
                                    <td class="mark break" colspan="5">BREAK</td>
                                </tr>';

                                $ix=$ix+1;

                                $time_sp=array('4:00-6:00',);
                                foreach($time_sp as $time){
                                    $jkp=0;

                                    foreach($classes as $class){
                                        $ix++;
                                        if($jkp==1){$time='';}
                                        $class_name=company_label($class);

                                        $hc=$time.' ('.$class_name.')';

                                        timetable($hc,$ix);
                                        $jkp=$jkp+1;
                                    }

                                }
                                ?>


                                </tbody>
                            </table>
                        </div>
                    </div>
                    <br/>
                    <div id="message"></div>
                    <div class="button_container">
                        <button type="button" class="btn btn-primary" onclick="save()" title="Save timetable"><span class="fa fa-save"></span> Save Changes</button>
                    </div>
                </div>

            </div>
            <div class="seperator"></div><div class="seperator"></div>

        </div>
    </div>
</div>
<script> $(".timetable_mu").addClass('active');</script>

<?php include'../_footer.php';?>