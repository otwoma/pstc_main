
<?php include '_header.class_coordinator.php';

$clax=mysql_real_escape_string($_REQUEST['class_id']);
$subj=mysql_real_escape_string($_REQUEST['year']);
$tgg_term = 1;
$tgg_year = 2017;
?>

<div id="main-container">
    <script type="text/javascript" charset="utf-8">
        $(function() {
            $('#responsiveTable').dataTable();
        } );
    </script>
    <div class="padding-md">
        <div align="center" class="panel-body">
            <form action="" method="get" class="form-inline no-margin">
                <div class="form-group">
                    <label class="sr-only">Classes</label>
                    <select id="class_id" class="form-control inline-block" name="class_id">
                        <?php
                        echo class_options();
                        ?>
                    </select>
                </div>
                <div class="form-group">
                    <label class="sr-only">Subjects</label>
                    <select id="myclasses_drop_stf" class="form-control inline-block" name="year">
                        <?php echo class_year_options();
                        ?>
                    </select>
                </div>

                <button type="submit" class="btn btn-sm btn-primary">Show Grades</button>
            </form>
        </div>

        <div class="panel panel-default table-responsive">
            <div class="padding-sm font-16 bg-grey" align="center">
                <?php
                $class_tag = make_class_tag($clax,$subj);

                if(isset($clax) && $clax!=''){
                    $vhcont='';
                    $rew_subject=ucwords(strtolower(unit_name($subj)));
                    echo 'Showing Grades for'.'<strong>'. $rew_classlab.' <small>'.$rew_subject.'</small></strong>';
                }else{ $vhcont='hidden';$rew_subject='';$rew_classlab='';echo "<div class='animate2 bounceIn font-normal'> <i class='fa fa-chevron-right'></i> Select a class and Unit above</div>";}?>
            </div>


            <div class="seperator"></div><div class="seperator"></div>
            <form class="<?php echo $vhcont;?>" name="grader" id="grade_form" method="post"  action="handle_grade.php">
                <table class="table table-striped" id="responsiveTable">
                    <thead>
                    <tr>
                        <th align="left"><span class=""></span>Recruit Name</th>
                        <th align="left"><span class=""></span>Sto No.</th>
                        <th width="12%" align="left"><span class=""></span> Cat  30%</th>
                        <th width="12%" align="left"><span class=""></span>Exams 70%</th>
                        <th width="12%" align="left"><span class=""></span>Final Result</th>
                        <th width="12%" align="left"><span class=""></span>Grade</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $list_query = mysql_query("SELECT DISTINCT recruit_id,exam_mark,cat_mark,final_mark,grade FROM grades WHERE status=1");
                    while($list_result = mysql_fetch_array($list_query)){
                        $sadminn = $list_result['recruit_id'];
                        $thecls_tag=$list_result['class_tag'];
                        $cat_mark=$list_result['cat_mark'];
                        $exam_mark=$list_result['exam_mark'];
                        $final_mark=$list_result['final_mark'];
                        $grade=$list_result['grade'];
                        ?>
                        <tr>
                        <td align="left"><?php echo recruit_name($sadminn)?></td>
                        <td align="left"><?php echo $sadminn?></td>
                        <td align="left"><?php echo $cat_mark?></td>
                        <td align="left"><?php echo $exam_mark?></td>
                        <td align="left"><?php echo $final_mark?></td>
                        <td align="left"><?php echo $grade?></td>


                        <?php
                    }

                    ?></tbody>

                    </tbody>
                </table>

            </form>
        </div>
    </div>
</div>


<script> $(".grade_mu").addClass('active');


    $(document).on("change", ".score", function () {
        var el = $(this);
        var target_id = el.data("id");
        var grade = el.val();

        var remark_el = $("#remark_"+target_id);
        remark_el.prop('required',false);

        if(grade==1){
            remark_el.hide();
            remark_el.val('');
        }

        if(grade==0){

            remark_el.show();
            remark_el.val('');

        }
        if(grade==3){

            remark_el.show();
            remark_el.val('');
            remark_el.prop('required',true);
        }




    });



    $(function onchange_subject() {
        $('#myclasses_drop_stf').change(function() {
            this.form.submit();
        });
    });

    function sp_subjects(){
        var class_id = $("#class_id").val();
        var placer= $('#myclasses_drop_stf');
        var staff_id = '<?php echo $_SESSION['_user_id'];?>';


        $.post('_ajax.php',{class_id:class_id,staff_id_subject_options:staff_id},function(subject_options_cl){
            console.log(subject_options_cl);
            placer.html(subject_options_cl);

        });

    }

    $(function onchange_class() {
        $('#class_id').change(function() {
            sp_subjects();
        });
    });

    sp_subjects();


</script>
<?php include'../_footer.php';?>

